# Dynamic Settings


## Quick Start
1. Drop this extension into `extensions/DynamicSettings/` in your MediaWiki installation.

2. Add this code block to the bottom of the LocalSettings.php file:

```php
wfLoadExtension('DynamicSettings');
require $IP.'/extensions/DynamicSettings/boot/DetectWiki.php';
```