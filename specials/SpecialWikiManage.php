<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Wiki Manage Special Page
 *
 * @author    Alexia E. Smith
 * @copyright (c) 2018 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
**/

use DynamicSettings\Lock;

class SpecialWikiManage extends DynamicSettings\SpecialPage {
	/**
	 * Output HTML
	 *
	 * @var string
	 */
	private $content;

	/**
	 * Wiki object storage for form editing.
	 *
	 * @var array
	 */
	private $wiki;

	/**
	 * Main Constructor
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct('WikiManage', 'wiki_manage', false);
	}

	/**
	 * Main Executor
	 *
	 * @param string Sub page passed in the URL.
	 *
	 * @return void [Outputs to screen]
	 */
	public function execute($subpage) {
		global $dsSiteKey;

		$this->checkPermissions();

		if (Lock::isLocked() == true) {
			$this->output->showErrorPage('dynamic_settings_error', 'error_settings_locked');
			return;
		}

		$this->siteSettings = new \DynamicSettings\Sites();

		$this->output->addModuleStyles(['ext.wikiSettings.styles']);
		$this->output->addModules(['ext.wikiSettings.scripts']);

		if ($dsSiteKey === 'master') {
			$siteKey = $this->wgRequest->getVal('siteKey');
		} else {
			$siteKey = $dsSiteKey;
		}

		if (Lock::isLocked($siteKey) == true) {
			$this->output->showErrorPage('dynamic_settings_error', 'error_wiki_locked');
			return;
		}

		$this->setHeaders();

		$this->wiki = \DynamicSettings\Wiki::loadFromHash($siteKey);

		if ($this->wiki === false) {
			$this->output->showErrorPage('site_settings_error', 'wiki_not_found');
			return;
		}

		if ($this->wiki->isDeleted()) {
			$this->output->showErrorPage('site_settings_error', 'deleted_wikis_no_modifications');
			return;
		}

		$this->manageForm();

		$this->output->addHTML($this->content);
	}

	/**
	 * Wiki Manage Form
	 *
	 * @return void [Outputs to screen]
	 */
	public function manageForm() {
		$this->output->setPageTitle(wfMessage('wiki_manage') . ' - ' . $this->wiki->getNameForDisplay());
		$this->content = TemplateWikiManage::manageForm($this->wiki, $this->getUser());
	}
}
