<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Global Page Editor Skin
 *
 * @author    Alexia E. Smith
 * @copyright (c) 2015 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
**/

class TemplateGlobalPageEditor {
	/**
	 * Global Page Editor
	 *
	 * @param array [Optional] Saved form data.
	 * @param string [Optional] Sample text unchanged as provided.
	 * @param string [Optional] Text diff preview of changes.
	 * @param boolean [Optional] Was the preview or queue operation successful?
	 *
	 * @return string Built HTML
	 */
	public function globalPageEditor($form = [], $sampleText = null, $newPageContent = null, $preview = null, $success = true) {
		global $wgRestrictionLevels;

		$queueURL = Title::newFromText('Special:GlobalPageEditor')->getFullURL() . "/queue";

		$html = "
		<div class='button_bar'>
			<div class='buttons_left'>
				<a href='{$queueURL}' class='mw-ui-button'>" . wfMessage('view_queue')->escaped() . "</a>
			</div>
		</div>";

		if (!empty($preview)) {
			$html .= "
		<div id='preview'>
			<pre>" . htmlentities($preview, ENT_QUOTES) . "</pre>
		</div>";
		}
		if ($form['queue']) {
			if ($success === false) {
				$html .= "
		<div class='errorbox'>" . wfMessage('error_gpe_unknown_queue_error')->escaped() . "</div>";
			} else {
				$html .= "
		<div id='queued' class='successbox'>
			" . wfMessage('gpe_successfully_queued', $form['identifier'])->parse() . "
		</div>";
			}
		}
		$html .= "
		<div class='editOptions'>
			<form id='gpe_find_replace' method='POST'>
				<fieldset>

					<h2>" . wfMessage('pages')->escaped() . "</h2>
					<div class='search_row'>
						<label class='indent' for='titles'>" . wfMessage('page_list')->escaped() . ":</label>
						<textarea class='pagelist' id='pagenames' name='titles' type='text' placeholder='" . wfMessage('enter_page_per_line')->escaped() . "'>" . htmlentities((isset($form['titles']) && is_array($form['titles']) ? implode("\n", $form['titles']) : ''), ENT_QUOTES) . "</textarea>
					</div>
					<div class='search_row'>
						<label class='indent' for='gpe_action'>" . wfMessage('gpe_action')->escaped() . ":</label>
						<select id='gpe_action' name='gpe_action'>
							<option " . ($form['gpe_action'] == "find_and_replace" ? " selected='selected'" : '') . " value='find_and_replace'>" . wfMessage('action_find_and_replace')->escaped() . "</option>
							<option " . ($form['gpe_action'] == "page_move" ? " selected='selected'" : '') . " value='page_move'>" . wfMessage('action_page_move')->escaped() . "</option>
							<option " . ($form['gpe_action'] == "page_rollback" ? " selected='selected'" : '') . " value='page_rollback'>" . wfMessage('action_page_rollback')->escaped() . "</option>
							<option " . ($form['gpe_action'] == "page_protect" ? " selected='selected'" : '') . " value='page_protect'>" . wfMessage('action_page_protect')->escaped() . "</option>
							<option " . ($form['gpe_action'] == "page_delete" ? " selected='selected'" : '') . " value='page_delete'>" . wfMessage('action_page_delete')->escaped() . "</option>
						</select>
					</div>

					<div class='gpe_action_option' id='page_move'>
						<h2>Move Pages</h2>
						<div class='search_row' id='move_pages_to'>
							<label class='indent' for='move_titles'>" . wfMessage('move_to')->escaped() . ":</label>
							<textarea class='pagelist' id='move_pagenames' name='move_titles' type='text' placeholder='" . wfMessage('enter_page_per_line_move')->escaped() . "'>" . htmlentities((isset($form['move_titles']) && is_array($form['move_titles']) ? implode("\n", $form['move_titles']) : ''), ENT_QUOTES) . "</textarea>
						</div>
					</div>

					<div class='gpe_action_option' id='page_rollback'>
						<h2>Rollback Last Edit</h2>
						<p>There are no options for this action. It will rollback every listed page by 1 revision.</p>
					</div>

					<div class='gpe_action_option' id='page_delete'>
						<h2>Delete Pages</h2>
						<p>There are no options for this action. It will delete every listed page.</p>
					</div>

					<div class='gpe_action_option' id='page_protect'>
						<h2>Protect Pages</h2>
						<div class='search_row'>
							<label class='indent' for='page_protect_edit_protection'>Edit Protection:</label>
							<select name='gpe_action_options[edit_protection]' id='page_protect_edit_protection'>
								<option value=\"all\" " . ($form['gpe_action_options']['edit_protection'] == "all" ? " selected='selected'" : '') . ">all</option>";
		foreach ($wgRestrictionLevels as $level) {
			if (!empty($level)) {
					$html .= "<option value=\"" . $level . "\" " . ($form['gpe_action_options']['edit_protection'] == $level ? " selected='selected'" : '') . ">" . $level . "</option>";
			}
		};
							$html .= "</select>
						</div>
						<div class='search_row'>
							<label class='indent' for='page_protect_edit_protection_expire'>Edit Protection Expire:</label>
							<input name='gpe_action_options[edit_protection_expire]' id='page_protect_edit_protection_expire' type='text' placeholder='infinite' value='" . (!empty($form['gpe_action_options']['edit_protection_expire']) ? $form['gpe_action_options']['edit_protection_expire'] : '') . "'>
						</div>
						<div class='search_row'>
							<label class='indent' for='page_protect_move_protection'>Move Protection:</label>
							<select name='gpe_action_options[move_protection]' id='page_protect_move_protection'>
								<option value=\"all\" " . ($form['gpe_action_options']['move_protection'] == "all" ? " selected='selected'" : '') . ">all</option>";
		foreach ($wgRestrictionLevels as $level) {
			if (!empty($level)) {
					$html .= "<option value=\"" . $level . "\" " . ($form['gpe_action_options']['move_protection'] == $level ? " selected='selected'" : '') . ">" . $level . "</option>";
			}
		};
							$html .= "</select>
						</div>
						<div class='search_row'>
							<label class='indent' for='page_protect_move_protection_expire'>Move Protection Expire:</label>
							<input name='gpe_action_options[move_protection_expire]' id='page_protect_move_protection_expire' type='text' placeholder='infinite' value='" . (!empty($form['gpe_action_options']['move_protection_expire']) ? $form['gpe_action_options']['move_protection_expire'] : '') . "'>
						</div>
						<div class='search_row'>
							<label class='indent' for='page_protect_cascade'>Cascade Protection:</label>
							<input id='page_protect_cascade' name='gpe_action_options[cascade]' type='checkbox' title='' value='1'" . (isset($form['gpe_action_options']['cascade']) && $form['gpe_action_options']['cascade'] == true ? " checked='checked'" : '') . ">
						</div>

					</div>

					<div class='gpe_action_option' id='find_and_replace'>
						<h2>Find & Replace</h2>
						<div class='search_row'>
							<label class='indent' for='template'>" . wfMessage('template')->escaped() . ":</label>
							<select name='template' id='template'></select>
						</div>
						<div class='search_row'>
							<label class='indent' for='sample_text'>" . wfMessage('sample_text')->escaped() . ":</label>
							<textarea name='sample_text' id='sample_text' type='text' placeholder='" . wfMessage('enter_text_for_preview')->escaped() . "'>" . htmlentities($sampleText, ENT_QUOTES) . "</textarea>
						</div>
						<div class='search_row'>
							<label class='indent' for='find'>" . wfMessage('find')->escaped() . ":</label>
							<textarea name='find' id='find' type='text'>" . htmlentities($form['find_replace'][0]['find'], ENT_QUOTES) . "</textarea>
						</div>
						<div class='search_row'>
							<label class='indent' for='replace'>" . wfMessage('replace')->escaped() . ":</label>
							<textarea name='replace' id='replace' type='text'>" . htmlentities($form['find_replace'][0]['replace'], ENT_QUOTES) . "</textarea>
						</div>
						<div class='search_row'>
							<label class='indent' for='option'>" . wfMessage('options')->escaped() . ":</label>
							<input id='ignore_whitespace' name='ignore_whitespace' type='checkbox' title='" . wfMessage('ignore_whitespace_help')->escaped() . "' value='1'" . ($form['find_replace'][0]['ignore_whitespace'] === true ? " checked='checked'" : '') . "><label for='ignore_whitespace' title='" . wfMessage('ignore_whitespace_help')->escaped() . "'>" . wfMessage('ignore_whitespace')->escaped() . "</label>

							<select name='type' id='option'>
								<option value='" . \DynamicSettings\FindAndReplace::PLAIN . "'" . ($form['find_replace'][0]['type'] == 0 ? " selected='selected'" : '') . ">" . wfMessage('replace_plain_text')->escaped() . "</option>
								<option value='" . \DynamicSettings\FindAndReplace::GLOB . "'" . ($form['find_replace'][0]['type'] == 1 ? " selected='selected'" : '') . ">" . wfMessage('replace_glob_text')->escaped() . "</option>
								<option value='" . \DynamicSettings\FindAndReplace::REGEX . "'" . ($form['find_replace'][0]['type'] == 2 ? " selected='selected'" : '') . ">" . wfMessage('replace_regex_text')->escaped() . "</option>
							</select>
							<input id='wrap_around' name='wrap_around' type='checkbox' title='" . wfMessage('wrap_around_help')->escaped() . "' value='1'" . ($form['find_replace'][0]['wrap_around'] === false ? '' : " checked='checked'") . "><label for='wrap_around' title='" . wfMessage('wrap_around_help')->escaped() . "'>" . wfMessage('wrap_around')->escaped() . "</label>
							<input id='ignore_case' name='ignore_case' type='checkbox' title='" . wfMessage('ignore_case_help')->escaped() . "' value='1'" . ($form['find_replace'][0]['ignore_case'] === true ? " checked='checked'" : '') . "><label for='ignore_case' title='" . wfMessage('ignore_case_help')->escaped() . "'>" . wfMessage('ignore_case')->escaped() . "</label>
							<input id='ignore_whitespace' name='ignore_whitespace' type='checkbox' title='" . wfMessage('ignore_whitespace_help')->escaped() . "' value='1'" . ($form['find_replace'][0]['ignore_whitespace'] === true ? " checked='checked'" : '') . "><label for='ignore_whitespace' title='" . wfMessage('ignore_whitespace_help')->escaped() . "'>" . wfMessage('ignore_whitespace')->escaped() . "</label>

							<input id='create_new_pages' name='create_new_pages' type='checkbox' title='" . wfMessage('create_new_pages')->escaped() . "' value='1'" . ($form['find_replace'][0]['create_new_pages'] === true ? " checked='checked'" : '') . "><label for='create_new_pages'>" . wfMessage('create_new_pages')->escaped() . ":</label>


						</div>
					</div>

					<div id=\"create_new_pages_container\">
						<h2>Create New Pages</h2>
						<div class='search_row'>
							<label class='indent' for='new_page_content'>" . wfMessage('content')->escaped() . ":</label>
							<textarea name='new_page_content' type='text' placeholder='" . wfMessage('enter_text_for_new_page_content')->escaped() . "'>" . htmlentities($newPageContent, ENT_QUOTES) . "</textarea>
						</div>
					</div>

					<h2>" . wfMessage('select_wikis')->escaped() . "</h2>
					<div class='search_row'>
						<div id='wiki_selection_container'>
							<input class='wiki_selections' name='wikis' data-select-key='globalpageeditor' data-select-type='addremove' type='hidden' value='" . (is_array($form['wikis']) && count($form['wikis']) ? json_encode($form['wikis']) : '[]') . "'/>
							<input type='hidden' class='everywhere' name='everywhere' value='" . (isset($form['everywhere']) && $form['everywhere'] == 1 ? 1 : 0) . "'>
						</div>
					</div>
					<div class='search_row'>
						<input type='hidden' name='identifier' value='" . (isset($form['identifier']) ? $form['identifier'] : null) . "'>
						<input type='submit' name='preview' value='" . wfMessage('preview')->escaped() . "' id='preview'/><button type='submit' name='queue' id='queue_job'" . (empty($preview) ? " disabled='disabled' title='" . wfMessage('use_preview_before_queue')->escaped() . "'" : '') . "/>" . wfMessage('queue_job')->escaped() . "</button>
					</div>
				</fieldset>
			</form>
		</div>";

		return $html;
	}

	/**
	 * Queue Status
	 *
	 * @param boolean [Optional] Has status available.
	 * @param array [Optional] Status information.
	 * @param array [Optional] Pagination
	 *
	 * @return string Built HTML
	 */
	public function queueStatus($identifier, $hasStatus = false, $status = [], $pagination = null, $runAgain = false, $revert = false) {
		global $wgScriptPath, $wgOut;

		$gpeURL = $queueURL = Title::newFromText('Special:GlobalPageEditor')->getFullURL();
		$queueURL = $gpeURL . "/queue";

		$html = "<div class='button_bar'>
					<div class='buttons'>";

		if ($revert) {
			$html .= "<form action=\"{$gpeURL}\" method=\"POST\">
						<input type=\"hidden\" name=\"jsonForm\" value=\"{$revert}\">
						<button type=\"submit\" name=\"preview\" value=\"true\" class=\"mw-ui-button\">Rollback Changes</button>
					</form> ";
		}

		if ($runAgain) {
			$html .= "<form action=\"{$gpeURL}\" method=\"POST\">
						<input type=\"hidden\" name=\"jsonForm\" value=\"{$runAgain}\">
						<button type=\"submit\" name=\"preview\" value=\"true\" class=\"mw-ui-button\">Run Again</button>
					</form>";
		}

		$html .= "	<a href='{$queueURL}' class='mw-ui-button'>" . wfMessage('view_queue')->escaped() . "</a>
					</div>
				</div>";

		if (!$hasStatus) {
			$html .= "
		<div class='errorbox'>" . wfMessage('error_gpe_queue_no_status')->escaped() . "</div>";
		} else {
			$html .= "<div>{$pagination}</div>";
			if (isset($status['result'])) {
				foreach ($status['result'] as $siteKey => $pages) {
					$wiki = \DynamicSettings\Wiki::loadFromHash($siteKey);
					if ($wiki === false) {
						$html .= "<p>" . wfMessage("error_gpe_site_key_not_found", $siteKey)->escaped() . "</p>";
						continue;
					} else {
						$html .= "<h3>" . $wiki->getNameForDisplay() . "</h3>";
					}
					$html .= "\n<ul>";
					foreach ($pages as $title => $result) {
						$resStatus = is_array($result['result']) ? $result['result'][0] : $result['result'];
						$resCode = isset($result['error_code']) ? (is_array($result['error_code']) ? $result['error_code'][0] : $result['error_code']) : false;
						if (!$resCode && isset($result['code'])) {
							$resCode = $result['code'];
						}
						$resDesc = false;

						$actionType = is_array($status['form']['options']['gpe_action']) ? $status['form']['options']['gpe_action'][0] : $status['form']['options']['gpe_action'];
						$actionType = str_replace("_", "-", $actionType);

						$oldrev = $result['oldrevid'];
						$newrev = $result['newrevid'];

						if (!$resultStatus) {
							// Alt Actions are non-conformists. Heh
							if (isset($result['error'])) {
								$resStatus = "Failure";
								$resCode = is_array($result['error']['code']) ? $result['error']['code'][0] : $result['error']['code'];
								$resDesc = trim(is_array($result['error']['info']) ? $result['error']['info'][0] : $result['error']['info']);
								$resDesc = $wgOut->parseInline($resDesc);
							} else {
								$successFieldName = str_replace("page-", "", $actionType);
								if (isset($result[$successFieldName])) {
									$resStatus = 'Success';
								}

								$oldrev = $result[$successFieldName]['old_revid'];
								$newrev = $result[$successFieldName]['revid'];
							}
						}

						$html .= "<li>";
						$messageKey = 'gpe-' . $actionType . '-' . ($resStatus == 'Success' ? 'success' : 'failure');
						$html .= wfMessage($messageKey, $title)->parse();
						$html .= "<small>";
						if ($resStatus == 'Failure') {
							if ($resCode == "abusefilter-disallowed") {
								$html .= " " . implode(" - ", $result['message']['params']);
							} else {
								if ($resDesc) {
									$html .= " " . $resDesc . "";
								} else {
									$html .= "  " . wfMessage('gpe-page-error-' . $resCode)->parse();
								}
							}
						} else {
							if (isset($result['nochange']) && $result['nochange'] === true) {
								$html .= "  " . wfMessage('gpe-page-edit-nochange')->parse();
							} else {
								// Show change diff link.
								if (!empty($newrev) && !empty($oldrev)) {
									$url = "http://" . $wiki->getDomains()->getDomain() . "{$wgScriptPath}/index.php?title=" . urlencode(str_replace(" ", "_", $title)) . "&type=revision&diff={$newrev}&oldid={$oldrev}";
									$html .= "  " . wfMessage('gpe-page-edit-revision', $url)->parse();
								} else {
									$url = "http://" . $wiki->getDomains()->getDomain() . "{$wgScriptPath}/index.php?title=" . urlencode(str_replace(" ", "_", $title)) . "&action=history";
									$html .= "  " . wfMessage('gpe-page-edit-history', $url)->parse();
								}
							}
						}
						$html .= "</small>";
						$html .= "</li>";
					}
					$html .= "</ul>\n";
				}
			}
			$html .= "<div>{$pagination}</div>";
		}

		$html .= "<button id=\"show_hide_debug\">Show / Hide Debug</button><pre id=\"debug_panel\" style=\"display: none;\">" . print_r($status, 1) . "</pre>";

		return $html;
	}

	/**
	 * Queue Status
	 *
	 * @param boolean [Optional] Has status available.
	 * @param array [Optional] Status information.
	 * @param array [Optional] Pagination
	 *
	 * @return string Built HTML
	 */
	public function queue($data, $pagination) {
		global $wgScriptPath;

		$gpeURL = Title::newFromText('Special:GlobalPageEditor')->getFullURL();

		$html = "<div class='button_bar'>
					<div class='buttons'>
						<a href='{$gpeURL}' class='mw-ui-button'>" . wfMessage('new_gpe')->escaped() . "</a>
					</div>
				</div>";

		$html .= $pagination;

		$html .= '<table id="wikilist">
					<thead>
						<tr>
							<th>Job Key</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody>';

		$skip = [];
		foreach ($data as $job) {
			if (!in_array($job['key'], $skip)) {
				$states = [];
				foreach ($job['states'] as $state => $count) {
					$states[] = $state . ": " . $count;
				}

				$linkKey = explode(":", $job['key']);
				$linkKey = $linkKey[(count($linkKey) - 1)];
				$link = "/Special:GlobalPageEditor/status/" . $linkKey;

				$html .= "<tr><td><a href=\"" . $link . "\">" . $job['key'] . "</a></td><td>" . implode(" / ", $states) . "</td></tr>";
				$html .= "</td></tr>";

				$skip[] = $job['key'];
			}
		}

		$html .= '	</tbody>
				</table>';

		$html .= $pagination;
		return $html;
	}
}
