<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Allowed Setting base class for all types of settings
 *
 * @author    Noah Manneschmidt
 * @copyright (c) 2015 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
 **/

namespace DynamicSettings\Settings;

use Exception;
use DynamicSettings\DSDBFactory;
use DynamicSettings\EditLog;
use DynamicSettings\Extensions\Extension;
use DynamicSettings\Wiki;
use DynamicSettings\WikiContextException;
use RequestContext;

class Setting {
	use \DynamicSettings\Traits\WikiContext;

	/**
	 * Allowed setting database data.
	 *
	 * @var array
	 */
	protected $data = [
		'asid'						=> 0,
		'setting_key'				=> null,
		'setting_default'			=> null,
		'setting_type'				=> 'string',
		'setting_group_name'		=> null,
		'setting_example'			=> null,
		'setting_visible'			=> 1,
		'setting_load_order'		=> 0,
		'allowed_extension_md5_key'	=> null,
		'deleted'					=> 0
	];

	/**
	 * @var boolean
	 */
	private static $allLoaded = false;

	/**
	 * Cache of extension info for loading by key.
	 *
	 * @var array
	 */
	private static $allCache = [];

	/**
	 * Cache of extensions which are marked as deleted.
	 *
	 * @var array
	 */
	private static $deletedCache = [];

	/**
	 * When false, a lookup has already been attempted.
	 *
	 * @var Extension this setting belongs to (may be null or false)
	 */
	protected $extension;

	/**
	 * @var mixed value for this setting for the given wiki context (or null for default)
	 */
	protected $wikiSetting;

	/**
	 * @var boolean|null whether the wikiSetting is using the default value
	 */
	protected $usingDefault;

	/**
	 * Delete when saving instead of inserting.
	 *
	 * @var boolean
	 */
	private $deleteOnSave = false;

	/**
	 * Main Constructor
	 *
	 * @param public Database row from the allowed settings table.
	 */
	public function __construct($data = []) {
		$this->data = array_merge($this->data, $data);
	}

	/**
	 * Resets cached wiki-specific values when the context wiki changes
	 *
	 * @param object Old Wiki object context.
	 *
	 * @return void
	 */
	protected function resetContext($oldContext) {
		if (($oldContext instanceof \DynamicSettings\Wiki && $this->wikiContext instanceof \DynamicSettings\Wiki && $oldContext->getSiteKey() !== $this->wikiContext->getSiteKey()) || $oldContext === null || $this->wikiContext === null) {
			$this->wikiSetting = null;
			$this->usingDefault = null;
		}
	}

	/**
	 * Get all the settings belonging to a given extension
	 *
	 * @param object Extension $ext
	 *
	 * @return object SettingCollection
	 */
	public static function loadFromExtension(Extension $ext) {
		self::loadAll();

		// filter the cached list down to those which have a matching md5 key
		$settings = array_filter(self::$allCache, function ($setting) use ($ext) {
			return $setting->getAllowedExtensionMd5Key() === $ext->getExtensionKey();
		});

		$settings = new SettingCollection($settings);
		$settings->setWikiContext($ext->getWikiContext());
		return $settings;
	}

	/**
	 * Get all the settings applicable to a given site.
	 *
	 * @param object Wiki $wiki
	 *
	 * @return object SettingCollection
	 */
	public static function loadFromWiki(Wiki $wiki) {
		self::loadAll();
		$extensionKeys = array_keys($wiki->getExtensions()->getArrayCopy());
		$settings = array_filter(self::$allCache, function ($setting) use ($extensionKeys) {
			return empty($setting->getAllowedExtensionMd5Key()) || in_array($setting->getAllowedExtensionMd5Key(), $extensionKeys);
		});

		$settings = new SettingCollection($settings);
		$settings->setWikiContext($wiki);
		return $settings;
	}

	/**
	 * Get all the settings belonging to a given group
	 *
	 * @param string $group
	 *
	 * @return object SettingCollection
	 */
	public static function loadFromGroup($group) {
		self::loadAll();

		// filter the cached list down to those which have a matching md5 key
		$settings = array_filter(self::$allCache, function ($setting) use ($ext) {
			return $setting->getSettingGroupName() === $group;
		});

		return new SettingCollection($settings);
	}

	/**
	 * Get an allowed setting by ID.
	 *
	 * @param integer Database ID of the setting.
	 *
	 * @return object Setting or null if not found
	 */
	public static function newFromId($asid) {
		if (isset(self::$allCache[$asid])) {
			return new self(self::$allCache[$asid]);
		} else {
			$db = DSDBFactory::getMasterDB(DB_MASTER);
			$result = $db->select(
				'wiki_allowed_settings',
				'*',
				['asid' => $asid],
				__METHOD__
			);
			$row = $result->fetchRow();
			if (!empty($row)) {
				$setting = self::newFromRow($row);
				self::$allCache[$asid] = $setting;
				return $setting;
			}
		}
	}

	/**
	 * Get an allowed setting object by key.
	 *
	 * @param string Setting Key
	 *
	 * @return object Setting or null if not found
	 */
	public static function newFromKey($key) {
		// The key is stored in the cache array as an alias to the database ID.
		if (isset(self::$allCache[$key]) && isset(self::$allCache[self::$allCache[$key]])) {
			return self::$allCache[self::$allCache[$key]];
		} else {
			$db = DSDBFactory::getMasterDB(DB_MASTER);
			$result = $db->select(
				'wiki_allowed_settings',
				'*',
				['setting_key' => $key],
				__METHOD__
			);
			$row = $result->fetchRow();
			if (!empty($row)) {
				$setting = self::newFromRow($row);
				self::$allCache[$setting->getId()] = $setting;
				self::$allCache[$setting->getSettingKey()] = $setting->getId();
				return $setting;
			}
		}
	}

	/**
	 * Get all allowed settings
	 *
	 * @return SettingCollection
	 */
	public static function getAll() {
		self::loadAll();
		return new SettingCollection(self::$allCache);
	}

	/**
	 * Get all deleted settings
	 *
	 * @return SettingCollection
	 */
	public static function getDeleted() {
		self::loadAll();
		return new SettingCollection(self::$deletedCache);
	}

	/**
	 * Get an instance of the correct Setting subclass from a database row.
	 *
	 * @param array Database Row
	 *
	 * @return object Setting subclass based on setting type in the database.
	 * @throws Exception	When no class is avialable for the row's setting_type.
	 */
	public static function newFromRow($row) {
		$classname = '\\DynamicSettings\\Settings\\' . ucfirst($row['setting_type']) . 'Setting';
		if (class_exists($classname)) {
			return new $classname($row);
		} else {
			throw new Exception("No class available for setting type: {$row['setting_type']}");
		}
	}

	/**
	 * Load all allowed settings into cache
	 *
	 * @return void
	 */
	private static function loadAll() {
		if (self::$allLoaded) {
			return;
		}

		$db = DSDBFactory::getMasterDB(DB_MASTER);
		$results = $db->select(
			['wiki_allowed_settings'],
			['*'],
			[],
			__METHOD__,
			['ORDER BY' => 'setting_group_name, setting_key']
		);

		if (!$results) {
			return;
		}

		foreach ($results as $result) {
			if ($result->deleted) {
				self::$deletedCache[$result->asid] = self::newFromRow((array)$result);
			} else {
				self::$allCache[$result->asid] = self::newFromRow((array)$result);
			}
		}

		self::$allLoaded = true;
	}

	/**
	 * Get the extension this setting belongs.
	 * Sets $this->extension to false if it does not belong to one and the look up was already done.
	 *
	 * @return mixed Extension or null
	 */
	public function getExtension() {
		if (is_null($this->extension)) {
			if ($this->data['allowed_extension_md5_key']) {
				$this->extension = Extension::newFromMd5($this->data['allowed_extension_md5_key']);
				$this->extension->setWikiContext($this->wikiContext);
			} else {
				$this->extension = false;
			}
		}
		if ($this->extension === false) {
			return null;
		}
		return $this->extension;
	}

	/**
	 * Get the setting value for the current wiki, falling back to the default if necessary.
	 *
	 * @return mixed setting value for the current wiki context
	 * @throws WikiContextException when no wiki context exists
	 */
	public function getValue() {
		if (!$this->wikiContext) {
			throw new WikiContextException();
		}

		if (is_null($this->wikiSetting)) {
			// look up wiki settings from DB
			$allSettings = $this->wikiContext->getRawSettings();
			if (isset($allSettings[$this->data['asid']]) && isset($allSettings[$this->data['asid']]['setting_value'])) {
				$value = $this->decodeDbString($allSettings[$this->data['asid']]['setting_value']);
				$this->usingDefault = false;
			} else {
				$value = $this->getSettingDefault(); // Decoded inside that function.
				$this->usingDefault = true;
			}
			$this->wikiSetting = serialize($value);
		}

		return $this->decodeDbString($this->wikiSetting);
	}

	/**
	 * Get the setting value encoded for entry into the wiki settings database table.
	 *
	 * @return string
	 * @throws WikiContextException	When no wiki context exists
	 */
	public function getValueForDb() {
		return $this->encodeDbString($this->getValue());
	}

	/**
	 * Returns the current value of the setting encoded for display in an HTML form.
	 *
	 * @return string
	 * @throws WikiContextException when no wiki context exists
	 */
	public function getValueForForm() {
		return $this->encodeFormString($this->getValue());
	}

	/**
	 * Set the setting value for the current wiki.  No validation is performed.
	 *
	 * @param mixed Value
	 *
	 * @return void
	 * @throws WikiContextException	When no wiki context exists
	 */
	public function setValue($value) {
		if (!$this->wikiContext) {
			throw new WikiContextException();
		}
		$value = $this->filterSettingValue($value);
		$this->wikiSetting = serialize($value);
	}

	/**
	 * Set the setting value for the current wiki from a user-entered form value
	 * Value will be validated against the setting type.
	 *
	 * @param string Value
	 *
	 * @return mixed True when successful or Message error message upon failure
	 * @throws WikiContextException	When no wiki context exists
	 */
	public function setValueFromForm($value) {
		$value = $this->decodeFormString($value);
		if ($this->isValid($value)) {
			if ($this->sameAsDefault($value)) {
				$this->markForDeletion();
			} else {
				$this->setValue($value);
			}
			return true;
		} else {
			return wfMessage('error-setting_value-invalid', $this->data['setting_key'], var_export($value, true));
		}
	}

	/**
	 * Returns true if the current value is simply falling back to the default
	 *
	 * @return boolean
	 * @throws WikiContextException when no wiki context exists
	 */
	public function usingDefault() {
		$this->getValue();
		return $this->usingDefault;
	}

	/**
	 * Override in child classes if the data model needs encoding for storage.
	 *
	 * @param mixed
	 *
	 * @return mixed
	 */
	protected function decodeDbString($value) {
		return unserialize($value, ['allowed_classes' => false]);
	}

	/**
	 * Override in child classes if the data model needs encoding for storage.
	 *
	 * @param mixed $value
	 *
	 * @return string
	 */
	protected function encodeDbString($value) {
		return serialize($value);
	}

	/**
	 * Override in child classes if the data needs to be processed before saving from a settings form.
	 *
	 * @param string $value
	 *
	 * @return mixed
	 */
	protected function decodeFormString($value) {
		return $value;
	}

	/**
	 * Override in child classses if the data needs to be processed before printing into a settings form.
	 *
	 * @param string $value
	 *
	 * @return string
	 */
	protected function encodeFormString($value) {
		return $value;
	}

	/**
	 * Get PHP code for file output.
	 *
	 * @return string PHP code for assigning/defining this setting for the current wiki context.
	 */
	public function getExportedCode() {
		return $this->data['setting_key'] . ' = ' . var_export($this->getValue(), true) . ';';
	}

	/**
	 * Validates the given value against the current setting type
	 *
	 * @param string Potential value for this setting.
	 *
	 * @return boolean
	 */
	public function isValid($value) {
		return false;
	}

	/**
	 * Checks if the given value is the same as the default.
	 *
	 * @param mixed Value to compare.
	 *
	 * @return boolean
	 */
	public function sameAsDefault($value) {
		if ($this->getSettingType() === 'string' || $this->getSettingType() === 'text') {
			return str_replace(["\r\n", "\r"], ["\n", "\n"], $this->getSettingDefault()) === str_replace(["\r\n", "\r"], ["\n", "\n"], $value);
		}
		return $this->getSettingDefault() === $value;
	}

	/**
	 * Mark for deletion.
	 *
	 * @return void
	 */
	protected function markForDeletion() {
		$this->deleteOnSave = true;
	}

	/**
	 * Mark for deletion.
	 *
	 * @return boolean Marked for deletion.
	 */
	public function isMarkedForDeletion() {
		return $this->deleteOnSave;
	}

	/**
	 * Get HTML display for a setting's visibility.
	 *
	 * @return string HTML
	 */
	public function visibilityDisplay() {
		return $this->data['setting_visible'] ?
			\HydraCore::awesomeIcon('eye', [], ['title' => 'Value displayed to wiki admins']) :
			\HydraCore::awesomeIcon('eye-slash', [], ['title' => 'Value hidden from wiki admins']);
	}

	/**
	 * Save allowed setting info
	 *
	 * @return string
	 */
	public function loadOrderDisplay() {
		switch ($this->data['setting_load_order']) {
			case -1:
				return \HydraCore::awesomeIcon('backward', [], ['title' => wfMessage('setting_load_order_before')->escaped()]);
			case 0:
				return \HydraCore::awesomeIcon('question', [], ['title' => wfMessage('setting_load_order_unknown')->escaped()]);
			case 1:
				return \HydraCore::awesomeIcon('forward', [], ['title' => wfMessage('setting_load_order_after')->escaped()]);
		}
	}

	/**
	 * Save all information to the database.
	 *
	 * @param string [Optional] Commit Message
	 *
	 * @return boolean Success
	 */
	public function save($commitMessage = '') {
		$wgUser = RequestContext::getMain()->getUser();

		$success = false;
		$db = DSDBFactory::getMasterDB(DB_MASTER);

		$save = [
			'setting_key'				=> $this->getSettingKey(),
			'setting_default'			=> $this->data['setting_default'], // Serialized version.
			'setting_type'				=> $this->getSettingType(),
			'setting_group_name'		=> $this->getSettingGroupName(),
			'setting_example'			=> $this->getSettingExample(),
			'setting_visible'			=> intval($this->isSettingVisible()),
			'setting_load_order'		=> $this->getSettingLoadOrder(),
			'allowed_extension_md5_key'	=> $this->getAllowedExtensionMd5Key(),
			'deleted'					=> intval($this->isDeleted()),
		];

		$db->startAtomic(__METHOD__);
		if ($this->getId()) {
			$result = $db->update(
				'wiki_allowed_settings',
				$save,
				['asid' => $this->getId()],
				__METHOD__
			);

			$databaseId = $this->getId();
		} else {
			$result = $db->insert(
				'wiki_allowed_settings',
				$save,
				__METHOD__
			);

			$databaseId = $db->insertId();
		}

		if (!$result) {
			$db->cancelAtomic(__METHOD__);
		} else {
			$success = true;
			$db->endAtomic(__METHOD__);

			$this->data['asid'] = $databaseId;

			$miscData = [
				'display_name' => $this->getSettingKey()
			];

			EditLog::addEntry(
				$this->getId(), // Log Key: allowed_setting_id
				$save,
				__METHOD__,
				$wgUser,
				$commitMessage,
				$miscData
			);
		}

		return $success;
	}

	/**
	 * Delete this setting.
	 *
	 * @return boolean Success
	 */
	public function delete() {
		$wgUser = RequestContext::getMain()->getUser();

		$success = false;

		$db = DSDBFactory::getMasterDB(DB_MASTER);
		$db->startAtomic(__METHOD__);
		$result = $db->update(
			'wiki_allowed_settings',
			['deleted' => 1],
			['asid' => $this->getId()],
			__METHOD__
		);

		if (!$result) {
			$db->cancelAtomic(__METHOD__);
		} else {
			$this->setDeleted(true);
			$success = true;
			$db->endAtomic(__METHOD__);

			$miscData = [
				'display_name' => $this->getSettingKey()
			];

			EditLog::addEntry(
				$this->getId(), // Log Key: allowed_setting_id
				$this->data,
				__METHOD__,
				$wgUser,
				$this->getSettingKey() . ' deleted',
				$miscData
			);
		}

		return $success;
	}

	/**
	 * Get the database ID.
	 *
	 * @return integer Database ID
	 */
	public function getId() {
		return intval($this->data['asid']);
	}

	/**
	 * Get the setting key.
	 *
	 * @return string Setting Key
	 */
	public function getSettingKey() {
		return $this->data['setting_key'];
	}

	/**
	 * Set the setting key.
	 *
	 * @param string Setting Key
	 *
	 * @return boolean Success
	 */
	public function setSettingKey($settingKey) {
		$settingKey = trim($settingKey);
		// See: http://php.net/language.variables.basics
		if (preg_match('#\$[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*#', $settingKey) > 0) {
			$this->data['setting_key'] = $settingKey;
			return true;
		}
		return false;
	}

	/**
	 * Get the setting default.
	 *
	 * @return string Setting Default
	 */
	public function getSettingDefault() {
		return $this->decodeDbString($this->data['setting_default']);
	}

	/**
	 * Set the setting default.  Enforces type standards.
	 * Arrays passed as JSON is not guaranteed to be reliable.  Only supported for backwards compatibility.
	 *
	 * @param mixed Setting Default
	 *
	 * @return boolean Success
	 */
	public function setSettingDefault($settingDefault) {
		$settingDefault = $this->filterSettingValue($settingDefault);

		$this->data['setting_default'] = serialize($settingDefault);

		return true;
	}

	/**
	 * Filter a setting value to be suitable for the default or per wiki value.
	 *
	 * @param mixed Setting
	 *
	 * @return mixed Filtered Setting
	 */
	public function filterSettingValue($setting) {
		switch ($this->getSettingType()) {
			case 'boolean':
				$setting = filter_var($setting, FILTER_VALIDATE_BOOLEAN);
				break;
			case 'integer':
				$setting = intval($setting);
				break;
			case 'string':
			case 'text':
				$setting = strval($setting);
				break;
			case 'array':
				if (!empty($setting) && !is_array($setting)) {
					$setting = @json_decode($setting, true);
					if (!is_array($setting)) {
						$setting = [];
					}
				}
				break;
		}
		return $setting;
	}

	/**
	 * Get the setting type.
	 *
	 * @return string Setting Type one of: ['boolean', 'integer', 'string', 'text', 'array', 'define']
	 */
	public function getSettingType() {
		return $this->data['setting_type'];
	}

	/**
	 * Set the setting type.
	 * Setting the type before calling setSettingDefault() will enforce type standards.
	 *
	 * @param string Setting Type
	 *
	 * @return boolean Success
	 */
	public function setSettingType($settingType) {
		$settingType = strtolower($settingType);
		if (in_array($settingType, ['boolean', 'integer', 'string', 'text', 'array', 'define'])) {
			$this->data['setting_type'] = $settingType;
			return true;
		}
		return false;
	}

	/**
	 * Get the setting group name.
	 *
	 * @return string Setting Group Name
	 */
	public function getSettingGroupName() {
		return $this->data['setting_group_name'];
	}

	/**
	 * Set the setting group_name.
	 *
	 * @param string Setting Group name
	 *
	 * @return void
	 */
	public function setSettingGroupName($settingGroupName) {
		$this->data['setting_group_name'] = trim($settingGroupName);
	}

	/**
	 * Get the setting example.
	 *
	 * @return string Setting Example
	 */
	public function getSettingExample() {
		return $this->data['setting_example'];
	}

	/**
	 * Set the setting example.
	 *
	 * @param string Setting Example
	 *
	 * @return boolean Success
	 */
	public function setSettingExample($settingExample) {
		$this->data['setting_example'] = trim($settingExample);
		return true;
	}

	/**
	 * Get the setting visible.
	 *
	 * @return string Setting Visible
	 */
	public function isSettingVisible() {
		return boolval($this->data['setting_visible']);
	}

	/**
	 * Set the setting visible.
	 *
	 * @param boolean Setting Visible
	 *
	 * @return boolean Success
	 */
	public function setSettingVisible($settingVisible = true) {
		$this->data['setting_visible'] = intval($settingVisible);
		return true;
	}

	/**
	 * Get the setting load order.
	 *
	 * @return integer Setting Load Order
	 */
	public function getSettingLoadOrder() {
		return intval($this->data['setting_load_order']);
	}

	/**
	 * Set the setting load order.
	 *
	 * @param integer Setting Load Order
	 *
	 * @return boolean Success
	 */
	public function setSettingLoadOrder($settingLoadOrder) {
		$settingLoadOrder = intval($settingLoadOrder);
		if ($settingLoadOrder >= -1 && $settingLoadOrder <= 1) {
			$this->data['setting_load_order'] = $settingLoadOrder;
			return true;
		}
		return false;
	}

	/**
	 * Get the allowed extension MD5 key.
	 *
	 * @return string Allowed Extension_md5_key
	 */
	public function getAllowedExtensionMd5Key() {
		return $this->data['allowed_extension_md5_key'];
	}

	/**
	 * Set the allowed extension MD5 key.
	 *
	 * @param string Allowed Extension MD5 Key
	 *
	 * @return boolean Success
	 */
	public function setAllowedExtensionMd5Key($allowedExtensionMd5Key) {
		if (empty($allowedExtensionMd5Key)) {
			$this->data['allowed_extension_md5_key'] = null;
			return true;
		}

		if (strlen($allowedExtensionMd5Key) != 32 || !Extension::exists($allowedExtensionMd5Key)) {
			return false;
		}

		$allowedExtension = Extension::newFromKey($allowedExtensionMd5Key);
		if ($allowedExtension) {
			$this->setSettingGroupName($allowedExtension->getExtensionName());
		}

		$this->data['allowed_extension_md5_key'] = $allowedExtensionMd5Key;
		return true;
	}

	/**
	 * Is deleted?
	 *
	 * @return boolean Deleted
	 */
	public function isDeleted() {
		return boolval($this->data['deleted']);
	}

	/**
	 * Set deleted.
	 *
	 * @param boolean Deleted
	 *
	 * @return void
	 */
	public function setDeleted($deleted = false) {
		$this->data['deleted'] = intval($deleted);
	}
}
