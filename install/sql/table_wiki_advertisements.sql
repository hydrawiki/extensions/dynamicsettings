CREATE TABLE /*_*/wiki_advertisements (
  `adid` int(12) UNSIGNED NOT NULL,
  `site_key` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `config` text COLLATE utf8_unicode_ci,
  `analytics` text COLLATE utf8_unicode_ci NOT NULL,
  `googleanalyticsid` text COLLATE utf8_unicode_ci NOT NULL,
  `atflb` text COLLATE utf8_unicode_ci NOT NULL,
  `btflb` text COLLATE utf8_unicode_ci NOT NULL,
  `atfmrec` text COLLATE utf8_unicode_ci NOT NULL,
  `btfmrec` text COLLATE utf8_unicode_ci NOT NULL,
  `footermrec` text COLLATE utf8_unicode_ci NOT NULL,
  `btfsrec` text COLLATE utf8_unicode_ci NOT NULL,
  `anchor` text COLLATE utf8_unicode_ci NOT NULL,
  `mobileatflb` text COLLATE utf8_unicode_ci NOT NULL,
  `mobileatfmrec` text COLLATE utf8_unicode_ci NOT NULL,
  `mobilebtfmrec` text COLLATE utf8_unicode_ci NOT NULL,
  `jstop` text COLLATE utf8_unicode_ci NOT NULL,
  `jsbot` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `mobilejstop` text COLLATE utf8_unicode_ci NOT NULL,
  `mobilejsbot` text COLLATE utf8_unicode_ci NOT NULL,
  `mobilebannerjs` text COLLATE utf8_unicode_ci NOT NULL,
  `btfhero` text COLLATE utf8_unicode_ci NOT NULL,
  `middlemrec` text COLLATE utf8_unicode_ci NOT NULL,
  `footerlinks` text COLLATE utf8_unicode_ci NOT NULL,
  `instart` text COLLATE utf8_unicode_ci NOT NULL,
  `wiki_ads_disabled` tinyint(1) NOT NULL DEFAULT '0'
) /*$wgDBTableOptions*/;

ALTER TABLE /*_*/wiki_advertisements ADD PRIMARY KEY (`adid`), ADD UNIQUE KEY `site_key` (`site_key`);

ALTER TABLE /*_*/wiki_advertisements MODIFY `adid` int(12) UNSIGNED NOT NULL AUTO_INCREMENT;