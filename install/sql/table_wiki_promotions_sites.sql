CREATE TABLE /*_*/wiki_promotions_sites (
  `psid` int(12) UNSIGNED NOT NULL,
  `promotion_id` int(12) UNSIGNED DEFAULT NULL,
  `site_key` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `override` tinyint(1) NOT NULL DEFAULT '0'
) /*$wgDBTableOptions*/;

ALTER TABLE /*_*/wiki_promotions_sites
  ADD PRIMARY KEY (`psid`),
  ADD KEY `site_key_promotion_id` (`site_key`,`promotion_id`),
  ADD KEY `override` (`override`);

ALTER TABLE /*_*/wiki_promotions_sites
  MODIFY `psid` int(12) UNSIGNED NOT NULL AUTO_INCREMENT;