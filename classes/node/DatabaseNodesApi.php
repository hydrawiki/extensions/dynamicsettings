<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * DatabaseNodesApi Class
 *
 * @author    Cameron Chunn
 * @copyright (c) 2016 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
 **/

namespace DynamicSettings\Node;

class DatabaseNodesApi extends \ApiBase {
	/**
	 * This runs the api
	 *
	 * @return true
	 */
	public function execute() {
		$params = $this->extractRequestParams();
		$result = $this->getResult();
		$key = isset($params['key']) ? $params['key'] : false;

		if ($key !== self::currentKey()) {
			$this->dieWithError(['apierror-permissiondenied-generic']);
		}

		$params = $this->extractRequestParams();
		$dbNodes = new DatabaseNodes();
		$nodes = $dbNodes->getAvailableNodes();

		$this->getResult()->addValue(null, 'Nodes', $nodes);
		return true;
	}

	/**
	 * Get the current key.
	 *
	 * @return string
	 *
	 * @throws Exception Thrown if $wgDSAPIServerToken is empty.
	 */
	public static function currentKey() {
		global $wgDSAPIServerToken;
		// Such security, much wow.
		if (empty($wgDSAPIServerToken)) {
			throw new \Exception('No API Salt found. Refusing to generate security key. Please configure $wgDSAPIServerToken');
		}
		// Robert told me to do this.
		return base64_encode($wgDSAPIServerToken);
	}

	/**
	 * Allowed params
	 *
	 * @return array
	 */
	public function getAllowedParams() {
		return [
			'key' => [
				\ApiBase::PARAM_TYPE => 'string',
				\ApiBase::PARAM_REQUIRED => true
			],
		];
	}
}
