<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Wiki Lock Functions
 *
 * @author    Alex Smith
 * @copyright (c) 2014 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
 **/

namespace DynamicSettings;

use RedisCache;

class Lock {
	/**
	 * Return the information the lockSettings key in Redis for controlling access to Dynamic Settings.
	 *
	 * @param string|null $siteKey Wiki Site Key to check.
	 *
	 * @return boolean Is Locked
	 */
	public static function isLocked($siteKey = null) {
		$redis = RedisCache::getClient('cache');
		if ($redis === false) {
			return false;
		}
		return (bool)unserialize($redis->get('lockSettings' . ($siteKey !== null ? ':' . $siteKey : '')));
	}

	/**
	 * Toggles the lock status on the lockSettings key in Redis for controlling access to Dynamic Settings.
	 *
	 * @param string|null $siteKey
	 *
	 * @return void
	 */
	public static function toggleLock($siteKey = null) {
		$redis = RedisCache::getClient('cache');
		if ($redis !== false) {
			$redis->set('lockSettings' . ($siteKey !== null ? ':' . $siteKey : ''), serialize(!self::isLocked($siteKey)));
		}
	}
}
