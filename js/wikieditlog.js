function WikiEditLog($) {
	'use strict';
	this.init = function() {
		//$(document).on('click', '.form_diff', wikiEditLog.closeFormDiff);
		//$(document).on('click', '.show_form_diff', wikiEditLog.openFormDiff);
		$('.form_diff').click(wikiEditLog.closeFormDiff);
		$('.show_form_diff').click(wikiEditLog.openFormDiff);
	};


	var wikiEditLog = {
		closeFormDiff: function() {
			$(this).fadeOut('slow');
		},

		openFormDiff: function() {
			var logId = $(this).data('log-id');
			$('.form_diff[data-log-id="'+logId+'"] pre').html("<img src='" + mw.config.get('wgScriptPath') + "/extensions/DynamicSettings/images/loading.gif' class='diff_loading'/>");
			$('.form_diff[data-log-id="'+logId+'"]').fadeIn('slow');
			var api = new mw.Api();
			api.get(
				{
					action: 'dseditlog',
					do: 'getLogDiff',
					logId: logId,
					format: 'json',
					formatversion: 2
				}
			).done(function (result) {
				$('.form_diff[data-log-id="'+logId+'"] pre').html(result.htmlDiff);
			}).fail(function (xhr, status) {
				alert(status.exception.message);
			});
		}
	};
}

var WEL = new WikiEditLog(jQuery);
jQuery(document).ready(WEL.init);