<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Create Global Bot
 *
 * @author    Cameron Chunn
 * @copyright (c) 2017 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
**/

namespace DynamicSettings;

require_once dirname(__DIR__, 3) . '/maintenance/Maintenance.php';

class createGlobalBot extends \Maintenance {
	/**
	 * Main Constructor
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
		$this->mDescription = "createGlobalBot";
	}

	/**
	 * Add the Bot User
	 *
	 * @return void
	 */
	public function execute() {
		// Just making assuptions. Should only have to run this once anyway.
		$name = "HydraGPEBot";

		$user = \User::newFromName($name);
		if ($user && $user->getId()) {
			$this->output("User already exists on this wiki.\n");
			$this->output("Updating global ID as needed.\n");
			$this->updateGlobalLink($user);
			return 1;
		}

		if (!$user) {
			$this->output(\Status::newFatal('config-admin-error-bot-user', $name) . "\n\n");
			return 1;
		}

		if ($user->idForName() == 0) {
			$userStatus = $user->addToDatabase();
			if (!$userStatus->isGood()) {
				$this->output(\Status::newFatal('config-admin-error-bot-user-add', $name) . "\n\n");
				return 1;
			}

			$user->addGroup('bot');
			$user->saveSettings();

			$this->updateGlobalLink($user);

			$ssUpdate = new \SiteStatsUpdate(0, 0, 0, 0, 1);
			$ssUpdate->doUpdate();
		}
		$status = \Status::newGood();

		$this->output($status . "\n\n");
		return 0;
	}

	/**
	 * Update global link on this user.
	 *
	 * @param object User
	 *
	 * @return void
	 */
	private function updateGlobalLink($user) {
		if (class_exists('HydraAuthIdLookup')) {
			$lookup = new \HydraAuthIdLookup();
			$globalId = $lookup->centralIdFromName($user->getName());
			if ($globalId > 0) {
				\HydraAuthUser::forceUpdateGlobalLink($user->getId(), $globalId);
			}
		}
	}
}

$maintClass = "\DynamicSettings\createGlobalBot";
require_once RUN_MAINTENANCE_IF_MAIN;
