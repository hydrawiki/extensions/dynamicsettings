<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * GlobalPageRunner Job Class
 *
 * @author    Alexia E. Smith
 * @copyright (c) 2016 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
 **/

namespace DynamicSettings\Job;

use DynamicSettings\Wiki;
use RedisCache;

class GlobalPageRunner extends \SyncService\Job {
	/**
	 * The Runner assists into breaking large jobs into individual wiki runs to GlobalPageJob.  It takes the same arguments as GlobalPageJob.
	 *
	 * @param array $args Named Arguments:
	 *                    - operations		Array of operations to process containing arrays of wiki hashes, titles, and replacements.
	 *                    - identifer		Unique identifier for this GPE invocation.
	 *
	 * @return integer exit value for this thread
	 */
	public function execute($args = []) {
		$operations = $args['operations'];
		$identifier = $args['identifier'];
		$form 		= $args['form'];

		$redis = RedisCache::getClient('cache');

		foreach ($operations as $operation) {
			if ($operation['everywhere']) {
				$wikis = Wiki::loadAll('wiki_domain', 'ASC', true, false);
			} else {
				$wikis = Wiki::loadFromHash($operation['wikis']['added']);
				if ($wikis instanceof Wiki) {
					$wikis[] = $wikis;
				}
			}
			if (is_array($operation['wikis']['removed']) && !empty($operation['wikis']['removed'])) {
				foreach ($operation['wikis']['removed'] as $siteKey) {
					unset($wikis[$siteKey]);
				}
			}

			// Set these to nothing so that we can manually control the individual wikis to queue.
			$operation['everywhere'] = false;
			$operation['wikis']['removed'] = [];

			foreach ($wikis as $siteKey => $wiki) {
				$operation['wikis']['added'] = [$siteKey];

				$success = GlobalPageJob::queue(
					[
						'operations'	=> [$operation],
						'identifier'	=> $identifier,
						'form'			=> $form
					]
				);
			}
		}
	}
}
