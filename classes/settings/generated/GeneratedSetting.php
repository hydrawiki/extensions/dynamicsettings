<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Base class for settings that are automatically generated from the values of other wiki settings.
 *
 * @author    Noah Manneschmidt
 * @copyright (c) 2015 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
 **/

namespace DynamicSettings\Settings\Generated;

use DynamicSettings\Extensions\Extension;

abstract class GeneratedSetting {
	use \DynamicSettings\Traits\WikiContext;

	/**
	 * @var mapping of allowed extension md5 to generated setting class names
	 */
	private static $generatedSettingsMap = [
		// SMW
		'af3f6146d292520feb8e5bd9da420faf' => ['SemanticNamespacesSetting']
	];

	/**
	 * Get any generated settings for a given extension
	 *
	 * @param object Extension $ext (must have a wiki context attached)
	 *
	 * @return array of GeneratedSetting instances
	 */
	public static function getForExtension(Extension $ext) {
		$generatedSettings = [];
		if (isset(self::$generatedSettingsMap[$ext->getExtensionKey()])) {
			foreach (self::$generatedSettingsMap[$ext->getExtensionKey()] as $generatedSettingClass) {
				$generatedSettingClass = '\\DynamicSettings\\Settings\\Generated\\' . $generatedSettingClass;
				$gs = new $generatedSettingClass();
				$gs->setWikiContext($ext->getWikiContext());
				$generatedSettings[] = $gs;
			}
		}
		return $generatedSettings;
	}

	/**
	 * Generate any necessary settings based on the configuration available in the wiki context
	 *
	 * @return string generated PHP code
	 */
	abstract public function getExportedCode();
}
