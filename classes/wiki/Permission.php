<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Permissions Class
 *
 * @author    Alexia E. Smith
 * @copyright (c) 2014 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
 **/

namespace DynamicSettings\Wiki;

use DynamicSettings\DSDBFactory;
use DynamicSettings\EditLog;
use RequestContext;

class Permission {
	/**
	 * Wiki Object
	 *
	 * @var object
	 */
	private $wiki;

	/**
	 * Data Container
	 *
	 * @var array
	 */
	private $data = [
		'gpid'			=> 0,
		'group_name'	=> '',
		'site_key'		=> '',
		'group_config'	=> [
			'allow'	=> [],
			'deny'	=> []
		]
	];

	/**
	 * Main Constructor
	 *
	 * @param object Wiki
	 *
	 * @return void
	 */
	private function __construct(\DynamicSettings\Wiki $wiki) {
		$this->wiki = $wiki;
	}

	/**
	 * Load a new Permission object from Wiki object.
	 *
	 * @param object Wiki
	 *
	 * @return mixed Permission object or false on failure.
	 */
	public static function loadFromWiki(\DynamicSettings\Wiki $wiki) {
		$permissions = false;
		$permission = new Permission($wiki);

		$db = DSDBFactory::getMasterDB(DB_MASTER);

		$results = $db->select(
			['wiki_group_permissions'],
			['*'],
			['site_key' => $wiki->getSiteKey()],
			__METHOD__
		);

		while ($row = $results->fetchRow()) {
			$permission = new Permission($wiki);
			if ($permission->load($row) === true) {
				$permissions[$row['gpid']] = $permission;
			}
		}

		return $permissions;
	}

	/**
	 * Load a new Permission object from a permission database ID.
	 *
	 * @param integer Permission Database ID
	 *
	 * @return mixed Permission object or false on failure.
	 */
	public static function loadFromId($permissionId) {
		$permission = new Permission($wiki);

		$db = DSDBFactory::getMasterDB(DB_MASTER);

		$results = $db->select(
			['wiki_group_permissions'],
			['*'],
			['wiki_group_permissions.gpid' => $permissionId],
			__METHOD__
		);

		if (!$permission->load($row)) {
			$permission = false;
		}

		return $permission;
	}

	/**
	 * Returns a new Permission object.
	 *
	 * @param object Wiki
	 *
	 * @return object Fresh Permission
	 */
	public static function loadFromNew(\DynamicSettings\Wiki $wiki) {
		$permission = new Permission($wiki);

		return $permission;
	}

	/**
	 * Loads permissions for this wiki.
	 *
	 * @param array Fetched database row array.
	 *
	 * @return boolean Success
	 */
	public function load($row) {
		if (!$row['gpid']) {
			return false;
		}

		$config = @unserialize($row['group_config'], [false]);

		if ($config === false || !is_array($config)) {
			$config = [
				'allow'	=> [],
				'deny'	=> []
			];
		}

		$this->data = [
			'gpid'			=> $row['gpid'],
			'group_name'	=> $row['group_name'],
			'site_key'		=> $row['site_key'],
			'group_config'	=> $config,
		];

		return true;
	}

	/**
	 * Save permission to the database.
	 *
	 * @param string [Optional] Commit Message
	 *
	 * @return boolean Success
	 */
	public function save($commitMessage = '') {
		$wgUser = RequestContext::getMain()->getUser();

		$db = DSDBFactory::getMasterDB(DB_MASTER);
		$db->startAtomic(__METHOD__);

		$save = [
			'group_name'	=> $this->data['group_name'],
			'site_key'		=> $this->wiki->getSiteKey(),
			'group_config'	=> serialize($this->data['group_config']),
		];

		if ($this->data['gpid'] > 0) {
			$result = $db->update(
				'wiki_group_permissions',
				$save,
				['gpid' => $this->data['gpid']],
				__METHOD__
			);

			$databaseId = $this->data['gpid'];
		} else {
			$result = $db->insert(
				'wiki_group_permissions',
				$save,
				__METHOD__
			);

			$databaseId = $db->insertId();
		}

		if (!$result) {
			$db->cancelAtomic(__METHOD__);
		} else {
			$success = true;
			$db->endAtomic(__METHOD__);

			$this->data['gpid'] = $databaseId;

			$miscData = [
				'display_name' => $this->wiki->getNameForDisplay(),
			];

			EditLog::addEntry(
				$this->wiki->getSiteKey(), // Log Key: site_key
				$save,
				__METHOD__,
				$wgUser,
				'`' . $this->getGroupName() . '` ' . $commitMessage,
				$miscData
			);
		}

		return true;
	}

	/**
	 * Delete this permission.
	 *
	 * @return boolean Success
	 */
	public function delete() {
		$wgUser = RequestContext::getMain()->getUser();

		$success = false;

		if (!$this->getDatabaseId()) {
			return $success;
		}

		$db = DSDBFactory::getMasterDB(DB_MASTER);
		$db->startAtomic(__METHOD__);
		$result = $db->delete(
			'wiki_group_permissions',
			['gpid' => $this->data['gpid']],
			__METHOD__
		);

		if (!$result) {
			$db->cancelAtomic(__METHOD__);
		} else {
			$success = true;
			$db->endAtomic(__METHOD__);

			$miscData = [
				'display_name' => $this->wiki->getNameForDisplay()
			];

			EditLog::addEntry(
				$this->wiki->getSiteKey(), // Log Key: site_key
				[],
				__METHOD__,
				$wgUser,
				'`' . $this->getGroupName() . '` deleted',
				$miscData
			);
		}

		return $success;
	}

	/**
	 * Return the database ID.
	 *
	 * @return mixed Database ID or false for not yet saved permission sets.
	 */
	public function getDatabaseId() {
		return (isset($this->data['gpid']) && $this->data['gpid'] > 0 ? $this->data['gpid'] : false);
	}

	/**
	 * Return permission group name.
	 *
	 * @return string Group Name
	 */
	public function getGroupName() {
		return $this->data['group_name'];
	}

	/**
	 * Set permission group name.
	 *
	 * @param string Group Name
	 *
	 * @return boolean Success
	 */
	public function setGroupName($name) {
		$name = str_replace(' ', '_', $name);
		$name = trim($name, " _-");
		if (empty($name)) {
			return false;
		}
		$this->data['group_name'] = $name;
		return true;
	}

	/**
	 * Clear list of allowed permissions.
	 *
	 * @return boolean Success
	 */
	public function clearAllow() {
		unset($this->data['group_config']['allow']);

		return true;
	}

	/**
	 * Return allowed permissions.
	 *
	 * @return mixed Array of allowed permissions or null.
	 */
	public function getAllow() {
		return isset($this->data['group_config']['allow']) && is_array($this->data['group_config']['allow']) ? $this->data['group_config']['allow'] : null;
	}

	/**
	 * Add an allowed permission.
	 *
	 * @param string Permission to allow.
	 *
	 * @return boolean Success
	 */
	public function addAllow($allow) {
		$allow = trim($allow);
		if (empty($allow)) {
			return false;
		}
		$this->data['group_config']['allow'][] = $allow;

		return true;
	}

	/**
	 * Clear list of denied permissions.
	 *
	 * @return boolean Success
	 */
	public function clearDeny() {
		unset($this->data['group_config']['deny']);

		return true;
	}

	/**
	 * Return denied permissions.
	 *
	 * @return mixed Array of denied permissions or null.
	 */
	public function getDeny() {
		return isset($this->data['group_config']['deny']) && is_array($this->data['group_config']['deny']) ? $this->data['group_config']['deny'] : null;
	}

	/**
	 * Add a deny permission.
	 *
	 * @param string Permission to deny.
	 *
	 * @return boolean Success
	 */
	public function addDeny($deny) {
		$deny = trim($deny);
		if (empty($deny)) {
			return false;
		}
		$this->data['group_config']['deny'][] = $deny;

		return true;
	}
}
