<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Wiki Class
 *
 * @author    Alexia E. Smith
 * @copyright (c) 2014 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
 **/

namespace DynamicSettings;

use DynamicSettings\Extensions\Extension;
use DynamicSettings\Extensions\ExtensionCollection;
use DynamicSettings\Settings\Setting;
use DynamicSettings\Settings\SettingCollection;
use MediaWiki\MediaWikiServices;
use MWLBFactory;
use MWException;
use RequestContext;
use StatusValue;

class Wiki {
	use \DynamicSettings\Traits\ExtractFieldDataTrait;
	use \DynamicSettings\Traits\StatusValueErrorsTrait;

	const WHITESPACE_REGEX = '/(\xC2\x85|\xc2\xa0|\xe1\xa0\x8e|\xe2\x80[\x80-\x8D]|\xe2\x80\xa8|\xe2\x80\xa9|\xe2\x80\xaF|\xe2\x81\x9f|\xe2\x81\xa0|\xe3\x80\x80|\xef\xbb\xbf)+/';

	/**
	 * Wiki database data.
	 *
	 * @var array
	 */
	private $data = [
		'wiki_name' => null,
		'wiki_meta_name' => null,
		'wiki_category' => null,
		'wiki_tags' => '',
		'wiki_language' => 'en',
		'wiki_managers' => '',
		'wiki_portal' => '-1',
		'wiki_group' => '-1',
		'wiki_notes' => '',
		'db_type' => 'mysql',
		'db_cluster' => null,
		'db_server' => null,
		'db_server_replica' => null,
		'db_port' => null,
		'db_name' => null,
		'db_user' => null,
		'db_password' => null,
		'search_type' => 'elastic',
		'search_server' => null,
		'search_port' => 9312,
		'created' => 0,
		'edited' => 0,
		'deleted' => 0,
		'group_file_repo' => 0,
		'use_s3' => 0,
		'use_gcs' => 1,
		'aws_region' => '',
		's3_bucket' => '',
		'cloudfront_id' => '',
		'cloudfront_domain' => '',
		'md5_key' => null,
		'last_recache' => 0,
		'installed' => 0
	];

	/**
	 * What is exposed to the API.
	 *
	 * 'property' => ['getter', 'setter', 'type', 'permission']
	 * Make sure to define all four, even with null, otherwise E_NOTICE may get thrown in some places.
	 * Use null to deny access to a getter or setter.
	 * Example: ['getSiteKey', null, 'string', null]
	 * If the end point requires permission to access declare the required user right.
	 * Example: ['isGroupFileRepo', 'setGroupFileRepo', 'boolean', 'totally_made_up_permission']
	 *
	 * @var array
	 */
	protected $apiExposed = [
		'wiki_name'			=> ['getName', 'setName', 'string', null],
		'wiki_display_name'	=> ['getNameForDisplay', null, 'string', null], // Psuedo element to expose this getter.
		'wiki_meta_name'	=> ['getMetaName', 'setMetaName', 'string', null],
		'wiki_category'		=> ['getCategory', 'setCategory', 'string', null],
		'wiki_tags'			=> ['getTags', 'setTags', 'array', null],
		'wiki_language'		=> ['getLanguage', 'setLanguage', 'string', null],
		'wiki_managers'		=> ['getManagers', 'setManagers', 'array', null],
		'wiki_portal'		=> ['getPortalKey', null, 'array', null],
		'wiki_group'		=> ['getGroupKey', 'setGroupKey', 'string', null],
		'wiki_notes'		=> ['getNotes', 'setNotes', 'string', null],
		'group_file_repo'	=> ['isGroupFileRepo', 'setGroupFileRepo', 'boolean', null],
		'md5_key'			=> ['getSiteKey', null, 'string', null],
		'last_recache'		=> ['getLastRecache', null, 'integer', null],
		'installed'			=> ['isInstalled', null, 'boolean', null]
	];

	/**
	 * Read Only Status
	 *
	 * @var boolean
	 */
	private $readOnly = false;

	/**
	 * Is a master wiki.
	 *
	 * @var boolean
	 */
	private $masterWiki = false;

	/**
	 * Domains Object
	 *
	 * @var object
	 */
	protected $domains;

	/**
	 * Key list of enabled extensions.
	 * Initialized to null to indicate it has yet to be loaded since the loaded state may be a blank array.
	 *
	 * @var array
	 */
	protected $enabledExtensionHashes = null;

	/**
	 * All settings for this wiki.
	 * Initialized to null to indicate it has yet to be loaded since the loaded state may be a blank array.
	 *
	 * @var array
	 */
	protected $rawSettings = null;

	/**
	 * Namespaces Object
	 *
	 * @var object
	 */
	protected $namespaces;

	/**
	 * Promotions Object
	 *
	 * @var object
	 */
	protected $promotions;

	/**
	 * Advertisements Object
	 *
	 * @var object
	 */
	protected $advertisements;

	/**
	 * Language information from includes/Names.php.
	 *
	 * @var array
	 */
	private static $coreLanguageNames = [];

	/**
	 * Last serach result total.
	 *
	 * @var integer
	 */
	private static $lastSearchResultTotal = 0;

	/**
	 * Create a new Wiki object from new.
	 *
	 * @return mixed Wiki object or false on failure.
	 */
	public static function loadFromNew() {
		$wiki = new self;

		$wiki->setManagers([]);
		$wiki->setTags([]);
		$wiki->setPortalKey(-1);
		$wiki->setGroupKey(-1);

		return $wiki;
	}

	/**
	 * Create a new Wiki object from a site key hash.
	 * If a single string key "..." is passed in a single Wiki object will be return.
	 * If an array of key(s) ["...", "..."] is passed in an array will be given back even if it is a single result.
	 *
	 * @param mixed $hash A single or an array of 32 character site key hash(es).
	 *
	 * @return mixed Single Wiki object or an array of Wiki objects; false on failure.
	 */
	public static function loadFromHash($hash) {
		$db = DSDBFactory::getMasterDB(DB_MASTER);

		$multiple = false;
		if (is_array($hash)) {
			if (empty($hash)) {
				$hash = [];
			}
			$multiple = true;
		} else {
			if (strlen($hash) != 32) {
				return false;
			}
		}

		$wikis = [];
		if (!empty($hash)) {
			$results = $db->select(
				['wiki_sites'],
				['*'],
				['md5_key' => $hash],
				__METHOD__
			);

			while ($row = $results->fetchRow()) {
				$wiki = new self;
				if ($wiki->load($row) === true) {
					$wikis[$row['md5_key']] = $wiki;
				}
			}
		}

		if (empty($wikis)) {
			return false;
		} elseif ($multiple === false) {
			return array_pop($wikis);
		} else {
			return $wikis;
		}
	}

	/**
	 * Create a new Wiki object from a domain name.
	 *
	 * @param string $domain Wiki domain name for any environment.
	 *
	 * @return mixed Wiki object or false on failure.
	 */
	public static function loadFromDomain($domain) {
		$lb = MediaWikiServices::getInstance()->getDBLoadBalancerFactory()->getMainLB();
		$db = $lb->getConnection(DB_MASTER);

		if (!$domain) {
			return false;
		} else {
			$where['domain'] = $domain;
		}

		$result = $db->select(
			['wiki_domains'],
			['site_key'],
			$where,
			__METHOD__
		);

		$row = $result->fetchRow();

		$wiki = self::loadFromHash($row['site_key']);

		return $wiki;
	}

	/**
	 * Create a new Wiki object from a database name.
	 *
	 * @param string $dbname Wiki database name.
	 *
	 * @return mixed Wiki object or false on failure.
	 */
	public static function loadFromDatabaseName($dbname) {
		$lb = MediaWikiServices::getInstance()->getDBLoadBalancerFactory()->getMainLB();
		$db = $lb->getConnection(DB_MASTER);

		if (empty($dbname)) {
			return false;
		}

		$result = $db->select(
			['wiki_sites'],
			['*'],
			['db_name' => $dbname],
			__METHOD__
		);

		$row = $result->fetchRow();
		$wiki = new self;
		if (!$wiki->load($row)) {
			return false;
		}

		return $wiki;
	}

	/**
	 * Load wiki objects from a group by Group Master site key.
	 *
	 * @param string $siteKey 32 character site key hash.
	 *
	 * @return mixed Wiki object or false on failure.
	 */
	public static function loadFromGroupKey($siteKey) {
		$db = DSDBFactory::getMasterDB(DB_MASTER);
		$where = "wiki_sites.md5_key = " . $siteKey . " OR wiki_sites.wiki_group = " . $siteKey;

		$wikis = [];
		$results = $db->select(
			['wiki_sites', 'wiki_domains'],
			['*'],
			$where,
			__METHOD__,
			[
				'wiki_domains' => [
					'LEFT JOIN', 'wiki_domains.site_key = wiki_sites.md5_key'
				]
			]
		);

		while ($row = $results->fetchRow()) {
			$wiki = new self;
			if ($wiki->load($row) === true) {
				$wikis[$row['md5_key']] = $wiki;
			}
		}

		if (empty($wikis)) {
			$wikis = false;
		}

		return $wikis;
	}

	/**
	 * Load wiki object from a namespace id.
	 *
	 * @param integer $namespaceId
	 *
	 * @return mixed bool|Wiki
	 */
	public static function loadWikiFromNamespaceId($namespaceId) {
		if (!is_numeric($namespaceId)) {
			return false;
		}

		$db = DSDBFactory::getMasterDB(DB_MASTER);
		$result = $db->select(
			['wiki_namespaces'],
			['site_key'],
			['snid' => intval($namespaceId)],
			__METHOD__
		);

		$row = $result->fetchRow();

		$wiki = self::loadFromHash($row['site_key']);

		return $wiki;
	}

	/**
	 * Load wiki object from an extension id.
	 *
	 * @param integer $permissionId
	 *
	 * @return mixed bool|Wiki
	 */
	public static function loadWikiFromPermissionId($permissionId) {
		if (!is_numeric($permissionId)) {
			return false;
		}

		$db = DSDBFactory::getMasterDB(DB_MASTER);
		$result = $db->select(
			['wiki_group_permissions'],
			['site_key'],
			['gpid' => intval($permissionId)],
			__METHOD__
		);

		$row = $result->fetchRow();

		$wiki = self::loadFromHash($row['site_key']);

		return $wiki;
	}

	/**
	 * Load all wikis with options.  A shortcut to loadFromSearch.
	 *
	 * @param string  $sortKey     [Optional] Database field name to sort by, defaults to 'wiki_name'.
	 * @param string  $sortDir     [Optional] Database sort direction, defaults to 'ASC'.
	 * @param boolean $hideDeleted [Optional] Hide deleted wikis, default true.
	 *
	 * @return array Array of Wiki objects, possibly empty.
	 */
	public static function loadAll($sortKey = 'wiki_name', $sortDir = 'ASC', $hideDeleted = true) {
		$wikis = self::loadFromSearch(null, null, null, $sortKey, $sortDir, $hideDeleted);

		return $wikis;
	}

	/**
	 * Load multiple Wiki Objects into an array based on search parameters.
	 *
	 * @param integer     $start         Zero based start position.
	 * @param integer     $itemsPerPage  Total number of results to return.
	 * @param string|null $searchTerm    [Optional] Search term to filter by.
	 * @param string      $sortKey       [Optional] Database field name to sort by, defaults to 'wiki_name'.
	 * @param string      $sortDir       [Optional] Database sort direction, defaults to 'ASC'.
	 * @param boolean     $hideDeleted   [Optional] Hide deleted wikis, default false.
	 * @param array       $fieldOverride [Optional] Override the default set of searchable fields with a custom limited set.
	 *
	 * @return array an array of resulting objects, possibly empty.
	 */
	public static function loadFromSearch($start, $itemsPerPage, $searchTerm = null, $sortKey = 'wiki_name', $sortDir = 'ASC', $hideDeleted = false, array $fieldOverride = []) {
		$db = DSDBFactory::getMasterDB(DB_MASTER);
		$searchableFields = ['wiki_name', 'wiki_meta_name', 'domain', 'wiki_category', 'wiki_tags', 'wiki_managers', 'db_server', 'db_name', 'db_port', 'md5_key'];
		$tables = ['wiki_sites'];
		$where = [];
		$joins = [];

		if (count($fieldOverride)) {
			$searchableFields = array_intersect($searchableFields, $fieldOverride);
		}

		if (!empty($searchTerm)) {
			$isExtensionSearch = false;
			$isSettingSearch = false;

			if (strpos(mb_strtolower($searchTerm, 'UTF-8'), 'extension:') === 0) {
				$itemSearchTerm = substr($searchTerm, 10);
				$isExtensionSearch = true;
				$items = Extension::getAll();
				$itemSearchableFields = ['extension_name', 'extension_folder'];
				$keyFunction = 'getExtensionKey';
				$joinTable = "wiki_extensions";
				$whereIn = "{$joinTable}.allowed_extension_md5_key";
				$joinSiteKey = "{$joinTable}.site_key";
			}

			if (strpos(mb_strtolower($searchTerm, 'UTF-8'), 'setting:') === 0) {
				$itemSearchTerm = substr($searchTerm, 8);
				$isSettingSearch = true;
				$items = Setting::getAll();
				$itemSearchableFields = ['setting_key', 'setting_group_name'];
				$keyFunction = "getId";
				$joinTable = "wiki_settings";
				$whereIn = "{$joinTable}.allowed_setting_id";
				$joinSiteKey = "{$joinTable}.site_key";
			}

			if (strpos(mb_strtolower($searchTerm, 'UTF-8'), 'manager:') === 0) {
				$itemSearchTerm = substr($searchTerm, 8);
				$searchManagerName = \User::getCanonicalName($itemSearchTerm);
				if ($searchManagerName !== false) {
					$searchableFields = ['wiki_managers']; // Force $searchableFields to only be managers since the people using this custom search are only interested in that look up.
					$searchTerm = $searchManagerName;
				}
			}

			if ($isExtensionSearch || $isSettingSearch) {
				$items = $items->search($itemSearchableFields, $itemSearchTerm);

				if (count($items)) {
					foreach ($items as $item) {
						$keys[] = $item->$keyFunction();
					}
				} else {
					// If no items returned in search, return an empty set.
					return [];
				}

				if (count($keys)) {
					$where[$whereIn] = $keys;
				}
				$tables[] = $joinTable;
				$joins[$joinTable] = [
					'LEFT JOIN', "{$joinSiteKey} = wiki_sites.md5_key"
				];
			} else {
				// Normal wiki_sites table search.
				foreach ($searchableFields as $field) {
					$_whereSearch[] = "`{$field}` LIKE '%{$db->strencode($searchTerm)}%'";
				}
				$where[] = "(" . implode(' OR ', $_whereSearch) . ")";
			}
		}
		if ($hideDeleted === true) {
			$where['deleted'] = 0;
		}

		// Include the domains table when searching by text or ordering.(See 'domain' in $searchableFields.)
		$tables[] = 'wiki_domains';
		$joins['wiki_domains'] = [
			'LEFT JOIN', 'wiki_domains.site_key = wiki_sites.md5_key'
		];

		$options['ORDER BY'] = ($db->fieldExists('wiki_sites', $sortKey) || $sortKey == 'domain' ? $sortKey : 'wiki_name') . ' ' . (strtoupper($sortDir) == 'DESC' ? 'DESC' : 'ASC');
		if ($start !== null) {
			$options['OFFSET'] = $start;
		}
		if ($itemsPerPage !== null) {
			$options['LIMIT'] = $itemsPerPage;
		}
		$options['GROUP BY'] = 'wiki_sites.md5_key';

		$wikis = [];
		$results = $db->select(
			$tables,
			['*'],
			$where,
			__METHOD__,
			$options,
			$joins
		);

		if (!$results) {
			self::$lastSearchResultTotal = 0;
			return [];
		}
		while ($row = $results->fetchRow()) {
			$wiki = new self;
			if ($wiki->load($row) === true) {
				$wikis[$row['md5_key']] = $wiki;
			}
		}

		$resultsTotal = $db->select(
			$tables,
			['count(DISTINCT wiki_sites.md5_key) as total'],
			$where,
			__METHOD__,
			null,
			$joins
		);
		$resultsTotal = $resultsTotal->fetchRow();
		self::$lastSearchResultTotal = intval($resultsTotal['total']);

		return $wikis;
	}

	/**
	 * Return the last search result total.
	 *
	 * @return integer Total
	 */
	public static function getLastSearchTotal() {
		return intval(self::$lastSearchResultTotal);
	}

	/**
	 * Check for existence based on a site key hash.
	 *
	 * @param string $hash 32 character site key hash.
	 *
	 * @return boolean Exists
	 */
	public static function exists($hash) {
		$db = DSDBFactory::getMasterDB(DB_MASTER);

		if (strlen($hash) != 32) {
			return false;
		} else {
			$where = 'md5_key = "' . $db->strencode($hash) . '"';
		}

		$result = $db->select(
			['wiki_sites'],
			['*'],
			$where,
			__METHOD__
		);

		if ($db->numRows($result) > 0) {
			return true;
		}
		return false;
	}

	/**
	 * Load wiki data onto the object from a provided database row.
	 *
	 * @param array $row Database row of data.
	 *
	 * @return boolean Success
	 */
	public function load($row) {
		if (strlen($row['md5_key']) != 32) {
			return false;
		}

		$this->data = $row;
		$this->data['db_server']			= $row['db_server'];
		$this->data['db_server_replica']	= $row['db_server_replica'];
		$this->data['db_port']				= $row['db_port'];
		$this->data['db_name']				= $row['db_name'];
		$this->data['db_user']				= $row['db_user'];
		$this->data['db_password']			= $row['db_password'];

		$this->data['wiki_managers'] = array_filter(explode(',', trim($row['wiki_managers'], ',')));
		if (!is_array($this->data['wiki_managers'])) {
			$this->data['wiki_managers'] = [];
		} else {
			natcasesort($this->data['wiki_managers']);
		}

		$this->data['wiki_tags'] = array_filter(explode(',', trim($row['wiki_tags'], ',')));
		if ($row['wiki_tags'] === null || !is_array($this->data['wiki_tags'])) {
			$this->data['wiki_tags'] = [];
		}

		$this->domains = $this->getDomains();

		Sites::registerWiki($this);

		return true;
	}

	/**
	 * Save all information to the database.
	 * Omit a commit message to avoid logging to the edit log.
	 *
	 * @param string $commitMessage [Optional] Commit Message
	 *
	 * @return boolean Success
	 *
	 * @throws MWException If wiki is in read only.
	 */
	public function save($commitMessage = '') {
		$wgUser = RequestContext::getMain()->getUser();

		if ($this->isReadOnly()) {
			throw new MWException(__METHOD__ . ': Attempting to save when read only.');
		}

		$this->setEdited(time());
		$success = false;
		$db = DSDBFactory::getMasterDB(DB_MASTER);

		$save = [
			'wiki_name'			=> $this->getName(),
			'wiki_meta_name'	=> $this->getMetaName(),
			'wiki_category'		=> $this->getCategory(),
			'wiki_tags'			=> (count($this->getTags()) ? ',' . implode(',', $this->getTags()) . ',' : null),
			'wiki_language'		=> $this->getLanguage(),
			'wiki_managers'		=> (count($this->getManagers()) ? ',' . implode(',', $this->getManagers()) . ',' : null),
			'wiki_portal'		=> $this->getPortalKey(),
			'wiki_group'		=> $this->getGroupKey(),
			'wiki_notes'		=> $this->getNotes(),
			'db_cluster'		=> $this->getDatabase()['db_cluster'],
			'db_name'			=> $this->getDatabase()['db_name'],
			'search_type'		=> $this->getSearchSetup()['search_type'],
			'search_server'		=> $this->getSearchSetup()['search_server'],
			'search_port'		=> intval($this->getSearchSetup()['search_port']),
			'created'			=> $this->getCreated(),
			'edited'			=> $this->getEdited(),
			'deleted'			=> intval($this->isDeleted()),
			'group_file_repo'	=> intval($this->isGroupFileRepo()),
			'use_s3'			=> intval($this->isUsingS3()),
			'use_gcs'			=> intval($this->isUsingGcs()),
			'aws_region'		=> $this->getAWSRegion(),
			's3_bucket'			=> $this->getS3Bucket(),
			'cloudfront_id'		=> $this->getCloudfrontId(),
			'cloudfront_domain'	=> $this->getCloudfrontDomain(),
			'last_recache'		=> $this->getLastRecache(),
			'installed'			=> intval($this->isInstalled())
		];

		$db->startAtomic(__METHOD__);
		if ($this->getSiteKey()) {
			$result = $db->update(
				'wiki_sites',
				$save,
				['md5_key' => $this->getSiteKey()],
				__METHOD__
			);

			if (!$this->isPortalMaster()) {
				// Remove this wiki as a master from all wikis that have it defined as a master.
				$result = $db->update(
					'wiki_sites',
					['wiki_portal' => -1],
					['wiki_portal' => $this->getSiteKey()],
					__METHOD__
				);
			}

			if (!$this->isGroupMaster()) {
				// Remove this wiki as a master from all wikis that have it defined as a master.
				$result = $db->update(
					'wiki_sites',
					['wiki_group' => -1],
					['wiki_group' => $this->getSiteKey()]
				);
			}
		} else {
			$this->setCreated(time());
			$save['created'] = $this->getCreated();
			$this->setSiteKey();
			if (!$this->getSiteKey()) {
				throw new MWException('Error saving new wiki, a site key could not be generated.');
			}
			$save['md5_key'] = $this->getSiteKey();
			$result = $db->insert(
				'wiki_sites',
				$save,
				__METHOD__
			);
		}
		if (!$result) {
			$db->cancelAtomic(__METHOD__);
		} else {
			$success = true;
			$db->endAtomic(__METHOD__);
		}

		if ($success && !empty($commitMessage)) {
			$entry = [
				'user'				=> $wgUser,
				'log_key'			=> $this->getSiteKey(),
				'commit_message'	=> $commitMessage,
				'serialized_data'	=> serialize($save)
			];

			$miscData = [
				'display_name'		=> $this->getNameForDisplay(),
			];

			EditLog::addEntry(
				$this->getSiteKey(), // Log Key: site_key
				$save,
				__METHOD__,
				$wgUser,
				$commitMessage,
				$miscData
			);
		}

		return $success;
	}

	/**
	 * Delete this wiki.
	 *
	 * @return boolean Success
	 *
	 * @throws MWException When attempting to delete a wiki that does not have a site key.
	 */
	public function delete() {
		$wgUser = RequestContext::getMain()->getUser();

		$success = false;

		if (!$this->getSiteKey()) {
			throw new MWException("Attempted to delete a wiki that does not have a site key.");
		}

		$db = DSDBFactory::getMasterDB(DB_MASTER);
		$db->startAtomic(__METHOD__);
		$result = $db->update(
			'wiki_sites',
			['deleted' => 1],
			['md5_key' => $this->getSiteKey()],
			__METHOD__
		);

		if (!$result) {
			$db->cancelAtomic(__METHOD__);
		} else {
			$this->setDeleted(true);
			$success = true;
			$db->endAtomic(__METHOD__);

			$miscData = [
				'display_name'		=> $this->getNameForDisplay()
			];

			EditLog::addEntry(
				$this->getSiteKey(), // Log Key: site_key
				$this->data,
				__METHOD__,
				$wgUser,
				$this->getName() . ' deleted',
				$miscData
			);
		}

		return $success;
	}

	/**
	 * Undelete this wiki.
	 * Performs checks to ensure no conflicts occur.
	 *
	 * @return StatusValue Good if no conflicts, Fatal with message describing the issue.
	 *
	 * @throws MWException When attempting to delete a wiki that does not have a site key.
	 */
	public function undelete() {
		$wgUser = RequestContext::getMain()->getUser();

		$success = new StatusValue;

		if (!$this->getSiteKey()) {
			throw new MWException("Attempted to undelete a wiki that does not have a site key.");
		}

		$conflicts = $this->getDomains()->checkForConflicts();

		if (count($conflicts)) {
			return StatusValue::newFatal('error-wiki_undelete_conflicts');
		}

		$db = DSDBFactory::getMasterDB(DB_MASTER);
		$db->startAtomic(__METHOD__);
		$result = $db->update(
			'wiki_sites',
			['deleted' => 0],
			['md5_key' => $this->getSiteKey()],
			__METHOD__
		);

		if (!$result) {
			$db->cancelAtomic(__METHOD__);
		} else {
			$this->setDeleted(false);
			$success = StatusValue::newGood();
			$db->endAtomic(__METHOD__);

			$miscData = [
				'display_name'		=> $this->getNameForDisplay()
			];

			EditLog::addEntry(
				$this->getSiteKey(), // Log Key: site_key
				$this->data,
				__METHOD__,
				$wgUser,
				$this->getName() . ' undeleted',
				$miscData
			);
		}

		return $success;
	}

	/**
	 * Get the Wiki Name
	 *
	 * @return string
	 */
	public function getName() {
		return $this->data['wiki_name'];
	}

	/**
	 * Get the Domain
	 *
	 * @param string $type Domain Type Constant
	 *
	 * @return string
	 */
	public function getDomain($type = false) {
		return $this->getDomains()->getDomain($type);
	}

	/**
	 * Returns the wiki name and language for common display usage
	 *
	 * @return string
	 */
	public function getNameForDisplay() {
		return sprintf('%s (%s)', $this->data['wiki_name'], mb_strtoupper($this->data['wiki_language'], 'UTF-8'));
	}

	/**
	 * Set the Wiki Name
	 *
	 * @param string $name Wiki Name
	 *
	 * @return object StatusValue
	 */
	public function setName($name) {
		// Remove all non-printable whitespace characters
		$name = preg_replace(self::WHITESPACE_REGEX, "", $name);

		$name = trim($name);
		if (empty($name)) {
			return \StatusValue::newFatal('error-wiki_name-invalid');
		}

		$this->data['wiki_name'] = $name;

		return \StatusValue::newGood();
	}

	/**
	 * Get the Wiki Meta Name
	 *
	 * @return string
	 */
	public function getMetaName() {
		return $this->data['wiki_meta_name'];
	}

	/**
	 * Set the Wiki Meta Name
	 *
	 * @param string $name Wiki Meta Name
	 *
	 * @return object StatusValue
	 */
	public function setMetaName($name) {
		// Remove all non-printable whitespace characters
		$name = preg_replace(self::WHITESPACE_REGEX, "", $name);

		$name = str_replace(' ', '_', trim($name));
		if (empty($name)) {
			return \StatusValue::newFatal('error-wiki_meta_name-invalid');
		}

		$this->data['wiki_meta_name'] = $name;

		return \StatusValue::newGood();
	}

	/**
	 * Return all database information.
	 *
	 * @return array Database information
	 */
	public function getDatabase() {
		$database['db_cluster'] = $this->data['db_cluster'];
		$database['db_name'] = $this->data['db_name'];

		if (empty($this->data['db_cluster'])) {
			$database['db_server']			= $this->data['db_server'];
			$database['db_server_replica']	= $this->data['db_server_replica'];
			$database['db_port']			= $this->data['db_port'];
			$database['db_user']			= $this->data['db_user'];
			$database['db_password']		= $this->data['db_password'];
			$database['db_type']			= ($this->data['db_type'] ? $this->data['db_type'] : 'mysql');
		} else {
			$database = array_merge($database, self::getDatabaseServerForSection($database['db_cluster']));
		}

		return $database;
	}

	/**
	 * Return a database load balancer.
	 *
	 * @return array Database information
	 */
	public function getDatabaseLB() {
		global $wgExternalServers;

		$config = \ConfigFactory::getDefaultInstance()->makeConfig('main');
		$dbDriver = $config->get('DSDBMySQLDriver');

		$database = $this->getDatabase();

		$master = [
			'host'		=> $database['db_server'] . ':' . $database['db_port'],
			'dbname'	=> $database['db_name'],
			'user'		=> $database['db_user'],
			'password'	=> $database['db_password'],
			'type'		=> $database['db_type'],
			'driver'	=> $dbDriver,
			'load'		=> 1
		];

		$replica = false;
		if (!empty($database['db_server_replica'])) {
			$replica = $master;
			$replica['host'] = $database['db_server_replica'] . ':' . $database['db_port'];
		}

		if (is_array($replica)) {
			$wgExternalServers[$this->getSiteKey()] = [
				$master,
				$replica
			];
		} else {
			$wgExternalServers[$this->getSiteKey()] = [
				$master
			];
		}

		// This code is copied from ServiceWiring get around shitty MediaWikiServices singleton issues.
		$services = MediaWikiServices::getInstance();
		$mainConfig = $services->getMainConfig();

		$lbConf = MWLBFactory::applyDefaultConfig(
			$mainConfig->get('LBFactoryConf'),
			$mainConfig,
			$services->getConfiguredReadOnlyMode(),
			$services->getLocalServerObjectCache(),
			$services->getMainObjectStash(),
			$services->getMainWANObjectCache()
		);
		$class = MWLBFactory::getLBFactoryClass($lbConf);

		$lb = new $class($lbConf);

		// We are responsible for destroying this connection before creating a new one.  Using newExternalLB()->getConnection() which create a brand new database connection every time instead of reusing.
		return $lb->newExternalLB(self::getSectionForDatabaseServer($database['db_server']));
	}

	/**
	 * Return a database connection.
	 *
	 * @param integer $dbtype either DB_MASTER or DB_SLAVE.
	 *
	 * @return object DatabaseBase
	 */
	public function getDatabaseLBDB($dbtype = DB_MASTER) {
		$lb = $this->getDatabaseLB();
		$database = $this->getDatabase();
		return $lb->getConnection($dbtype, false, $database['db_name']);
	}

	/**
	 * Set all database information.
	 *
	 * @param array $database Database Information
	 *
	 * @return object StatusValue
	 */
	public function setDatabase($database) {
		$status = new StatusValue;

		if (empty($this->data['db_server']) && (!isset($database['db_cluster']) || empty($database['db_cluster']))) {
			$status->fatal('error-db_cluster-invalid');
		} else {
			$this->data['db_cluster'] = $database['db_cluster'];
		}

		if (!isset($database['db_name']) || empty($database['db_name']) || !preg_match("#^[a-zA-Z0-9_]+$#", $database['db_name']) || strlen($database['db_name']) > 64) {
			$status->fatal('error-db_name-invalid');
		} else {
			$this->data['db_name'] = $database['db_name'];
		}

		return $status;
	}

	/**
	 * Return all search information.
	 *
	 * @return array Search information
	 */
	public function getSearchSetup() {
		$search['search_server']	= $this->data['search_server'];
		$search['search_port']		= $this->data['search_port'];
		$search['search_type']		= ($this->data['search_type'] ? $this->data['search_type'] : 'elastic');

		return $search;
	}

	/**
	 * Set all search information.
	 *
	 * @param array $search Search Information
	 *
	 * @return object StatusValue
	 */
	public function setSearchSetup($search) {
		$status = new \StatusValue;

		if (!isset($search['search_type']) || !in_array($search['search_type'], \DynamicSettings\Sites::$searchTypes)) {
			$status->fatal('error-search_type-invalid');
		} else {
			$this->data['search_type'] = $search['search_type'];
		}

		if (!isset($search['search_server']) || empty($search['search_server'])) {
			$status->fatal('error-search_server-invalid');
		} else {
			$this->data['search_server'] = $search['search_server'];
		}
		if (isset($search['search_server']) && strpos($search['search_server'], ':') !== false) {
			$status->fatal('error_search_server_no_port');
		}

		if (!isset($search['search_port']) || empty($search['search_port'])) {
			$status->fatal('error-search_port-invalid');
		} else {
			$this->data['search_port'] = $search['search_port'];
		}

		return $status;
	}

	/**
	 * Get the site key for this wiki.
	 *
	 * @return mixed 32 character hash key or false for an invalid key.
	 */
	public function getSiteKey() {
		return (isset($this->data['md5_key']) && strlen($this->data['md5_key']) == 32 ? $this->data['md5_key'] : false);
	}

	/**
	 * Sets the hash key for this site.  Will throw an exception if the site hash key is attempted to be changed after already being set.
	 *
	 * @return boolean Success
	 *
	 * @throws MWException When trying to changing the hash key of an existing wiki.
	 */
	public function setSiteKey() {
		if (strlen($this->data['md5_key']) == 32) {
			throw new MWException('Tried to change the site hash key of wiki ' . $this->data['md5_key'] . '.  This action is not allowed.');
		}
		if (!$this->getMetaName() || !$this->getDomains()->getDomain(Wiki\Domains::ENV_PRODUCTION) || !$this->getCreated()) {
			return false;
		}

		$this->data['md5_key'] = md5($this->getMetaName() . $this->getDomains()->getDomain(Wiki\Domains::ENV_PRODUCTION) . $this->getCreated());

		return true;
	}

	/**
	 * Returns the installed status.
	 *
	 * @return boolean Installation status.
	 */
	public function isInstalled() {
		return $this->data['installed'] ? true : false;
	}

	/**
	 * Set the installed status.
	 *
	 * @param boolean $installed [Optional] Installation status.
	 *
	 * @return void
	 */
	public function setInstalled($installed = true) {
		$this->data['installed'] = boolval($installed);
	}

	/**
	 * Returns the full array of wiki managers.
	 *
	 * @return array
	 */
	public function getManagers() {
		return $this->data['wiki_managers'];
	}

	/**
	 * Set the entirety of wiki managers.  Overwrites any existing entries.
	 *
	 * @param array $managers Wiki Managers
	 *
	 * @return object StatusValue
	 */
	public function setManagers($managers) {
		if (!is_array($managers)) {
			return \StatusValue::newFatal('error-wiki_managers-invalid');
		}

		$managers = $this->checkManagerStatus($managers);

		$this->data['wiki_managers'] = $managers;
		natcasesort($this->data['wiki_managers']);

		return \StatusValue::newGood();
	}

	/**
	 * Add managers and do not remove existing entries.
	 *
	 * @param array $managers Wiki Manager Names
	 *
	 * @return object StatusValue
	 */
	public function addManagers($managers) {
		if (!is_array($managers)) {
			return \StatusValue::newFatal('error-wiki_managers-invalid');
		}

		$managers = $this->checkManagerStatus($managers);

		$this->data['wiki_managers'] = array_unique(array_merge($this->data['wiki_managers'], $managers));
		natcasesort($this->data['wiki_managers']);

		return \StatusValue::newGood();
	}

	/**
	 * Remove managers specified and do not remove other entries.
	 *
	 * @param array $managers Wiki Manager Names
	 *
	 * @return object StatusValue
	 */
	public function removeManagers($managers) {
		if (!is_array($managers)) {
			return \StatusValue::newFatal('error-wiki_managers-invalid');
		}

		$total = count($this->data['wiki_managers']);
		// We do not use checkManagerStatus() here since that function checks if they are in a proper manager group, but the person may have already demoted.
		foreach ($managers as $key => $manager) {
			$manager = trim($manager);
			if (!empty($manager)) {
				$mgIndex = array_search($manager, $this->data['wiki_managers']);
				if ($mgIndex !== false) {
					unset($this->data['wiki_managers'][$mgIndex]);
				}
			}
		}

		if (count($this->data['wiki_managers']) != $total) {
			return \StatusValue::newFatal('error-wiki_managers-invalid');
		}

		return \StatusValue::newGood();
	}

	/**
	 * Check if the proposed manager names are allowed to be managers.
	 *
	 * @param array $managers Wiki Manager Names
	 *
	 * @return array Manager names that were found and exist in the proper groups.
	 */
	private function checkManagerStatus($managers) {
		$dbManagers = [];
		$db = DSDBFactory::getMasterDB(DB_MASTER);

		$config = \ConfigFactory::getDefaultInstance()->makeConfig('main');
		$wikiManagerGroups = $config->get('DSWikiManagerGroups');

		if (is_array($managers) && count($managers)) {
			foreach ($managers as $key => $manager) {
				$manager = trim($manager);
				if (empty($manager)) {
					unset($managers[$key]);
				} else {
					$managers[$key] = $db->strencode($manager);
				}
			}

			// Yes, we have to check this again.  Doing a blank query is dumb.
			if (count($managers) && is_array($managers)) {
				$result = $db->select(
					['user', 'user_groups'],
					['user.user_name'],
					["user.user_name IN('" . implode("', '", $managers) . "')", "user_groups.ug_group IN('" . implode("', '", $wikiManagerGroups) . "')"],
					__METHOD__,
					['GROUP BY' => 'user.user_id'],
					[
						'user_groups' => [
							'INNER JOIN', 'user_groups.ug_user = user.user_id'
						]
					]
				);

				while ($row = $result->fetchRow()) {
					$dbManagers[] = $row['user_name'];
				}
			}
		}

		$finalManagers = array_intersect($managers, $dbManagers);

		return $finalManagers;
	}

	/**
	 * Returns the category for this wiki.
	 *
	 * @return string Category
	 */
	public function getCategory() {
		return $this->data['wiki_category'];
	}

	/**
	 * Sets the category for this wiki.
	 *
	 * @param string $category Category
	 *
	 * @return object StatusValue
	 */
	public function setCategory($category) {
		$this->data['wiki_category'] = trim($category);

		return \StatusValue::newGood();
	}

	/**
	 * Returns the language for this wiki.
	 *
	 * @return string
	 */
	public function getLanguage() {
		return $this->data['wiki_language'];
	}

	/**
	 * Sets the language for this wiki.
	 *
	 * @param string $language Language
	 *
	 * @return object StatusValue
	 */
	public function setLanguage($language) {
		$language = strtolower(trim($language));
		if (!array_key_exists($language, \Language::fetchLanguageNames())) {
			return \StatusValue::newFatal('error-wiki_language-invalid');
		}

		$this->data['wiki_language'] = $language;

		return \StatusValue::newGood();
	}

	/**
	 * Returns the notes for this wiki.
	 *
	 * @return string
	 */
	public function getNotes() {
		return $this->data['wiki_notes'];
	}

	/**
	 * Sets the notes for this wiki.
	 *
	 * @param string $notes Notes
	 *
	 * @return object StatusValue
	 */
	public function setNotes($notes) {
		$this->data['wiki_notes'] = strip_tags($notes);
		return \StatusValue::newGood();
	}

	/**
	 * Returns the array of wiki tags.
	 *
	 * @return array
	 */
	public function getTags() {
		return $this->data['wiki_tags'];
	}

	/**
	 * Set the entirety of wiki tags.  Overwrites any existing entries.
	 *
	 * @param array $tags Wiki Tags
	 *
	 * @return object StatusValue
	 */
	public function setTags($tags) {
		if (!is_array($tags)) {
			return \StatusValue::newFatal('error-wiki_tags-invalid');
		}

		foreach ($tags as $index => $tag) {
			$tag = mb_strtolower(trim($tag), 'UTF-8');
			if (empty($tag)) {
				unset($tags[$index]);
				continue;
			}
			$tags[$index] = $tag;
		}

		$this->data['wiki_tags'] = $tags;
		natcasesort($this->data['wiki_tags']);

		return \StatusValue::newGood();
	}

	/**
	 * Returns the portal key for this wiki.
	 *
	 * @return string
	 */
	public function getPortalKey() {
		return $this->data['wiki_portal'];
	}

	/**
	 * Sets the portal key for this wiki.
	 *
	 * @param string $portalKey Portal Key
	 *
	 * @return object StatusValue
	 */
	public function setPortalKey($portalKey) {
		$portalMasters = \DynamicSettings\Sites::getPortalMasters();

		if ((array_key_exists($portalKey, $portalMasters) && $portalKey != $this->getSiteKey()) || $portalKey == -1 || (strlen($portalKey) == 1 && $portalKey == 0)) {
			$this->data['wiki_portal'] = $portalKey;
		} else {
			return \StatusValue::newFatal('error-portal-invalid');
		}

		return \StatusValue::newGood();
	}

	/**
	 * Is this wiki a portal master?
	 *
	 * @return boolean
	 */
	public function isPortalMaster() {
		return ($this->data['wiki_portal'] == 0 && strlen($this->data['wiki_portal']) != 32);
	}

	/**
	 * Is this wiki a child of a portal?
	 *
	 * @return boolean
	 */
	public function isPortalChild() {
		return ($this->data['md5_key'] != $this->data['wiki_portal'] && strlen($this->data['wiki_portal']) == 32);
	}

	/**
	 * Return all portal children.
	 *
	 * @return mixed False on error or an array of Wiki objects.
	 */
	public function getPortalChildren() {
		if (!$this->isPortalMaster()) {
			return false;
		}
		$db = DSDBFactory::getMasterDB(DB_MASTER);

		$result = $db->select(
			['wiki_sites'],
			['*'],
			'wiki_portal = "' . $db->strencode($this->getSiteKey()) . '"',
			__METHOD__
		);

		$wikis = [];
		while ($row = $result->fetchRow()) {
			$wiki = new self;
			if ($wiki->load($row) === true) {
				$wikis[$row['md5_key']] = $wiki;
			}
		}

		return $wikis;
	}

	/**
	 * Returns the group key for this wiki.
	 *
	 * @return string
	 */
	public function getGroupKey() {
		return $this->data['wiki_group'];
	}

	/**
	 * Sets the group key for this wiki.
	 *
	 * @param string $groupKey Group Key
	 *
	 * @return object StatusValue
	 */
	public function setGroupKey($groupKey) {
		$groupMasters = \DynamicSettings\Sites::getGroupMasters();

		if ((array_key_exists($groupKey, $groupMasters) && $groupKey != $this->getSiteKey()) || $groupKey == -1 || (strlen($groupKey) == 1 && $groupKey == 0)) {
			$this->data['wiki_group'] = $groupKey;
		} else {
			return \StatusValue::newFatal('error-group-invalid');
		}

		return \StatusValue::newGood();
	}

	/**
	 * Is this wiki a group master?
	 *
	 * @return boolean
	 */
	public function isGroupMaster() {
		return ($this->data['wiki_group'] == 0 && strlen($this->data['wiki_group']) != 32);
	}

	/**
	 * Return the group master.
	 *
	 * @return mixed False on error or the group master Wiki object.
	 */
	public function getGroupMaster() {
		if ($this->isGroupMaster() || !$this->isGroupChild()) {
			return false;
		}

		return self::loadFromHash($this->getGroupKey());
	}

	/**
	 * Is this wiki a child of a group?
	 *
	 * @return boolean
	 */
	public function isGroupChild() {
		return ($this->data['md5_key'] != $this->data['wiki_group'] && strlen($this->data['wiki_group']) == 32);
	}

	/**
	 * Return all group children.
	 *
	 * @return mixed False on error or an array of Wiki objects.
	 */
	public function getGroupChildren() {
		if (!$this->isGroupMaster()) {
			return false;
		}
		$db = DSDBFactory::getMasterDB(DB_MASTER);

		$result = $db->select(
			['wiki_sites'],
			['*'],
			'wiki_group = "' . $db->strencode($this->getSiteKey()) . '"',
			__METHOD__
		);

		$wikis = [];
		while ($row = $result->fetchRow()) {
			$wiki = new self;
			if ($wiki->load($row) === true) {
				$wikis[$row['md5_key']] = $wiki;
			}
		}

		return $wikis;
	}

	/**
	 * Grab all group members based on domain.
	 *
	 * @param string $domain
	 *
	 * @return array Wiki objects
	 */
	public static function getGroupMembersByDomain($domain) {
		$members = [];
		$wiki = self::loadFromDomain($domain);

		if (!$wiki) {
			// This happens when the group parent is deleted.
			return $members;
		}

		// Check to see if it is a group master.
		if ($wiki->isGroupMaster()) {
			$members = $wiki->getGroupChildren();
		} elseif ($wiki->getGroupKey() != -1) {
			$_wiki = self::loadFromHash($wiki->getGroupKey());
			$members = $_wiki->getGroupChildren();
			$members[$_wiki->getSiteKey()] = $_wiki;
		}

		return $members;
	}

	/**
	 * Returns the created date in a preferred format.
	 *
	 * @param string $format [Optional] Format for PHP date() function.
	 *
	 * @return string Formatted date.
	 */
	public function getCreated($format = 'U') {
		return date($format, $this->data['created']);
	}

	/**
	 * Set the creation date.
	 *
	 * @param integer $timestamp Unix style timestamp.
	 *
	 * @return boolean Success
	 */
	public function setCreated($timestamp) {
		if (isset($this->data['created']) && $this->data['created'] > 0) {
			return false;
		}
		$this->data['created'] = intval($timestamp);
	}

	/**
	 * Returns the edited date in a preferred format.
	 *
	 * @param string $format [Optional] Format for PHP date() function.
	 *
	 * @return string Formatted date.
	 */
	public function getEdited($format = 'U') {
		return date($format, $this->data['edited']);
	}

	/**
	 * Set the edited date.
	 *
	 * @param integer $timestamp Unix style timestamp.
	 *
	 * @return void
	 */
	public function setEdited($timestamp) {
		$this->data['edited'] = intval($timestamp);
	}

	/**
	 * Returns the last recache date in a preferred format.
	 *
	 * @param string $format [Optional] Format for PHP date() function.
	 *
	 * @return string Formatted date.
	 */
	public function getLastRecache($format = 'U') {
		return date($format, $this->data['last_recache']);
	}

	/**
	 * Set the last recache timestamp.
	 *
	 * @param integer $timestamp Unix style timestamp.
	 *
	 * @return void
	 */
	public function setLastRecache($timestamp) {
		$this->data['last_recache'] = intval($timestamp);
	}

	/**
	 * Is this wiki deleted?
	 *
	 * @return boolean
	 */
	public function isDeleted() {
		return boolval($this->data['deleted']);
	}

	/**
	 * Set this wiki as deleted.
	 *
	 * @param boolean $deleted Set to deleted.
	 *
	 * @return void
	 */
	public function setDeleted($deleted = true) {
		$this->data['deleted'] = intval($deleted);
	}

	/**
	 * Return if is set to use a group file repository.
	 *
	 * @return boolean Set to use a group file repository.
	 */
	public function isGroupFileRepo() {
		return (bool)$this->data['group_file_repo'];
	}

	/**
	 * Return a Wiki object for the group file repo.
	 *
	 * @return mixed Array of file backend and repository information suitable for $wgForeignFileRepos or false on error.
	 * [
	 * 		'backend'	=> (array) or false,
	 * 		'repo'		-> (array)
	 * ]
	 */
	public function getGroupFileRepo() {
		$fileBackend = false;
		$foreignFileRepo = false;
		$sectionByDB = false;

		if (!$this->isGroupFileRepo() || !$this->isGroupChild()) {
			return ['backend' => $fileBackend, 'repo' => $foreignFileRepo, 'sectionByDB' => $sectionByDB];
		}

		$wiki = self::loadFromHash($this->getGroupKey());

		if ($wiki === false) {
			return ['backend' => $fileBackend, 'repo' => $foreignFileRepo, 'sectionByDB' => $sectionByDB];
		}

		if (strtolower($wiki->getDatabase()['db_type']) == 'mysql') {
			$dbServer = $wiki->getDatabase()['db_server'] . ":" . $wiki->getDatabase()['db_port'];
		} else {
			$dbServer = $wiki->getDatabase()['db_server'];
		}

		$config = \ConfigFactory::getDefaultInstance()->makeConfig('main');

		$cloudfrontId = (!empty($wiki->getCloudfrontId()) ? $wiki->getCloudfrontId() : $config->get('DSDefaultCloudfrontId'));
		$cloudfrontDomain = (!empty($wiki->getCloudfrontDomain()) ? $wiki->getCloudfrontDomain() : $config->get('DSDefaultCloudfrontDomain'));
		$foreignDbName = $wiki->getDatabase()['db_name'];
		if ($wiki->isUsingS3()) {
			$fileBackend = [
				'class'				=> 'S3FileBackend',
				'name'				=> 's3-' . $foreignDbName,
				'wikiId'			=> $foreignDbName,
				'lockManager'		=> 'nullLockManager',
				'awsRegion'			=> (!empty($wiki->getAWSRegion()) ? $wiki->getAWSRegion() : $config->get('DSDefaultAWSRegion')),
				'bucket'			=> (!empty($wiki->getS3Bucket()) ? $wiki->getS3Bucket() : $config->get('DSDefaultS3Bucket')),
				'cloudfrontId'		=> $cloudfrontId,
				'cloudfrontDomain'	=> $cloudfrontDomain
			];
		}

		$foreignFileRepo = [
			'class'				=> 'ForeignDBViaLBRepo',
			'name'				=> $wiki->getMetaName(),
			'url'				=> ($wiki->isUsingS3() ? "https://" . (!empty($wiki->getCloudfrontDomain()) ? $wiki->getCloudfrontDomain() : $config->get('DSDefaultCloudfrontDomain') . "/" . $foreignDbName) : "https://{$wiki->getDomains()->getDomain()}/media/{$wiki->getDomains()->getDomain()}"),
			'hashLevels'		=> 2, // This must be the same for the other family member
			'wiki'				=> $foreignDbName,
			'hasSharedCache'	=> false,
			'descBaseUrl'		=> "https://{$wiki->getDomains()->getDomain()}/File:",
			'fetchDescription'	=> false
		];
		if ($wiki->isUsingGcs()) {
			$foreignFileRepo = array_merge(
				$foreignFileRepo,
				[
					'class' => 'Fandom\Includes\Vignette\VignetteAwareForeignDBViaLBRepo',
					'backend' => 'gcs-backend',
					'url' => "https://static.wikia.nocookie.net/{$foreignDbName}/images",
					'hashLevels' => 2,
					'thumbScriptUrl' => false,
					'transformVia404' => true,
					'deletedHashLevels' => 3,
					'zones' => [
						'public' => [
							'container' => $foreignDbName,
							'directory' => 'images',
						],
						'temp' => [
							'container' => $foreignDbName,
							'directory' => 'images/temp',
						],
						'thumb' => [
							'container' => $foreignDbName,
							'directory' => 'images/thumb',
						],
						'deleted' => [
							'container' => $foreignDbName,
							'directory' => 'images/deleted',
						],
						'archive' => [
							'container' => $foreignDbName,
							'directory' => 'images/archive',
						],
					],
				]
			);
		}
		$sectionByDB = [
			$foreignDbName => self::getSectionForDatabaseServer($wiki->getDatabase()['db_server'])
		];
		if ($wiki->isUsingS3()) {
			$foreignFileRepo['backend'] = 's3-' . $foreignDbName;
		}

		return ['backend' => $fileBackend, 'repo' => $foreignFileRepo, 'sectionByDB' => $sectionByDB];
	}

	/**
	 * Return the section in $wgLBFactoryConf which corresponds to the given database server.
	 *
	 * @param string $server Database server host name
	 *
	 * @return string|null Section name
	 */
	public static function getSectionForDatabaseServer($server) {
		$mainConfig = MediaWikiServices::getInstance()->getMainConfig();
		$wgLBFactoryConf = $mainConfig->get('LBFactoryConf');

		if ($wgLBFactoryConf['class'] !== 'LBFactoryMulti') {
			return null;
		}

		$hostsByName = $wgLBFactoryConf['hostsByName'];
		$namesByHost = [];
		foreach ($hostsByName as $name => $hostWithPort) {
			$host = explode(':', $hostWithPort)[0];
			$namesByHost[$host][] = $name;
		}

		$sectionLoads = $wgLBFactoryConf['sectionLoads'];
		$sectionsByName = [];
		foreach ($sectionLoads as $section => $loads) {
			foreach ($loads as $name => $load) {
				$sectionsByName[$name] = $section;
			}
		}

		if (!isset($namesByHost[$server])) {
			return null;
		}

		$serverNames = array_flip($namesByHost[$server]);
		if (empty(array_intersect_key($serverNames, $sectionsByName))) {
			return null;
		}
		$serverNames = array_intersect_key($serverNames, $sectionsByName);

		return $sectionsByName[array_rand($serverNames)];
	}

	/**
	 * Return the database details in $wgLBFactoryConf which corresponds to the given database cluster section name.
	 *
	 * @param string $section Database cluster section name.
	 *
	 * @return array Database details
	 */
	public static function getDatabaseServerForSection($section) {
		$mainConfig = MediaWikiServices::getInstance()->getMainConfig();
		$wgLBFactoryConf = $mainConfig->get('LBFactoryConf');

		if ($wgLBFactoryConf['class'] !== 'LBFactoryMulti') {
			return [];
		}

		$template = $wgLBFactoryConf['serverTemplate'];

		$templateOverridesBySection = $wgLBFactoryConf['templateOverridesBySection'] ?? [];

		if (isset($templateOverridesBySection[$section])) {
			$template = $templateOverridesBySection[$section] + $template;
		}

		$database = [
			'db_user' => $template['user'],
			'db_password' => $template['password'],
			'db_type' => $template['type']
		];

		if (isset($wgLBFactoryConf['sectionLoads'][$section])) {
			$loads = $wgLBFactoryConf['sectionLoads'][$section];
			foreach ($loads as $name => $load) {
				list($server, $port) = explode(':', $wgLBFactoryConf['hostsByName'][$name]);
				$database['db_port'] = $port;
				if ($load === 0) {
					$database['db_server'] = $server;
				}
				if ($load === 1) {
					$database['db_server_replica'] = $server;
				}
			}
		}

		return $database;
	}

	/**
	 * Set to use the group master file repository.
	 *
	 * @param boolean $groupRepo Set to use group file repository.
	 *
	 * @return object StatusValue
	 */
	public function setGroupFileRepo($groupRepo = true) {
		if (!is_bool($groupRepo) || ($groupRepo === true && !$this->isGroupChild())) {
			return StatusValue::newFatal('error-group_file_repo-invalid');
		}
		$this->data['group_file_repo'] = intval($groupRepo);

		return StatusValue::newGood();
	}

	/**
	 * Return PHP barf for file backend and repository for the local file repository.
	 *
	 * @return mixed Array of file backend and repository information suitable for $wgLocalFileRepo or false on error.
	 * [
	 * 		'backend'	=> (array) or false,
	 * 		'repo'		-> string representation of an array or false, - Array as a string is used here due to the variable interpolation that needs to occur during the LocalSettings.php load process.
	 * ]
	 */
	public function getS3FileRepo() {
		global $wgUploadDirectory, $wgScriptPath, $wgHashedUploadDirectory, $wgThumbnailScriptPath, $wgGenerateThumbnailOnParse, $wgDeletedDirectory, $wgHashedUploadDirectory;

		$fileBackend = false;
		$fileRepo = false;

		if (!$this->isUsingS3()) {
			return ['backend' => $fileBackend, 'repo' => $fileRepo];
		}

		$config = \ConfigFactory::getDefaultInstance()->makeConfig('main');

		$cloudfrontId = (!empty($this->getCloudfrontId()) ? $this->getCloudfrontId() : $config->get('DSDefaultCloudfrontId'));
		$cloudfrontDomain = (!empty($this->getCloudfrontDomain()) ? $this->getCloudfrontDomain() : $config->get('DSDefaultCloudfrontDomain'));
		$fileBackend = [
			'class'				=> 'S3FileBackend',
			'name'				=> 's3-' . $this->getDatabase()['db_name'],
			'wikiId'			=> $this->getDatabase()['db_name'],
			'lockManager'		=> 'nullLockManager',
			'awsRegion'			=> (!empty($this->getAWSRegion()) ? $this->getAWSRegion() : $config->get('DSDefaultAWSRegion')),
			'bucket'			=> (!empty($this->getS3Bucket()) ? $this->getS3Bucket() : $config->get('DSDefaultS3Bucket')),
			'cloudfrontId'		=> $cloudfrontId,
			'cloudfrontDomain'	=> $cloudfrontDomain
		];

		// The backend name has to match the 'name' key in the file backend configuration.
		$fileRepo .= "[
			'class' => 'LocalRepo',
			'name' => 'local',
			'backend' => 's3-{$this->getDatabase()['db_name']}',
			'directory' => \$wgUploadDirectory,
			'scriptDirUrl' => \$wgScriptPath,
			'scriptExtension' => '.php',
			'url' => 'https://{$cloudfrontDomain}/{$this->getDatabase()['db_name']}',
			'hashLevels' => \$wgHashedUploadDirectory ? 2 : 0,
			'thumbScriptUrl' => \$wgThumbnailScriptPath,
			'transformVia404' => !\$wgGenerateThumbnailOnParse,
			'deletedDir' => \$wgDeletedDirectory,
			'deletedHashLevels' => \$wgHashedUploadDirectory ? 3 : 0
		]";

		return ['backend' => $fileBackend, 'repo' => $fileRepo];
	}

	/**
	 * Is this wiki using S3FileBackend?
	 *
	 * @return boolean
	 */
	public function isUsingS3() {
		return boolval($this->data['use_s3']);
	}

	/**
	 * Set this wiki to use the S3FileBackend.
	 *
	 * @param boolean $useS3 Set to use S3.
	 *
	 * @return object StatusValue
	 */
	public function setUsingS3($useS3 = true) {
		$this->data['use_s3'] = intval($useS3);

		return \StatusValue::newGood();
	}

	/**
	 * Returns the AWS Region for this wiki.
	 *
	 * @return string AWS Region
	 */
	public function getAWSRegion() {
		return $this->data['aws_region'];
	}

	/**
	 * Sets the AWS Region for this wiki.
	 *
	 * @param string $region AWS Region
	 *
	 * @return object StatusValue
	 */
	public function setAWSRegion($region) {
		$this->data['aws_region'] = trim($region);

		return \StatusValue::newGood();
	}

	/**
	 * Returns the S3 Bucket for this wiki.
	 *
	 * @return string S3 Bucket
	 */
	public function getS3Bucket() {
		return $this->data['s3_bucket'];
	}

	/**
	 * Sets the S3 Bucket for this wiki.
	 *
	 * @param string $bucket S3 Bucket
	 *
	 * @return object StatusValue
	 */
	public function setS3Bucket($bucket) {
		$this->data['s3_bucket'] = trim($bucket);

		return \StatusValue::newGood();
	}

	/**
	 * Returns the Cloudfront ID for this wiki.
	 *
	 * @return string Cloudfront ID
	 */
	public function getCloudfrontID() {
		return $this->data['cloudfront_id'];
	}

	/**
	 * Sets the Cloudfront ID for this wiki.
	 *
	 * @param string $bucket Cloudfront ID
	 *
	 * @return object StatusValue
	 */
	public function setCloudfrontID($bucket) {
		$this->data['cloudfront_id'] = trim($bucket);

		return \StatusValue::newGood();
	}

	/**
	 * Returns the Cloudfront Domain for this wiki.
	 *
	 * @return string Cloudfront Domain
	 */
	public function getCloudfrontDomain() {
		return $this->data['cloudfront_domain'];
	}

	/**
	 * Sets the Cloudfront Domain for this wiki.
	 *
	 * @param string $bucket Cloudfront Domain
	 *
	 * @return object StatusValue
	 */
	public function setCloudfrontDomain($bucket) {
		$this->data['cloudfront_domain'] = trim($bucket);

		return \StatusValue::newGood();
	}

	/**
	 * Is this wiki using Google Cloud Storage?
	 *
	 * @return boolean
	 */
	public function isUsingGcs() {
		return boolval($this->data['use_gcs']);
	}

	/**
	 * Set this wiki to use the S3FileBackend.
	 *
	 * @param boolean $useS3 Set to use S3.
	 *
	 * @return object StatusValue
	 */
	public function setUsingGcs($useGcs = true) {
		$this->data['use_gcs'] = intval($useGcs);

		return \StatusValue::newGood();
	}

	/**
	 * Return Domains object.
	 *
	 * @return object Wiki\Domains
	 */
	public function getDomains() {
		if (!$this->domains instanceof Wiki\Domains) {
			$this->domains = Wiki\Domains::loadFromWiki($this);
		}
		return $this->domains;
	}

	/**
	 * Return Settings object.
	 *
	 * @return SettingCollection
	 */
	public function getSettings() {
		return Setting::loadFromWiki($this);
	}

	/**
	 * Return non-deleted Extensions enabled on this wiki
	 *
	 * @return object ExtensionCollection
	 */
	public function getExtensions() {
		return Extension::loadFromWiki($this);
	}

	/**
	 * Return Extensions object, including deleted extensions
	 *
	 * @return object ExtensionCollection
	 */
	public function getAllExtensions() {
		return Extension::loadFromWiki($this, true);
	}

	/**
	 * Get md5 hashes of explicitly enabled extensions
	 *
	 * @return array
	 */
	public function getEnabledExtensionHashes() {
		if ($this->enabledExtensionHashes === null) {
			$db = DSDBFactory::getMasterDB(DB_MASTER);
			$res = $db->select(
				['wiki_extensions'],
				['allowed_extension_md5_key'],
				['site_key' => $this->getSiteKey()],
				__METHOD__
			);

			$this->enabledExtensionHashes = [];
			if ($res) {
				foreach ($res as $row) {
					$this->enabledExtensionHashes[$row->allowed_extension_md5_key] = true;
				}
			}
		}
		return (array)$this->enabledExtensionHashes; // Force an array against null if needed.
	}

	/**
	 * Replaces all enabled extensions for this wiki with the given set
	 *
	 * @param object ExtensionCollection $exts          extentions that should be enabled on this wiki
	 * @param string                     $commitMessage what is written to the wiki change log
	 *
	 * @return boolean success
	 */
	public function setEnabledExtensions(ExtensionCollection $exts, $commitMessage) {
		$this->enabledExtensionHashes = [];
		foreach ($exts as $ext) {
			$this->enabledExtensionHashes[$ext->getExtensionKey()] = true;
		}
		$db = DSDBFactory::getMasterDB(DB_MASTER);
		$db->startAtomic(__METHOD__);

		// delete existing extensions
		$res = $db->delete('wiki_extensions', [
			'site_key' => $this->getSiteKey()
		], __METHOD__);

		// insert new rows
		$rows = [];
		foreach ($this->enabledExtensionHashes as $md5 => $enabled) {
			$rows[] = [
				'site_key' => $this->getSiteKey(),
				'allowed_extension_md5_key' => $md5,
			];
		}

		$res = $db->insert('wiki_extensions', $rows, __METHOD__);

		if ($res) {
			$wgUser = RequestContext::getMain()->getUser();

			$db->endAtomic(__METHOD__);

			$miscData = [
				'display_name'		=> $this->getNameForDisplay(),
			];

			EditLog::addEntry(
				$this->getSiteKey(), // Log Key: site_key
				$this->getEnabledExtensionHashes(),
				__METHOD__,
				$wgUser,
				$commitMessage,
				$miscData
			);
		} else {
			$db->cancelAtomic(__METHOD__);
		}

		return $res;
	}

	/**
	 * Get raw settings values from the wiki_settings DB table for this wiki
	 *
	 * @return array keyed by allowed_setting_id
	 */
	public function getRawSettings() {
		if ($this->rawSettings === null) {
			$db = DSDBFactory::getMasterDB(DB_MASTER);
			$res = $db->select(
				['wiki_settings'],
				['ssid', 'allowed_setting_id', 'setting_value'],
				['site_key' => $this->getSiteKey()],
				__METHOD__,
				[
					'ORDER BY'	=> 'allowed_setting_id ASC'
				]
			);

			$this->rawSettings = [];
			if ($res) {
				foreach ($res as $row) {
					$this->rawSettings[$row->allowed_setting_id] = (array)$row;
				}
			}
		}
		return $this->rawSettings;
	}

	/**
	 * Saves values of the given wiki settings to the DB
	 *
	 * @param object SettingCollection $settings
	 * @param string                   $commitMessage
	 *
	 * @return boolean Success
	 *
	 * @throws MWException When attempting to save a wiki in read only mode.
	 */
	public function saveSettings(SettingCollection $settings, $commitMessage) {
		$wgUser = RequestContext::getMain()->getUser();

		if ($this->isReadOnly()) {
			throw new MWException(__METHOD__ . ': Attempting to save when read only.');
		}

		$db = DSDBFactory::getMasterDB(DB_MASTER);
		$db->startAtomic(__METHOD__);

		$asids = [];
		$save = [];
		foreach ($settings as $asid => $setting) {
			if (!$setting->isMarkedForDeletion()) {
				$save[] = [
					'site_key'           => $this->getSiteKey(),
					'allowed_setting_id' => $asid,
					'setting_value'      => $setting->getValueForDb()
				];
			}
			$asids[] = $asid;
		}

		if (count($asids)) {
			$db->delete(
				'wiki_settings',
				[
					'site_key'				=> $this->getSiteKey(),
					'allowed_setting_id'	=> $asids
				],
				__METHOD__
			);
		}

		if (count($save)) {
			$success = $db->insert(
				'wiki_settings',
				$save,
				__METHOD__
			);

			if (!$success) {
				$db->cancelAtomic(__METHOD__);
				return false;
			}
		}

		$db->endAtomic(__METHOD__);

		$miscData = [
			'display_name' => $this->getNameForDisplay(),
		];

		EditLog::addEntry(
			$this->getSiteKey(), // Log Key: site_key
			$save,
			__METHOD__,
			$wgUser,
			$commitMessage,
			$miscData
		);

		return true;
	}

	/**
	 * Return Namespace objects.
	 *
	 * @return object Wiki\DSNamespace
	 */
	public function getNamespaces() {
		if (!$this->namespaces instanceof Wiki\DSNamespace) {
			$this->namespaces = Wiki\DSNamespace::loadFromWiki($this);
		}
		return $this->namespaces;
	}

	/**
	 * Return Promotion objects.
	 *
	 * @return object Wiki\Promotion
	 */
	public function getPromotions() {
		if (!$this->promotions instanceof Wiki\Promotion) {
			$this->promotions = Wiki\Promotion::loadFromWiki($this);
		}
		return $this->promotions;
	}

	/**
	 * Return Permission objects.
	 *
	 * @return object Wiki\Permission
	 */
	public function getPermissions() {
		if (!$this->promotions instanceof Wiki\Permission) {
			$this->promotions = Wiki\Permission::loadFromWiki($this);
		}
		return $this->promotions;
	}

	/**
	 * Return Advertisements object.
	 *
	 * @return object Wiki\Advertisements
	 */
	public function getAdvertisements() {
		if (!$this->advertisements instanceof Wiki\Advertisements) {
			$this->advertisements = Wiki\Advertisements::loadFromWiki($this);
		}
		return $this->advertisements;
	}

	/**
	 * Returns the read only status of this object.
	 *
	 * @return boolean
	 */
	public function isReadOnly() {
		return $this->readOnly;
	}

	/**
	 * Set read only status of this object, defaults to true.
	 *
	 * @param boolean $readOnly [Optional] Read only status, defaults to true.
	 *
	 * @return void
	 */
	public function setReadOnly($readOnly = true) {
		$this->readOnly = boolval($readOnly);
	}

	/**
	 * Delete any loaded data after it is not needed to save memory
	 *
	 * @param array $members Data objects to release.
	 *
	 * @return void
	 *
	 * @throws MWException Passed a non-array value for $members.
	 */
	public function releaseData($members = null) {
		if ($members !== null && !is_array($members)) {
			throw new MWException('Non-array value for $members passed to ' . __METHOD__ . '.');
		}
		if (!is_array($members)) {
			$members = [
				'domains',
				'settings',
				'extensions',
				'namespaces',
				'promotions',
				'advertisements',
			];
		}
		foreach ($members as $member) {
			unset($this->$member);
		}
	}

	/**
	 * Returns if this is a master wiki.
	 *
	 * @return boolean
	 */
	public function isMasterWiki() {
		return $this->masterWiki;
	}

	/**
	 * Set master wiki status of this object, defaults to false.
	 *
	 * @param boolean $masterWiki [Optional] Master status, defaults to false.
	 *
	 * @return void
	 */
	public function setMasterWiki($masterWiki = false) {
		$this->masterWiki = boolval($masterWiki);
	}

	/**
	 * Return a "fake" main Hydra wiki.
	 *
	 * @return object Wiki
	 */
	public static function getFakeMainWiki() {
		global $wgSitename, $wgMetaNamespace, $wgServerName, $wgExternalServers;

		$wiki = self::loadFromNew();
		$wiki->setName($wgSitename);
		$wiki->setMetaName($wgMetaNamespace);
		$wiki->getDomains()->setDomain($wgServerName, false, true);
		$wiki->setLanguage('en');
		$wiki->setCreated(1348090200);
		$wiki->setCreated(time());
		$wiki->data['md5_key'] = 'hydra-master-df63298e4fbf4f7277d'; // $wiki->setSiteKey('hydra'); - Manually setting the historical key due to setDomain() refusing to set a site key without an ENV_PRODUCTION domain available while in development.  Also, consistency.
		$wiki->setDatabase(['db_name' => $wgExternalServers['master'][0]['dbname']]);
		$wiki->setMasterWiki(true);
		$wiki->setReadOnly(true);

		return $wiki;
	}
}
