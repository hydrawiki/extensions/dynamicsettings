function WikiExtensions($) {
	'use strict';
	this.init = function() {
		$(document).on('hover', '#extensions_container .hover_hand', extensions.showHide);
		$(document).on('mousemove', '#extensions_container .hover_hand', extensions.addCSS);
		$(document).on('focus', '#inline_extension_search', extensions.searchList);
		$('#inline_extension_search').on('keyup', extensions.adjustList);

		$(".extension_warning").each(function(){
			$(this).parent().on('mousein', extensions.showHide);
			$(this).parent().on('mouseout', extensions.showHide);
		});

		$("#extension_extra").linedtextarea({
			selectedLine: $('#extension_extra').attr('data-line'),
			selectedClass: 'lineselect'
		});
	};

	var extensions = {
		showHide: function() {
			if (false == $('.hover_container').is(':visible')) {
				$('.hover_container', this).show();
			} else {
				$('.hover_container', this).hide();
			}
		},

		addCSS: function(e) {
			$('.hover_container', this).css({
				top: e.clientY + 10,
				left: e.clientX + 10
			});
		},

		searchList: function() {
			$(this).val('');
		},

		adjustList: function() {
			var search =  $('#inline_extension_search').val().toLowerCase();
			if (search == '') {
				$("label.hideable").removeClass('selected');
				$("label.hideable").show();
				return;
			}
			$("label.hideable").removeClass('selected');
			$("label.hideable").hide();
			$("label.hideable[data-name*='"+search+"']").addClass('selected');
			$("label.hideable[data-name*='"+search+"']").show();
		}
	};
}

var WE = new WikiExtensions(jQuery);
jQuery(document).ready(WE.init);