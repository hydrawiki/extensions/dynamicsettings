<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Wiki Allowed Extensions Special Page
 *
 * @author    Alex Smith
 * @copyright (c) 2014 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
**/

use DynamicSettings\AllowedExtensions;
use DynamicSettings\Extensions\Extension;
use DynamicSettings\Lock;

class SpecialWikiAllowedExtensions extends DynamicSettings\SpecialPage {
	/**
	 * Output HTML
	 *
	 * @var string
	 */
	private $content;

	/**
	 * Valid actions for this page.
	 *
	 * @var array
	 */
	private $validActions = [
		'add',
		'edit',
		'delete',
		'resetSearch',
		'save',
		'search'
	];

	/**
	 * Current action.
	 *
	 * @var string|null
	 */
	private $action = null;

	/**
	 * Main Constructor
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct('WikiAllowedExtensions', 'wiki_allowed_extensions');
	}

	/**
	 * Main Executor
	 *
	 * @param string Sub page passed in the URL.
	 *
	 * @return void [Outputs to screen]
	 */
	public function execute($subpage) {
		$this->checkPermissions();

		if (Lock::isLocked() == true) {
			$this->output->showErrorPage('dynamic_settings_error', 'error_settings_locked');
			return;
		}

		$this->output->addModuleStyles(['ext.wikiExtensions.styles']);
		$this->output->addModules(['ext.wikiExtensions.scripts']);

		$this->setHeaders();

		$this->templates = new TemplateWikiAllowedExtensions();

		if (in_array($this->getRequest()->getVal('action'), $this->validActions)) {
			$this->action = $this->getRequest()->getVal('action');
		}

		switch ($this->wgRequest->getVal('section')) {
			default:
			case 'list':
				$this->allowedExtensionsList();
				break;
			case 'extension':
				$this->allowedExtensionsForm();
				break;
		}

		$this->output->addHTML($this->content);
	}

	/**
	 * Wiki Allowed Extensions Form
	 *
	 * @return void [Outputs to screen]
	 */
	public function allowedExtensionsList() {
		$start = $this->wgRequest->getInt('st');
		$itemsPerPage = 100;
		$cookieExpire = time() + 900;

		$allowedExtensions = Extension::getAll();

		$hideDeleted = false;
		if ($this->wgRequest->getVal('hide_deleted') == 'true' or ($this->wgRequest->getCookie('hideDeletedExtensions') == 'true' and $this->wgRequest->getVal('hide_deleted') != 'false')) {
			$hideDeleted = true;
			$this->wgRequest->response()->setcookie('hideDeletedExtensions', 'true');
		} elseif ($this->wgRequest->getVal('hide_deleted') == 'false') {
			$this->wgRequest->response()->setcookie('hideDeletedExtensions', 'false');
		}
		if (!$hideDeleted) {
			$allowedExtensions = $allowedExtensions->includeDeleted();
		}

		$hideDefault = false;
		if ($this->wgRequest->getVal('hide_default') == 'true' or ($this->wgRequest->getCookie('hideDefaultExtensions') == 'true' and $this->wgRequest->getVal('hide_default') != 'false')) {
			$hideDefault = true;
			$this->wgRequest->response()->setcookie('hideDefaultExtensions', 'true');
		} elseif ($this->wgRequest->getVal('hide_default') == 'false') {
			$this->wgRequest->response()->setcookie('hideDefaultExtensions', 'false');
		}
		if ($hideDefault) {
			$allowedExtensions = $allowedExtensions->withoutDefault();
		}

		$searchTerm = '';

		$total = 0;
		$reverseSort = false;
		$sortDir = 'asc';
		if ($allowedExtensions->count()) {
			if ($this->action == 'resetSearch') {
				$this->wgRequest->response()->setcookie('wikiAllowedExtensionsSearchTerm', '', 1);
			} else {
				$listSearch = $this->wgRequest->getVal('list_search');
				$cookieSearch = $this->wgRequest->getCookie('wikiAllowedExtensionsSearchTerm');
				if (($this->action == 'search' && !empty($listSearch)) || !empty($cookieSearch)) {
					if (!empty($cookieSearch) && empty($listSearch)) {
						$searchTerm = $cookieSearch;
					} else {
						$searchTerm = $listSearch;
					}
					$allowedExtensions = $allowedExtensions->search(['extension_name', 'extension_folder'], $searchTerm);
					$this->wgRequest->response()->setcookie('wikiAllowedExtensionsSearchTerm', $searchTerm, $cookieExpire);
				}
			}

			$sortDir = $this->wgRequest->getVal('sort_dir');
			if ((strtolower($this->wgRequest->getCookie('wikiAllowedExtensionsSortDir')) === 'desc' && !$sortDir) || strtolower($sortDir) === 'desc') {
				$reverseSort = true;
				$sortDir = 'desc';
			}
			$this->wgRequest->response()->setcookie('wikiAllowedExtensionsSortDir', $sortDir, $cookieExpire);

			$total = $allowedExtensions->count();

			$sortKey = 'getExtensionName';
			$allowedExtensions = $allowedExtensions->sort($sortKey, $reverseSort)->slice($start, $itemsPerPage);
		}

		$pagination = HydraCore::generatePaginationHtml($this->getFullTitle(), $total, $itemsPerPage, $start);

		$this->output->setPageTitle(wfMessage('wikiallowedextensions'));
		$this->content = $this->templates->allowedExtensionsList($allowedExtensions, $pagination, $hideDeleted, $hideDefault, $sortKey, $sortDir, $searchTerm);
	}

	/**
	 * Wiki Allowed Extensions Form
	 *
	 * @return void [Outputs to screen]
	 */
	public function allowedExtensionsForm() {
		$md5Key = $this->wgRequest->getVal('md5_key');
		$extension = Extension::newFromKey($md5Key);
		if (is_null($extension)) {
			$extension = new Extension;
		}

		if ($this->action == 'edit' && $md5Key != $extension->getExtensionKey()) {
			$this->output->showErrorPage('allowed_extensions_error', 'allowed_extensions_no_extension');
			return;
		}

		if ($extension->isDeleted()) {
			$this->output->showErrorPage('allowed_extensions_error', 'allowed_extensions_deleted');
			return;
		}

		if ($this->action == 'delete' && $extension->getExtensionKey()) {
			if ($extension->isDefaultExtension()) {
				$this->output->showErrorPage('allowed_extensions_error', 'allowed_delete_default_extenstion');
				return;
			}
			if ($this->wgRequest->wasPosted() && $this->wgRequest->getVal('do') === 'confirm') {
				if ($extension->delete()) {
					$this->output->redirect($this->getPageTitle()->getFullURL());
					return;
				}
			}
			$this->output->setPageTitle(wfMessage('delete_extension') . ' - ' . $extension->getExtensionName());
			$this->templates = new TemplateWikiAllowedExtensions();
			$this->content = $this->templates->allowedExtensionsDelete($extension);
		} else {
			$errors = [];
			$errors = $this->allowedExtensionsSave($extension);

			$this->output->setPageTitle(($extension->getExtensionKey() ? wfMessage('edit_extension')->escaped() . ' - ' . $extension->getExtensionName() : wfMessage('add_extension')->escaped()));
			$this->content = $this->templates->allowedExtensionsForm(
				$extension,
				AllowedExtensions::getInstance()->getNonDefault(),
				$errors,
				$this->action
			);
		}
	}

	/**
	 * Saves submitted Wiki Extensions Forms.
	 *
	 * @param object Existing extension information.
	 *
	 * @return array Array of errors.
	 */
	private function allowedExtensionsSave($extension) {
		global $IP;

		$errors = [];

		if ($this->wgRequest->wasPosted() && $this->wgRequest->getVal('do') === 'save') {
			if (!$extension->setExtensionName($this->wgRequest->getText('extension_name'))) {
				$errors['extension_name'] = wfMessage('error-extension_name-invalid')->escaped();
			}

			if (!$extension->setExtensionDesc($this->wgRequest->getText('extension_desc'))) {
				$errors['extension_desc'] = wfMessage('error-extension_desc-invalid')->escaped();
			}

			if (!$extension->setExtensionFilename($this->wgRequest->getText('extension_filename'))) {
				$errors['extension_filename'] = wfMessage('error-extension_filename-invalid')->escaped();
			}

			if (!$extension->setExtensionFolder($this->wgRequest->getText('extension_folder'))) {
				$errors['extension_folder'] = wfMessage('error-extension_folder-invalid')->escaped();
			}

			$extension->setDefaultExtension($this->wgRequest->getBool('default_extension', false));
			$extension->setCriticalExtension($this->wgRequest->getBool('critical_extension', false));
			$extension->setUpdateRequired($this->wgRequest->getBool('update_required', false));

			$loadType = $this->wgRequest->getInt('load_type');
			$extension->setLoadType(($loadType < 0 || $loadType > 3 ? 0 : $loadType));

			$extension->setWeight($this->wgRequest->getInt('weight'));

			$extension->setDependencies($this->wgRequest->getArray('dependencies'));

			$extraErrors = $extension->setExtensionExtra($this->wgRequest->getText('extension_extra'));
			if ($extraErrors !== true) {
				$errors = array_merge($errors, $extraErrors);
			}

			$extension->setExtensionInformation($this->wgRequest->getText('extension_information'));

			switch ($extension->getLoadType()) {
				case Extension::LOAD_EXTENSION:
					$file = "{$IP}/extensions/{$extension->getExtensionFolder()}/extension.json";
					break;
				case Extension::LOAD_EXTENSION_LEGACY:
					$file = "{$IP}/extensions/{$extension->getExtensionFolder()}/{$extension->getExtensionFilename()}.php";
					break;
				case Extension::LOAD_SKIN:
					$file = "{$IP}/skins/{$extension->getExtensionFolder()}/skin.json";
					break;
				default:
					$file = null;
					break;
			}

			if (!is_file($file)) {
				$errors['file'] = wfMessage('error-extension_file_path-invalid')->escaped();
			}

			$commitMessage = trim($this->wgRequest->getText('commit_message'));
			if (!$commitMessage) {
				$errors['commit_message'] = wfMessage('error_no_commit_message')->escaped();
			}

			if ($existingExt = Extension::newFromKey(md5($extension->getExtensionFolder() . $extension->getExtensionFilename()))) {
				if ($extension->getId() != $existingExt->getId()) {
					$errors['extension_name'] = wfMessage('error_extension_save_error', $existingExt->getExtensionName())->escaped();
				}
			}

			if (!count($errors)) {
				if ($extension->save($commitMessage)) {
					$this->output->redirect($this->getPageTitle()->getFullURL());
					return;
				} else {
					throw new MWException('There was a fatal error saving the data for this allowed extension.');
				}
			}
		}
		return $errors;
	}
}
