<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Collection class to hold an array of Extension instances
 *
 * @author    Noah Manneschmidt
 * @copyright (c) 2015 Curse Inc.
 * @license   Proprietary
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
**/

namespace DynamicSettings\Extensions;

/**
 * Override for ArrayAccess to lazily create Extension objects from database row arrays when accessed
 */
trait ExtensionArrayAccessContext {
	public function offsetGet($offset) {
		$ret = parent::offsetGet($offset);
		if ($ret instanceof Extension) {
			$ret->setWikiContext($this->wikiContext);
			// save class-wrapped result back to internal data
			$this[$offset] = $ret;
		}
		return $ret;
	}
}
