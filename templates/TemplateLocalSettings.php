<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * LocalSettings PHP Template
 *
 * @author    Alex Smith
 * @copyright (c) 2014 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
**/

use DynamicSettings\Wiki;

class TemplateLocalSettings {
	/**
	 * Output PHP
	 *
	 * @var string
	 */
	private $PHP;

	/**
	 * Wiki Local Settings Template
	 *
	 * @param array Array of wiki information
	 * @param integer Server Environment
	 * @param array Settings outside of the main wiki settings.
	 * @param array Namespaces
	 *
	 * @return string Built PHP
	 */
	public function localSettings($wiki, $environment) {
		$config = \ConfigFactory::getDefaultInstance()->makeConfig('main');

		$host = $wiki->getDomains()->getDomain($environment);

		$this->PHP = "<?php
\$wgSitename          = " . var_export($wiki->getName(), true) . ";
\$wgMetaNamespace     = " . var_export($wiki->getMetaName(), true) . ";
\$wgServer            = " . var_export("https://" . $host, true) . ";
\$wgLanguageCode      = " . var_export($wiki->getLanguage(), true) . ";
\$dsSiteKey           = " . var_export($wiki->getSiteKey(), true) . ";
\$wgWikiCategory      = " . var_export($wiki->getCategory(), true) . ";
\$wgWikiTags          = " . var_export($wiki->getTags(), true) . ";
\$wgWikiManagers      = " . var_export($wiki->getManagers(), true) . ";
\$wgCacheDirectory    = \"{\$wgHydraCacheBasePath}/{$host}\";
\$wgUploadDirectory   = \"{\$wgHydraMediaBasePath}/{$host}\";";

		if ($wiki->isPortalMaster() || $wiki->isPortalChild()) {
			if ($wiki->isPortalMaster()) {
				$portalWiki = $wiki;
			} else {
				$portalWiki = \DynamicSettings\Wiki::loadFromHash($wiki->getPortalKey());
			}
			if ($portalWiki !== false) {
				$this->PHP .= "
\$wgCurseAuthCookieDomain = " . var_export('.' . $portalWiki->getDomains()->getDomain($environment), true) . ";
\$wgCrossSiteAJAXdomains[] = " . var_export('*.' . $portalWiki->getDomains()->getDomain($environment), true) . ";
";
			}
		}

		$groupMasterWiki = $wiki->getGroupMaster();
		if ($wiki->isGroupChild() && $groupMasterWiki !== false) {
			$this->PHP .= "
\$wgGroupMasterDomain		= " . var_export($groupMasterWiki->getDomains()->getDomain(), true) . ";
\$wgGroupMasterSiteKey		= " . var_export($groupMasterWiki->getSiteKey(), true) . ";
";
		}

		$this->PHP .= "

## Database settings";
		if (isset($wiki->getDatabase()['db_server'])) {
			if (strtolower($wiki->getDatabase()['db_type']) == 'mysql') {
				// The single server variables below are generally ignored since we use $wgDBservers now.  Older code may reference them for some reason so they are setup regardless.
				$this->PHP .= "
\$wgDBserver         = " . var_export($wiki->getDatabase()['db_server'] . ":" . $wiki->getDatabase()['db_port'], true) . ";";
			} else {
				$this->PHP .= "
\$wgDBserver         = " . var_export($wiki->getDatabase()['db_server'], true) . ";
\$wgDBport	         = " . var_export($wiki->getDatabase()['db_port'], true) . ";";
			}
		}
		$this->PHP .= "
\$wgDBname           = " . var_export($wiki->getDatabase()['db_name'], true) . ";
\$wgDBuser           = " . var_export($wiki->getDatabase()['db_user'], true) . ";
\$wgDBpassword       = " . var_export($wiki->getDatabase()['db_password'], true) . ";";

		if (!empty($wiki->getDatabase()['db_server_replica'])) {
			// The first is always the master server.  The rest are replicas.
			$this->PHP .= "
\$wgDBservers = [
	[
		'host' => \$wgDBserver,
		'dbname' => \$wgDBname,
		'user' => \$wgDBuser,
		'password' => \$wgDBpassword,
		'type' => " . var_export($wiki->getDatabase()['db_type'], true) . ",
		'driver' => " . var_export(($wiki->getDatabase()['db_type'] === 'mysql' ? $config->get('DSDBMySQLDriver') : null), true) . ",
		'flags' => DBO_DEFAULT,
		'load' => 0
	],
	[
		'host' => " . var_export($wiki->getDatabase()['db_server_replica'] . ":" . $wiki->getDatabase()['db_port'], true) . ",
		'dbname' => \$wgDBname,
		'user' => \$wgDBuser,
		'password' => \$wgDBpassword,
		'type' => " . var_export($wiki->getDatabase()['db_type'], true) . ",
		'driver' => " . var_export(($wiki->getDatabase()['db_type'] === 'mysql' ? $config->get('DSDBMySQLDriver') : null), true) . ",
		'flags' => DBO_DEFAULT,
		'load' => 1
	]
];";
		} else {
			$this->PHP .= "
\$wgDBservers = [
	[
		'host' => \$wgDBserver,
		'dbname' => \$wgDBname,
		'user' => \$wgDBuser,
		'password' => \$wgDBpassword,
		'type' => " . var_export($wiki->getDatabase()['db_type'], true) . ",
		'driver' => " . var_export(($wiki->getDatabase()['db_type'] === 'mysql' ? $config->get('DSDBMySQLDriver') : null), true) . ",
		'flags' => DBO_DEFAULT,
		'load' => 0
	]
];";
		}

		// Add the necessary LBFactoryMulti-compatible config
		$this->PHP .= "
\$wgLBFactoryConf['sectionsByDB'][" .
			var_export($wiki->getDatabase()['db_name'], true) . "] = " .
			var_export(!empty($wiki->getDatabase()['db_cluster']) ? $wiki->getDatabase()['db_cluster'] : Wiki::getSectionForDatabaseServer($wiki->getDatabase()['db_server']), true) . ";
";

		$this->PHP .= "
\$wgLocalInterwiki = strtolower(\$wgSitename);
";

		$fileRepo = $wiki->getGroupFileRepo();
		if ($fileRepo['backend'] !== false) {
			$this->PHP .= "
//Group file backend access for this group child.
\$wgFileBackends[] = " . var_export($fileRepo['backend'], true) . ";
";
		}
		if ($fileRepo['repo'] !== false) {
			$this->PHP .= "
//Group file repository access for this group child.
if (isset(\$wgForeignFileRepos) && is_array(\$wgForeignFileRepos)) {
	array_unshift(\$wgForeignFileRepos, " . var_export($fileRepo['repo'], true) . ");
} else {
	\$wgForeignFileRepo[] = " . var_export($fileRepo['repo'], true) . ";
}
";
		}
		if ($fileRepo['sectionByDB'] !== false) {
			$this->PHP .= "
//Configuration for LBFactoryMulti
";
			foreach ($fileRepo['sectionByDB'] as $dbName => $section) {
				$this->PHP .= "
\$wgLBFactoryConf['sectionsByDB'][" . var_export($dbName, true) . "] = " . var_export($section, true) . ";
";
			}
		}

		if ($wiki->isGroupFileRepo() && $wiki->isGroupMaster()) {
			$groupChildren = $wiki->getGroupChildren();
			foreach ($groupChildren as $groupChild) {
				$corsDomains[] = $groupChild->getDomains()->getDomain();
			}
			if (count($corsDomains)) {
				$this->PHP .= "
//Add domains to CORS for group childs accessing the file repository.
\$wgCrossSiteAJAXdomains = array_merge(\$wgCrossSiteAJAXdomains, " . var_export($corsDomains, true) . ");
";
			}
		}

		if ($wiki->isUsingS3()) {
			$fileRepo = $wiki->getS3FileRepo();
			if ($fileRepo['backend'] !== false) {
				$this->PHP .= "
\$wgFileBackends['{$fileRepo['backend']['name']}'] = " . var_export($fileRepo['backend'], true) . ";
\$wgMathFileBackend = " . var_export($fileRepo['backend']['name'], true) . ";
\$wgTimelineFileBackend = " . var_export($fileRepo['backend']['name'], true) . ";
";
			}
			if ($fileRepo['repo'] !== false) {
				$this->PHP .= "
\$wgLocalFileRepo = " . $fileRepo['repo'] . ";
";
			}
			$this->PHP .= "
\$wgUploadPath = \$wgLocalFileRepo['url'];
";
		}
		if ($wiki->isUsingGcs()) {
			$this->PHP .= "
\$wgUploadBucket = " . var_export($wiki->getDatabase()['db_name'], true) . ";
\$wgUploadPath = 'https://" . $host . "/media/" . $host . "';
";
		} else {
			$this->PHP .= "
\$wgUploadBucket = null;
";
		}
		$this->PHP .= "
\$wgLogo = \"{\$wgUploadPath}/b/bc/Wiki.png\";
\$wgFavicon = \"{\$wgUploadPath}/6/64/Favicon.ico\";
\$wgMobileFrontendLogo = \"{\$wgUploadPath}/0/00/Mobile_logo.png\";
";

		$namespaces = $wiki->getNamespaces();
		if (is_array($namespaces) && count($namespaces)) {
			foreach ($namespaces as $snid => $namespace) {
				$nameDefine = \DynamicSettings\Wiki\DSNamespace::createConstantName($namespace->getName());
				$talkDefine = \DynamicSettings\Wiki\DSNamespace::createConstantName($namespace->getTalk());
				$this->PHP .= "
\n\n//Namespace: {$namespace->getName()}
define('{$nameDefine}', {$namespace->getId()});
define('{$talkDefine}', {$namespace->getTalkId()});

\$wgExtraNamespaces[{$nameDefine}] = '{$namespace->getName()}';
\$wgExtraNamespaces[{$talkDefine}] = '{$namespace->getTalk()}';";
				$nameAlias = $namespace->getNameAlias();
				if (!empty($nameAlias)) {
					$this->PHP .= "
\$wgNamespaceAliases['{$nameAlias}'] = {$nameDefine};";
				}
				$talkAlias = $namespace->getTalkAlias();
				if (!empty($talkAlias)) {
					$this->PHP .= "
\$wgNamespaceAliases['{$talkAlias}'] = {$talkDefine};";
				}
				$this->PHP .= "
\$wgNamespacesWithSubpages[{$nameDefine}] = true;\n";
				if ($namespace->isContent()) {
					$this->PHP .= "\n\$wgContentNamespaces[] = {$namespace->getId()};";
				} else {
					$this->PHP .= "\n\$wgNamespaceRobotPolicies[$nameDefine] = 'noindex';";
				}
				if ($namespace->isSearchable()) {
					$this->PHP .= "\n\$wgNamespacesToBeSearchedDefault[{$nameDefine}] = true;";
				}
				if ($namespace->getProtection()) {
					$this->PHP .= "\n\$wgNamespaceProtection[{$nameDefine}] = ['{$namespace->getProtection()}'];";
				}
			}
		}

		foreach ($wiki->getSettings()->getGroup('Mediawiki') as $setting) {
			$this->PHP .= "\n" . $setting->getExportedCode() . "\n";
		}

		$this->PHP .= "
/* Don't include extensions if SETTINGS_ONLY is defined. */\nif (!defined('SETTINGS_ONLY')) {
## Search settings
wfLoadExtension('Elastica');
require_once(\"{\$IP}/extensions/CirrusSearch/CirrusSearch.php\");
";
		if (count($wiki->getExtensions()->inLoadOrder())) {
			foreach ($wiki->getExtensions()->inLoadOrder() as $ext) {
				$this->PHP .= $ext->getExportedCode();
			}
		}
		$this->PHP .= "\n}";

		$this->PHP .= "
\$wgCirrusSearchClusters = [
	'default' => [
		[
			'host' => " . var_export($wiki->getSearchSetup()['search_server'], true) . ",
			'port' => " . var_export($wiki->getSearchSetup()['search_port'], true) . "
		]
	]
];
\$wgCirrusSearchReplicas = '0-1';
\$wgCirrusSearchShardCount = [ 'content' => 3, 'general' => 3, 'archive' => 3, 'titlesuggest' => 3 ];
\$wgCirrusSearchUseExperimentalHighlighter = true;
\$wgCirrusSearchOptimizeIndexForExperimentalHighlighter = true;
\$wgCirrusSearchWikimediaExtraPlugin['regex'] = ['build', 'use'];
\$wgCirrusSearchWikimediaExtraPlugin['super_detect_noop'] = true;
\$wgCirrusSearchWikimediaExtraPlugin['documentVersion'] = true;
\$wgCirrusSearchWikimediaExtraPlugin['id_hash_mod_filter'] = true;
";

		foreach ($wiki->getSettings()->withoutExtensions()->withoutGroup('Mediawiki') as $setting) {
			$this->PHP .= "\n" . $setting->getExportedCode() . "\n";
		}

		$groupPermissions = $wiki->getPermissions();
		if (is_array($groupPermissions) && count($groupPermissions)) {
			$this->PHP .= "\n\n//Custom Permissions\n";
			foreach ($groupPermissions as $gpid => $groupPermission) {
				$groupAllow = $groupPermission->getAllow();
				if (is_array($groupAllow) && count($groupAllow)) {
					foreach ($groupAllow as $value) {
						$value = str_replace("'", "\'", $value);
						$this->PHP .= "\$wgGroupPermissions['{$groupPermission->getGroupName()}']['{$value}'] = true;\n";
						$this->PHP .= "\$wgGrantPermissions['custom_{$groupPermission->getGroupName()}']['{$value}'] = true;\n";
					}
				}
				$groupDeny = $groupPermission->getDeny();
				if (is_array($groupDeny) && count($groupDeny)) {
					foreach ($groupDeny as $value) {
						$value = str_replace("'", "\'", $value);
						$this->PHP .= "\$wgGroupPermissions['{$groupPermission->getGroupName()}']['{$value}'] = false;\n";
					}
				}
			}
		}

		$this->PHP .= "\n
					if (file_exists(__DIR__.'/Advertisements.php')) { @include_once(__DIR__.'/Advertisements.php'); }
					if (file_exists(__DIR__.'/Promotions.php')) { @include_once(__DIR__.'/Promotions.php'); }
					";

		$mini = "";
		$tokens = token_get_all($this->PHP);
		foreach ($tokens as $token) {
			if (is_array($token)) {
				if (in_array(token_name($token[0]), ['T_COMMENT','T_DOC_COMMENT'])) {
					continue; // Strip the comments
				}
				$token = $token[1];
			}
			$mini .= $token;
		}
		$mini = preg_replace('/\t+/', '', $mini); // strip tabs
		$mini = preg_replace('~[\r\n]+~', "\r\n", $mini); // strip blank lines
		$this->PHP = $mini;

		return $this->PHP;
	}

	/**
	 * Wiki Local Settings Redirect Template
	 *
	 * @param array Array of wiki information
	 * @param integer Server Environment
	 *
	 * @return string Built PHP
	 */
	public function localSettingsRedirect($wiki, $environment) {
		$this->PHP = "<?php
	\$domain           = 'https://" . str_replace("'", "\'", $wiki->getDomains()->getDomain($environment)) . "';
\$URI = preg_replace('#^/mediawiki/(.*)$#s', '/$1', \$_SERVER['REQUEST_URI']);
\$URI = preg_replace('#^/article/(.*)$#s', '/$1', \$URI);
\$URI = preg_replace('#^/wiki/(.*)$#s', '/$1', \$URI);
\$URI = preg_replace('#^/w/(.*?.php.*)$#s', '/$1', \$URI);

header('HTTP/1.1 301 Moved Permanently');
header('Location: '.\$domain.\$URI);
exit;
";

		return $this->PHP;
	}
}
