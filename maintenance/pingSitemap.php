<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Dynamic Settings Ping Sitemaps
 *
 * @author    Tim Aldridge
 * @copyright (c) 2013 Curse Inc.
 * @license   Proprietary
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
**/

if (PHP_SAPI != 'cli') {
	exit;
}

class pingSitemap {
	/**
	 * @var array
	 */
	protected $searchEngines = [
		'google' => "https://www.google.com/webmasters/tools/ping?sitemap=",
		'bing' => "https://www.bing.com/webmaster/ping.aspx?siteMap="
	];

	/**
	 * @var array
	 */
	protected $ignore = [
		'cgi-bin',
		'.',
		'..',
		'sitemap-index-hydra.xml'
	];

	/**
	 * @var string
	 */
	protected $sitemapfile;

	/**
	 * Main Constructor
	 */
	public function __construct() {
	}

	/**
	 * @param $domain
	 *
	 * @return string
	 */
	public function pingSearchEngine($domain) {
		// Now we need to get the title of the sitemap & setup the full url path
		$sitemapPath = '/media/hydra-media/sitemaps/' . $domain . '/';
		if (is_dir($sitemapPath) && ($handle = opendir($sitemapPath)) !== false) {
			// Time to loop over the path
			while (false !== ($file = readdir($handle))) {
				if (!in_array($file, $this->ignore)) {
					preg_match('#sitemap-index.*#is', $file, $match);
					if ($match) {
						$this->sitemapfile = $match[0];
					}
				}
			}
			$sitemap = 'https://' . $domain . '/sitemaps/' . $domain . '/' . $this->sitemapfile;

			$useragent = "Site Map Ping/1.0 (Hydra)";
			$cookieFileHash = md5($useragent);

			echo "Pinging {$domain}...\n";
			// Ok, let's get them submitted out to the search engines.
			foreach ($this->searchEngines as $site => $endPoint) {
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $endPoint . urlencode($sitemap));
				curl_setopt($ch, CURLOPT_TIMEOUT, 10);
				curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
				curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
				curl_setopt($ch, CURLOPT_MAXREDIRS, 4);
				curl_setopt($ch, CURLOPT_COOKIEFILE, '/tmp/' . $cookieFileHash);
				curl_setopt($ch, CURLOPT_COOKIEJAR, '/tmp/' . $cookieFileHash);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				$output = curl_exec($ch);

				$responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

				if ($responseCode != 200) {
					curl_close($ch);
					echo "There was an error pinging out ({$sitemap}): HTTP {$responseCode}\n";
					continue;
				}

				echo "Sitemap ({$sitemap}) has been successfully pinged out to {$site}!\n";
				curl_close($ch);
			}
		} else {
			echo "There was no sitemap to ping out!\n";
		}
	}
}

$domain = $argv[1];
$pinger = new pingSitemap();
$pinger->pingSearchEngine($domain);
