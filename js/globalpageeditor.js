$(function(){
	handleCreateNewPages( $('#create_new_pages').is(':checked') );

	$('#create_new_pages').change(function(){
		handleCreateNewPages(this.checked);
	});

	function handleCreateNewPages(checked) {
		var div = $('#create_new_pages_container');
		if (checked) {
			div.show();
		} else {
			div.hide();
		}
	}

	function gpeAction() {
		$('.gpe_action_option').hide(); // Hide em all.
		var showId = $('#gpe_action').val();
		$('#'+showId).show();
	}

	gpeAction();

	$('#gpe_action').change(function(){
		gpeAction();
	});

	var templates = [
		{
			name: "----------", /* DONT REMOVE THE FIRST ONE K? */
			sample: "",
			find: "",
			replace: "",
			type: 0 //type: 0 = Plain Text, 1 = Glob Pattern, 2 = Regex
		},
		{
			name: "Helper - Append To Pages",
			sample: "This is some content on a page.",
			find: "^(.*?)$",
			replace: "$1\nAnd this is extra content to be added to the end of the page.",
			type: 2 //type: 0 = Plain Text, 1 = Glob Pattern, 2 = Regex
		},
		{
			name: "Helper - Prepend To Pages",
			sample: "This is some content on a page.",
			find: "^(.*?)$",
			replace: "And this is extra content to be added to the top of the page.\n$1",
			type: 2 //type: 0 = Plain Text, 1 = Glob Pattern, 2 = Regex
		},
		{
			name: "Example - Plain Text",
			sample: "Foo foo FOO fOO food barfoo\n\nPlain text is case sensitive, and finds any instance",
			find: "foo",
			replace: "bar",
			type: 0 //type: 0 = Plain Text, 1 = Glob Pattern, 2 = Regex
		},
		{
			name: "Example - Glob Pattern",
			sample: "Foo foo FOO fOO food barfoo fooooooo.",
			find: "f*o",
			replace: "bar",
			type: 1 //type: 0 = Plain Text, 1 = Glob Pattern, 2 = Regex
		},
		{
			name: "Example - Regular Expression",
			sample: "Foo foo FOO fOO food barfoo\n\nWant to learn Regex? http://php.net/manual/en/reference.pcre.pattern.syntax.php\nNote that in the find, do not use delimiters or flgs as they will be added for you automatically.",
			find: "(foo)",
			replace: "bar",
			type: 2 //type: 0 = Plain Text, 1 = Glob Pattern, 2 = Regex
		}
	];

	for(var x in templates) {
		var opt = "<option value=\"" + x + "\">"+ templates[x].name +"</option>";
		$("#template").append(opt);
	}

	$("#template").change(function(){
		var val = $("#template").val();
		var template = templates[val];

		console.log(template);

		$("#sample_text").val(template.sample);
		$("#find").val(template.find);
		$("#replace").val(template.replace);

		$("#option option").prop('selected',false);
		$('#option option:eq(' + template.type + ')').prop('selected', true);

	});


	$('#show_hide_debug').click(function(){
		$('#debug_panel').toggle();
	});
});

/*
Global Page Editor brought to you by Chickens.
They have tendies, and they won't share.

    ,~.
   ,-'__ `-,
  {,-'  `. }              ,')
 ,( a )   `-.__         ,',')~,
<=.) (         `-.__,==' ' ' '}
  (   )                      /)
   `-'\   ,                    )
       |  \        `~.        /
       \   `._        \      /
        \     `._____,'    ,'
         `-.             ,'
            `-._     _,-'
                77jj'
               //_||
            __//--'/`
          ,--'/`  '
*/