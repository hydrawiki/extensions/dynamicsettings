function WikiAdvertisements($) {
	'use strict';
	this.init = function() {
		$('.lined').each(function() {
			advertisements.setupLinedTextArea(this);
		});

		$('.slot_state_selector').each(function() {
			$(this).change(function() {
				if ($(this).val() == -1) {
					$('textarea[name="'+$(this).attr('data-slot')+'"]').attr('disabled', true);
				} else {
					$('textarea[name="'+$(this).attr('data-slot')+'"]').attr('disabled', false);
				}
			});
		});
	};

	var advertisements = {
		setupLinedTextArea: function(element) {
			$(element).linedtextarea();
		}
	};
}

var WA = new WikiAdvertisements(jQuery);
jQuery(document).ready(WA.init);
