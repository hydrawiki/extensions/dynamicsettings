<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Dynamic Settings Ping All Sitemaps
 *
 * @author    Tim Aldridge
 * @copyright (c) 2013 Curse Inc.
 * @license   Proprietary
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
**/

require_once dirname(__DIR__, 3) . '/maintenance/Maintenance.php';

/**
 * Class pingAllSitemaps
 */
class pingAllSitemaps extends Maintenance {
	/**
	 * Main Constructor
	 */
	public function __construct() {
		parent::__construct();

		define('MAINTENANCE_DIR', __DIR__);

		$this->mDescription = "Generate Sitemaps for All of Hydra.";
	}

	/**
	 * Main executor for the maintenance script.
	 */
	public function execute() {
		$this->output('Queuing Ping All Sitemaps job');
		\DynamicSettings\Job\PingAllSitemapsJob::queue();
	}
}

$maintClass = "pingAllSitemaps";
require_once RUN_MAINTENANCE_IF_MAIN;
