<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Promotions Class
 *
 * @author    Alexia E. Smith
 * @copyright (c) 2014 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
 **/

namespace DynamicSettings\Wiki;

use DynamicSettings\DSDBFactory;
use DynamicSettings\EditLog;
use RequestContext;

class Promotion {
	use \DynamicSettings\Traits\ExtractFieldDataTrait;

	/**
	 * Promotion Types
	 *
	 * @var array
	 */
	private $types = ['promotion', 'notice', 'advertisement'];

	/**
	 * Promotion Container
	 *
	 * @var array
	 */
	private $data = [
		'pid'			=> 0,
		'name'			=> null,
		'description'	=> null,
		'image'			=> null,
		'link'			=> null,
		'begins'		=> null,
		'expires'		=> null,
		'weight'		=> 0,
		'type'			=> 'promotion',
		'everywhere'	=> false,
		'paused'		=> false,
		'wikis'			=> []
	];

	/**
	 * What is exposed to the API.
	 *
	 * 'property' => ['getter', 'setter', 'type', 'permission']
	 * Make sure to define all four, even with null, otherwise E_NOTICE may get thrown in some places.
	 * Use null to deny access to a getter or setter.
	 * Example: ['getSiteKey', null, 'string', null]
	 * If the end point requires permission to access declare the required user right.
	 * Example: ['getWeight', 'setWeight', 'integer', 'totally_made_up_permission']
	 *
	 * @var array
	 */
	protected $apiExposed = [
		'name'			=> ['getName', 'setName', 'string', 'wiki_promotions'],
		'description'	=> ['getDescription', 'setDescription', 'string', 'wiki_promotions'],
		'image'			=> ['getImage', 'setImage', 'string', 'wiki_promotions'],
		'link'			=> ['getLink', 'setLink', 'string', 'wiki_promotions'],
		'begins'		=> ['getBegins', 'setBegins', 'integer', 'wiki_promotions'],
		'expires'		=> ['getExpires', 'setExpires', 'integer', 'wiki_promotions'],
		'weight'		=> ['getWeight', 'setWeight', 'integer', 'wiki_promotions'],
		'type'			=> ['getType', 'setType', 'integer', 'wiki_promotions'],
		'everywhere'	=> ['isEnabledEverywhere', 'setEnabledEverywhere', 'boolean', 'wiki_promotions'],
		'paused'		=> ['isPaused', 'setPaused', 'boolean', 'wiki_promotions'],
		'wikis'			=> ['getWikis', 'setWikis', 'array', 'wiki_promotions']
	];

	/**
	 * Load all Promotion objects.
	 *
	 * @param string [Optional] Column to sort by.
	 * @param string [Optional] Sort direction.
	 *
	 * @return mixed Array of Promotion objects or false for no results.
	 */
	public static function loadAll($sortKey = 'name', $sortDir = 'ASC') {
		$promotions = false;
		$promotion = new Promotion();

		$db = DSDBFactory::getMasterDB(DB_MASTER);

		$results = $db->select(
			['wiki_promotions', 'wiki_promotions_sites'],
			['*'],
			[],
			__METHOD__,
			[
				'ORDER BY'	=> ($db->fieldExists('wiki_promotions', $sortKey) ? $sortKey : 'name') . ' ' . ($sortDir == 'DESC' ? 'DESC' : 'ASC'),
			],
			[
				'wiki_promotions_sites' => [
					'LEFT JOIN', 'wiki_promotions_sites.promotion_id = wiki_promotions.pid'
				]
			]
		);

		$_promotions = [];
		$_promotion['wikis'] = [];
		while ($row = $results->fetchRow()) {
			if (array_key_exists($row['pid'], $_promotions)) {
				$_promotions[$row['pid']]['wikis'][] = $row['site_key'];
				continue;
			}
			$_promotions[$row['pid']] = $row;
			$_promotions[$row['pid']]['wikis'][] = ['site_key' => $row['site_key'], 'override' => intval($row['override'])];
		}

		foreach ($_promotions as $pid => $_promotion) {
			$promotion = new Promotion();
			if ($promotion->load($_promotion) === true) {
				$promotions[$pid] = $promotion;
			}
			unset($_promotions[$pid]);
		}

		return $promotions;
	}

	/**
	 * Load a new Promotion object from a promotion database ID.
	 *
	 * @param integer Promotion Database ID
	 *
	 * @return mixed Promotion object or false on failure.
	 */
	public static function loadFromId($promotionId) {
		$promotion = new Promotion();

		$db = DSDBFactory::getMasterDB(DB_MASTER);

		$results = $db->select(
			['wiki_promotions', 'wiki_promotions_sites'],
			['*'],
			'wiki_promotions.pid = ' . intval($promotionId),
			__METHOD__,
			[],
			[
				'wiki_promotions_sites' => [
					'LEFT JOIN', 'wiki_promotions_sites.promotion_id = wiki_promotions.pid'
				]
			]
		);

		$_promotion = [];
		$_promotion['wikis'] = [];
		while ($row = $results->fetchRow()) {
			if (!isset($_promotion['pid'])) {
				$_promotion = $row; // This needs to come before the site_key check below.
				$_promotion['wikis'] = [];
			}

			if (isset($_promotion['pid'])) {
				if (isset($row['site_key'])) {
					$_promotion['wikis'][] = ['site_key' => $row['site_key'], 'override' => intval($row['override'])];
				}
			}
		}

		if (!$promotion->load($_promotion)) {
			$promotion = false;
		}
		unset($_promotion);

		return $promotion;
	}

	/**
	 * Load a new Promotion object from Wiki object.
	 *
	 * @param object Wiki
	 *
	 * @return mixed Promotion object or false on failure.
	 */
	public static function loadFromWiki(\DynamicSettings\Wiki $wiki) {
		$promotions = false;
		$promotion = new Promotion();

		$db = DSDBFactory::getMasterDB(DB_MASTER);

		$results = $db->select(
			['wiki_promotions_sites'],
			['*'],
			"wiki_promotions_sites.site_key = '" . $db->strencode($wiki->getSiteKey()) . "'",
			__METHOD__
		);

		$show = [];
		$hide = [];
		while ($row = $results->fetchRow()) {
			if ($row['override'] == 1) {
				$show[] = $row['promotion_id'];
			}
			if ($row['override'] == -1) {
				$hide[] = $row['promotion_id'];
			}
		}

		$results = $db->select(
			['wiki_promotions'],
			['*'],
			(count($show) ? "pid IN(" . implode(', ', $show) . ") OR " : null) . "(everywhere = 1" . (count($hide) ? " AND pid NOT IN(" . implode(', ', $hide) . ")" : null) . ")",
			__METHOD__,
			[
				'ORDER BY'	=> 'weight ASC',
				'GROUP BY'	=> 'pid',
			]
		);

		$_promotions = [];
		while ($row = $results->fetchRow()) {
			if (!isset($_promotions[$row['pid']])) {
				$_promotions[$row['pid']] = $row; // This needs to come before the site_key check below.
				$_promotions[$row['pid']]['wikis'] = [];
			}

			if (isset($_promotions[$row['pid']])) {
				if (isset($row['site_key'])) {
					$_promotions[$row['pid']]['wikis'][] = ['site_key' => $row['site_key'], 'override' => intval($row['override'])];
				}
			}
		}

		foreach ($_promotions as $pid => $_promotion) {
			$promotion = new Promotion();
			if ($promotion->load($_promotion) === true) {
				$promotions[$pid] = $promotion;
			}
			unset($_promotions[$pid]);
		}

		return $promotions;
	}

	/**
	 * Returns a new Promotion object.
	 *
	 * @return object Fresh Namespace
	 */
	public static function loadFromNew() {
		$promotion = new Promotion();

		return $promotion;
	}

	/**
	 * Loads promotions for this wiki.
	 *
	 * @param array Fetched database row array.
	 *
	 * @return boolean Success
	 */
	public function load($row) {
		if (!isset($row['pid'])) {
			return false;
		}

		$this->data = [
			'pid'			=> $row['pid'],
			'name'			=> $row['name'],
			'description'	=> $row['description'],
			'image'			=> $row['image'],
			'link'			=> $row['link'],
			'begins'		=> $row['begins'],
			'expires'		=> $row['expires'],
			'weight'		=> $row['weight'],
			'type'			=> $row['type'],
			'everywhere'	=> (bool)$row['everywhere'],
			'paused'		=> (bool)$row['paused'],
			'wikis'			=> $row['wikis']
		];

		return true;
	}

	/**
	 * Save promotion to the database.
	 *
	 * @return boolean Success
	 */
	public function save() {
		$promotionSites = $this->data['wikis'];

		$success = false;

		$db = DSDBFactory::getMasterDB(DB_MASTER);
		$db->startAtomic(__METHOD__);

		$save = [
			'name'			=> $this->data['name'],
			'description'	=> $this->data['description'],
			'image'			=> $this->data['image'],
			'link'			=> $this->data['link'],
			'begins'		=> $this->data['begins'],
			'expires'		=> $this->data['expires'],
			'weight'		=> $this->getWeight(),
			'type'			=> $this->data['type'],
			'everywhere'	=> intval($this->data['everywhere']),
			'paused'		=> intval($this->data['paused'])
		];

		if ($this->data['pid']) {
			$result = $db->update(
				'wiki_promotions',
				$save,
				['pid' => $this->data['pid']],
				__METHOD__
			);

			$databaseId = $this->data['pid'];
		} else {
			$result = $db->insert(
				'wiki_promotions',
				$save,
				__METHOD__
			);

			$databaseId = $db->insertId();
		}

		$wikis = [];
		foreach ($promotionSites as $data) {
			$wikis[] = [
				'promotion_id'	=> $databaseId,
				'site_key'		=> $data['site_key'],
				'override'		=> $data['override']
			];
		}

		if ($result) {
			// Time to delete the linking table
			$result = $db->delete(
				'wiki_promotions_sites',
				['promotion_id' => $databaseId],
				__METHOD__
			);
		} else {
			$db->cancelAtomic(__METHOD__);
			return $success;
		}

		if ($result && count($wikis)) {
			$result = $db->insert(
				'wiki_promotions_sites',
				$wikis,
				__METHOD__
			);
		} elseif (!$result) {
			$db->cancelAtomic(__METHOD__);
			return $success;
		}

		if (!$result) {
			$db->cancelAtomic(__METHOD__);
		} else {
			$wgUser = RequestContext::getMain()->getUser();

			$success = true;
			$db->endAtomic(__METHOD__);

			$this->data['pid'] = $databaseId;

			$miscData = [
				'display_name' => $this->getName(),
			];

			$save['wikis'] = json_encode($wikis);
			EditLog::addEntry(
				$this->data['pid'], // Log Key: pid
				$save,
				__METHOD__,
				$wgUser,
				'`' . $this->getName() . '` edited',
				$miscData
			);
		}

		return $success;
	}

	/**
	 * Return the database ID.
	 *
	 * @return integer Database ID
	 */
	public function getDatabaseId() {
		return ($this->data['pid'] ? $this->data['pid'] : false);
	}

	/**
	 * Return promotion name.
	 *
	 * @return string Name
	 */
	public function getName() {
		return $this->data['name'];
	}

	/**
	 * Set promotion name.
	 *
	 * @param string Name
	 *
	 * @return boolean Success
	 */
	public function setName($name) {
		if (empty($name)) {
			return false;
		}
		$this->data['name'] = $name;
		return true;
	}

	/**
	 * Return promotion description.
	 *
	 * @return string Description
	 */
	public function getDescription() {
		return $this->data['description'];
	}

	/**
	 * Set promotion description.
	 *
	 * @param string Description
	 *
	 * @return boolean Success
	 */
	public function setDescription($description) {
		$this->data['description'] = $description;
		if (!$description) {
			return false;
		}
		return true;
	}

	/**
	 * Return promotion image.
	 *
	 * @return string Image
	 */
	public function getImage() {
		return $this->data['image'];
	}

	/**
	 * Set promotion image.
	 *
	 * @param string Image
	 *
	 * @return boolean Success
	 */
	public function setImage($image) {
		if (!empty($image) && !filter_var($image, FILTER_VALIDATE_URL)) {
			return false;
		}
		$this->data['image'] = $image;
		return true;
	}

	/**
	 * Return promotion link.
	 *
	 * @return string Link
	 */
	public function getLink() {
		return $this->data['link'];
	}

	/**
	 * Set promotion link.
	 *
	 * @param string Link
	 *
	 * @return boolean Success
	 */
	public function setLink($link) {
		if (!empty($link) && !filter_var($link, FILTER_VALIDATE_URL)) {
			return false;
		}
		$this->data['link'] = $link;
		return true;
	}

	/**
	 * Return promotion languages.
	 *
	 * @return string Languages
	 */
	public function getLanguages() {
		return (isset($this->data['languages']) ? strval($this->data['languages']) : '');
	}

	/**
	 * Set promotion languages.
	 *
	 * @param string Languages
	 *
	 * @return boolean Success
	 */
	public function setLanguages($languages) {
		if (empty($languages)) {
			$this->data['languages'] = null;
		} else {
			$this->data['languages'] = $languages;
		}
		return true;
	}

	/**
	 * Return promotion begins.
	 *
	 * @return string Begins
	 */
	public function getBegins() {
		return $this->data['begins'];
	}

	/**
	 * Set promotion begins.
	 *
	 * @param string Begins
	 *
	 * @return boolean Success
	 */
	public function setBegins($begins) {
		if (empty($begins)) {
			$this->data['begins'] = null;
		} else {
			$this->data['begins'] = intval($begins);
		}
		return true;
	}

	/**
	 * Return promotion expires.
	 *
	 * @return string Expires
	 */
	public function getExpires() {
		return $this->data['expires'];
	}

	/**
	 * Set promotion expires.
	 *
	 * @param string Expires
	 *
	 * @return boolean Success
	 */
	public function setExpires($expires) {
		if (empty($expires)) {
			$this->data['expires'] = null;
		} else {
			$this->data['expires'] = intval($expires);
		}
		return true;
	}

	/**
	 * Return if the promotion is paused.
	 *
	 * @return boolean Paused
	 */
	public function isPaused() {
		return (bool)$this->data['paused'];
	}

	/**
	 * Set if the promotion is paused.  Automatically toggles if not specified which to do.
	 *
	 * @param boolean [Optional] Whether to pause or not.
	 *
	 * @return boolean Success
	 */
	public function setPaused($pause = null) {
		if (!$this->data['paused'] || $pause === true) {
			$this->data['paused'] = 1;
		} else {
			$this->data['paused'] = 0;
		}
		return true;
	}

	/**
	 * Return promotion weight.
	 *
	 * @return string Weight
	 */
	public function getWeight() {
		return intval($this->data['weight']);
	}

	/**
	 * Set promotion weight.
	 *
	 * @param string Weight
	 *
	 * @return boolean Success
	 */
	public function setWeight($weight) {
		$this->data['weight'] = intval($weight);

		return true;
	}

	/**
	 * Return promotion type.
	 *
	 * @return string Type
	 */
	public function getType() {
		return $this->data['type'];
	}

	/**
	 * Set promotion type.
	 *
	 * @param string Type
	 *
	 * @return boolean Success
	 */
	public function setType($type) {
		if (!in_array($type, $this->types)) {
			$this->data['type'] = 'promotion';
		} else {
			$this->data['type'] = $type;
		}
		return true;
	}

	/**
	 * Return all the selected wiki information for this promotion.
	 *
	 * @return array Wiki Information
	 */
	public function getWikis() {
		return $this->data['wikis'];
	}

	/**
	 * Clear the assigned wikis.
	 *
	 * @return void
	 */
	public function clearWikis() {
		$this->data['wikis'] = [];
	}

	/**
	 * Assign a wiki to this promotion.
	 *
	 * @param string Site Key for the wiki to add.
	 * @param integer [Optional] Override value.  -1 = Do not show on this regardless of being enabled everywhere.  0 = Default, show on this wiki.  1 = Show on this wiki regardless if disabled everywhere.
	 *
	 * @return boolean Success
	 */
	public function addWiki($siteKey, $override = 0) {
		if (\DynamicSettings\Wiki::exists($siteKey)) {
			$this->data['wikis'][] = ['site_key' => $siteKey, 'override' => intval($override)];
			return true;
		}
		return false;
	}

	/**
	 * Return if this promotion is enabled everywhere.
	 *
	 * @return boolean Enabled Everywhere
	 */
	public function isEnabledEverywhere() {
		return (bool)$this->data['everywhere'];
	}

	/**
	 * Set if this promotion is enabled everywhere.
	 *
	 * @param boolean [Optional] Enabled Everywhere, default true.
	 *
	 * @return void
	 */
	public function setEnabledEverywhere($enabled = true) {
		return $this->data['everywhere'] = (bool)$enabled;
	}

	/**
	 * Return if this promotion is an advertisement
	 *
	 * @return boolean this promo is an ad
	 */
	public function isAdvertisement() {
		return $this->data['type'] == 'advertisement';
	}
}
