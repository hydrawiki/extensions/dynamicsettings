<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Exception thrown in methods that require a valid wiki context
 *
 * @author    Noah Manneschmidt
 * @copyright (c) 2015 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
 **/

namespace DynamicSettings;

use Exception;

class WikiContextException extends Exception {
	public function __construct($msg = 'No wiki context available', $code = 0, Exception $prev = null) {
		parent::__construct($msg, $code, $prev);
	}
}
