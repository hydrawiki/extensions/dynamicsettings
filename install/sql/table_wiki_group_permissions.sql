CREATE TABLE /*_*/wiki_group_permissions (
  `gpid` int(12) NOT NULL,
  `group_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `site_key` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `group_config` text COLLATE utf8_unicode_ci
) /*$wgDBTableOptions*/;

ALTER TABLE /*_*/wiki_group_permissions
  ADD PRIMARY KEY (`gpid`),
  ADD KEY `site_key` (`site_key`);

ALTER TABLE /*_*/wiki_group_permissions
  MODIFY `gpid` int(12) NOT NULL AUTO_INCREMENT;