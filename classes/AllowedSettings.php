<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Allowed Settings Class
 *
 * @author    Alexia E. Smith
 * @copyright (c) 2014 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
 **/

namespace DynamicSettings;

class AllowedSettings {
	/**
	 * Mediawiki Database Object
	 *
	 * @var object
	 */
	private $DB;

	/**
	 * Allowed Settings Container
	 *
	 * @var array
	 */
	private $allowedSettings = null;

	/**
	 * Cached Object Instance
	 *
	 * @var object
	 */
	private static $instance = null;

	/**
	 * Function Documentation
	 *
	 * @return void
	 */
	public function __construct() {
		$this->DB = DSDBFactory::getMasterDB(DB_MASTER);
	}

	/**
	 * Get a cached object instance.  Use new to get a fresh instance if required.
	 *
	 * @return object Self
	 */
	public static function getInstance() {
		if (self::$instance === null) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	/**
	 * Loads the allowed settings.
	 *
	 * @return void
	 */
	private function loadAllowedSettings() {
		$results = $this->DB->select(
			['wiki_allowed_settings'],
			['*'],
			[],
			__METHOD__,
			[
				'ORDER BY'	=> 'setting_group_name ASC, setting_key ASC'
			]
		);

		while ($row = $results->fetchRow()) {
			$this->allowedSettings[$row['asid']] = $row;
			if ($row['setting_type'] == 'array' && !empty($row['setting_default'])) {
				$this->allowedSettings[$row['asid']]['setting_default'] = unserialize($row['setting_default']);
			}
		}
	}

	/**
	 * Returns all the loaded allowed settings.
	 *
	 * @return array Multidimensional array of allowed setting information.  $allowedSettingID => $allowedSettingInfo[]
	 */
	public function getAll() {
		if ($this->allowedSettings === null) {
			$this->loadAllowedSettings();
		}
		return $this->allowedSettings;
	}
}
