<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * StatusValueErrors Trait
 *
 * @author    Alexia Smith
 * @copyright (c) 2018 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
 **/

namespace DynamicSettings\Traits;

trait StatusValueErrorsTrait {
	/**
	 * Extract errors out of a StatusValue into a compatible [field => message_key] array format.
	 * Error message keys returned from models are such: error-field_name-error_type - Example: error-wiki_name-invalid
	 * This splits out the field into $errors[$field] = $error['message'].
	 *
	 * @param object StatusValue
	 * @param array Array of existing errors.  Will overwrite duplicate errors.
	 *
	 * @return void
	 */
	public function extractStatusValueErrors(\StatusValue $status, array &$errors) {
		if (!$status->isGood()) {
			foreach ($status->getErrors() as $error) {
				list(, $field, ) = explode('-', $error['message']);
				$errors[$field] = $error['message'];
			}
		}
	}
}
