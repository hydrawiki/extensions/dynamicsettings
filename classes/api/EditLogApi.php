<?php
/**
 * DynamicSettings
 * Edit Log API
 *
 * @author    Alexia E. Smith
 * @copyright (c) 2018 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   DynamicSettings
 * @link      https://gitlab.com/hydrawiki
 **/

namespace DynamicSettings\Api;

use DynamicSettings\EditLog;
use DynamicSettings\EditLog\Entry;
use DynamicSettings\Environment;

class EditLogApi extends \ApiBase {
	/**
	 * Main Executor
	 *
	 * @return void [Outputs to screen]
	 */
	public function execute() {
		if (!Environment::isMasterWiki()) {
			$this->dieWithError(['apierror-permissiondenied-generic']);
			return;
		}

		$this->params = $this->extractRequestParams();

		if (!$this->getUser()->isLoggedIn()) {
			$this->dieWithError(['invaliduser', $this->params['do']]);
		}

		switch ($this->params['do']) {
			case 'getLogDiff':
				$response = $this->getLogDiff();
				break;
			default:
				$this->dieWithError(['invaliddo', $this->params['do']]);
				break;
		}

		foreach ($response as $key => $value) {
			$this->getResult()->addValue(null, $key, $value);
		}
	}

	/**
	 * Requirements for API call parameters.
	 *
	 * @return array Merged array of parameter requirements.
	 */
	public function getAllowedParams() {
		return [
			'do' => [
				\ApiBase::PARAM_TYPE		=> 'string',
				\ApiBase::PARAM_REQUIRED => true
			],
			'logId' => [
				\ApiBase::PARAM_TYPE		=> 'integer',
				\ApiBase::PARAM_REQUIRED => true
			]
		];
	}

	/**
	 * Descriptions for API call parameters.
	 *
	 * @return array Merged array of parameter descriptions.
	 */
	public function getParamDescription() {
		return [
			'do'	=> 'Action to take.',
			'logId'	=> 'Log ID to look up.'
		];
	}

	/**
	 * Merge API errors into the mediawiki error array.
	 *
	 * @return array
	 */
	public function getPossibleErrors() {
		return array_merge(
			parent::getPossibleErrors(),
			[
				[
					'code' => 'NoLogId',
					'info' => 'No log ID was passed.'
				]
			]
		);
	}

	/**
	 * Search for possible wikis to return.
	 *
	 * @return array API result.
	 */
	public function getLogDiff() {
		if (!$this->getUser()->isAllowed('wiki_edit_log_form_data')) {
			return ['success' => false, 'htmlDiff' => ''];
		}

		$logId = $this->params['logId'];

		$editLog = new EditLog();
		$entry = Entry::newFromId($logId);

		if (!$entry) {
			return ['success' => false, 'htmlDiff' => ''];
		}

		$output = $entry->getDiffAgainstPrevious();

		return ['success' => true, 'htmlDiff' => $output];
	}

	/**
	 * Get version of this API Extension.
	 *
	 * @return string API Extension Version
	 */
	public function getVersion() {
		return '1.0';
	}
}
