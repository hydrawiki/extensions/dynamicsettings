<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Lock Settings Skin
 *
 * @author    Alex Smith
 * @copyright (c) 2014 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
**/

class TemplateLockSettings {
	/**
	 * Output HTML
	 *
	 * @var string
	 */
	private $HMTL;

	/**
	 * Lock Settings Page
	 *
	 * @param array Lock Status Information
	 *
	 * @return string Built HTML
	 */
	public function lockSettingsPage($locked) {
		global $wgOut, $wgUser;

		$lockSettingsPage	= Title::newFromText('Special:LockSettings');
		$lockSettingsURL	= $lockSettingsPage->getFullURL();

		$HTML = wfMessage('lock_status_message', wfMessage($locked ? 'lock_status_locked' : 'lock_status_unlocked')->escaped())->escaped();
		$HTML .= "<br/>
		<a href='{$lockSettingsURL}?do=toggleLock' class='mw-ui-button'>" . wfMessage('toggle_lock') . "</a>";

		return $HTML;
	}
}
