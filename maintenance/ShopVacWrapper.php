<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Populate Default Skin, But using shopvac
 *
 * @author    Cameron Chunn
 * @copyright (c) 2017 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @link      https://gitlab.com/hydrawiki
 */

require_once dirname(__DIR__, 3) . '/maintenance/Maintenance.php';

/**
 * Class PopulateDefaultSkin
 */
class ShopVacWrapper extends Maintenance {
	/**
	 * Main Constructor
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
		$this->mDescription = "Wrapper for shopvac used for local imports";
		$this->addOption('loadout', 'The default loadout to pull from.', true, true);
		$this->addOption('domain', 'The doimain to import into.', true, true);
	}

	/**
	 * Chunk dump file and import single page dumps at a time.
	 *
	 * @return void
	 */
	public function execute() {
		$shopVac = dirname(__DIR__, 3) . "/extensions/ShopVac/script/shopvac.js";
		$loadout = $this->getOption('loadout', false);
		$domain = $this->getOption('domain', false);

		$cmd = "node {$shopVac} --srcWiki=" . escapeshellarg($loadout) . " --destWiki=" . escapeshellarg($domain) . " --purgeRevisions";

		$this->output("About to spawn shopvac...");
		$this->output(" > {$cmd}");
		system($cmd, $return);
		$this->output("ShopVac complete.");
		return $return;
	}
}

$maintClass = "ShopVacWrapper";
require_once RUN_MAINTENANCE_IF_MAIN;
