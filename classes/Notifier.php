<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Notification Functions
 *
 * @author    Alex Smith
 * @copyright (c) 2014 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
 **/

namespace DynamicSettings;

use DynamicSettings\Reverb\NotificationUserLocator;
use RequestContext;
use Reverb\Notification\NotificationBroadcast;

class Notifier {
	/**
	 * Send email to all admins.
	 *
	 * @param string $task             Task Performed
	 * @param array  $eventExtra       [Optional] Extra information for email body template.
	 * @param string $notificationType [Optional] Notification Type - tools or wiki-edit
	 *
	 * @return void
	 */
	public static function sendAdminNotification($task, $eventExtra = [], $notificationType = 'site-management-tools') {
		$wgUser = RequestContext::getMain()->getUser();

		$success = false;

		$db = DSDBFactory::getMasterDB(DB_MASTER);

		if (!is_array($eventExtra)) {
			$eventExtra = [];
		}
		$eventExtra['environment'] = (!empty($_SERVER['PHP_ENV']) ? $_SERVER['PHP_ENV'] : $_SERVER['SERVER_NAME']);
		$eventExtra['task'] = $task;
		$subject = wfMessage('notification-tools-subject-' . $task, $eventExtra['misc']['display_name']);
		$eventExtra['subject'] = "~" . strtoupper($eventExtra['environment']) . "~ " . (!empty($subject) ? $subject : "Automated email from the administrators of Hydra.");
		$eventExtra['log_url'] = \Title::newFromText(
			'Special:' . ($notificationType == 'tools' ? 'WikiToolsLog' : 'WikiEditLog')
		)->getFullUrl(
			($notificationType == 'site-management-tools' ? ['log_type' => ucfirst($task)] : []),
			false,
			PROTO_HTTPS
		);

		$typeUrlParameters = $eventExtra['entry']->getTypeLinkParameters();
		$typeUrl = $typeUrlParameters['title']->getFullUrl($typeUrlParameters['url_arguments']);

		$keyUrlParameters = $eventExtra['entry']->getKeyLinkParameters();
		$keyUrl = $keyUrlParameters['title']->getFullUrl($keyUrlParameters['url_arguments']);

		$broadcast = NotificationBroadcast::newMulti(
			$notificationType,
			$wgUser,
			NotificationUserLocator::getHydraAdmins(),
			[
				'url' => $eventExtra['log_url'] ,
				'message' => [
					[
						'user_note',
						isset($eventExtra['entry']) ? $eventExtra['entry']->getCommitMessage() : ''
					],
					[
						1,
						$eventExtra['performer']->getName()
					],
					[
						2,
						$typeUrl
					],
					[
						3,
						$typeUrlParameters['text']
					],
					[
						4,
						$keyUrl
					],
					[
						5,
						$keyUrlParameters['text']
					]
				]
			]
		);
		if ($broadcast) {
			$broadcast->transmit();
			$success = true;
		}

		return $success;
	}
}
