<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Collection class to hold an array of Extension instances
 *
 * @author    Noah Manneschmidt
 * @copyright (c) 2015 Curse Inc.
 * @license   Proprietary
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
**/

namespace DynamicSettings\Extensions;

/**
 * Iterator which lazily converts data into instances
 * of the Extension class upon access.
 * Maintains a wiki context and passes it down to contained data.
 */
class ExtensionIterator extends \ArrayIterator {
	use ExtensionArrayAccessContext, \DynamicSettings\Traits\WikiContext;

	public function current() {
		$ret = parent::current();
		if ($ret instanceof Extension) {
			$ret->setWikiContext($this->wikiContext);
			// save class-wrapped result back to internal data
			$this[$this->key()] = $ret;
		}
		return $ret;
	}
}
