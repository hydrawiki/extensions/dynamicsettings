<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * FindAndReplace Class
 *
 * @author    Alexia E. Smith
 * @copyright (c) 2015 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
 **/

namespace DynamicSettings;

class FindAndReplace {
	/**
	 * Replacements
	 *
	 * @var array
	 */
	private $replacements = [];

	/**
	 * Plain find and replace type.
	 *
	 * @var integer
	 */
	const PLAIN = 0;

	/**
	 * Glob find and replace type.
	 *
	 * @var integer
	 */
	const GLOB = 1;

	/**
	 * Regular Expression find and replace type.
	 *
	 * @var integer
	 */
	const REGEX = 2;

	/**
	 * Main Constructor
	 *
	 * @return void
	 */
	public function __construct() {
		// code...
	}

	/**
	 * Add a find and replace pair.
	 *
	 * @param string  $find             Pattern to Find
	 * @param string  $replace          Replacement Pattern
	 * @param integer $limit            Limit - Zero is no limit, positive limits X number of replacements forward, negative limits X number of replacements backwards.
	 * @param boolean $ignoreCase       Ignore Case
	 * @param boolean $ignoreWhitespace Ignore Whitespace
	 * @param boolean $wrapAround       Wrap Around(Ignore New Lines/Line Wrap)
	 * @param integer $type             Find and replace type constant from this class.
	 *
	 * @return string Identifier Key - Can be used to remove a replacement later if needed.
	 */
	public function addReplacement($find, $replace, $limit = 0, $ignoreCase = true, $ignoreWhitespace = false, $wrapAround = true, $type = self::PLAIN) {
		$identifier = $this->createIdentifier($find, $replace, $limit, $ignoreCase, $ignoreWhitespace, $wrapAround, $type);

		$this->replacements[$identifier] = [
			'find'				=> $find,
			'replace'			=> $replace,
			'limit'				=> intval($limit),
			'ignore_case'		=> (bool)$ignoreCase,
			'ignore_whitespace'	=> (bool)$ignoreWhitespace,
			'wrap_around'		=> (bool)$wrapAround,
			'type'				=> $type
		];
		return $identifier;
	}

	/**
	 * Remove a previously added replacement.
	 *
	 * @param string $identifier Identifier for the replacement to be removed.
	 *
	 * @return boolean Successfully Removed
	 */
	public function removeReplacement($identifier) {
		if (array_key_exists($identifier, $this->replacement)) {
			unset($this->replacements[$identifier]);
			return true;
		}
		return false;
	}

	/**
	 * Do any replacements on the provided text.
	 *
	 * @param string $text Text to run replacements over.
	 *
	 * @return string Text with replacements performed.
	 */
	public function doReplacements($text) {
		foreach ($this->replacements as $replacement) {
			if (empty($replacement['find'])) {
				continue;
			}

			$reverse = false;
			if ($replacement['limit'] < 0) {
				$reverse = true;
			}

			$textLength = strlen($text);

			switch ($replacement['type']) {
				case self::PLAIN:
					$function = 'str' . ($reverse ? 'r' : '') . ($replacement['ignore_case'] ? 'i' : '') . 'pos';

					$offset = ($reverse ? -1 : 0);
					$increment = strlen($replacement['find']);
					$iterations = 0;

					$positions = [];
					while (($position = $function($text, $replacement['find'], $offset)) !== false) {
						$positions[] = $position;

						$offset = ($reverse ? ($textLength - ($position - 1)) * -1 : $position + $increment);

						$iterations++;
						if ($iterations >= 1000 || ($iterations >= abs($replacement['limit']) && $replacement['limit'] !== 0) || abs($offset) > $textLength) {
							// Break out if over the system limit or user requested limit.
							break;
						}
					}

					rsort($positions); // Sorting the positions descending avoids the issue of the positions being off after the first replacement.

					foreach ($positions as $position) {
						$text = substr_replace($text, $replacement['replace'], $position, $increment);
					}
					break;
				case self::GLOB:
					$replacement['find'] = $this->convertGlobToRegex($replacement['find']);
				case self::REGEX:
					$regex = "#" . str_replace('#', '\#', $replacement['find']) . "#" . ($replacement['ignore_case'] ? 'i' : '') . ($replacement['ignore_whitespace'] ? 'x' : '') . ($replacement['wrap_around'] ? 's' : '');
					if (preg_match_all($regex, $text, $matches, PREG_OFFSET_CAPTURE) !== false) {
						$matches = $matches[0];
						if ($reverse) {
							$matches = array_reverse($matches, true);
						}

						$iterations = 0;
						$offsetDiff = 0;
						foreach ($matches as $match) {
							list($find, $offset) = $match;

							$replace = preg_replace($regex, $replacement['replace'], $find);
							$text = substr_replace($text, $replace, $offset + $offsetDiff, strlen($find));

							if (!$reverse) {
								$offsetDiff = $offsetDiff + (strlen($replace) - strlen($find));
							}

							$iterations++;
							if ($iterations >= 1000 || ($iterations >= abs($replacement['limit']) && $replacement['limit'] !== 0) || abs($offset) > $textLength) {
								// Break out if over the system limit or user requested limit.
								break;
							}
						}
					}
					break;
			}
		}
		return $text;
	}

	/**
	 * Convert a glob pattern to a regular expression.
	 *
	 * @param string $glob Glob Pattern
	 *
	 * @return string Regular Expression
	 */
	private function convertGlobToRegex($glob) {
		$characters = str_split($glob);
		$inGroup = false;
		$extended = false; // This is hard coded to false for now.  May be supported in the future, but the functionality works.
		$regex = '';
		foreach ($characters as $character) {
			switch ($character) {
				case "\\":
				case "/":
				case "$":
				case "^":
				case "+":
				case ".":
				case "(":
				case ")":
				case "=":
				case "!":
				case "|":
					$regex .= "\\" . $character;
					break;
				case "?":
					if ($extended) {
						$regex .= ".";
						break;
					}
				case "[":
				case "]":
					if ($extended) {
						$regex .= $character;
						break;
					}
				case "{":
					if ($extended) {
						$inGroup = true;
						$regex .= "(";
						break;
					}
				case "}":
					if ($extended) {
						$inGroup = false;
						$regex .= ")";
						break;
					}
				case ",":
					if ($inGroup) {
						$regex .= "|";
						break;
					}
					$regex .= "\\" . $character;
					break;
				case "*":
					$regex .= ".*?"; // Non-greedy by default.
					break;
				default:
					$regex .= $character;
					break;
			}
		}
		return $regex;
	}

	/**
	 * Create an identifier for the given find and replace parameters.
	 *
	 * @return string SHA1 Identifier
	 */
	private function createIdentifier() {
		return sha1(implode('', func_get_args()));
	}
}
