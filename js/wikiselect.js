(function(mw, $) {
	/**
	 * @class mw.WikiSelect
	 *
	 * @constructor
	 * @param element DOM element to anchor to.
	 * @throws	Error	Thrown when the element is not found.
	 */
	function WikiSelect(container) {
		if ($(container).length < 1) {
			throw new Error('Invalid DOM element passed to WikiSelect.');
		}

		this.container = container;

		this.setup();

		return this;
	}

	/* Private members */

	//var a, b, c, etcetera...
	var
	container = null;


	/* Static members */

	/**
	 * Example Static Method
	 *
	 * @static
	 * @method
	 *
	 * @return
	 */
	/*WikiSelect.example = function() {
	};*/

	/* Public members */
	WikiSelect.prototype = {
		searchTimeout: null,
		wikiSelections: {},
		wikiCache: {},
		selectType: 'addremove', //addremove, add, remove, single
		everywhere: false,
		onChangeFunction: false,

		setup: function() {
			var self = this;

			$(self.container).attr('data-loaded','1');

			var dataHolder = $(".wiki_selections", self.container);

			///Check the data holder for existing data.
			if ($(dataHolder).length > 0) {
				if ($(dataHolder).val().length > 0) {
					self.wikiSelections = JSON.parse($(dataHolder).val());
				}
			} else {
				//In case the existing data holder was not provided by the page lets created a default.
				dataHolder = $('<input>')
					.addClass('.wiki_selections')
					.attr('data-select-key', 'generic')
					.attr('data-select-type', 'addremove')
					.attr('type', 'hidden')
					.val(JSON.stringify(self.wikiSelections));

				$(self.container).append(dataHolder);
			}

			//Data sanity on self.wikiSelections.
			if (Array.isArray(this.wikiSelections) || this.wikiSelections === null || typeof this.wikiSelections !== 'object') {
				this.wikiSelections = {};
			}

			//Setup the control style being used and enforce data sanity as needed.
			var selectType = $(dataHolder).attr('data-select-type');
			if (selectType == 'addremove' || selectType == 'add' || selectType == 'remove' || selectType == 'single') {
				this.selectType = selectType;
			}

			if (!this.wikiSelections.hasOwnProperty('single')) {
				this.wikiSelections.single = '';
			}

			if (!this.wikiSelections.hasOwnProperty('added') || !Array.isArray(this.wikiSelections.added)) {
				this.wikiSelections.added = [];
			}

			if (!this.wikiSelections.hasOwnProperty('removed') || !Array.isArray(this.wikiSelections.removed)) {
				this.wikiSelections.removed = [];
			}

			var input = $("<input>")
				.addClass('wiki_search')
				.attr('type', 'text')
				.attr('autocomplete', 'off')
				.attr('placeholder', mw.message('type_to_search').escaped())
				.on('keyup', function(event) {
					if (event.type == 'change' || (event.type == 'keyup' && event.keyCode >= 48 && event.keyCode <= 90)) {
						clearTimeout(self.searchTimeout);
						var target = $(this);
						self.searchTimeout = setTimeout(function() {
							var searchTerm = target.val();

							self.showProgressIndicator($('.wiki_search_container', this.container));

							searchTerm = searchTerm.trim();

							if (!searchTerm) {
								$('.wiki_search_container .wiki_selector', self.container).fadeOut(200, function() {
									$(this).remove();
								});
								return;
							}

							var resultData = self.getWiki(searchTerm);

							$('.wiki_search_container .wiki_selector', self.container).fadeOut(200, function() {
								$(this).remove();
							});
							resultData.done(function(data) {
								self.hideProgressIndicator($('.wiki_search_container', this.container));
								if (data.success == true) {
									$.each(data.data, function(siteKey, wiki) {
										if (self.wikiSelections.single !== siteKey) {
											var wikiElement = self.createWikiSelector(siteKey, wiki);
											$(wikiElement).hide().appendTo('.wiki_search_container', self.container).fadeIn(200);
										}
									});
								}
							});
						}, 500);
					}
			});

			var legend = $("<div>").addClass('wiki_legend').append(input);

			$(this.container).append(legend);
			$(this.container).append($("<div>").addClass('wiki_search_container'));
			if (this.selectType == 'single') {
				$(this.container).append($("<div>").addClass('wiki_single_container'));
			}
			$(legend).append($("<span>"));
			if (this.selectType == 'addremove' || this.selectType == 'add') {
				$("span", legend).html(mw.message('wikis_added').escaped());
				$(this.container).append($("<div>").addClass('wiki_added_container'));
			}
			if (this.selectType == 'addremove' || this.selectType == 'remove') {
				$("span", legend).html(mw.message('wikis_removed').escaped());
				$(this.container).append($("<div>").addClass('wiki_removed_container'));
			}

			if (this.selectType == 'addremove') {
				this.setupEverywhere();
				$(this.container).prepend($("<span>").append($('<label>').append(this.everywhereInput).append(mw.message('all_wikis').escaped())));
				$(this.container).addClass('everywhere');
			}

			// Load From List Controller.
			if (this.selectType != 'single') {
				$(this.container).prepend(
					$("<div>").addClass('loadListWrapper').append(
						$('<button>').addClass('loadListShow').addClass('mw-ui-button').addClass('mw-ui-progressive').append(mw.message('load_from_list').escaped())
					).append(
						$('<div>').addClass('loadListContainer').append(
							$('<textarea>').addClass('loadListList')
						).append(
							$('<div>').addClass('loadListControls').append(
								$('<button>').addClass('loadListCancel').append('Cancel')
							).append(
								$('<button>').addClass('loadListSubmit').append('Load List')
							)
						)
					)
				);
			}

			$('.loadListShow').click(function(e){
				e.preventDefault();
				$('.loadListContainer').show();
				$('.loadListShow').hide();
			});

			$('.loadListCancel').click(function(e){
				e.preventDefault();
				$('.loadListContainer').hide();
				$('.loadListShow').show();
			});

			$('.loadListSubmit').click(function(e){
				e.preventDefault();


				$list = $('.loadListList').val().split("\n");
				console.log($list);


				self.wikiCache = {};


				//process out and only grab subdomains.
				for (x in $list) {
					var s = $list[x];
						s = s.trim();
						s = s.split('.');
						s = s[0];

					if (s.length > 0) {
						self.getWiki(s).done(function(data) {
							var s = this;
							if (data.success == true) {
								console.log(data.data);
								for (x in data.data) {
									console.log(s+": "+x);


									self.wikiAdd(x);
								}

								$.extend(self.wikiCache, data.data);
								self.wikiSelections.added = [];
								self.wikiSelections.removed = [];
								for(x in self.wikiCache) {
									self.wikiSelections.added.push(x);
								}

								$('.wiki_selector').each(function(){ $(this).remove(); });
								console.log(self.wikiSelections.added);
								self.buildSelectorsFromWikiCache();
							}
						}.bind(s));
					}
				}

			});

			this.initialWikiSelectorSetup();

			var hiddenEverywhere = $("input[type='hidden'][name='everywhere']", self.container);
			if ($(hiddenEverywhere).length) {
				var isEverywhere = $(hiddenEverywhere).val();
				this.setCurrentlyEverywhere((isEverywhere === 1 || isEverywhere === '1' ? true : false));
				$(hiddenEverywhere).remove();
			}
		},

		/**
		 * Return the everywhere input checkbox.
		 *
		 * @return void
		 */
		setupEverywhere: function() {
			var self = this;

			this.everywhereInput = $("<input>").addClass('everywhere').attr('name', 'everywhere').attr('type', 'checkbox').val('1').on('change click', function(event) {
				if ($(this).is(':checked')) {
					self.setCurrentlyEverywhere(true);
				} else {
					self.setCurrentlyEverywhere(false);
				}
			}).change();
		},

		/**
		 * Set if currently selecting all wikis.
		 *
		 * @param boolean Currently operating on all wikis?
		 */
		setCurrentlyEverywhere: function(isEverywhere) {
			if (isEverywhere) {
				if (!$(this.everywhereInput).is(':checked')) {
					$(this.everywhereInput).attr('checked', true);
				}
				this.everywhere = true;
				$(".wiki_search_container", self.container).addClass('everywhere');
				$(".wiki_search_container", self.container).removeClass('singles');
				$(".wiki_legend span", self.container).html(mw.message('wikis_removed').escaped());
				$('.wiki_added_container', self.container).hide();
				$('.wiki_removed_container', self.container).show().css('display', 'inline-block'); //This is to force an issue where jQuery derps and forgets to pull the display property from the existing CSS.
				$(self.container).attr('data-everywhere', 'true');
			} else {
				if ($(this.everywhereInput).is(':checked')) {
					$(this.everywhereInput).attr('checked', false);
				}
				this.everywhere = false;
				$(".wiki_search_container", self.container).removeClass('everywhere');
				$(".wiki_search_container", self.container).addClass('singles');
				$(".wiki_legend span", self.container).html(mw.message('wikis_added').escaped());
				$('.wiki_added_container', self.container).show().css('display', 'inline-block');
				$('.wiki_removed_container', self.container).hide();
				$(self.container).attr('data-everywhere', 'false');
			}
			this.everywhere = isEverywhere;
		},

		/**
		 * Get if currently selecting all wikis.
		 *
		 * @return boolean Currently operating on all wikis?
		 */
		getCurrentlyEverywhere: function() {
			return this.everywhere;
		},

		getWiki: function(data) {
			var parameters = {
				action: 'dswiki',
				do: 'getWiki',
				format: 'json',
				formatversion: 2
			};

			if (Array.isArray(data)) {
				parameters.site_keys = data.join(',');
			} else {
				parameters.search = data;
			}

			var api = new mw.Api();
			return api.post(parameters).done(function (result) {
				return result;
			}).fail(function (xhr, status) {
				alert(status.exception);
				return [];
			});
		},

		/**
		 * Return the current single wiki key.
		 *
		 * @return mixed String if the key is valid or false.
		 */
		getSingleWikiKey: function() {
			return (this.wikiSelections.single.length == 32 ? this.wikiSelections.single : false);
		},

		createWikiSelector: function(siteKey, wiki) {
			this.wikiCache[siteKey] = wiki;

			var title = wiki.wiki_display_name;
			var wikiSelector = $("<div>").addClass('wiki_selector').attr('data-site-key', siteKey).attr('title', title);
			if (this.selectType == 'single') {
				$(wikiSelector).addClass('single');
			}
			if (this.selectType == 'addremove' || this.selectType == 'add') {
				var wikiAdd = $('<span>').addClass('wiki_add wiki_selector_control').append('+');
			}
			if (this.selectType == 'addremove' || this.selectType == 'remove') {
				var wikiRemove = $('<span>').addClass('wiki_remove wiki_selector_control').append('-');
			}
			var wikiDelete = $('<span>').addClass('wiki_delete wiki_selector_control').append('x');

			var wikiElement = wikiSelector.append(wikiAdd).append(wikiRemove).append(wikiDelete).append(title);

			this.setupWikiSelectorControls(wikiElement);

			return wikiElement;
		},

		initialWikiSelectorSetup: function() {
			var self = this;

			var existingData = [];
			if (this.wikiSelections.added.length) {
				existingData = existingData.concat(this.wikiSelections.added);
			}
			if (this.wikiSelections.removed.length) {
				existingData = existingData.concat(this.wikiSelections.removed);
			}
			if (this.wikiSelections.single.length == 32) {
				existingData.push(this.wikiSelections.single);
			}

			if (existingData.length) {
				this.showProgressIndicator($('.wiki_added_container', this.container));
				this.showProgressIndicator($('.wiki_removed_container', this.container));

				var resultData = this.getWiki(existingData);
				resultData.done(function(data) {
					self.hideProgressIndicator($('.wiki_added_container', self.container));
					self.hideProgressIndicator($('.wiki_removed_container', self.container));
					if (data.success === true) {
						self.wikiCache = data.data;
						self.buildSelectorsFromWikiCache();
					}
				});
			}
		},

		buildSelectorsFromWikiCache: function() {
			var self = this;

			if (self.wikiSelections.single.length > 0) {
				var wikiElement = self.createWikiSelector(self.wikiSelections.single, self.wikiCache[self.wikiSelections.single]);
				$(wikiElement).hide().appendTo('.wiki_single_container', self.container).fadeIn(200);
			}
			$.each(self.wikiSelections.added, function(index, siteKey) {
				if (self.wikiCache[siteKey]) {
					var wikiElement = self.createWikiSelector(siteKey, self.wikiCache[siteKey]);
					$(wikiElement).hide().appendTo('.wiki_added_container', self.container).fadeIn(200);
				}
			});
			$.each(self.wikiSelections.removed, function(index, siteKey) {
				if (self.wikiCache[siteKey]) {
					var wikiElement = self.createWikiSelector(siteKey, self.wikiCache[siteKey]);
					$(wikiElement).hide().appendTo('.wiki_removed_container', self.container).fadeIn(200);
				}
			});

		},

		arrayUnique: function(a) {
			return a.reduce(function(p, c) {
				if (p.indexOf(c) < 0) p.push(c);
				return p;
			}, []);
		},

		wikiAdd: function(siteKey) {
			this.wikiSelections.added.push(siteKey);
			this.wikiSelections.added = this.arrayUnique(this.wikiSelections.added);

			var index = this.wikiSelections.removed.indexOf(siteKey);
			if (index > -1) {
				this.wikiSelections.removed.splice(index, 1);
			}

			this.updateInputs();
			this.onChange();
		},

		wikiRemove: function(siteKey) {
			this.wikiSelections.removed.push(siteKey);
			this.wikiSelections.removed = this.arrayUnique(this.wikiSelections.removed);

			var index = this.wikiSelections.added.indexOf(siteKey);
			if (index > -1) {
				this.wikiSelections.added.splice(index, 1);
			}

			this.updateInputs();
			this.onChange();
		},

		wikiDelete: function(siteKey) {
			var index = this.wikiSelections.added.indexOf(siteKey);
			if (index > -1) {
				this.wikiSelections.added.splice(index, 1);
			}

			index = this.wikiSelections.removed.indexOf(siteKey);
			if (index > -1) {
				this.wikiSelections.removed.splice(index, 1);
			}

			this.updateInputs();
			this.onChange();
		},

		setupWikiSelectorControls: function(element) {
			var self = this;

			if ($(element).hasClass('single')) {
				$(element).click(function() {
					var siteKey = $(this).attr('data-site-key');

					$('.wiki_single_container .wiki_selector', self.container).remove();

					self.moveWikiSelector(this, $('.wiki_single_container', self.container));
					self.wikiSelections.single = siteKey;

					self.updateInputs();
					self.onChange();
				});
			}

			if ($('.wiki_add', element).length) {
				$('.wiki_add', element).click(function() {
					var wiki = $(this).parent('.wiki_selector');
					var siteKey = $(wiki).attr('data-site-key');

					var existingSelector = $('.wiki_added_container .wiki_selector[data-site-key="'+siteKey+'"]', self.container);
					if ($(existingSelector).length) {
						self.wikiDelete(siteKey);
						$(existingSelector).remove();
					}

					self.moveWikiSelector(wiki, $('.wiki_added_container', self.container));
					self.wikiAdd(siteKey);
				});
			}

			if ($('.wiki_remove', element).length) {
				$('.wiki_remove', element).click(function() {
					var wiki = $(this).parent('.wiki_selector');
					var siteKey = $(wiki).attr('data-site-key');

					var existingSelector = $('.wiki_removed_container .wiki_selector[data-site-key="'+siteKey+'"]', self.container);
					if ($(existingSelector).length) {
						self.wikiDelete(siteKey);
						$(existingSelector).remove();
					}

					self.moveWikiSelector(wiki, $('.wiki_removed_container', self.container));
					self.wikiRemove(siteKey);
				});
			}

			if ($('.wiki_delete', element).length) {
				$('.wiki_delete', element).click(function() {
					var wiki = $(this).parent('.wiki_selector');
					var siteKey = $(wiki).attr('data-site-key');

					self.moveWikiSelector(wiki, 'trash');
					self.wikiDelete(siteKey);
				});
			}
		},

		moveWikiSelector: function(element, destination) {
			if (destination === 'trash') {
				$(element).fadeOut(200, function() {
					$(element).remove();
				});
			} else {
				$(element).fadeOut(200, function() {
					$(element).detach();
					$(element).appendTo(destination);
					$(element).fadeIn();
				});
			}
		},

		updateInputs: function() {
			$('.wiki_selections', this.container).val(JSON.stringify(this.wikiSelections));
		},

		/**
		 * Show the progress indicator over top the selected area.
		 *
		 * @param object jQuery DOM Element
		 * @return void
		 */
		showProgressIndicator: function(container) {
			if (!this.progressIndicator) {
				this.progressIndicator = $("<div>").addClass('sk-spinner').addClass('sk-spinner-fading-circle');
				for (var i = 1; i <= 12; i++) {
					$(this.progressIndicator).append($('<div>').addClass('sk-circle').addClass('sk-circle'+i));
				}
			}

			$(this.progressIndicator).clone().appendTo(container);
		},

		/**
		 * Hide the progress indicator over top the search area.
		 *
		 * @return void
		 */
		hideProgressIndicator: function(container) {
			$('.sk-spinner', container).remove();
		},

		/**
		 * Register a function to call when the selected wikis have changed.
		 *
		 * @return boolean Successfully Registered
		 */
		registerOnChange: function(callable) {
			if (callable instanceof Function) {
				this.onChangeFunction = callable;
				return true;
			} else {
				return false;
			}
		},

		/**
		 * Trigger the registered onChange function.
		 *
		 * @return void
		 */
		onChange: function() {
			if (this.onChangeFunction !== false) {
				this.onChangeFunction(this);
			}
		}
	};

	//Expose
	mw.WikiSelect = WikiSelect;

	if ($('#wiki_selection_container').length) {
		if ($('#wiki_selection_container').attr('data-setup') != 'manual') {
			new mw.WikiSelect('#wiki_selection_container');
		}
	}

	setTimeout(function() {
		if ($('#wiki_selection_container').length) {
			if ($('#wiki_selection_container').data('loaded') != '1') {
				new mw.WikiSelect('#wiki_selection_container');
				console.log('Recovering from failed WikiSelect Load.');
			}
		}
	}, 2000);
}(mediaWiki, jQuery));
