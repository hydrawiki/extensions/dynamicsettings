<?php
/**
 * Curse Inc.
 * Dynamic Settings
 *
 * @author    Alexia E. Smith
 * @copyright (c) 2017 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   DynamicSettings
 * @link      https://gitlab.com/hydrawiki
 **/

namespace DynamicSettings\Reverb;

use DynamicSettings\DSDBFactory;
use MediaWiki\MediaWikiServices;

class NotificationUserLocator {
	/**
	 * Locate users to notify for an event.
	 *
	 * @return array Array of User IDs => User objects.
	 */
	public static function getHydraAdmins() {
		$mainConfig = MediaWikiServices::getInstance()->getMainConfig();
		$wgDSAdminEmailGroup = $mainConfig->get('DSAdminEmailGroup');

		$db = DSDBFactory::getMasterDB(DB_MASTER);

		$result = $db->select(
			['user_groups', 'user'],
			['*'],
			[
				"user_groups.ug_group"	=> $wgDSAdminEmailGroup
			],
			__METHOD__,
			['GROUP BY' => 'user.user_id'],
			[
				'user' => [
					'INNER JOIN', 'user.user_id = user_groups.ug_user'
				]
			]
		);

		$users = [];
		while ($row = $result->fetchObject()) {
			$user = \User::newFromRow($row);
			if (!empty($user) && $user->getId() > 0) {
				$users[$user->getId()] = $user;
			}
		}
		return $users;
	}
}
