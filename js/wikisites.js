function WikiSites($) {
	'use strict';
	this.init = function() {
		$('#wiki_category_select').on('change', wiki.changeCategory);
		$('.reindex-search').on('click', wiki.queueReindex);
		if (window.location.href.indexOf('action=add') > -1) {
			$(document).on('click', '#wiki_fill_cancel_button', wiki.closeWikiFillForm);
			$(document).on('click', '#wiki_fill_button', wiki.openWikiFillForm);
			$(document).on('click', '#wiki_fill_go_button', wiki.fillWikiForm)
			$(document).on('change', '#db_node', wiki.changeDbNodeFill);
		}

		if (typeof availableTags == undefined) {
			var availableTags = [];
		}

		$('#wiki_tags').tagsInput({
			autocomplete_url: '',
			autocomplete: {
				selectFirst: true,
				width: '100px',
				autoFill: true,
				source: availableTags
			}
		});

		$('#wiki_fill #tld_fill').after($('.wiki_language').clone()).after($('label[for="wiki_language"]').clone());

		$('#wikilist .popup_list').hover(function() {
			var listBottom = $('ul', this).offset().top + $('ul', this).outerHeight();
			var wrapperBottom = $('#pageWrapper').offset().top + $('#pageWrapper').outerHeight();
			if (listBottom > wrapperBottom) {
				$('ul', this).css('margin-top', '-'+(listBottom - wrapperBottom)+'px');
			}
		});

		$('button.confirmWithTitle').click(function(e){
			var title = $(this).attr('title');
			if (confirm(mw.message('confirm_action_are_you_sure', title).plain())) {
				return true;
			} else {
				return false;
			}
		});

		$('input#action_all').click(function(event) {
			if ($(this).prop('checked')) {
				$('input[name="mass_action[]"]').each(function(index) {
					$(this).prop('checked', true);
				});
			} else {
				$('input[name="mass_action[]"]').each(function(index) {
					$(this).prop('checked', false);
				});
			}
		});

		showHideMassEdit($(this));

		$('input#action_all, input[name="mass_action[]"]').change(function(event) {
			showHideMassEdit();
		});

		$('form#mass_action #mass_button_holder > button').click(function(event) {
			var sites = [];
			$('input[name="mass_action[]"]').each(function(index) {
				if ($(this).is(':checked')) {
					sites.push($(this).val());
				}
			});
			$('form#mass_action').append($("<input>").attr('name', 'sites').attr('hidden', true).val(sites.join(',')));
		});
	};

	function showHideMassEdit(element) {
		var checked = false;
		$('input[name="mass_action[]"]').each(function(index, element) {
			if ($(element).prop('checked')) {
				//Keep going until one is found checked then exit.
				$('form#mass_action #mass_button_holder').show();
				checked = true;
			}
		});
		if (checked) {
			return true;
		}

		$('form#mass_action #mass_button_holder').hide();
		return false;
	}

	var wiki = {
		templateParse: function(id, node) {
			var str = mw.config.get('dbNodesConfig').templates[id];
			var node = "" + node;

			str = str.split('{node%1}').join(node);

			var pad = "000";
			var node = pad.substring(0, pad.length - node.length) + node;

			var first = node.slice(0,1);
			if (first == "0") {
				var node2 = node.slice(-2);
			} else {
				var node2 = node;
			}

			str = str.split('{node%2}').join(node2);
			str = str.split('{node%3}').join(node);

			return str;
		},

		changeCategory: function() {
			var newVal = $('#wiki_category_select').val();
			if (newVal !== '' && newVal !== '0') {
				$('#wiki_category').val(newVal);
				$('#wiki_category_select').val('0');
			}
			$('#wiki_category').change();
		},

		closeWikiFillForm: function() {
			$('#wiki_fill').fadeToggle();
		},

		openWikiFillForm: function() {
			$("#wiki_fill select.wiki_language option").prop("selected", false);
			$("#wiki_fill select.wiki_language option[value='"+$('#wiki_settings_form select.wiki_language option:selected').val()+"']").prop("selected", true);

			$('#wiki_fill').fadeToggle();
			$('#name_fill').focus();
			wiki.changeDbNodeFill();
		},

		changeDbNodeFill: function() {
			var dbNodeDetails = wiki.getSelectedDbNodeDetails();
			if (dbNodeDetails !== null && dbNodeDetails.free > 0) {
				$("#wiki_fill_go_button").attr('disabled', false);
			} else {
				$("#wiki_fill_go_button").attr('disabled', true);
			}
		},

		getSelectedDbNodeDetails: function() {
			var dbNodeDetailsRaw = $("#db_node").find(':selected').attr('data-node');
			try {
				return JSON.parse(dbNodeDetailsRaw);
			} catch (e) {
				return null;
			}
		},

		fillWikiForm: function() {
			var name = $('#name_fill').val();
			var domain = $('#domain_fill').val();
			var tld = $('#tld_fill option:selected').val();
			var language = $('#wiki_fill select.wiki_language option:selected').val();
			var dbNode = $("#db_node").val();
			var dbNodeDetails = wiki.getSelectedDbNodeDetails();
			var developmentTLD = mw.config.get('HydraTLDDevelopment');
			var liveTLD = mw.config.get('HydraTLDLive');
			var stagingTLD = mw.config.get('HydraTLDStaging');

			var rawName = name;
			if ($('#append_wiki').prop('checked')) {
				name = mw.message('append_wiki', name).plain();
			}

			/********************************/
			/* WIKI                         */
			/********************************/
			$('#wiki_name').val(name);
			$('#commit_message').val(name);
			$('#wiki_meta_name').val(name.replace(/ /g, '_').replace(/[^\w\s]/gi, ''));
			$('#wiki_domain').val(domain+tld);
			$('#wiki_domain_local').val(domain+'.'+developmentTLD);
			$('#wiki_domain_staging').val(domain+'.'+stagingTLD);
			var portal = domain.substr(domain.indexOf('.')+1);
			$("select#portal option").each(function() {
				if ($(this).text() === portal+tld || $(this).text() === portal+'.'+developmentTLD || $(this).text() === portal+'.'+stagingTLD) {
					$("select#portal option").prop("selected", false);
					$(this).prop("selected", true);
				}
			});
			$("#wiki_settings_form select.wiki_language option").prop("selected", false);
			$("#wiki_settings_form select.wiki_language option[value='"+language+"']").prop("selected", true);

			$('#wiki_tags').addTag('game:'+rawName.toLowerCase().replace(/[,]/g, ''));

			/********************************/
			/* DATABASE                     */
			/********************************/
			if (typeof dbNode != 'undefined' && dbNode !== null && dbNodeDetails !== null && dbNodeDetails.free > 0) {
				$("#db_cluster").val(dbNode);
			}

			var period = domain.indexOf('.');
			var db_name;
			if (period > -1) {
				db_name = domain.replace(/\./g, '_').replace(/-/g, '_');
			} else {
				db_name = domain.replace(/-/g, '_');
			}
			language = language.replace('-', '');
			var languageSuffix = '_'+language;

			db_name = db_name.substr(0, 64);
			if (db_name.indexOf(languageSuffix+'_') == -1) {
				if (db_name.length + languageSuffix.length > 64) {
					db_name = db_name.substr(0, db_name.length - ((db_name.length + languageSuffix.length) - 64));
				}
				db_name = db_name+languageSuffix;
			}

			$('#db_name').val(db_name);

			/********************************/
			/* SEARCH                       */
			/********************************/
			if (typeof dbNode != 'undefined' && dbNode !== null && dbNodeDetails !== null && dbNodeDetails.free > 0) {
				$('#search_server').val(mw.config.get('dbNodesConfig').searchHost);
				$('#search_port').val(mw.config.get('dbNodesConfig').searchPort);
			}

			$('#wiki_fill').fadeToggle();
		}
	}
}

var WS = new WikiSites(jQuery);
jQuery(document).ready(WS.init);
