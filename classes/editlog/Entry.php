<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Edit Log Entry Class
 *
 * @author    Alexia E. Smith
 * @copyright (c) 2018 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
 **/

namespace DynamicSettings\EditLog;

use DynamicSettings\DSDBFactory;
use DynamicSettings\Diff;
use DynamicSettings\Wiki\Promotion;
use Linker;
use RequestContext;
use SpecialPage;

class Entry {
	/**
	 * Edit Log Entry Type Constants
	 */
	const LOGTYPE_ALLOWEDEXTENSION = 0;
	const LOGTYPE_ALLOWEDSETTING = 1;
	const LOGTYPE_WIKI = 2;
	const LOGTYPE_PERMISSIONS = 3;
	const LOGTYPE_EXTENSIONS = 4;
	const LOGTYPE_NAMESPACES = 5;
	const LOGTYPE_SETTINGS = 6;
	const LOGTYPE_DOMAINS = 7;
	const LOGTYPE_ADVERTISEMENTS = 8;
	const LOGTYPE_PROMOTIONS = 9;

	/**
	 * Edit Log Type mapping variable
	 *
	 * @var array
	 */
	public static $typeToStringMap = [
		0 => 'allowed_extension',
		1 => 'allowed_settings',
		2 => 'wiki',
		3 => 'permissions',
		4 => 'extensions',
		5 => 'namespaces',
		6 => 'settings',
		7 => 'domains',
		8 => 'advertisements',
		9 => 'promotions'
	];

	/**
	 * Edit Log strings to Type mapping variable
	 *
	 * @var array
	 */
	public static $stringToTypeMap = [
		'allowed_extension'	=> 0,
		'allowed_settings'	=> 1,
		'wiki'				=> 2,
		'permissions'		=> 3,
		'extensions'		=> 4,
		'namespaces'		=> 5,
		'settings'			=> 6,
		'domains'			=> 7,
		'advertisements'	=> 8,
		'promotions'		=> 9
	];

	/**
	 * Edit log entry database data.
	 *
	 * @var array
	 */
	protected $data = [
		'lid'				=> 0,
		'user_id'			=> 0,
		'log_type'			=> 0,
		'log_key'			=> null,
		'class_method'		=> null,
		'commit_message'	=> null,
		'serialized_data'	=> null,
		'timestamp'			=> 0,
		'cache_timestamp'	=> 0,
		'ip_address'		=> null,
		'legacy'			=> 0,
		'deleted'			=> 0
	];

	/**
	 * Main Constructor
	 *
	 * @param public Database row from the allowed settings table.
	 */
	public function __construct($data = []) {
		$this->data = array_merge($this->data, $data);
	}

	/**
	 * Get an allowed setting by ID.
	 *
	 * @param integer Database ID of the entry.
	 *
	 * @return object Entry or null if not found
	 */
	public static function newFromId($logId) {
		$db = DSDBFactory::getMasterDB(DB_MASTER);
		$result = $db->select(
			['wiki_edit_log'],
			['*'],
			['lid' => $logId],
			__METHOD__
		);
		$row = $result->fetchRow();
		if (!empty($row)) {
			$entry = self::newFromRow($row);
			return $entry;
		}
	}

	/**
	 * Get an instance this class.
	 *
	 * @param array Database Row
	 *
	 * @return object Entry
	 */
	public static function newFromRow($row) {
		return new self($row);
	}

	/**
	 * Save all information to the database.
	 *
	 * @return boolean Success
	 */
	public function save() {
		$wgUser = RequestContext::getMain()->getUser();

		$success = false;
		$db = DSDBFactory::getMasterDB(DB_MASTER);

		$save = [
			'user_id'			=> $this->getUserId(),
			'log_type'			=> $this->getLogType(),
			'log_key'			=> $this->getLogKey(),
			'class_method'		=> $this->getClassMethod(),
			'commit_message'	=> $this->getCommitMessage(),
			'serialized_data'	=> $this->getSerializedData(),
			'timestamp'			=> $this->getTimestamp(),
			'cache_timestamp'	=> $this->getCacheTimestamp(),
			'ip_address'		=> $this->getIpAddress(),
			'legacy'			=> intval($this->isLegacy()),
			'deleted'			=> intval($this->isDeleted())
		];

		$db->startAtomic(__METHOD__);
		if ($this->getId()) {
			$result = $db->update(
				'wiki_edit_log',
				$save,
				['lid' => $this->getId()],
				__METHOD__
			);

			$databaseId = $this->getId();
		} else {
			$result = $db->insert(
				'wiki_edit_log',
				$save,
				__METHOD__
			);

			$databaseId = $db->insertId();
		}

		if (!$result) {
			$db->cancelAtomic(__METHOD__);
		} else {
			$this->data['lid'] = $databaseId;
			$success = true;
			$db->endAtomic(__METHOD__);
		}

		return $success;
	}

	/**
	 * Delete this entry.
	 *
	 * @param boolean [Optional] Hard Delete - Whether to just mark the deleted column as 1(soft delete) or outright delete the database entry(hard delete).
	 *
	 * @return boolean Success
	 */
	public function delete($hardDelete = false) {
		$success = false;

		if (!$this->getId()) {
			return $success;
		}

		if ($hardDelete) {
			$db = DSDBFactory::getMasterDB(DB_MASTER);
			$db->startAtomic(__METHOD__);
			$result = $db->delete(
				'wiki_edit_log',
				['lid' => $this->getId()],
				__METHOD__
			);

			if (!$result) {
				$db->cancelAtomic(__METHOD__);
			} else {
				$success = true;
				$db->endAtomic(__METHOD__);
			}
		} else {
			$this->setDeleted(true);
			$success = $this->save();
		}

		return $success;
	}

	/**
	 * Get the database ID.
	 *
	 * @return integer Database ID
	 */
	public function getId() {
		return intval($this->data['lid']);
	}

	/**
	 * Get the user ID of who made this change.
	 *
	 * @return integer User Id
	 */
	public function getUserId() {
		return intval($this->data['user_id']);
	}

	/**
	 * Set the user ID of who made this change.
	 *
	 * @return int User ID
	 *
	 * @return void
	 */
	public function setUserId($userId) {
		$this->data['user_id'] = intval($userId);
	}

	/**
	 * Get the user object of who made this change.
	 *
	 * @return mixed User object or null.
	 */
	public function getUser() {
		return $this->getUserId() > 0 ? \User::newFromId($this->getUserId()) : null;
	}

	/**
	 * Get the log type.
	 *
	 * @return integer Log Type
	 */
	public function getLogType() {
		return intval($this->data['log_type']);
	}

	/**
	 * Set the log type.
	 *
	 * @return int Log Type
	 *
	 * @return void
	 */
	public function setLogType($logType) {
		if ($logType === null) {
			throw new \MWException(__METHOD__ . ': Null log type passed.  There be a missing method to log type mapping in the mapMethodToLogType function.');
		}
		$this->data['log_type'] = intval($logType);
	}

	/**
	 * Get the log key.
	 *
	 * @return string Log Key
	 */
	public function getLogKey() {
		return $this->data['log_key'];
	}

	/**
	 * Set the log key.
	 *
	 * @return string Log Key
	 *
	 * @return void
	 */
	public function setLogKey($logKey) {
		$this->data['log_key'] = trim($logKey);
	}

	/**
	 * Get the class method.
	 *
	 * @return string Class Method
	 */
	public function getClassMethod() {
		return $this->data['class_method'];
	}

	/**
	 * Set the class method.
	 *
	 * @return string Class Method
	 *
	 * @return void
	 */
	public function setClassMethod($classMethod) {
		list($class, $function) = explode('::', $classMethod);
		if (!method_exists($class, $function)) {
			throw new \MWException('Uncallable method ' . $classMethod . ' passed to ' . __METHOD__ . '.');
		}

		$this->setLogType(self::mapMethodToLogType($classMethod)); // Force update the log type when changing the method.

		$this->data['class_method'] = trim($classMethod);
	}

	/**
	 * Get the commit message.
	 *
	 * @return string Commit Message
	 */
	public function getCommitMessage() {
		return $this->data['commit_message'];
	}

	/**
	 * Set the commit message.
	 *
	 * @return string Commit Message
	 *
	 * @return void
	 */
	public function setCommitMessage($commitMessage) {
		$this->data['commit_message'] = trim($commitMessage);
	}

	/**
	 * Get the data.
	 * Data will be unserialized automatically.
	 *
	 * @return array Data
	 */
	public function getData() {
		return @unserialize($this->data['serialized_data']);
	}

	/**
	 * Get the data in serialized format.
	 *
	 * @return string Serialized Data
	 */
	public function getSerializedData() {
		return $this->data['serialized_data'];
	}

	/**
	 * Set the serialized data.
	 * Data will be serialized automatically.
	 *
	 * @return array Data
	 *
	 * @return void
	 */
	public function setData($data = []) {
		if (!is_array($data)) {
			throw new \MWException('No data passed to ' . __METHOD__ . '.');
		}
		$this->data['serialized_data'] = serialize($data);
	}

	/**
	 * Get the Unix timestamp of when this entry was created.
	 *
	 * @param string Data Format: See date()
	 *
	 * @return integer Timestamp
	 */
	public function getTimestamp($format = 'U') {
		return gmdate($format, $this->data['timestamp']);
	}

	/**
	 * Set the Unix timestamp for when this entry was created.
	 *
	 * @return int Timestamp
	 *
	 * @return void
	 */
	public function setTimestamp($timestamp) {
		$this->data['timestamp'] = intval($timestamp);
	}

	/**
	 * Get the cache timestamp for when the recache functionality claims to have cached this entry/change out to disk.
	 *
	 * @return integer Cache Timestamp
	 */
	public function getCacheTimestamp() {
		return $this->data['cache_timestamp'];
	}

	/**
	 * Set the cache timestamp for when the recache functionality claims to have cached this entry/change out to disk.
	 *
	 * @return int Cache Timestamp
	 *
	 * @return void
	 */
	public function setCacheTimestamp($cacheTimestamp) {
		$this->data['cache_timestamp'] = intval($cacheTimestamp);
	}

	/**
	 * Get the IP address.
	 *
	 * @return string IP Address
	 */
	public function getIPAddress() {
		return $this->data['ip_address'];
	}

	/**
	 * Set the ip address.
	 *
	 * @return string IP Address
	 *
	 * @return void
	 */
	public function setIPAddress($ipAddress) {
		$this->data['ip_address'] = trim($ipAddress);
	}

	/**
	 * Is this a legacy entry that may not diff properly?
	 *
	 * @return boolean Legacy
	 */
	public function isLegacy() {
		return boolval($this->data['legacy']);
	}

	/**
	 * Set the legacy.
	 *
	 * @return boolean Legacy
	 *
	 * @return void
	 */
	public function setLegacy($legacy = true) {
		$this->data['legacy'] = intval($legacy);
	}

	/**
	 * Is this entry soft deleted?
	 *
	 * @return boolean Deleted
	 */
	public function isDeleted() {
		return boolval($this->data['deleted']);
	}

	/**
	 * Mark as soft deleted.
	 *
	 * @return boolean Deleted
	 *
	 * @return void
	 */
	public function setDeleted($deleted = true) {
		$this->data['deleted'] = intval($deleted);
	}

	/**
	 * Can this entry be deleted?
	 *
	 * @return boolean Deletable
	 */
	public function isDeletable() {
		return !$this->isLegacy() && !$this->isDeleted();
	}

	/**
	 * Can this entry be recached out to disk?
	 *
	 * @return boolean Recacheable
	 */
	public function canRecache() {
		return !$this->getCacheTimestamp() && $this->getLogKey() !== self::LOGTYPE_ALLOWEDEXTENSION &&
			$this->getLogKey() !== self::LOGTYPE_ALLOWEDSETTING && !$this->isLegacy() &&
			$this->getLogType() >= self::LOGTYPE_WIKI && $this->getLogType() <= self::LOGTYPE_DOMAINS;
	}

	/**
	 * Get the icon style for this entry.
	 *
	 * @return string Icon Style
	 */
	public function getIcon() {
		$icon = 'question-circle';
		switch ($this->getLogType()) {
			case self::LOGTYPE_ALLOWEDEXTENSION:
			case self::LOGTYPE_EXTENSIONS:
				$icon = 'plug';
				break;
			case self::LOGTYPE_ALLOWEDSETTING:
			case self::LOGTYPE_SETTINGS:
				$icon = 'cogs';
				break;
			case self::LOGTYPE_WIKI:
			case self::LOGTYPE_DOMAINS:
				$icon = 'book';
				break;
			case self::LOGTYPE_NAMESPACES:
				$icon = 'list';
				break;
			case self::LOGTYPE_PERMISSIONS:
				$icon = 'key';
				break;
			case self::LOGTYPE_ADVERTISEMENTS:
				$icon = 'dollar-sign';
				break;
			case self::LOGTYPE_PROMOTIONS:
				$icon = 'ad';
				break;
		}
		return \HydraCore::awesomeIcon($icon);
	}

	/**
	 * Get the display text for links or otherwise.
	 *
	 * @return string Text
	 */
	public function getDisplayText() {
		$displayText = wfMessage('no_information_provided')->escaped();
		switch ($this->getLogType()) {
			case self::LOGTYPE_ALLOWEDEXTENSION:
				$allowedExtensions = \DynamicSettings\AllowedExtensions::getInstance()->getAll();
				if (isset($allowedExtensions[$this->getLogKey()]['extension_name'])) {
					$displayText = $allowedExtensions[$this->getLogKey()]['extension_name'];
				} else {
					$displayText = wfMessage('entry_extension_not_found', $this->getLogKey())->escaped();
				}
				break;
			case self::LOGTYPE_ALLOWEDSETTING:
				$allowedSettings = \DynamicSettings\AllowedSettings::getInstance()->getAll();
				if (isset($allowedSettings[$this->getLogKey()]['setting_key'])) {
					$displayText = $allowedSettings[$this->getLogKey()]['setting_key'];
				} else {
					$displayText = wfMessage('entry_setting_not_found', $this->getLogKey())->escaped();
				}
				break;
			case self::LOGTYPE_WIKI:
			case self::LOGTYPE_DOMAINS:
			case self::LOGTYPE_EXTENSIONS:
			case self::LOGTYPE_SETTINGS:
			case self::LOGTYPE_NAMESPACES:
			case self::LOGTYPE_PERMISSIONS:
			case self::LOGTYPE_ADVERTISEMENTS:
				$wiki = \DynamicSettings\Wiki::loadFromHash($this->getLogKey());
				if ($wiki !== false) {
					$displayText = $wiki->getNameForDisplay();
				} else {
					$displayText = wfMessage('entry_wiki_not_found', $this->getLogKey())->escaped();
				}
				break;
			case self::LOGTYPE_PROMOTIONS:
				$promotion = Promotion::loadFromId($this->getLogKey());
				if ($promotion !== false) {
					$displayText = $promotion->getName();
				} else {
					$displayText = wfMessage('entry_promotion_not_found', $this->getLogKey())->escaped();
				}
				break;
		}
		return $displayText;
	}

	/**
	 * Get the parameters to generate a link to the specific log_type(more specific) for this entry.
	 *
	 * @return array
	 */
	public function getTypeLinkParameters(): array {
		$typeText = $this->getIcon() . wfMessage(self::$typeToStringMap[$this->getLogType()])->escaped();
		$arguments = [];

		switch ($this->getLogType()) {
			case self::LOGTYPE_ALLOWEDEXTENSION:
				$typeTitle = SpecialPage::getTitleFor("WikiAllowedExtensions");
				$arguments = [
					'section'	=> 'extension',
					'action'	=> 'edit',
					'md5_key'	=> $this->getLogKey()
				];
				break;
			case self::LOGTYPE_ALLOWEDSETTING:
				$typeTitle = SpecialPage::getTitleFor("WikiAllowedSettings");
				$arguments = [
					'section'	=> 'setting',
					'action'	=> 'edit',
					'asid'		=> $this->getLogKey()
				];
				break;
			case self::LOGTYPE_WIKI:
			case self::LOGTYPE_DOMAINS:
				$typeTitle = SpecialPage::getTitleFor("WikiSites");
				$arguments = [
					'section'	=> 'wiki',
					'action'	=> 'edit',
					'siteKey'	=> $this->getLogKey()
				];
				break;
			case self::LOGTYPE_EXTENSIONS:
				$typeTitle = SpecialPage::getTitleFor("WikiExtensions");
				$arguments = [
					'siteKey' => $this->getLogKey()
				];
				break;
			case self::LOGTYPE_SETTINGS:
				$typeTitle = SpecialPage::getTitleFor("WikiSettings");
				$arguments = [
					'siteKey' => $this->getLogKey()
				];
				break;
			case self::LOGTYPE_NAMESPACES:
				$typeTitle = SpecialPage::getTitleFor("WikiNamespaces");
				$arguments = [
					'siteKey' => $this->getLogKey()
				];
				break;
			case self::LOGTYPE_PERMISSIONS:
				$typeTitle = SpecialPage::getTitleFor("WikiPermissions");
				$arguments = [
					'siteKey' => $this->getLogKey()
				];
				break;
			case self::LOGTYPE_ADVERTISEMENTS:
				$typeTitle = SpecialPage::getTitleFor("WikiAdvertisements");
				$arguments = [
					'section'	=> 'wiki',
					'action'	=> 'edit',
					'siteKey'	=> $this->getLogKey()
				];
				break;
			case self::LOGTYPE_PROMOTIONS:
				$promotion = Promotion::loadFromId($this->getLogKey());
				if ($promotion === false) {
					$typeTitle = SpecialPage::getTitleFor("WikiPromotions");
				} else {
					$typeTitle = SpecialPage::getTitleFor($promotion->getType() === 'notice' ? "WikiNotices" : "WikiPromotions");
					$arguments = [
						'section'		=> 'form',
						'do'			=> 'edit',
						'promotion_id'	=> $this->getLogKey()
					];
				}
				break;
		}

		return [
			'title' => $typeTitle,
			'text' => $typeText,
			'url_arguments' => $arguments
		];
	}

	/**
	 * Get the link to the specific log_type(more specific) for this entry.
	 * Example: Linking to the specific Special:WikiSettings page for a log_key from wiki_sites/site_key.
	 *
	 * @return string HTML
	 */
	public function getTypeLink() {
		$parameters = $this->getTypeLinkParameters();

		$typeLink = Linker::link(
			$parameters['title'],
			$parameters['text'],
			[],
			$parameters['url_arguments']
		);

		return $typeLink;
	}

	/**
	 * Get the link parameters to the specific log_key(the most generic) for this entry.
	 *
	 * @return array
	 */
	public function getKeyLinkParameters(): array {
		$search = null;
		$keyTitle = null;

		switch ($this->getLogType()) {
			case self::LOGTYPE_ALLOWEDEXTENSION:
				$search = $this->getDisplayText();
				$keyTitle = SpecialPage::getTitleFor("WikiAllowedExtensions");
				break;
			case self::LOGTYPE_ALLOWEDSETTING:
				$search = $this->getDisplayText();
				$keyTitle = SpecialPage::getTitleFor("WikiAllowedSettings");
				break;
			case self::LOGTYPE_WIKI:
			case self::LOGTYPE_DOMAINS:
			case self::LOGTYPE_EXTENSIONS:
			case self::LOGTYPE_SETTINGS:
			case self::LOGTYPE_NAMESPACES:
			case self::LOGTYPE_PERMISSIONS:
				$search = $this->getLogKey();
				$keyTitle = SpecialPage::getTitleFor("WikiSites");
				break;
			case self::LOGTYPE_ADVERTISEMENTS:
				$search = $this->getLogKey();
				$keyTitle = SpecialPage::getTitleFor("WikiAdvertisements");
				break;
			case self::LOGTYPE_PROMOTIONS:
				$promotion = Promotion::loadFromId($this->getLogKey());
				if ($promotion) {
					$keyTitle = SpecialPage::getTitleFor($promotion->getType() === 'notice' ? "WikiNotices" : "WikiPromotions");
				} else {
					$keyTitle = SpecialPage::getTitleFor("WikiPromotions");
				}
		}

		$arguments = [];
		if ($search !== null) {
			$arguments = [
				'section'		=> 'list',
				'action'		=> 'search',
				'list_search'	=> $search
			];
		}

		return [
			'title' => $keyTitle,
			'text' => $this->getDisplayText(),
			'url_arguments' => $arguments
		];
	}

	/**
	 * Get the link to the specific log_key(the most generic) for this entry.
	 * Example: Linking to the specific item listing page for a log_key from Special:WikiEditLog.
	 *
	 * @return string HTML
	 */
	public function getKeyLink() {
		$parameters = $this->getKeyLinkParameters();

		if (empty($parameters['title']) && empty($parameters['url_arguments'])) {
			return $parameters['text'];
		}

		$keyLink = Linker::link(
			$parameters['title'],
			$parameters['text'],
			[],
			$parameters['url_arguments']
		);

		return $keyLink;
	}

	/**
	 * Is a database update required?
	 *
	 * @return boolean Database Update Required
	 */
	public function isDatabaseUpdateRequired() {
		$updateRequired = false;

		$allowedExtensions = \DynamicSettings\AllowedExtensions::getInstance()->getAll();
		switch ($this->getLogType()) {
			case self::LOGTYPE_ALLOWEDEXTENSION:
				if (isset($allowedExtensions[$this->getLogKey()]) && $allowedExtensions[$this->getLogKey()]['update_required'] && $allowedExtensions[$this->getLogKey()]['default_extension']) {
					$updateRequired = true;
				}
				break;
			case self::LOGTYPE_EXTENSIONS;
				$_extensionsChanged = $this->getData();
				if (is_array($_extensionsChanged)) {
					foreach ($_extensionsChanged as $key => $enabled) {
						if ($enabled) {
							if ($allowedExtensions[$key]['update_required']) {
								$updateRequired = true;
							}
						}
					}
				}
				break;
		}
		return $updateRequired;
	}

	/**
	 * Retrieve the previous entry related to this log if one exists.
	 *
	 * @return mixed Entry object or null no previous log.
	 */
	public function getPreviousEntry() {
		$entries = [];

		$db = DSDBFactory::getMasterDB(DB_MASTER);

		$where = [
			'log_key' => $this->getLogKey(),
			'log_type' => $this->getLogType()
		];

		$data = $this->getData();
		switch ($this->getLogType()) {
			case self::LOGTYPE_DOMAINS:
				if (isset($data['type'])) {
					$where[] = "serialized_data LIKE '%" . $db->strencode(serialize('type') . serialize($data['type'])) . "%'";
				}
				break;
			case self::LOGTYPE_NAMESPACES:
				if (isset($data['namespace_name'])) {
					$where[] = "serialized_data LIKE '%" . $db->strencode(serialize('namespace_name') . serialize($data['namespace_name'])) . "%'";
				}
				break;
			case self::LOGTYPE_PERMISSIONS:
				if (isset($data['group_name'])) {
					$where[] = "serialized_data LIKE BINARY '%" . $db->strencode(serialize('group_name') . serialize($data['group_name'])) . "%'";
				}
				break;
		}

		$result = $db->select(
			'wiki_edit_log',
			['*'],
			$where,
			__METHOD__,
			[
				'LIMIT'		=> 2,
				'ORDER BY'	=> 'lid DESC'
			]
		);

		$entry = null;
		while ($row = $db->fetchRow($result)) {
			if (intval($row['lid']) === $this->getId()) {
				continue;
			}

			$entry = self::newFromRow($row);
		}

		return $entry;
	}

	/**
	 * Get edit log entries to diff
	 *
	 * @param array $entries
	 *
	 * @return mixed array|string
	 */
	public function getDiffAgainstPrevious() {
		$diff = false;

		$newText = explode("\n", print_r($this->getData(), true));

		$oldText = [];
		$oldEntry = $this->getPreviousEntry();
		if ($oldEntry !== null) {
			$oldText = explode("\n", print_r($oldEntry->getData(), true));
		}

		$diff = new Diff\Diff($oldText, $newText, []);
		$renderer = new Diff\DiffRendererHtmlSideBySide;
		$output = $diff->Render($renderer);

		if (empty($output)) {
			if ($oldEntry !== null) {
				$output = wfMessage('same_diff')->text();
			} else {
				$output = wfMessage('no_diff')->text();
			}
		}

		return $output;
	}

	/**
	 * Map DS methods to specific log types.
	 *
	 * @param string Class Method
	 *
	 * @return mixed Integer Constant or null for none found.
	 */
	public static function mapMethodToLogType($method) {
		switch ($method) {
			case 'SpecialWikiAllowedExtensions::allowedExtensionsForm':
			case 'SpecialWikiAllowedExtensions::allowedExtensionsSave':
			case 'SpecialWikiAllowedExtensions::importAllowedExtensions':
			case strpos($method, 'DynamicSettings\Extension') !== false:
				return self::LOGTYPE_ALLOWEDEXTENSION;
			case 'SpecialWikiAllowedSettings::allowedSettingsForm':
			case 'SpecialWikiAllowedSettings::importAllowedSettings':
			case 'SpecialWikiAllowedSettings::allowedSettingsSave':
			case strpos($method, 'DynamicSettings\Setting') !== false:
				return self::LOGTYPE_ALLOWEDSETTING;
			case 'SpecialWikiSites::wikiForm':
			case 'SpecialWikiSites::wikiSave':
			case 'SpecialWikiList::wikiSave':
			case 'SpecialWikiList::wikiForm':
			case 'DynamicSettings\Wiki::save':
			case 'DynamicSettings\Wiki::delete':
			case 'DynamicSettings\Wiki::undelete':
				return self::LOGTYPE_WIKI;
			case 'SpecialWikiPermissions::permissionsSave':
			case 'SpecialWikiGroupPermissions::groupPermissionsForm':
			case 'SpecialWikiGroupPermissions::groupPermissionsSave':
			case 'DynamicSettings\Wiki\Permission::save':
			case 'DynamicSettings\Wiki\Permission::delete':
				return self::LOGTYPE_PERMISSIONS;
			case 'SpecialWikiExtensions::extensionsSave':
			case 'DynamicSettings\Wiki\Extensions::save':
			case 'DynamicSettings\Wiki::setEnabledExtensions':
				return self::LOGTYPE_EXTENSIONS;
			case 'SpecialWikiNamespaces::namespacesSave':
			case 'SpecialWikiNamespaces::namespaceDelete':
			case 'SpecialWikiNamespaces::namespaceSave':
			case 'DynamicSettings\Wiki\DSNamespace::save':
			case 'DynamicSettings\Wiki\DSNamespace::delete':
				return self::LOGTYPE_NAMESPACES;
			case 'SpecialWikiSettings::settingsSave':
			case 'DynamicSettings\Wiki\Settings::save':
			case 'DynamicSettings\Wiki::saveSettings':
				return self::LOGTYPE_SETTINGS;
			case 'DynamicSettings\Wiki\Domains::logDomainChange':
				return self::LOGTYPE_DOMAINS;
			case 'DynamicSettings\Wiki\Advertisements::save':
				return self::LOGTYPE_ADVERTISEMENTS;
			case 'DynamicSettings\Wiki\Promotion::save':
				return self::LOGTYPE_PROMOTIONS;
			default:
				return null;
		}
	}
}
