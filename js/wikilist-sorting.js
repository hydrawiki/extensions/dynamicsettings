$(document).ready(function(){
	$('#wikilist .sortable th span[data-sort]').click(function() {
		if ($(this).hasClass('unsortable')) {
			return;
		}
		var sort = $(this).attr('data-sort');
		var selected = $(this).attr('data-selected');
		var dir = $('#wikilist .sortable').attr('data-sort-dir');

		if (selected == 'true' && dir == 'asc') {
			dir = 'desc';
		} else if (selected == 'true' && dir == 'desc') {
			dir = 'asc';
		} else {
			dir = 'asc';
		}
		const params = new URLSearchParams(location.search);
		params.set('sort', sort);
		params.set('sort_dir', dir);
		document.location.href = `${location.pathname}?${params}`
	});
});
