<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Edit Log Class
 *
 * @author    Tim Aldridge
 * @copyright (c) 2015 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
 **/

declare(strict_types=1);

namespace DynamicSettings;

use DynamicSettings\EditLog\Entry;
use User;

class EditLog {
	/**
	 * Add an entry into the wiki edit log.
	 *
	 * @param string  $logKey        Log Key - Usually one of: ["site_key", "allowed_extension_md5_key", "allowed_setting_id"]
	 * @param array   $data          Data to insert serialized into the log.
	 * @param string  $method        The method used to call this function as returned by __METHOD__.
	 * @param object  $user          User object of the user performing the action.
	 * @param string  $commitMessage [Optional] User entered change message.
	 * @param array   $miscData      [Optional] Extra data to provide for template usage or otherwise.  Not saved to the database.
	 * @param boolean $sendEmail     [Optional] Send Email
	 *
	 * @return boolean Success
	 */
	public static function addEntry(string $logKey, array $data, string $method, User $user, string $commitMessage, array $miscData = [], bool $sendEmail = true) {
		$db = DSDBFactory::getMasterDB(DB_MASTER);

		$entry = new Entry;
		$entry->setLogKey($logKey);
		$entry->setData($data);
		$entry->setClassMethod($method);
		$entry->setUserId($user->getId());
		$entry->setCommitMessage($commitMessage);
		$entry->setTimestamp(time());
		$entry->setIPAddress(User::isIP($user->getName()) ? $user->getName() : $_SERVER['REMOTE_ADDR']);

		$success = $entry->save();

		if ($success) {
			$eventExtra = [
				'entry'     => $entry,
				'timestamp' => $entry->getTimestamp(), // Override automatic timestamp with the one from the entry.
				'misc'      => $miscData,
				'performer' => $user,
				'method'    => $method
			];

			if ($sendEmail) {
				Notifier::sendAdminNotification('logtype-' . Entry::$typeToStringMap[$entry->getLogType()], $eventExtra, 'site-management-wiki-edit');
			}
		}

		return $success;
	}

	/**
	 * Retrieve edit log entries by any uncached ("stale") wikis
	 *
	 * @param string|null $logKey  Log Key to limit by.
	 * @param string|null $logType Log Type to limit by.
	 *
	 * @return array EditLog\Entry objects.
	 */
	public static function getStaleEntries(?string $logKey = null, ?string $logType = null) {
		$where['cache_timestamp'] = 0;

		if (!empty($logKey)) {
			$where['log_key'] = $logKey;
		}

		if (!empty($logType)) {
			$where['log_type'] = $logType;
		}

		$entries = self::queryEditLog($where);

		return $entries;
	}

	/**
	 * Query helper function for the edit log.
	 *
	 * @param array $where clause
	 *
	 * @return array EditLog\Entry objects.
	 */
	private static function queryEditLog(array $where) {
		$entries = [];

		$db = DSDBFactory::getMasterDB(DB_MASTER);
		$result = $db->select(
			'wiki_edit_log',
			['*'],
			$where,
			__METHOD__,
			[
				'ORDER BY'	=> 'lid DESC'
			]
		);

		while ($row = $db->fetchRow($result)) {
			$entry = Entry::newFromRow($row);
			if ($entry !== null) {
				$entries[] = $entry;
			}
		}

		return $entries;
	}

	/**
	 * Get the total number of entries.
	 *
	 * @param array $where [Optional] Query delimiters.
	 *
	 * @return integer Total Entries
	 */
	public static function getTotalEntries($where = []) {
		$db = DSDBFactory::getMasterDB(DB_MASTER);
		$total = $db->selectRow(
			'wiki_edit_log',
			['count(*) AS total'],
			$where,
			__METHOD__,
			[
				'ORDER BY'	=> 'lid DESC'
			]
		);
		return intval($total->total);
		;
	}
}
