<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Wiki Extensions Special Page
 *
 * @author    Alex Smith
 * @copyright (c) 2014 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
**/

use DynamicSettings\Extensions\Extension;
use DynamicSettings\Lock;

class SpecialWikiExtensions extends DynamicSettings\SpecialPage {
	/**
	 * Output HTML
	 *
	 * @var string
	 */
	private $content;

	/**
	 * Wiki object storage for form editing.
	 *
	 * @var array
	 */
	private $wiki;

	/**
	 * Main Constructor
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct('WikiExtensions', 'wiki_extensions', false);
	}

	/**
	 * Main Executor
	 *
	 * @param string Sub page passed in the URL.
	 *
	 * @return void [Outputs to screen]
	 */
	public function execute($subpage) {
		$this->checkPermissions();

		if (Lock::isLocked() == true) {
			$this->output->showErrorPage('dynamic_settings_error', 'error_settings_locked');
			return;
		}

		$this->siteSettings = new \DynamicSettings\Sites();

		$this->templates = new TemplateWikiExtensions();

		$this->output->addModuleStyles(['ext.wikiExtensions.styles']);
		$this->output->addModules(['ext.wikiExtensions.scripts']);

		$siteKey = $this->wgRequest->getVal('siteKey');

		if (Lock::isLocked($siteKey) == true) {
			$this->output->showErrorPage('dynamic_settings_error', 'error_wiki_locked');
			return;
		}

		$this->setHeaders();

		$this->wiki = \DynamicSettings\Wiki::loadFromHash($siteKey);

		if ($this->wiki === false) {
			$this->output->showErrorPage('site_extensions_error', 'wiki_not_found');
			return;
		}

		if ($this->wiki->isDeleted()) {
			$this->output->showErrorPage('site_extensions_error', 'deleted_wikis_no_modifications');
			return;
		}

		$this->extensions = Extension::getNonDefault();
		$this->extensions->setWikiContext($this->wiki);
		$this->extensionsForm();

		$this->output->addHTML($this->content);
	}

	/**
	 * Wiki Extensions Form
	 *
	 * @return void [Outputs to screen]
	 */
	public function extensionsForm() {
		$errors = $this->extensionsSave();

		$this->output->setPageTitle(wfMessage('site_extensions') . ' - ' . $this->wiki->getNameForDisplay());
		$this->content = $this->templates->extensionsForm($this->extensions, $errors);
	}

	/**
	 * Saves submitted Wiki Extensions Forms.
	 *
	 * @return array Array of errors.
	 */
	private function extensionsSave() {
		$errors = [];
		if ($this->wgRequest->getVal('do') == 'save') {
			$commitMessage = trim($this->wgRequest->getText('commit_message'));
			if (!$commitMessage) {
				$errors['commit_message'] = wfMessage('error_no_commit_message');
			}

			$_extensions = $this->wgRequest->getArray('extensions', []);

			if (!count($errors)) {
				// Create an ExtensionCollection from the selected extensions
				$newExtensions = $this->extensions->filter(function ($ext) use ($_extensions) {
					return in_array($ext->getExtensionKey(), $_extensions);
				});
				if ($newExtensions->setEnabledOnWiki($commitMessage)) {
					$page = Title::newFromText('Special:WikiSettings');
					$this->output->redirect($page->getFullURL(['siteKey' => $this->wiki->getSiteKey(), 'extension_message' => 'true']));
				} else {
					$errors['extensions'] = 'There was a problem saving the new extension set.';
				}
			}
		}
		return $errors;
	}
}
