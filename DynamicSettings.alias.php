<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Dynamic Settings Aliases
 *
 * @author    Alex Smith
 * @copyright (c) 2014 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
**/

$specialPageAliases = [];

/**
 * English (English)
*/
$specialPageAliases['en'] = [
	'GlobalPageEditor' => ['GlobalPageEditor'],
	'LockSettings' => ['LockSettings'],
	'WikiAdvertisements' => ['WikiAdvertisements'],
	'WikiAllowedExtensions' => ['WikiAllowedExtensions'],
	'WikiAllowedSettings' => ['WikiAllowedSettings'],
	'WikiEditLog' => ['WikiEditLog'],
	'WikiExtensions' => ['WikiExtensions'],
	'WikiNamespaces' => ['WikiNamespaces'],
	'WikiNotices' => ['WikiNotices'],
	'WikiPermissions' => ['WikiPermissions'],
	'WikiPromotions' => ['WikiPromotions'],
	'WikiSettings' => ['WikiSettings'],
	'WikiSites' => ['WikiSites'],
	'WikiToolsLog' => ['WikiToolsLog'],
];
