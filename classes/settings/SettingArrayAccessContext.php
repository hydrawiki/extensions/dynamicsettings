<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Collection class for holding an array of Setting instances
 *
 * @author    Noah Manneschmidt
 * @copyright (c) 2015 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
 **/

namespace DynamicSettings\Settings;

/**
 * Override for ArrayAccess to lazily create Setting objects from database row arrays when accessed
 */
trait SettingArrayAccessContext {
	public function offsetGet($offset) {
		$ret = parent::offsetGet($offset);
		if ($ret instanceof \DynamicSettings\Settings\Setting) {
			$ret->setWikiContext($this->wikiContext);
			// save class-wrapped result back to internal data
			$this[$offset] = $ret;
		}
		return $ret;
	}
}
