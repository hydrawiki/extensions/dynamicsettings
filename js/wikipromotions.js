(function(mw, $) {
	$(document).ready(function() {
		var tabType = 'text';

		if ($("#typeTabs").length) {
			$("#typeTabs").tabs({
				activate: function(event,ui) {
					tabType = ui.newPanel.data('type');
				}
			});
		}

		if ($("#dialog-confirm").length) {
			$("#dialog-confirm").dialog({
			  autoOpen: false,
			  height: 300,
			  width: 350,
			  modal: true,
			  buttons: {
				"Save With Text": function(){
					$( "#wiki_settings_form" ).submit();
				},
				"Delete Text & Save": function() {
					$('#description').val('');
					$( "#wiki_settings_form" ).submit();
				}
			  }
			});
		}

		$("#wiki_submit").click(function( event ) {
	  		event.preventDefault();
			console.log(tabType);
			switch (tabType) {
				case 'image':
					if ($('#description').val().trim() !== "") {
						$( "#dialog-confirm" ).dialog( "open" );
					} else {
						$( "#wiki_settings_form" ).submit();
					}
				break;
				case 'text':
				default:
					$( "#wiki_settings_form" ).submit();
				break;
			}

		});

		var imgInput = $('input#image').change(function() {
			$('#image_display').attr('src', imgInput.val());
		});

		var descInput = $('input#description').change(function() {
			$('#image_display').attr('title', descInput.val());
		});

		var setupPromotionDatepickers = function() {
			$("input#begins_datepicker, input#expires_datepicker").datetimepicker(
				{
					dateFormat: "yy-mm-dd",
					constrainInput: true,
					onSelect: function(dateText) {
						var epochInput = '#'+$(this).attr('data-input');
						$(epochInput).val(epochDate(this));
					}
				}
			);
		};
		setupPromotionDatepickers();

		var epochDate = function(dateField) {
			var time = $(dateField).datepicker("getDate");
			var offset = time.getTimezoneOffset() * 60;
			var epoch = time.getTime() / 1000 - offset;

			return epoch;
		};

		$('input#begins, input#expires').change(function() {
			var currentVal = $(this).val();
			var time = new Date(0);
			if (currentVal > 0) {
				var existEpochDate = new Date((currentVal * 1000) + (time.getTimezoneOffset() * 60000));
				var picker = '#'+$(this).attr('id')+'_datepicker';
				$(picker).datepicker("setDate", existEpochDate);
			}
		}).change();

		if ($('input#begins').val()) {
			$('input#begins_display').val(new Date($('input#begins').val() * 1000).toString());
		}
		if ($('input#expires').val()) {
			$('input#expires_display').val(new Date($('input#expires').val() * 1000).toString());
		}

		$('#checkAll').click(function() {
			$('input:checkbox').attr('checked', 'checked');
		});

		$('#uncheckAll').click(function() {
			$('input:checkbox').removeAttr('checked');
		});

		var sortPromotionHelper = function(e, tr) {
			var $originals = tr.children();
			var $helper = tr.clone();
			$helper.children().each(function(index) {
				$(this).width($originals.eq(index).width());
			});
			return $helper;
		},
		updatePromotionIndex = function(e, ui) {
			var weightChange = {};
			$("tr", ui.item.parent()).each(function (i) {
				var promotionId = $(this).attr('data-id');
				var weight = i + 1;
				weightChange[promotionId] = weight;
			});

			var parameters = {
				action: 'dspromotion',
				do: 'updateWeight',
				weight: JSON.stringify(weightChange),
				format: 'json',
				formatversion: 2
			};

			var api = new mw.Api();
			return api.post(parameters).done(function (result) {
				if (result.success) {
					$("tr", ui.item.parent()).each(function (i) {
						var promotionId = $(this).attr('data-id');
						$("td.weight", this).html(weightChange[promotionId]);
					});
				}
			}).fail(function (xhr, status) {
				alert(status.error);
			});
		};

		$(".weight_sort tbody").sortable({
			helper: sortPromotionHelper,
			stop: updatePromotionIndex
		}).disableSelection();

		ajaxNoticePreview = function(text) {
			$.post(mw.config.get('wgScriptPath')+'/api.php?action=parse&disablepp=true&prop=text&format=json', {text: text}, function(parsedText) {
				var cleanedPreview = $(parsedText['parse']['text']['*']).remove();
				$('#noticePreview').html(parsedText['parse']['text']['*']);
				$('#noticePreview span.editsection').remove();
				$('#noticePreview').fadeIn('slow');
			});
		};

		$('input#noticePreviewButton').click(function() {
			ajaxNoticePreview($('textarea#description').val());
		});

		$('#noticePreview').click(function() {
			$(this).fadeOut('slow');
		});
	});
}(mediaWiki, jQuery));