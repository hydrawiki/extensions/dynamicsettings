<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Lock Settings Special Page
 *
 * @author    Alex Smith
 * @copyright (c) 2014 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
**/

use DynamicSettings\Lock;

class SpecialLockSettings extends DynamicSettings\SpecialPage {
	/**
	 * Output HTML
	 *
	 * @var string
	 */
	private $content;

	/**
	 * Main Constructor
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct('LockSettings', 'lock_settings');
	}

	/**
	 * Main Executor
	 *
	 * @param string Sub page passed in the URL.
	 *
	 * @return void [Outputs to screen]
	 */
	public function execute($subpage) {
		$this->checkPermissions();

		$this->templates = new TemplateLockSettings();

		$this->output->addModuleStyles(['ext.dynamicSettings.styles']);
		$this->output->addModules(['ext.dynamicSettings.scripts']);

		$this->setHeaders();

		$this->lockSettingsPage();

		$this->output->addHTML($this->content);
	}

	/**
	 * Lock Settings Page
	 *
	 * @return void [Outputs to screen]
	 */
	public function lockSettingsPage() {
		if ($this->wgRequest->getVal('do') == 'toggleLock') {
			Lock::toggleLock();
		}
		$locked = Lock::isLocked();

		$this->output->setPageTitle(wfMessage('locksettings'));
		$this->content = $this->templates->lockSettingsPage($locked);
	}
}
