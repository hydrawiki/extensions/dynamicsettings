CREATE TABLE /*_*/wiki_promotions (
  `pid` int(12) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `languages` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `begins` int(14) DEFAULT NULL,
  `expires` int(14) DEFAULT NULL,
  `weight` int(10) NOT NULL DEFAULT '0',
  `type` enum('promotion','notice','advertisement') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'promotion',
  `everywhere` tinyint(1) NOT NULL DEFAULT '0',
  `paused` tinyint(1) NOT NULL DEFAULT '0'
) /*$wgDBTableOptions*/;

ALTER TABLE /*_*/wiki_promotions
  ADD PRIMARY KEY (`pid`),
  ADD KEY `type` (`type`),
  ADD KEY `everywhere` (`everywhere`);

ALTER TABLE /*_*/wiki_promotions
  MODIFY `pid` int(12) UNSIGNED NOT NULL AUTO_INCREMENT;