<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Sites Class
 *
 * @author    Alexia E. Smith
 * @copyright (c) 2014 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
 **/

namespace DynamicSettings;

class Sites {
	use \DynamicSettings\Traits\CheckPhpSyntaxTrait;

	/**
	 * Wiki information.
	 *
	 * @var array
	 */
	private static $wikis = [];

	/**
	 * Wikis designated as masters in a portal setup.
	 *
	 * @var array
	 */
	private static $portalMasters = null;

	/**
	 * Wikis designated as masters in a group setup.
	 *
	 * @var array
	 */
	private static $groupMasters = null;

	/**
	 * Wiki Tags
	 *
	 * @var array
	 */
	private static $wikiTags = null;

	/**
	 * Wiki Categories
	 *
	 * @var array
	 */
	private static $wikiCategories = null;

	/**
	 * Wiki Managers
	 *
	 * @var array
	 */
	private static $wikiManagers = null;

	/**
	 * Database Types
	 *
	 * @var array
	 */
	public static $dbTypes = ['mysql', 'postgres', 'sqlite'];

	/**
	 * Search Types
	 *
	 * @var array
	 */
	public static $searchTypes = ['elastic'];

	/**
	 * Register the presence of a Wiki object at the global level.
	 *
	 * @param object $wiki Wiki object.
	 *
	 * @return void
	 */
	public static function registerWiki(Wiki $wiki) {
		self::$wikis[$wiki->getSiteKey()] = $wiki;
	}

	/**
	 * Load portal and group masters.
	 *
	 * @return void
	 */
	private static function loadPortalsAndGroups() {
		$db = DSDBFactory::getMasterDB(DB_MASTER);
		$result = $db->select(
			['wiki_sites'],
			['wiki_portal', 'wiki_group', 'md5_key'],
			'wiki_portal != -1 OR wiki_group != -1',
			__METHOD__
		);

		self::$portalMasters = [];
		self::$groupMasters = [];
		while ($row = $result->fetchRow()) {
			if ($row['wiki_portal'] == 0 && strlen($row['wiki_portal']) != 32 && !array_key_exists($row['md5_key'], self::$portalMasters)) {
				self::$portalMasters[$row['md5_key']] = [];
			}

			if (strlen($row['wiki_portal']) == 32) {
				self::$portalMasters[$row['wiki_portal']][] = $row['md5_key'];
			}

			if ($row['wiki_group'] == 0 && strlen($row['wiki_group']) != 32 && !array_key_exists($row['md5_key'], self::$groupMasters)) {
				self::$groupMasters[$row['md5_key']] = [];
			}

			if (strlen($row['wiki_group']) == 32) {
				self::$groupMasters[$row['wiki_group']][] = $row['md5_key'];
			}
		}

		asort(self::$groupMasters);
		asort(self::$portalMasters);
	}

	/**
	 * Returns wiki portal masters.
	 *
	 * @return array Array of $masterSiteKey => [$childSiteKeys] information.
	 */
	public static function getPortalMasters() {
		if (self::$portalMasters === null) {
			self::loadPortalsAndGroups();
		}
		return self::$portalMasters;
	}

	/**
	 * Returns wiki group masters.
	 *
	 * @return array Array of $masterSiteKey => [$childSiteKeys] information.
	 */
	public static function getGroupMasters() {
		if (self::$groupMasters === null) {
			self::loadPortalsAndGroups();
		}
		return self::$groupMasters;
	}

	/**
	 * Loads all unique wiki tags.
	 *
	 * @return void
	 */
	private static function loadAllTags() {
		$db = DSDBFactory::getMasterDB(DB_MASTER);
		$result = $db->select(
			['wiki_sites'],
			['wiki_tags'],
			[],
			__METHOD__
		);

		self::$wikiTags = [];
		while ($row = $result->fetchRow()) {
			$wikiTags = explode(',', trim($row['wiki_tags'], ','));
			if ($row['wiki_tags'] === null || !is_array($wikiTags)) {
				continue;
			}
			self::$wikiTags = array_merge(self::$wikiTags, $wikiTags);
		}
		self::$wikiTags = array_unique(self::$wikiTags);
		sort(self::$wikiTags);
	}

	/**
	 * Returns all unique wiki tags.
	 *
	 * @return array All unique wiki tags.
	 */
	public static function getAllTags() {
		if (self::$wikiTags === null) {
			self::loadAllTags();
		}
		return self::$wikiTags;
	}

	/**
	 * Loads all unique wiki categories.
	 *
	 * @return void
	 */
	private static function loadAllCategories() {
		$db = DSDBFactory::getMasterDB(DB_MASTER);
		$result = $db->select(
			['wiki_sites'],
			['wiki_category'],
			[],
			__METHOD__
		);

		self::$wikiCategories = [];
		while ($row = $result->fetchRow()) {
			$category = trim($row['wiki_category']);
			if (!empty($category)) {
				self::$wikiCategories[] = $row['wiki_category'];
			}
		}
		self::$wikiCategories = array_unique(self::$wikiCategories);
		natcasesort(self::$wikiCategories);
	}

	/**
	 * Returns all unique wiki categories.
	 *
	 * @return array All unique wiki categories.
	 */
	public static function getAllCategories() {
		if (self::$wikiCategories === null) {
			self::loadAllCategories();
		}
		return self::$wikiCategories;
	}

	/**
	 * Loads all manager user names.
	 *
	 * @return void
	 */
	private static function loadAllManagers() {
		$config = \ConfigFactory::getDefaultInstance()->makeConfig('main');
		$wikiManagerGroups = $config->get('DSWikiManagerGroups');

		$db = DSDBFactory::getMasterDB(DB_MASTER);

		self::$wikiManagers = [];
		$wikiManagers = [];
		/*
			Example entry:
			integer userId => [
			'user' => User object,
			'wikis' => [assigned wikis]
			]
		*/
		if (is_array($wikiManagerGroups) && count($wikiManagerGroups)) {
			$result = $db->select(
				['user_groups', 'user'],
				['*'],
				["user_groups.ug_group"	=> $wikiManagerGroups],
				__METHOD__,
				['GROUP BY' => 'user.user_id'],
				[
					'user' => [
						'INNER JOIN', 'user.user_id = user_groups.ug_user'
					]
				]
			);

			while ($row = $result->fetchObject()) {
				$user = \User::newFromRow($row);
				if ($user && $user->getId()) {
					if ($user->isBot()) {
						continue;
					}
					$wikiManagers[$user->getId()] = ['user' => $user, 'wikis' => []];
				}
			}
		}

		// We have to look at the assigned managers on the wiki_sites table since there be managers assigned there still that have been demoted out of the configured wiki manager groups.
		$result = $db->select(
			['wiki_sites'],
			[
				'wiki_managers',
				'md5_key'
			],
			[
				"wiki_managers != '' OR wiki_managers != NULL",
				'deleted' => 0
			],
			__METHOD__
		);

		while ($row = $result->fetchRow()) {
			$assignedWikiManagers = array_filter(explode(',', trim($row['wiki_managers'], ',')));
			if (!is_array($assignedWikiManagers)) {
				continue;
			}
			foreach ($assignedWikiManagers as $manager) {
				$user = \User::newFromName($manager);
				if ($user && $user->getId()) {
					if ($user->isBot()) {
						continue;
					}
					if (isset($wikiManagers[$user->getId()])) {
						$wikiManagers[$user->getId()]['wikis'][] = $row['md5_key'];
					} else {
						$wikiManagers[$user->getId()] = [
							'user' => $user,
							'wikis' => [$row['md5_key']]
						];
					}
				}
			}
		}

		if (is_array($wikiManagers) && count($wikiManagers)) {
			uasort($wikiManagers, function ($a, $b) {
				$testUnsort = [$a['user']->getName(), $b['user']->getName()];
				$testSort = $testUnsort;
				natcasesort($testSort); // The natcasesort function preserves key associations which is why reset is used below.
				if (reset($testSort) !== reset($testUnsort)) {
					return 1;
				}
				return 0;
			});
		}

		self::$wikiManagers = $wikiManagers;
	}

	/**
	 * Returns all manager user names.
	 *
	 * @param boolean $bySites [Optional] Return managers for each site instead of sites for each manager.  Defaults to false.
	 *
	 * @return array All manager user names
	 */
	public static function getAllManagers($bySites = false) {
		if (self::$wikiManagers === null) {
			self::loadAllManagers();
		}

		// This returns the managers as $siteKeys => $managers instead of $managers => $siteKeys.
		if ($bySites) {
			foreach (self::$wikiManagers as $manager => $details) {
				$siteKeys = $details['wikis'];
				foreach ($siteKeys as $siteKey) {
					$wikiManagers[$siteKey][$details['user']->getId()] = $details['user'];
				}
			}
			return $wikiManagers;
		}

		return self::$wikiManagers;
	}

	/**
	 * Returns total number of sites.
	 *
	 * @param string|null $searchTerm  [Optional] Search term to filter by.
	 * @param boolean     $hideDeleted [Optional] Hide deleted wikis, default false.
	 *
	 * @return mixed False on error or the total number of sites.
	 */
	public static function getTotalSites($searchTerm = null, $hideDeleted = false) {
		$db = DSDBFactory::getMasterDB(DB_MASTER);

		$searchableFields = ['wiki_name', 'wiki_meta_name', 'domain', 'wiki_category', 'wiki_tags', 'wiki_managers'];

		if (!empty($searchTerm)) {
			foreach ($searchableFields as $field) {
				$where[] = "`" . $field . "` LIKE '%" . $db->strencode($searchTerm) . "%'";
			}
			$where = "(" . implode(' OR ', $where) . ")";
		}
		if ($hideDeleted === true) {
			$hide[] = '`deleted` = 0';
		}

		$hide[] = "wiki_domains.type = " . Wiki\Domains::ENV_PRODUCTION;

		if (count($hide)) {
			if (!empty($where)) {
				$where .= ' AND (' . implode(' AND ', $hide) . ')';
			} else {
				$where = implode(' AND ', $hide);
			}
		}

		$options = [];
		$result = $db->select(
			['wiki_sites', 'wiki_domains'],
			['count(md5_key) as total'],
			$where,
			__METHOD__,
			$options,
			[
				'wiki_domains' => [
					'LEFT JOIN', 'wiki_domains.site_key = wiki_sites.md5_key'
				]
			]
		);

		$total = $result->fetchRow();

		if ($total !== false) {
			return $total['total'];
		} else {
			return false;
		}
	}

	/**
	 * Gets the site folder for a domain.
	 *
	 * @param string $domain Site domain
	 *
	 * @return string Folder path.
	 */
	public static function getSiteFolder($domain) {
		return SITES_FOLDER . "/{$domain}";
	}

	/**
	 * Creates site directories.
	 *
	 * @param string $domain Site domain
	 *
	 * @return boolean Successful creation.
	 */
	public static function makeSiteFolder($domain) {
		$folder = self::getSiteFolder($domain);

		if (is_link($folder)) {
			if (!unlink($folder)) {
				throw new \MWException(__METHOD__ . ': Unable to unlink legacy symlink for ' . $domain);
			}
		}

		$success = false;
		if (!is_dir($folder)) {
			$success = mkdir($folder, 0775);
		} else {
			if ((fileperms($folder) & 0777) !== 0775) {
				chmod($folder, 0775);
			}
			$success = true;
		}

		if ($success === false) {
			throw new \MWException(__METHOD__ . ': Could not create folder for ' . $folder . '.  This is most likely due to incorrect permissions.');
		}

		return $success;
	}

	/**
	 * Deletes the site folder from disk.
	 *
	 * @param string $domain Site domain
	 *
	 * @return boolean Success
	 */
	public static function deleteSiteFolder($domain) {
		$folder = self::getSiteFolder($domain);

		if (is_dir($folder)) {
			return rmdir($folder);
		}
		return false;
	}

	/**
	 * Writes out a site file to disk.
	 *
	 * @param string  $domain      Site domain(Folder)
	 * @param string  $filename    File Name
	 * @param string  $content     PHP Content
	 * @param boolean $syntaxCheck [Optional] Check syntax before writing.
	 *
	 * @return boolean Successful write.
	 */
	public static function writeSiteFile($domain, $filename, $content, $syntaxCheck = true) {
		if ($syntaxCheck !== false) {
			$syntax = self::checkPHPSyntax($content);
			if (!$syntax['is_valid']) {
				throw new \Exception("Aborting file write!  Invalid syntax detected before writing: {$filename} for domain {$domain}.\n  Temporary file available at: " . $syntax['temp_file']);
			}
		}

		$folder = self::getSiteFolder($domain);
		$filePath = "{$folder}/{$filename}";
		$tmpFilePath = "{$filePath}." . sprintf("%08d", mt_rand(0, 99999999)) . ".tmp";
		$success = @file_put_contents($tmpFilePath, $content);
		if ($success === false || $success === 0) {
			throw new \Exception("Could not write {$tmpFilePath} for {$domain}.  This is most likely due to incorrect permissions.");
		}
		@chmod($tmpFilePath, 0775);
		if (@rename($tmpFilePath, $filePath) === false) {
			throw new \Exception("Could not rename {$tmpFilePath} to {$filePath} for {$domain}.  This is most likely due to incorrect permissions.");
		}
		return true;
	}

	/**
	 * Deletes a site file from disk.
	 *
	 * @param string $domain    Site domain(Folder)
	 * @param mixed  $filenames String file name or an array of file names.
	 *
	 * @return array Array of file names as the keys indicating if they were successfully deleted.
	 */
	public static function deleteSiteFile($domain, $filenames) {
		$filenames = (array)$filenames;

		$success = [];
		$folder = self::getSiteFolder($domain);

		foreach ($filenames as $filename) {
			$success[$filename] = false;
			$filePath = "{$folder}/{$filename}";
			if (file_exists($filePath)) {
				$success[$filename] = @unlink($filePath);
			}
		}

		return $success;
	}

	/**
	 * Verify Hydra base paths are useable in scripts.
	 *
	 * @return mixed Boolean true for all good, array of bad paths otherwise.
	 */
	public static function verifyHydraPaths() {
		$config = \ConfigFactory::getDefaultInstance()->makeConfig('main');
		$wgHydraCacheBasePath = $config->get('HydraCacheBasePath');
		$wgHydraMediaBasePath = $config->get('HydraMediaBasePath');

		$errors = false;

		if (!is_dir($wgHydraCacheBasePath) || !is_writable($wgHydraCacheBasePath)) {
			$errors[] = "\$wgHydraCacheBasePath = '" . $wgHydraCacheBasePath . "'";
		}

		if (!is_dir($wgHydraMediaBasePath) || !is_writable($wgHydraMediaBasePath)) {
			$errors[] = "\$wgHydraMediaBasePath = '" . $wgHydraMediaBasePath . "'";
		}

		if (!is_dir(SITES_FOLDER) || !is_writable(SITES_FOLDER)) {
			$errors[] = "define('SITES_FOLDER', '" . SITES_FOLDER . "')";
		}

		if ($errors !== false) {
			return $errors;
		} else {
			return true;
		}
	}

	/**
	 * Get the cache path for a domain.
	 *
	 * @param string $domain Domain Name
	 *
	 * @return string Full Folder Path
	 */
	public static function getSiteCachePath($domain) {
		$config = \ConfigFactory::getDefaultInstance()->makeConfig('main');
		$wgHydraCacheBasePath = $config->get('HydraCacheBasePath');
		return $wgHydraCacheBasePath . '/' . $domain;
	}

	/**
	 * Get the cache path for a domain.
	 *
	 * @param string $domain Domain Name
	 *
	 * @return string Full Folder Path
	 */
	public static function getSiteMediaPath($domain) {
		$config = \ConfigFactory::getDefaultInstance()->makeConfig('main');
		$wgHydraMediaBasePath = $config->get('HydraMediaBasePath');
		return $wgHydraMediaBasePath . '/' . $domain;
	}
}
