<?php
/**
 * Curse Inc.
 * Curse Skin
 * Populate Default Skin
 * Run the updateDefaultContent script in master/tools to generate fresh XML data for this script
 *
 * @author    Brent Copeland, Noah Manneschmidt
 * @copyright (c) 2013 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Curse Skin
 * @link      https://gitlab.com/hydrawiki
 */

require_once dirname(__DIR__, 3) . '/maintenance/Maintenance.php';

/**
 * Class PopulateDefaultSkin
 */
class PopulateDefaultSkin extends Maintenance {
	protected $mFilename;			///< Import filename.

	protected $mFileHandle;			///< Open filename handle.

	protected $mDB;					///< Database link object.

	protected $mSource;				///< Import source object.

	protected $mImporter;			///< WikiImporter object.

	protected $mUser;				///< Importer user

	protected $mComment;			///< Comment

	protected $mTimestamp;			///< Timestamp.

	protected $mStatPages, $mStatFiles;	///< Statistics.

	/**
	 * @var array
	 */
	public $templates = [];

	/**
	 * Main Constructor
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
		$this->mDescription = "Populate Wiki with Deafult pages, css and categories. Use with hostHelper.php";
		$this->addOption('overwrite', "Overwrite existing files");
		$this->addOption('dumpFile', 'The dumpfile to use.', true, true);
	}

	public function execute() {
		if (is_null($_SERVER['HTTP_HOST'])) {
			$this->error('This script must be run with hostHelper.php to specify a target wiki', true);
		}

		$extDir = __DIR__;
		// If the wiki is in read-only state, die.
		if (wfReadOnly()) {
			$this->error("Wiki is in read-only mode; you'll need to disable it for import to work.\n", true);
		}

		// insert interwiki data
		$this->output("Inserting gphelp into interwiki... \n");
		$this->interwikiInsert();
		$this->output("gphelp has been inserted into interwiki... \n");

		// import xml files
		$template_file = $this->getOption('dumpFile', false);
		if (!is_file($template_file)) {
			$this->error("Could not find content dump at {$template_file}", true);
			exit;
		}
		$this->output("Using File: " . $template_file . " to import default content... \n");
		$this->importXML($template_file);
		stream_wrapper_unregister('uploadsource');
		$this->output("Default content has been imported! \n");

		// Update recent changes table.
		$this->updateRecentChanges();
	}

	/**
	 * Perform recent changes update.
	 */
	protected function updateRecentChanges() {
		$this->output("+ Rebuilding recent changes...\n");
		$this->runChild('RebuildRecentchanges', 'rebuildrecentchanges.php');
		$this->output("  + Done.\n");
	}

	public function importXML($filename) {
		global $wgUser, $wgMetaNamespace, $wgSitename;
		$metaPrefix = str_replace('_', ' ', $wgMetaNamespace);

		$xml = file_get_contents($filename);

		// Handle meta namespace.
		$xml = str_replace(
			[
				'Default Loadout Wiki:',
				'Category:Default Loadout Wiki',
				'case="first-letter">Default Loadout Wiki'
			],
			[
				$metaPrefix . ':',
				'Category:' . $metaPrefix,
				'case="first-letter">' . $metaPrefix
			],
			$xml
		);

		// Handle sitename.  This occurs after meta name since otherwise it might get false positives.
		$xml = str_replace(
			[
				'Default Loadout Wiki',
				'Default_Loadout_Wiki'
			],
			[
				htmlspecialchars($wgSitename, ENT_NOQUOTES),
				str_replace(' ', '_', htmlspecialchars($wgSitename, ENT_NOQUOTES))
			],
			$xml
		);

		$this->mFileHandle = tmpfile();
		fwrite($this->mFileHandle, $xml);
		fflush($this->mFileHandle);
		fseek($this->mFileHandle, 0);

		// $this->mFilename = $filename;
		// $this->mFileHandle = fopen( $this->mFilename, 'rb' );
		// if ( !$this->mFileHandle ) {
		// 	throw new MWException( __CLASS__ . ": Failed to open {$this->mFilename}." );
		// }

		$this->mDB = wfGetDB(DB_MASTER);
		$this->mSource = new ImportStreamSource($this->mFileHandle);
		$this->mImporter = new WikiImporter($this->mSource);

		$userName = $this->getOption('user', 'Maintenance script');

		$this->mUser = User::newFromName($userName);
		$wgUser = $this->mUser;

		$this->mComment = wfMessage('default import')->inContentLanguage()->plain();
		$this->mTimestamp = wfTimestampNow();

		$this->mImporter->setRevisionCallback([&$this, 'handleRevision']);

		$basefn = wfBasename($this->mFilename);
		$this->output("+ Importing {$basefn}...\n");

		$this->mStatPages = 0;
		$this->mStatFiles = 0;
		$this->mImporter->doImport();

		$this->output("  + Done. {$this->mStatPages} page(s) and {$this->mStatFiles} file(s) imported.\n");
		fclose($this->mFileHandle);
	}

	/**
	 * Importer revision callback.  Called from WikiImporter.
	 */
	public function handleRevision($revision) {
		$title = $revision->getTitle();
		$title_text = $title->getPrefixedText();

		if ($title->getNamespace() == NS_FILE) {
			$base = $title->getDBkey();
			$this->output("  + File: '{$base}'...");
			$image = wfLocalFile($title);
			if (!$image->exists() || $this->getOption('overwrite')) {
				$this->output(" uploading...");
				$filepath = dirname($this->mFilename) . '/' . $base;
				$archive = $image->upload(
					$filepath,
					$this->mComment,
					$revision->getText(),
					0,
					false,
					$this->mTimestamp,
					$this->mUser
				);

				if (!$archive->isGood()) {
					$this->output(" failed.\n");
					return false;
				} else {
					$this->output(" success.\n");
					$this->mStatFiles++;
					return true;
				}
			} else {
				$this->output(" file exists, skipping.\n");
				return false;
			}
		} else {
			$this->output("  + Page: '{$title_text}'...\n");
			$revision->setUsername($this->mUser->getName());
			$revision->setComment($this->mComment);
			$revision->setTimestamp($this->mTimestamp);
			$result = $this->mDB->deadlockLoop([$revision, 'importOldRevision']);

			if ($result) { $this->mStatPages++;
			}
			return $result;
		}
	}

	public function interwikiInsert() {
		$dbw = wfGetDB(DB_MASTER);

		$data = [
			'iw_prefix' => 'gphelp',
			'iw_url' => 'https://help.gamepedia.com/$1',
			'iw_local' => 1,
			'iw_trans' => 1
		];

		return $dbw->insert('interwiki', $data, __METHOD__, 'IGNORE');
	}
}

$maintClass = "PopulateDefaultSkin";
require_once RUN_MAINTENANCE_IF_MAIN;
