<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Allowed Extensions Skin
 *
 * @author    Alex Smith
 * @copyright (c) 2014 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
**/

use DynamicSettings\Extensions\Extension;

class TemplateWikiAllowedExtensions {
	/**
	 * Output HTML
	 *
	 * @var string
	 */
	private $HMTL;

	/**
	 * Allowed Extensions List
	 *
	 * @param array Array of Allowed Extensions
	 * @param array Pagination
	 * @param boolean Hide Deleted Extensions
	 * @param boolean Hide Default Extensions
	 * @param string Data sorting key
	 * @param string Data sorting direction
	 * @param string Search Term
	 *
	 * @return string Built HTML
	 */
	public function allowedExtensionsList($allowedExtensions, $pagination, $hideDeleted, $hideDefault, $sortKey, $sortDir, $searchTerm) {
		global $wgOut, $wgUser;

		$wikiExtensionPage = Title::newFromText('Special:WikiAllowedExtensions');

		$wikiSettingsPage = Title::newFromText('Special:WikiAllowedSettings');

		$html = "
	<div>{$pagination}</div>
	<div class='button_bar'>
		<div class='buttons_left'>
			<form method='get' action='" . $wikiExtensionPage->getFullURL() . "'>
				<fieldset>
					<input type='hidden' name='section' value='list' />
					<input type='hidden' name='action' value='search' />
					<input type='text' name='list_search' value='" . htmlentities($searchTerm, ENT_QUOTES) . "' class='search_field' placeholder='" . wfMessage('list_search')->escaped() . "'/>
					<input type='submit' value='" . wfMessage('list_search')->escaped() . "' class='mw-ui-button mw-ui-progressive' />
					<a href='{$wikiExtensionPage->getFullURL(['action' => 'resetSearch'])}' class='mw-ui-button mw-ui-destructive'>" . wfMessage('list_reset')->escaped() . "</a>
				</fieldset>
			</form>
		</div>
		<div class='button_break'></div>
		<div class='buttons_right'>
			<a href='" . $wikiExtensionPage->getFullURL(['hide_deleted' => ($hideDeleted ? 'false' : 'true')]) . "' class='mw-ui-button with_legend'><span class='legend_deleted'></span>" . wfMessage(($hideDeleted ? 'show' : 'hide') . '_deleted_extensions')->escaped() . "</a>
			<a href='" . $wikiExtensionPage->getFullURL(['hide_default' => ($hideDefault ? 'false' : 'true')]) . "' class='mw-ui-button with_legend'><span class='legend_default'></span>" . wfMessage(($hideDefault ? 'show' : 'hide') . '_default_extensions')->escaped() . "</a>
			<a href='" . $wikiExtensionPage->getFullURL(['section' => 'extension', 'action' => 'add']) . "' class='mw-ui-button mw-ui-constructive'>" . wfMessage('add_extension')->escaped() . "</a>
		</div>
	</div>
	<table id='wikilist'>
		<thead>
			<tr class='sortable' data-sort-dir='" . ($sortDir == 'desc' ? 'desc' : 'asc') . "'>
				<th class='controls'>&nbsp;</th>
				<th" . ($sortKey == 'extension_name' ? " data-selected='true'" : '') . "><span data-sort='extension_name' data-selected='true'>" . wfMessage('extension_name')->escaped() . "</span></th>
				<th class='unsortable'>" . wfMessage('extension_desc')->escaped() . "</th>
				<th class='unsortable'>" . wfMessage('extension_filefolder')->escaped() . "</th>
				<th class='unsortable'>" . wfMessage('extension_update_required_column')->escaped() . "</th>
				<th class='unsortable'>" . wfMessage('extension_load_type')->escaped() . "</th>
			</tr>
		</thead>
		<tbody>
		";
		if (count($allowedExtensions)) {
			foreach ($allowedExtensions as $allowedExtension) {
				$page = Title::newFromText('gphelp:Extension:' . $allowedExtension->getExtensionName());
				$url = Linker::link($page, $allowedExtension->getExtensionName());
				$html .= "
				<tr class='" . ($allowedExtension->isDeleted() ? "deleted " : ($allowedExtension->isDefaultExtension() ? "default " : '') . ($allowedExtension->isCriticalExtension() ? "critical " : '')) . "'>
					<td class='controls'>
						<div class='controls_container'>";
				if (!$allowedExtension->isDeleted()) {
					$html .= "
						" . HydraCore::awesomeIcon('wrench') . "
							<span class='dropdown'>
								" . ($wgUser->isAllowed('wiki_allowed_extensions') ? "<a href='" . $wikiExtensionPage->getFullURL(['section' => 'extension', 'action' => 'edit', 'md5_key' => $allowedExtension->getExtensionKey()]) . "' title='Edit extension'>" . HydraCore::awesomeIcon('pencil-alt') . wfMessage('edit')->escaped() . "</a>" : null) . "
								" . ($wgUser->isAllowed('wiki_allowed_extensions') ? "<a href='" . $wikiSettingsPage->getFullURL(['action' => 'search', 'list_search' => $allowedExtension->getExtensionName()]) . "' title='Extension Settings'>" . HydraCore::awesomeIcon('cog') . wfMessage('settings')->escaped() . "</a>" : null) . "
								" . ($wgUser->isAllowed('wiki_allowed_extensions') && !$allowedExtension->isDefaultExtension() ? "<a href='" . $wikiExtensionPage->getFullURL(['section' => 'extension', 'action' => 'delete', 'md5_key' => $allowedExtension->getExtensionKey()]) . "' title='Delete extension'>" . HydraCore::awesomeIcon('minus-circle') . wfMessage('delete')->escaped() . "</a>" : null) . "
							</span>";
				} else {
					$html .= HydraCore::awesomeIcon('minus');
				}
				$html .= "
						</div>
					</td>
					<td>{$url}</td>
					<td" . ($allowedExtension->getExtensionInformation() ? " class='hover_hand'" : null) . ">" . htmlentities($allowedExtension->getExtensionDesc(), ENT_QUOTES) . ($allowedExtension->getExtensionInformation() ? "<div class='hover_container'>" . str_replace(["\r\n", "\n\r", "\n", "\r"], '<br/>', htmlentities($allowedExtension->getExtensionInformation(), ENT_QUOTES)) . "</div>" : null) . "</td>
					<td>" . htmlentities($allowedExtension->getExtensionFolder(), ENT_QUOTES) . "</td>
					<td class='center'>" . ($allowedExtension->isUpdateRequired() ? HydraCore::awesomeIcon("check") : '') . "</td>
					<td class='center'>";

				switch ($allowedExtension->getLoadType()) {
					case Extension::LOAD_EXTENSION:
						$html .= "wfLoadExtension()";
						break;
					case Extension::LOAD_EXTENSION_LEGACY:
						$html .= "require()";
						break;
					case Extension::LOAD_SKIN:
						$html .= "wfLoadSkin()";
						break;
				}

					$html .= "
					</td>
				</tr>";
			}
		} else {
			$html .= "
			<tr>
				<td colspan='6'>" . wfMessage('no_extensions_found')->escaped() . "</td>
			</tr>
			";
		}
		$html .= "
		</tbody>
	</table>";

		$html .= $pagination;

		return $html;
	}

	/**
	 * Allowed Extension Form
	 *
	 * @param array Array of allowed extension information.
	 * @param array Allowed Extensions(For choosing dependencies.)
	 * @param array Key name => Error of errors
	 * @param string [Optional] Form action; one of: ['add', 'edit']
	 *
	 * @return string Built HTML
	 */
	public function allowedExtensionsForm($extension, $allowedExtensions, $errors, $action = 'add') {
		global $wgRequest;

		$page = Title::newFromText('Special:WikiAllowedExtensions');

		$html = '';

		if ($extension->isCriticalExtension()) {
			$html .= "
			<div class='warningbox'>" . wfMessage('extension_marked_as_critical')->escaped() . "</div>";
		}

		$html .= "
		<form id='wiki_settings_form' method='post' action='{$page->getFullURL(['section' => 'extension', 'action' => $action, 'do' => 'save'])}'>
			<fieldset>
				" . (isset($errors['extension_name']) ? '<span class="error">' . $errors['extension_name'] . '</span>' : '') . "
				<label for='extension_name' class='label_above'>" . wfMessage('extension_name')->escaped() . "</label>
				<input id='extension_name' name='extension_name' type='text' value='{$extension->getExtensionName()}'/>

				" . (isset($errors['extension_desc']) ? '<span class="error">' . $errors['extension_desc'] . '</span>' : '') . "
				<label for='extension_desc' class='label_above'>" . wfMessage('extension_desc')->escaped() . "</label>
				<input id='extension_desc' name='extension_desc' type='text' value='" . htmlspecialchars($extension->getExtensionDesc(), ENT_QUOTES) . "'/>

				" . (isset($errors['extension_filename']) ? '<span class="error">' . $errors['extension_filename'] . '</span>' : '') . "
				<label for='extension_filename' class='label_above'>" . wfMessage('extension_filename')->escaped() . "</label>
				<input id='extension_filename' name='extension_filename' type='text' value='{$extension->getExtensionFilename()}'/>

				" . (isset($errors['extension_folder']) ? '<span class="error">' . $errors['extension_folder'] . '</span>' : '') . "
				<label for='extension_folder' class='label_above'>" . wfMessage('extension_folder')->escaped() . "</label>
				<input id='extension_folder' name='extension_folder' type='text' value='{$extension->getExtensionFolder()}'/>
				" . (isset($errors['file']) ? '<span class="error">' . $errors['file'] . '</span>' : '') . "

				<br/><input id='default_extension' name='default_extension' type='checkbox' value='default_extension'" . ($extension->isDefaultExtension() ? ' checked="checked"' : null) . "/><label for='default_extension'>" . wfMessage('default_extension')->escaped() . "</label><br/>
				<input id='critical_extension' name='critical_extension' type='checkbox' value='critical_extension'" . ($extension->isCriticalExtension() ? ' checked="checked"' : null) . "/><label for='critical_extension'>" . wfMessage('critical_extension')->escaped() . "</label><br/>
				<input id='update_required' name='update_required' type='checkbox' value='1' " . ($extension->isUpdateRequired() ? 'checked' : '') . "/><label for='update_required'>" . wfMessage('extension_update_required')->escaped() . "</label><br/>

				<label for='load_type' class='label_above'>" . wfMessage('extension_load_method')->escaped() . "</label>
				<select id='load_type' name='load_type'>
					<option " . ($extension->getLoadType() == Extension::LOAD_EXTENSION ? "selected" : '') . " value='" . Extension::LOAD_EXTENSION . "'>wfLoadExtension (requires extension.json file, MW 1.25+)</option>
					<option " . ($extension->getLoadType() == Extension::LOAD_EXTENSION_LEGACY ? "selected" : '') . " value='" . Extension::LOAD_EXTENSION_LEGACY . "'>require (for legacy plugins only)</option>
					<option " . ($extension->getLoadType() == Extension::LOAD_SKIN ? "selected" : '') . " value='" . Extension::LOAD_SKIN . "'>wfLoadSkin (for skins in MW 1.25+)</option>
				</select>

				" . (isset($errors['weight']) ? '<span class="error">' . $errors['weight'] . '</span>' : '') . "
				<label for='weight' class='label_above'>" . wfMessage('load_order')->escaped() . "</label>
				<input id='weight' name='weight' type='text' value='{$extension->getWeight()}'/>

				<label class='label_above'>" . wfMessage('extension_dependencies')->escaped() . "</label>
				<input id='inline_extension_search' type='text' name='inline_extension_search' placeholder='" . wfMessage('type_to_search')->escaped() . "' class='search_field'/>
				";
		if (isset($errors['dependencies'])) {
			$html .= '<span class="error">' . $errors['dependencies'] . '</span>';
		}
				$html .= "
				<div id='extensions_container'>";
		if (count($allowedExtensions)) {
			foreach ($allowedExtensions as $md5Key => $info) {
				if ($md5Key == $extension->getExtensionKey()) {
					continue;
				}
				$html .= "
					<label class='hideable" . ($info['extension_information'] ? " hover_hand" : null) . "' data-name='" . htmlentities(strtolower($info['extension_name']), ENT_QUOTES) . "'><input type='checkbox' name='dependencies[]' value='{$md5Key}'" . (in_array($md5Key, $extension->getDependencies()) ? " checked='checked'" : null) . "/> {$info['extension_name']}" . ($info['update_required'] || $info['extension_information'] ? ' <span class="extension_warning">' . ($info['update_required'] ? '(' . wfMessage('db_update_required')->escaped() . ')' : null) . ($info['extension_information'] ? '*' : null) . '</span>' : null) . ($info['extension_information'] ? "<div class='hover_container'>" . str_replace(["\r\n", "\n\r", "\n", "\r"], '<br/>', htmlentities($info['extension_information'], ENT_QUOTES)) . "</div>" : null) . "</label>";
			}
		}
				$html .= "
				</div>

				" . (isset($errors['extension_extra']) ? '<span class="error">' . $errors['extension_extra'] . '</span>' : '') . "
				<label for='extension_extra' class='label_above'>" . wfMessage('extension_extra')->escaped() . "</label>
				<textarea id='extension_extra' name='extension_extra' class='lined wide' data-line='" . (isset($errors['extension_extra_line']) ? $errors['extension_extra_line'] : '') . "'>" . htmlspecialchars($extension->getExtensionExtra(), ENT_QUOTES, 'UTF-8') . "</textarea>

				" . (isset($errors['extension_information']) ? '<span class="error">' . $errors['extension_information'] . '</span>' : '') . "
				<label for='extension_information' class='label_above'>" . wfMessage('extension_information')->escaped() . "</label>
				<textarea id='extension_information' name='extension_information'>" . htmlspecialchars($extension->getExtensionInformation(), ENT_QUOTES, 'UTF-8') . "</textarea>
			</fieldset>
			<fieldset id='commit_fieldset'>
				" . (isset($errors['commit_message']) ? '<span class="error">' . $errors['commit_message'] . '</span>' : '') . "
				<label for='commit_message' class='label_above'>" . wfMessage('commit_message')->escaped() . "</label>
				<input id='commit_message' name='commit_message' type='text' value='" . htmlentities(trim($wgRequest->getText('commit_message')), ENT_QUOTES) . "'/><br/>
				<input id='md5_key' name='md5_key' type='hidden' value='{$extension->getExtensionKey()}'/>
				<button id='wiki_submit' name='wiki_submit' class='mw-ui-button mw-ui-progressive'>" . wfMessage('save_key_value')->escaped() . "</button>
			</fieldset>
		</form>
		";

		return $html;
	}

	/**
	 * Allowed Extension Deletion Form
	 *
	 * @param array Extension information
	 *
	 * @return string Built HTML
	 */
	public function allowedExtensionsDelete($extension) {
		$action = Title::newFromText('Special:WikiAllowedExtensions');
		$html = "
		<form method='post' action='" . $action->getFullUrl(['section' => 'extension', 'action' => 'delete']) . "'>
			" . wfMessage('delete_extension_confirm')->escaped() . "<br/>
			<input type='hidden' name='do' value='confirm'/>
			<input type='hidden' name='md5_key' value='{$extension->getExtensionKey()}'/>
			<button type='submit' class='mw-ui-button mw-ui-destructive'>" . wfMessage('delete_extension')->escaped() . "</button>
		</form>";

		return $html;
	}
}
