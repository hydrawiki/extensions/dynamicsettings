<?php
/**
 * Hydra
 * Email Test
 *
 * @license GPL
 * @package Email Test
 **/

require_once dirname(__DIR__, 3) . "/maintenance/Maintenance.php";

class EmailTest extends Maintenance {
	/**
	 * Main Constructor
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
		$this->mDescription = "EmailTest";

		$this->addOption('user', 'The user to email.', true, true);
	}

	/**
	 * Send a test email.
	 *
	 * @return void
	 */
	public function execute() {
		$user = User::newFromName($this->getOption('user'));

		$userEmail = $user->getEmail();
		if (Sanitizer::validateEmail($userEmail)) {
			$address[] = new MailAddress($userEmail);
		}

		$email = new UserMailer();
		$status = $email->send(
			$address,
			$from,
			'Email test from Hydra - ' . $_SERVER['SERVER_NAME'],
			'This is an email test.'
		);
		var_dump($status);
	}
}

$maintClass = "EmailTest";
require_once RUN_MAINTENANCE_IF_MAIN;
