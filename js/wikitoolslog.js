(function(mw,$){
	var api = new mw.Api();
	var jobkey = $("#jobkey").val();
	var status = $("#status").val();
	var lastContent = '';

	// This variable controls how much content will be displayed at a time during the live loaded logs
	// This should be a balance of performance and useful info.
	var maxLength = 5000; // number of chars
	var refreshInterval = 5000; // miliseconds between refresh

	console.log(jobkey, status);

	function formatOutput(output) {
		var ansi_up = new AnsiUp();
		ansi_up.escape_for_html = false;
		var ansi = ansi_up.ansi_to_html(output);
		return ansi;
	}

	function writeOutput(output) {
		$("#toolLogOutput").html(output);
		$("#toolLogOutput").scrollTop($("#toolLogOutput")[0].scrollHeight);
	}

	function getCurrentLogs() {
		api.get({
			action: 'dstoolslog',
			do: 'getLogOutput',
			jobkey: jobkey,
			truncate: maxLength // <-- Use if we run into issues with really large issues.
		}).done( function( data ) {
			console.log(data);
			var content = data.content;
			if( content.length + 1 >= maxLength ) {
				content = "--- above content truncated, please wait for job to finish to view full logs --- \n\n" + content;
			}
			output = formatOutput(content);
			writeOutput(output);
			if (data.status_code !== 'JOB_START') {
				$("#statusline").html(data.status);
			} else {
				setTimeout(function(){
					getCurrentLogs();
				},refreshInterval)
			}
		});
	}

	if (status == "JOB_START") {
		// We only bother doing live reloading on running jobs.
	
		getCurrentLogs();
	} else {
		var output = $("#toolLogOutput").html();
		output = formatOutput(output);
		writeOutput(output);
	}
})(mw,$);
