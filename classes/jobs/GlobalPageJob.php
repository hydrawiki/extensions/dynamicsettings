<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * GlobalPageJob Job Class
 *
 * @author    Alexia E. Smith
 * @copyright (c) 2016 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
 **/

namespace DynamicSettings\Job;

use DynamicSettings\GlobalPageEdit;
use RedisCache;

class GlobalPageJob extends \SyncService\Job {
	/**
	 * Handles invoking page searches across wikis and doing find and replace actions.
	 *
	 * @param array $args Named Arguments:
	 *                    - operations		Array of operations to process containing arrays of wiki hashes, titles, and replacements.
	 *                    - identifer		Unique identifier for this GPE invocation.
	 *
	 * @return integer exit value for this thread
	 */
	public function execute($args = []) {
		$operations = $args['operations'];
		$identifier = $args['identifier'];
		$form		= $args['form'];

		$redis = RedisCache::getClient('cache');
		$redisKey = GlobalPageEdit::getRedisQueueStatusKey($identifier);
		$redis->lpush('gpe:queue', $redisKey);

		foreach ($operations as $operation) {
			$result = GlobalPageEdit::doOperation($operation, false, $identifier);
			$identifiers = GlobalPageEdit::getLastIdentifiers();
			if (!empty($identifiers[0])) {
				$identifier = array_shift($identifiers);
				$redis->lpush(
					$redisKey,
					json_encode(
						[
							'form' 			=> $form,
							'identifiers' 	=> $identifiers,
							'result' 		=> $result
						]
					)
				);
			}
		}
	}
}
