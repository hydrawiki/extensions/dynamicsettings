<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Collection class for holding an array of Setting instances
 *
 * @author    Noah Manneschmidt
 * @copyright (c) 2015 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
 **/

namespace DynamicSettings\Settings;

class SettingIterator extends \ArrayIterator {
	use SettingArrayAccessContext, \DynamicSettings\Traits\WikiContext;

	public function current() {
		$ret = parent::current();
		if ($ret instanceof \DynamicSettings\Settings\Setting) {
			$ret->setWikiContext($this->wikiContext);
			// save class-wrapped result back to internal data
			$this[$this->key()] = $ret;
		}
		return $ret;
	}
}
