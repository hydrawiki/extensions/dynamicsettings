<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Allowed Settings Skin
 *
 * @author    Alex Smith
 * @copyright (c) 2014 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
**/

class TemplateWikiAllowedSettings {
	/**
	 * Output HTML
	 *
	 * @var string
	 */
	private $HMTL;

	/**
	 * Allowed Settings List
	 *
	 * @param array Array of Allowed Settings
	 * @param array Pagination
	 * @param boolean Hide Deleted Settings
	 * @param string Data sorting key
	 * @param string Data sorting direction
	 * @param string Search Term
	 *
	 * @return string Built HTML
	 */
	public function allowedSettingsList($allowedSettings, $pagination, $hideDeleted, $sortKey, $sortDir, $searchTerm) {
		global $wgOut, $wgUser;

		$wikiSettingsPage = Title::newFromText('Special:WikiAllowedSettings');

		$html = "
	<div>{$pagination}</div>
	<div class='button_bar'>
		<div class='buttons_left'>
			<form method='get' action='{$wikiSettingsPage->getFullURL()}'>
				<fieldset>
					<input type='hidden' name='section' value='list' />
					<input type='hidden' name='action' value='search' />
					<input type='text' name='list_search' value='" . htmlentities($searchTerm, ENT_QUOTES) . "' class='search_field' placeholder='" . wfMessage('list_search')->escaped() . "'/>
					<input type='submit' value='" . wfMessage('list_search')->escaped() . "' class='mw-ui-button mw-ui-progressive' />
					<a href='{$wikiSettingsPage->getFullURL(['action' => 'resetSearch'])}' class='mw-ui-button mw-ui-destructive'>" . wfMessage('list_reset')->escaped() . "</a>
				</fieldset>
			</form>
		</div>
		<div class='button_break'></div>
		<div class='buttons_right'>
			<a href='" . $wikiSettingsPage->getFullURL(['hide_deleted' => ($hideDeleted ? 'false' : 'true')]) . "' class='mw-ui-button with_legend'><span class='legend_deleted'></span>" . wfMessage(($hideDeleted ? 'show' : 'hide') . '_deleted_settings')->escaped() . "</a>
			<a href='" . $wikiSettingsPage->getFullURL(['section' => 'setting', 'action' => 'add']) . "' class='mw-ui-button mw-ui-constructive'>" . wfMessage('add_setting')->escaped() . "</a>
		</div>
	</div>
	<table id='wikilist'>
		<thead>
			<tr class='sortable' data-sort-dir='" . ($sortDir == 'desc' ? 'desc' : 'asc') . "'>
				<th class='controls unsortable'>&nbsp;</th>
				<th" . ($sortKey == 'setting_key' ? " data-selected='true'" : '') . "><span data-sort='setting_key'" . ($sortKey == 'setting_key' ? " data-selected='true'" : '') . ">" . wfMessage('setting_key')->escaped() . "</span></th>
				<th class='unsortable'>" . wfMessage('setting_default')->escaped() . "</th>
				<th" . ($sortKey == 'setting_type' ? " data-selected='true'" : '') . "><span data-sort='setting_type'" . ($sortKey == 'setting_type' ? " data-selected='true'" : '') . ">" . wfMessage('setting_type')->escaped() . "</span></th>
				<th" . ($sortKey == 'setting_group_name' ? " data-selected='true'" : '') . "><span data-sort='setting_group_name'" . ($sortKey == 'setting_group_name' ? " data-selected='true'" : '') . ">" . wfMessage('setting_group_name')->escaped() . "</span></th>
				<th class='unsortable'><span class='fa fa-question' title='" . wfMessage('load_order_heading')->escaped() . "'></span></th>
				<th class='unsortable'><span class='fa fa-eye' title='" . wfMessage('setting_visibility_heading')->escaped() . "'></span></th>
			</tr>
		</thead>
		<tbody>
		";
		if ($allowedSettings->count()) {
			foreach ($allowedSettings as $allowedSetting) {
				$html .= "
				<tr" . ($allowedSetting->isDeleted() ? " class='deleted'" : '') . ">
					<td class='controls'>
						<div class='controls_container'>";
				if (!$allowedSetting->isDeleted()) {
					$html .= "
							" . HydraCore::awesomeIcon('wrench') . "
							<span class='dropdown'>
								" . ($wgUser->isAllowed('wiki_allowed_settings') ? "<a href='" . $wikiSettingsPage->getFullURL(['section' => 'setting', 'action' => 'edit', 'asid' => $allowedSetting->getId()]) . "' title='Edit setting'>" . HydraCore::awesomeIcon('pencil-alt') . wfMessage('edit')->escaped() . "</a>" : null) . "
								" . ($wgUser->isAllowed('wiki_allowed_settings') ? "<a href='" . $wikiSettingsPage->getFullURL(['section' => 'setting', 'action' => 'delete', 'asid' => $allowedSetting->getId()]) . "' title='Delete setting'>" . HydraCore::awesomeIcon('minus-circle') . wfMessage('delete')->escaped() . "</a>" : null) . "
							</span>";
				}
				$html .= "
						</div>
					</td>
					<td>" . htmlentities($allowedSetting->getSettingKey()) . "</td>
					<td>" . htmlentities(var_export($allowedSetting->getSettingDefault(), true)) . "</td>
					<td>{$allowedSetting->getSettingType()}</td>
					<td>" . htmlentities($allowedSetting->getSettingGroupName()) . "</td>
					<td class='center'>{$allowedSetting->loadOrderDisplay()}</td>
					<td class='center'>{$allowedSetting->visibilityDisplay()}</td>
				</tr>
";
			}
		} else {
			$html .= "
			<tr>
				<td colspan='7'>" . wfMessage('no_settings_found')->escaped() . "</td>
			</tr>
			";
		}
		$html .= "
		</tbody>
	</table>";

		$html .= $pagination;

		return $html;
	}

	/**
	 * Allowed Setting Form
	 *
	 * @param array Array of setting information
	 * @param array Allowed Extensions from the siteSettings class
	 * @param array Key name => Error of errors
	 * @param string [Optional] Form action; one of: ['add', 'edit']
	 *
	 * @return string Built HTML
	 */
	public function allowedSettingsForm($setting, $allowedExtensions, $errors, $action = 'add') {
		global $wgRequest;

		$page = Title::newFromText('Special:WikiAllowedSettings');

		$arrayDefault = htmlspecialchars(($setting->getSettingType() == 'array' ? json_encode($setting->getSettingDefault(), JSON_UNESCAPED_SLASHES) : '[]'), ENT_QUOTES);
		$otherDefault = htmlentities(($setting->getSettingType() != 'array' ? $setting->getSettingDefault() : ''), ENT_QUOTES);

		$html = "
		<form id='wiki_settings_form' method='post' action='{$page->getFullURL(['section' => 'setting', 'action' => $action, 'do' => 'save'])}'>
			<fieldset>
				" . (isset($errors['setting_key']) ? '<span class="error">' . $errors['setting_key'] . '</span>' : '') . "
				<label for='setting_key' class='label_above'>" . wfMessage('setting_key')->escaped() . "</label>
				<input id='setting_key' name='setting_key' type='text' maxlength='255' value='" . htmlentities($setting->getSettingKey(), ENT_QUOTES) . "'/>";

				$html .= "
				" . (isset($errors['setting_type']) ? '<span class="error">' . $errors['setting_type'] . '</span>' : '') . "
				<label for='setting_type' class='label_above'>" . wfMessage('setting_type')->escaped() . "</label>
				<select id='setting_type' name='setting_type'>
					<option value='integer'" . ($setting->getSettingType() == 'integer' ? ' selected="selected"' : '') . ">integer</option>
					<option value='string'" . ($setting->getSettingType() == 'string' ? ' selected="selected"' : '') . ">string</option>
					<option value='text'" . ($setting->getSettingType() == 'text' ? ' selected="selected"' : '') . ">text</option>
					<option value='boolean'" . ($setting->getSettingType() == 'boolean' ? ' selected="selected"' : '') . ">boolean</option>
					<option value='array'" . ($setting->getSettingType() == 'array' ? ' selected="selected"' : '') . ">array</option>
					<option value='define'" . ($setting->getSettingType() == 'define' ? ' selected="selected"' : '') . ">define</option>
				</select>

				" . (isset($errors['setting_default']) ? '<span class="error">' . $errors['setting_default'] . '</span>' : '') . "
				<div id='setting_default' class='fake_fieldset'>
					<label for='setting_default' class='label_above'>" . wfMessage('setting_default')->escaped() . "</label>
					<input name='setting_default_string' class='input_string hidden' type='text' maxlength='65535' value='{$otherDefault}'/>
					<textarea name='setting_default_text' class='input_text hidden' maxlength='65535'>{$otherDefault}</textarea>
					<input name='setting_default_integer' class='input_integer hidden' type='text' maxlength='65535' value='{$otherDefault}'/>
					<span class='yesno hidden'><span class='yesno_yes'><input name='setting_default_boolean' type='radio' value='true'" . ($setting->getSettingDefault() ? " checked='checked'" : '') . "><label for='default_boolean_yes'>Yes</label></span><span class='yesno_no'><input name='setting_default_boolean' type='radio' value='false'" . (!$setting->getSettingDefault() ? " checked='checked'" : '') . "><label for='default_boolean_no'>No</label></span></span>
					<input name='setting_default_array' class='array_builder_input' type='hidden' value='{$arrayDefault}'/>
					<input name='setting_default_define' class='input_string hidden' type='text' maxlength='65535' value='{$otherDefault}'/>";
				$html .= "
				</div>

				" . (isset($errors['setting_example']) ? '<span class="error">' . $errors['setting_example'] . '</span>' : '') . "
				<label for='setting_example' class='label_above'>" . wfMessage('setting_example')->escaped() . "</label>
				<input id='setting_example' name='setting_example' type='text' maxlength='255' value='" . htmlentities($setting->getSettingExample(), ENT_QUOTES) . "'/>

				" . (isset($errors['setting_load_order']) ? '<span class="error">' . $errors['setting_load_order'] . '</span>' : '') . "
				<label for='setting_load_order' class='label_above'>" . wfMessage('setting_load_order')->escaped() . "</label>
				<select id='setting_load_order' name='setting_load_order'>
					<option value='-1' " . ($setting->getSettingLoadOrder() == -1 ? 'SELECTED' : '') . ">" . wfMessage('setting_load_order_before')->escaped() . "</option>
					<option value='0' " . ($setting->getSettingLoadOrder() == 0 ? 'SELECTED' : '') . ">" . wfMessage('setting_load_order_unknown')->escaped() . "</option>
					<option value='1' " . ($setting->getSettingLoadOrder() == 1 ? 'SELECTED' : '') . ">" . wfMessage('setting_load_order_after')->escaped() . "</option>
				</select>

				<label for='setting_visible' class='label_above'>" . wfMessage('setting_visible')->escaped() . "</label>
				<select id='setting_visible' name='setting_visible'>
					<option value='-1' " . ($setting->isSettingVisible() ? ' selected="selected"' : '') . ">" . wfMessage('setting_visible_yes')->escaped() . "</option>
					<option value='0' " . (!$setting->isSettingVisible() ? ' selected="selected"' : '') . ">" . wfMessage('setting_visible_no')->escaped() . "</option>
				</select>

				<br/><h2>" . wfMessage('choose_extension_or_group')->escaped() . "</h2>
				<label for='allowed_extension_md5_key' class='label_above'>" . wfMessage('belongs_to_extension')->escaped() . "</label>
				<select id='allowed_extension_md5_key' name='allowed_extension_md5_key'>
					<option value='0'" . (!$setting->getAllowedExtensionMd5Key() ? ' selected="selected"' : '') . ">&nbsp;</option>
				";
		foreach ($allowedExtensions as $allowedExtension) {
			if ($allowedExtension->isDeleted()) {
				continue;
			}
			$html .= "
							<option value='{$allowedExtension->getExtensionKey()}'" . ($setting->getAllowedExtensionMd5Key() === $allowedExtension->getExtensionKey() ? ' selected="selected"' : '') . ">{$allowedExtension->getExtensionName()}</option>
					";
		}
				$html .= "
				</select>

				<label for='setting_group_name' class='label_above'>" . wfMessage('setting_group_name')->escaped() . "</label>
				<input id='setting_group_name' name='setting_group_name' type='text' value='" . htmlentities($setting->getSettingGroupName(), ENT_QUOTES) . "'/>
			</fieldset>
			<fieldset id='commit_fieldset'>
				" . (isset($errors['commit_message']) ? '<span class="error">' . $errors['commit_message'] . '</span>' : '') . "
				<label for='commit_message' class='label_above'>" . wfMessage('commit_message')->escaped() . "</label>
				<input id='commit_message' name='commit_message' type='text' value='" . htmlentities(trim($wgRequest->getText('commit_message')), ENT_QUOTES) . "'/>
				<input id='asid' name='asid' type='hidden' value='{$setting->getId()}'/><br/>
				<button id='wiki_submit' name='wiki_submit' class='mw-ui-button mw-ui-progressive'>" . wfMessage('save_key_value')->escaped() . "</button>
			</fieldset>
		</form>";

		return $html;
	}

	/**
	 * Allowed Setting Deletion Form
	 *
	 * @param array Setting information
	 *
	 * @return string Built HTML
	 */
	public function allowedSettingsDelete($setting) {
		$action = Title::newFromText('Special:WikiAllowedSettings');
		$html = "
		<form method='post' action='" . $action->getFullUrl(['section' => 'setting', 'action' => 'delete']) . "'>
			" . wfMessage('delete_setting_confirm')->escaped() . "<br/>
			<input type='hidden' name='do' value='confirm'/>
			<input type='hidden' name='asid' value='{$setting->getId()}'/>
			<button type='submit' class='mw-ui-button mw-ui-destructive'>" . wfMessage('delete_setting')->escaped() . "</button>
		</form>";

		return $html;
	}
}
