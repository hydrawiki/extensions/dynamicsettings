<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Wiki Advertisements Special Page
 *
 * @author    Brent Copeland
 * @copyright (c) 2014 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
 **/

use DynamicSettings\Lock;
use DynamicSettings\Wiki\Advertisements;

class SpecialWikiAdvertisements extends DynamicSettings\SpecialPage {
	/**
	 * Output HTML
	 *
	 * @var string
	 */
	private $content;

	/**
	 * Wiki object storage for form editing.
	 *
	 * @var array
	 */
	private $wiki;

	/**
	 * Valid actions for this page.
	 *
	 * @var array
	 */
	private $validActions = [
		'disableAds',
		'disableSlotsToggle',
		'enableAds',
		'edit',
		'massedit',
		'resetSearch',
		'revert',
		'search'
	];

	/**
	 * Current action.
	 *
	 * @var string|null
	 */
	private $action = null;

	/**
	 * Main Constructor
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct('WikiAdvertisements', 'wiki_advertisements');
	}

	/**
	 * Main Executor
	 *
	 * @param string Sub page passed in the URL.
	 *
	 * @return void [Outputs to screen]
	 */
	public function execute($subpage) {
		$this->checkPermissions();

		if (Lock::isLocked() == true) {
			$this->output->showErrorPage('dynamic_settings_error', 'error_settings_locked');
			return;
		}

		$this->siteSettings = new \DynamicSettings\Sites();

		$this->output->addModuleStyles(['ext.wikiAdvertisements.styles']);
		$this->output->addModules(['ext.wikiAdvertisements.scripts']);

		$this->setHeaders();

		if (in_array($this->getRequest()->getVal('action'), $this->validActions)) {
			$this->action = $this->getRequest()->getVal('action');
		}

		switch ($this->wgRequest->getVal('section')) {
			default:
			case 'list':
				$this->wikiAdList();
				break;
			case 'wiki':
				$this->wikiAdForm();
				break;
			case 'recache':
				$this->recache();
				break;
		}

		$this->output->addHTML($this->content);
	}

	/**
	 * Ad List
	 *
	 * @return void [Outputs to screen]
	 */
	public function wikiAdList() {
		$start = $this->wgRequest->getInt('st');
		$itemsPerPage = 100;
		$searchTerm = null;
		$cookieExpire = time() + 900;

		if ($this->action == 'resetSearch') {
			$this->wgRequest->response()->setcookie('wikiAdsSearchTerm', '', 1);
		} else {
			$listSearch = $this->wgRequest->getVal('list_search');
			$cookieSearch = $this->wgRequest->getCookie('wikiAdsSearchTerm');
			if (($this->action == 'search' && !empty($listSearch)) || !empty($cookieSearch)) {
				if (!empty($cookieSearch) && empty($listSearch)) {
					$searchTerm = $cookieSearch;
				} else {
					$searchTerm = $listSearch;
				}
				$this->wgRequest->response()->setcookie('wikiAdsSearchTerm', $searchTerm, $cookieExpire);
			}
		}

		if ($this->wgRequest->getCookie('wikiAdsSortKey') && !$this->wgRequest->getVal('sort')) {
			$sort = $this->wgRequest->getCookie('wikiAdsSortKey');
		} else {
			$sort = $this->wgRequest->getVal('sort');
		}
		if (!empty($sort)) {
			$sortKey = $sort;
		} else {
			$sortKey = 'wiki_name';
		}
		$extra['sort'] = $sortKey;
		$this->wgRequest->response()->setcookie('wikiAdsSortKey', $sortKey, $cookieExpire);

		$sortDir = $this->wgRequest->getVal('sort_dir');
		if ((strtolower($this->wgRequest->getCookie('wikiAdsSortDir')) === 'desc' && !$sortDir) || strtolower($sortDir) === 'desc') {
			$sortDir = 'desc';
		} else {
			$sortDir = 'asc';
		}
		$extra['sort_dir'] = $sortDir;
		$this->wgRequest->response()->setcookie('wikiAdsSortDir', $sortDir, $cookieExpire);

		$this->wikis = Advertisements::loadFromSearch($start, $itemsPerPage, $searchTerm, $sortKey, $sortDir, true);

		$total = Advertisements::getLastSearchTotal();

		$pagination = HydraCore::generatePaginationHtml($this->getFullTitle(), $total, $itemsPerPage, $start, 4, $extra);

		$this->output->setPageTitle(wfMessage('wikiadvertisements')->escaped());
		$this->content = \TemplateWikiAdvertisements::wikiAdvertisements(
			$this->wikis,
			$pagination,
			$sortKey,
			strtolower($sortDir),
			$searchTerm
		);
	}

	/**
	 * Wiki Advertisements Form
	 *
	 * @return void [Outputs to screen]
	 */
	public function wikiAdForm() {
		$siteKey = $this->wgRequest->getVal('siteKey');

		if (Lock::isLocked($siteKey) == true) {
			$this->output->showErrorPage('dynamic_settings_error', 'error_wiki_locked');
			return;
		}

		$defaultAdsWiki = \DynamicSettings\Wiki::loadFromNew();
		$defaultAdsWiki->setName('Default Ad Tags');
		if ($siteKey == '-1' && ($this->action != 'revert' || $this->action == 'disableSlotsToggle')) {
			$this->wiki = $defaultAdsWiki;
			$defaultAds = true;
		} else {
			$defaultAds = false;
			$this->wiki = \DynamicSettings\Wiki::loadFromHash($siteKey);
		}

		if ($defaultAds && !$this->getUser()->isAllowed('wiki_advertisements_defaults')) {
			throw new PermissionsError('wiki_advertisements_defaults');
		}

		if ($this->wiki === false) {
			$this->output->showErrorPage('advertisements_error', 'wiki_not_found');
			return;
		}

		if ($this->wiki->isDeleted()) {
			$this->output->showErrorPage('advertisements_error', 'deleted_wikis_no_modifications');
			return;
		}

		if ($defaultAds === false) {
			if ($this->action == 'disableSlotsToggle') {
				$this->wiki->getAdvertisements()->setDisabled(!$this->wiki->getAdvertisements()->isDisabled());
				$this->wiki->getAdvertisements()->save();

				$this->output->redirect($this->getPageTitle()->getFullURL());
				return;
			}
			if ($this->action == 'disableAds' || $this->action == 'enableAds') {
				$advertisements = $this->wiki->getAdvertisements();
				$config = $advertisements->getConfig();
				foreach (Advertisements::getAdSlots() as $slot) {
					if ($this->action == 'disableAds') {
						$config[$slot] = Advertisements::SLOT_DISABLE;
					} else {
						$config[$slot] = Advertisements::SLOT_APPEND;
					}
				}
				$advertisements->setConfig($config);
				$advertisements->save();

				$this->output->redirect($this->getPageTitle()->getFullURL());
				return;
			}
			if ($this->action == 'revert') {
				if ($this->getRequest()->wasPosted() && $this->wgRequest->getVal('do') == 'confirm') {
					$this->DB->startAtomic(__METHOD__);
					$this->DB->delete(
						'wiki_advertisements',
						['site_key' => $this->wiki->getSiteKey()],
						__METHOD__
					);
					$this->DB->endAtomic(__METHOD__);

					$this->output->redirect($this->getPageTitle()->getFullURL());
				}
				$this->output->setPageTitle(wfMessage('revert_ads') . ' - ' . $this->wiki->getName());
				$this->content = \TemplateWikiAdvertisements::wikiAdRevert($this->wiki);
				return;
			}
		}

		$return = $this->wikiAdSave();

		$this->output->setPageTitle(wfMessage('edit_advertisements')->escaped() . ' - ' . $this->wiki->getNameForDisplay());
		$this->content = \TemplateWikiAdvertisements::wikiAdForm($this->wiki, $return['errors']);
	}

	/**
	 * Saves submitted advertisements forms.
	 *
	 * @return array Array containing an array of processed form information and array of corresponding errors.
	 */
	private function wikiAdSave() {
		$errors = [];
		if ($this->getRequest()->wasPosted() && $this->getRequest()->getVal('do') == 'save') {
			$advertisements = $this->wiki->getAdvertisements();
			$config = [];
			foreach (Advertisements::getSlots() as $slot) {
				$state = $this->wgRequest->getInt($slot . '_state', false);
				if ($state !== false && $state >= Advertisements::SLOT_DISABLE && $state <= Advertisements::SLOT_OVERRIDE) {
					$config[$slot] = $state;
				}

				if (!$advertisements->setBySlot($slot, $this->wgRequest->getText($slot), $error)) {
					$lineErrors = explode("\n", trim($error));
					$lineErrors = array_unique($lineErrors);
					sort($lineErrors);
					$errors[$slot] = wfMessage('error_ads_any', $slot, implode("<br/>", $lineErrors))->parse();
				}
			}

			if (!count($errors)) {
				$advertisements->setConfig($config);
				if ($advertisements->save()) {
					$this->output->redirect($this->getPageTitle()->getFullURL());
					return;
				}
			}
		}
		return ['errors' => $errors];
	}

	/**
	 * Perform a recache of advertisements from the web interface.
	 *
	 * @return void
	 */
	public function recache() {
		if (!$this->wgUser->isAllowed('wiki_recache_ads')) {
			throw new PermissionsError('wiki_recache_ads');
		}

		$parameters = ['task' => 'recacheAds', 'dist' => true];

		$siteKey = $this->wgRequest->getVal('siteKey');
		if (!empty($siteKey)) {
			$wiki = \DynamicSettings\Wiki::loadFromHash($siteKey);
			if (!$wiki) {
				throw new \ErrorPageError(wfMessage('tools_recache_error'), wfMessage('wiki_not_found'));
			}
			$parameters['sitekey'] = $wiki->getSiteKey();
		} else {
			$parameters['all'] = true;
		}

		ob_start();
		$html_errors = ini_get('html_errors');
		ini_set('html_errors', '0');
		try {
			$recache = new \DynamicSettings\Tools;
			$recache->loadParamsAndArgs('Tools', $parameters);
			$success = $recache->execute();
		} catch (Exception $e) {
			$recacheOut .= $e->getMessage() . "\n";
		}

		if ($success) {
			$this->output->setPageTitle(wfMessage('tools_recache_successful')->escaped());
		} else {
			$this->output->setPageTitle(wfMessage('tools_recache_error')->escaped());
		}

		ini_set('html_errors', $html_errors);
		$recacheOut .= ob_get_contents();
		ob_end_clean();

		$templates = new TemplateWikiSites();
		$this->content = $templates->runToolOutput($recacheOut, 'WikiAdvertisements', 'recacheAds');
	}
}
