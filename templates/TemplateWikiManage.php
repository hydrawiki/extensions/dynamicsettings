<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Wiki Manage Skin
 *
 * @author    Alexia E. Smith
 * @copyright (c) 2018 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
**/

class TemplateWikiManage {
	/**
	 * Wiki Manage Form
	 *
	 * @param object Wiki object
	 * @param object User viewing this form.
	 *
	 * @return string Built HTML
	 */
	public static function manageForm($wiki, $user) {
		$sections = [
			'settings',
			'extensions',
			'group_permissions',
			'namespaces',
			'tools_log',
			'edit_log',
			'promotions',
			'advertisements'
		];
		foreach ($sections as $section) {
			if ($user->isAllowed('wiki_' . $section)) {
				$tabs[] = "<div>" . $section . "</div>";
			}
		}
		return implode("<br/>", $tabs);
	}
}
