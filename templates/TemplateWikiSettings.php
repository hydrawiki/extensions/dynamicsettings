<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Wiki Settings Skin
 *
 * @author    Alex Smith
 * @copyright (c) 2014 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
**/

class TemplateWikiSettings {
	/**
	 * Wiki Settings Form
	 *
	 * @param object Wiki object.
	 * @param array Form Errors
	 *
	 * @return string Built HTML
	 */
	public function settingsForm($wiki, $errors) {
		global $wgRequest;

		$HTMLprefix = '';
		$toc = [];

		if ($wgRequest->getVal('extension_message') == 'true') {
			$page = Title::newFromText('Special:WikiSites');
			$url = $page->getFullURL();
			$HTMLprefix = "<div class='successbox'>" . wfMessage('extensions_have_changed') . "<br/>
			<a href='{$url}'>" . wfMessage('return_to_wiki_sites') . "</a></div>";
		}

		if (count($errors)) {
			$HTMLprefix = "<div class='errorbox'>" . wfMessage('error_save_submit')->text() . "<br /><ul>";
			foreach ($errors as $field => $error) {
				if (is_numeric($field)) {
					$anchor = '#setting-wsf_' . $field;
				} else {
					$anchor = '#' . $field;
				}
				$error = Html::element('a', ['href' => $anchor], $error);
				$HTMLprefix .= '<li><span class="error">' . $error . '</span></li>';
			}
			$HTMLprefix .= "</ul></div>";
		}

		$page = Title::newFromText('Special:WikiSettings');
		$html = "
		<form id='wiki_settings_form' method='post' action='{$page->getFullURL(['do' => 'save'])}'>";
		$settings = $wiki->getSettings();
		if ($settings->count()) {
			$lastGroupName = false;
			foreach ($settings as $asid => $info) {
				if ($lastGroupName !== false && $info->getSettingGroupName() && $info->getSettingGroupName() !== $lastGroupName) {
					if (isset($lastGroupName)) {
						$html .= '</fieldset>';
					}
					$settingID = htmlentities(strtolower(str_replace(' ', '-', $info->getSettingGroupName())), ENT_QUOTES);
					$toc[] = "<li><a href='#$settingID'>{$info->getSettingGroupName()}</a></li>";
					$html .= "<fieldset id=" . $settingID . "><legend>{$info->getSettingGroupName()}</legend>";
				}
				$lastGroupName = $info->getSettingGroupName();
				$html .= "<div data-key='wsf_{$asid}' id='setting-wsf_{$asid}' class='fake_fieldset'>";
				if (isset($errors[$asid])) {
					$html .= '<span class="error">' . $errors[$asid] . '</span>';
				}
				$html .= "<label for='wsf_{$asid}' class='label_above'>{$info->getSettingKey()}</label>";
				if (!empty($info->getSettingExample())) {
					$html .= '<p class="example">' . wfMessage('example')->escaped() . ': ' . htmlentities($info->getSettingExample(), ENT_QUOTES) . '</p>';
				}

				// Get processed values from form
				$value = htmlentities($info->getValueForForm(), ENT_QUOTES);

				switch ($info->getSettingType()) {
					case 'string':
						$html .= "<input name='wsf_{$asid}' class='input_string' type='text' value='{$value}'/>";
						break;
					case 'text':
						$html .= "<textarea name='wsf_{$asid}' class='input_text'>{$value}</textarea>";
						break;
					case 'integer':
						$html .= "<input name='wsf_{$asid}' class='input_integer' type='text' value='{$value}'/>";
						break;
					case 'boolean':
						$html .= "<span class='yesno_yes'><input name='wsf_{$asid}' type='radio' value='true'" . ($value ? " checked='checked'" : '') . "><label for='wsf_{$asid}_yes'>Yes</label></span><span class='yesno_no'><input name='wsf_{$asid}' type='radio' value='false'" . (!$value ? " checked='checked'" : '') . "><label for='wsf_{$asid}_no'>No</label></span>";
						break;
					case 'array':
						$html .= "
						<input name='wsf_{$asid}' class='array_builder_input' type='hidden' value='{$value}'/>
						";
						break;
					case 'define':
						$html .= "<input name='wsf_{$asid}' class='input_string' type='text' value='{$value}'/>";
						break;
				}
				$html .= "</div>";
			}
		}
		$html .= "
			</fieldset>
			<fieldset id='commit_fieldset'>
				" . (isset($errors['commit_message']) ? '<span class="error">' . $errors['commit_message'] . '</span>' : '') . "
				<label for='commit_message' class='label_above'>" . wfMessage('commit_message')->escaped() . "</label>
				<input id='commit_message' name='commit_message' type='text' value='" . htmlentities(trim($wgRequest->getText('commit_message')), ENT_QUOTES) . "'/><br/>
				<input id='wiki_id' name='siteKey' type='hidden' value='{$wiki->getSiteKey()}'/>
				<button id='wiki_submit' name='wiki_submit' type='submit' class='mw-ui-button mw-ui-progressive'>" . wfMessage('save_key_value')->escaped() . "</button>
			</fieldset>
		</form>";

		if (count($toc) > 1) {
			$HTMLtoc = '<h1>' . wfMessage('site_settings_toc_header')->escaped() . '</h1><ol>' . implode($toc) . '</ol>';
		} else {
			$HTMLtoc = '';
		}

		return $HTMLprefix . $HTMLtoc . $html;
	}
}
