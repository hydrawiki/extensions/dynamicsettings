<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * CheckPhpSyntax Trait
 *
 * @author    Alexia Smith
 * @copyright (c) 2018 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
 **/

namespace DynamicSettings\Traits;

trait CheckPhpSyntaxTrait {
	/**
	 * Run PHP code through a syntax checker.
	 *
	 * @param string PHP Code to check
	 *
	 * @return array Array of syntax information
	 */
	private static function checkPHPSyntax($php) {
		$tempFile = tempnam(sys_get_temp_dir(), 'syntax');
		$errorMsgLine = ' in ' . $tempFile;

		$syntax = [
			'is_valid'	=> false,
			'error'		=> '',
			'temp_file'	=> $tempFile
		];

		file_put_contents($tempFile, $php);

		exec('php -l ' . $tempFile . ' 2>&1', $output);

		if (count($output)) {
			foreach ($output as $string) {
				if (!$string) {
					continue;
				}

				if (strpos($string, 'Parse error') !== false) {
					$syntax['error'] = str_replace($errorMsgLine, '', $string);
					if (preg_match('#line (?P<line>\d*)#is', $syntax['error'], $matches)) {
						$syntax['line'] = intval($matches['line']);
					}
				}

				if (strpos($string, 'No syntax errors detected') !== false) {
					$syntax['is_valid'] = true;
					break;
				}
			}
		}

		return $syntax;
	}
}
