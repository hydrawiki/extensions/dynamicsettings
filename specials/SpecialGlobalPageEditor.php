<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Global Page Editor Special Page
 *
 * @author    Alexia E. Smith
 * @copyright (c) 2015 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
**/

class SpecialGlobalPageEditor extends DynamicSettings\SpecialPage {
	/**
	 * Output HTML
	 *
	 * @var string
	 */
	private $content;

	/**
	 * Wiki object storage for form editing.
	 *
	 * @var array
	 */
	private $wiki;

	/**
	 * Main Constructor
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct('GlobalPageEditor', 'global_page_editor');

		$this->wgRequest	= $this->getRequest();
		$this->output		= $this->getOutput();
	}

	/**
	 * Main Executor
	 *
	 * @param string Sub page passed in the URL.
	 *
	 * @return void [Outputs to screen]
	 */
	public function execute($subpage) {
		$this->checkPermissions();

		$this->output->addModuleStyles(['ext.globalPageEditor.styles']);
		$this->output->addModules(['ext.globalPageEditor.scripts']);

		$this->templates = new TemplateGlobalPageEditor;

		$this->setHeaders();

		if ($subpage == "queue") {
			$this->showQueue();
		} elseif (strpos($subpage, 'status/') === 0) {
			list(, $identifier) = explode('/', $subpage);
			$this->queueStatus($identifier);
		} else {
			$this->globalPageEditor();
		}

		$this->output->addHTML($this->content);
	}

	/**
	 * Global Editor Form
	 *
	 * @return void [Outputs to screen]
	 */
	public function globalPageEditor() {
		$form = [
			'options'		=> [
				'titles'			=> [],
				'everywhere'		=> 0,
				'preview'			=> false,
				'queue'				=> false,
				'find_replace'		=> [
					[
						'find'				=> '',
						'replace'			=> '',
						'type'				=> 0,
						'ignore_whitespace'	=> false,
						'ignore_case'		=> false,
						'wrap_around'		=> true,
						'create_new_pages'	=> false
					]
				],
				'wikis'					=> [],
				'gpe_action'			=> null,
				'gpe_action_options'	=> null,
				'identifier'			=> null
			],
			'sample_text'			=> '',
			'new_page_content'		=> '',
			'preview'				=> '',
			'success'				=> false
		];

		if ($this->wgRequest->wasPosted()) {
			$jsonForm = $this->wgRequest->getText('jsonForm');
			if ($jsonForm) {
				$form = json_decode($jsonForm, 1);
			} else {
				$form = $this->processForm();
			}
		}

		if (is_array($form['sample_text'])) {
			$form['sample_text'] = $form['sample_text'][0];
		}

		if (is_array($form['new_page_content'])) {
			$form['new_page_content'] = $form['new_page_content'][0];
		}

		$this->output->setPageTitle(wfMessage('globalpageeditor')->escaped());
		$this->content = $this->templates->globalPageEditor($form['options'], $form['sample_text'], $form['new_page_content'], $form['preview'], $form['success']);
	}

	/**
	 * Process GPE Form
	 *
	 * @return array Processed options, wikis, and
	 */
	private function processForm() {
		$isPreview = $this->wgRequest->getCheck('preview');
		$isQueue = $this->wgRequest->getCheck('queue');
		$sampleText = $this->wgRequest->getText('sample_text');
		$newPageContent = $this->wgRequest->getText('new_page_content');

		$_titles = $this->wgRequest->getText('titles');
		$_titles = str_replace(["\r\n", "\r"], "\n", $_titles);
		$titles = explode("\n", $_titles);
		$titles = array_map("trim", $titles);
		$titles = array_unique($titles);

		foreach ($titles as $key => $title) {
			if (empty($title)) {
				unset($titles[$key]);
			}
		}

		$action = $this->wgRequest->getText('gpe_action');

		$options = [
			'titles'			=> (!is_array($titles) ? [] : $titles),
			'everywhere'		=> $this->wgRequest->getCheck('everywhere'),
			'preview'			=> $isPreview,
			'queue'				=> $isQueue,
			'gpe_action'			=> $action
		];

		$options['gpe_action_options'] = $this->wgRequest->getArray('gpe_action_options');

		switch ($action) {
			case "find_and_replace":
				$options['find_replace'] = [
					[
						'find'				=> $this->wgRequest->getText('find'),
						'replace'			=> $this->wgRequest->getText('replace'),
						'type'				=> $this->wgRequest->getInt('type'),
						'ignore_case'		=> $this->wgRequest->getBool('ignore_case'),
						'ignore_whitespace'	=> $this->wgRequest->getBool('ignore_whitespace'),
						'wrap_around'		=> $this->wgRequest->getBool('wrap_around'),
						'create_new_pages'	=> $this->wgRequest->getBool('create_new_pages'),
						'new_page_content'	=> $newPageContent,
						'limit'				=> null
					]
				];
			break;
			case "page_move":
				$_moveTitles = $this->wgRequest->getText('move_titles');
				$_moveTitles = str_replace(["\r\n", "\r"], "\n", $_moveTitles);
				$moveTitles = explode("\n", $_moveTitles);
				$moveTitles = array_map("trim", $moveTitles);
				$options['move_titles'] = $moveTitles;
				$moves = [];
				foreach ($titles as $index => $value) {
					if (isset($moveTitles[$index])) {
						$moves[$value] = $moveTitles[$index];
					}
				}
				$options['page_move'] = $moves;
			break;
			case "page_rollback":
			case "page_delete":
			case "page_protect":
				$options[$action] = $titles;
			break;

		}

		$wikis = $this->wgRequest->getVal('wikis');
		$wikis = @json_decode($wikis, true);

		$options['wikis']['added'] = [];
		if (is_array($wikis['added']) && count($wikis['added'])) {
			foreach ($wikis['added'] as $siteKey) {
				if (strlen($siteKey) == 32) {
					$options['wikis']['added'][] = $siteKey;
				}
			}
		}
		$options['wikis']['removed'] = [];
		if (is_array($wikis['removed']) && count($wikis['removed'])) {
			foreach ($wikis['removed'] as $siteKey) {
				if (strlen($siteKey) == 32) {
					$options['wikis']['removed'][] = $siteKey;
				}
			}
		}

		$identifier = sha1(var_export($options, true));

		$form = [
			'options'			=> $options,
			'sample_text'		=> $sampleText,
			'new_page_content' 	=> $newPageContent
		];

		$preview = false;
		if ($isQueue) {
			$success = \DynamicSettings\Job\GlobalPageRunner::queue(
				[
					'operations'	=> [$options],
					'identifier'	=> $identifier,
					'form'			=> $form
				]
			);
			$options['identifier'] = $identifier;
		} else {
			switch ($action) {
				case "find_and_replace":
					$fr = new \DynamicSettings\FindAndReplace;
					foreach ($options['find_replace'] as $far) {
						$frIdentifiers[] = $fr->addReplacement($far['find'], $far['replace'], $far['limit'], $far['ignore_case'], $far['ignore_whitespace'], $far['wrap_around'], $far['type']);
					}
					$preview = $fr->doReplacements($sampleText);
				break;
				case "page_move":
					$preview = "Moving: \n";
					foreach ($moves as $from => $to) {
						$preview .= $from . " => " . $to . "\n";
					}
				break;
				case "page_rollback":
					$preview = "The following pages will be rolled back by 1 revision:\n" . implode("\n", $titles);
				break;
				case "page_delete":
					$preview = "The following pages will be deleted:\n" . implode("\n", $titles);
				break;
				case "page_protect":
					$preview = "The following pages will be protected:\n" . implode("\n", $titles);
				break;
			}
		}

		return [
			'options'			=> $options,
			'wikis'				=> $wikis,
			'sample_text'		=> $sampleText,
			'new_page_content'	=> $newPageContent,
			'preview'			=> $preview,
			'success'			=> $preview !== false || $success === true
		];
	}

	/**
	 * Show the queue status page.
	 *
	 * @param string Identifier Key
	 *
	 * @return void [Outputs to screen]
	 */
	private function queueStatus($identifier) {
		$start = $this->wgRequest->getInt('st');
		$itemsPerPage = 100;

		$redis = \RedisCache::getClient('cache');

		$key = \DynamicSettings\GlobalPageEdit::getRedisQueueStatusKey($identifier);

		$hasStatus = $redis->exists($key);

		$status = [
			'identifiers'	=> [],
			'result'		=> []
		];
		if ($hasStatus) {
			$rawData = $redis->lrange($key, $start, ($start + $itemsPerPage - 1));

			foreach ($rawData as $rawStatus) {
				$_status = @json_decode($rawStatus, true);
				$status = array_merge_recursive($status, $_status);
			}
		}

		$total = $redis->llen($key);
		$pagination = HydraCore::generatePaginationHtml($this->getFullTitle(), $total, $itemsPerPage, $start, 4);

		if (isset($status['form'])) {
			// Build a JSON object to throw into a form for run-again purposes.
			$ra = $status['form'];
			$ra['options']['preview'] = false;
			$ra['options']['queue'] = false;
			$ra['options']['titles'] = array_unique($ra['options']['titles']);

			/*remove duplicates - todo: figure out why there are dupes*/
			$ra['options']['wikis']['added'] = array_unique($ra['options']['wikis']['added']);
			$ra['options']['wikis']['removed'] = array_unique($ra['options']['wikis']['removed']);

			$runAgain = htmlspecialchars(json_encode($ra));

			if ($ra['options']['gpe_action'] == "find_and_replace") {
				$ra['options']['gpe_action'] = "page_rollback";
				$ra['options']['titles'] = array_unique($ra['options']['titles']);
				if (isset($ra['options'])) {
					unset($ra['options']['find_replace']);
				}
				$revert = htmlspecialchars(json_encode($ra));
			}

		} else {
			$runAgain = false;
			$revert = false;
		}

		$this->output->setPageTitle(wfMessage('gpe_queue_status', (!empty($identifier) ? $identifier : '...'))->escaped());
		$this->content = $this->templates->queueStatus($identifier, $hasStatus, $status, $pagination, $runAgain, $revert);
	}

	/**
	 * Show the queue page.
	 *
	 * @return void [Outputs to screen]
	 */
	private function showQueue() {
		$start = $this->wgRequest->getInt('st');
		$itemsPerPage = 25;

		$redis = \RedisCache::getClient('cache');
		$entries = $redis->lRange('gpe:queue', $start, ($start + $itemsPerPage - 1));
		$total = $redis->lSize('gpe:queue');

		$pagination = HydraCore::generatePaginationHtml($this->getFullTitle(), $total, $itemsPerPage, $start, 4);

		$data = [];

		foreach ($entries as $key) {
			if (!empty($key)) {
				$status = [
					'identifiers'	=> [],
					'result'		=> []
				];
				$hasStatus = $redis->exists($key);
				if ($hasStatus) {
					$length = $redis->llen($key);
					$rawData = $redis->lrange($key, 0, $length);
					foreach ($rawData as $rawStatus) {
						$_status = @json_decode($rawStatus, true);
						$status = array_merge_recursive($status, $_status);
					}
				}

				$states = [];

				foreach ($status['result'] as $results) {
					foreach ($results as $keys) {
						if (isset($keys['result'])) {
							if (!is_array($keys['result'])) {
								$keys['result'] = [$keys['result']];
							}
							foreach ($keys['result'] as $result) {
								if (isset($states[$result])) {
									$states[$result]++;
								} else {
									$states[$result] = 1;
								}
							}
						}
					}
				}

				$data[] = [
					'key' => $key,
					'states' => $states,
					'status' => $status
				];
			}
		}

		$this->output->setPageTitle(wfMessage('globalpageeditor-queue')->escaped());
		$this->content = $this->templates->queue($data, $pagination);
	}
}
