<?php
if (isset($_SERVER['PHP_ENV']) && $_SERVER['PHP_ENV'] === 'development' && is_file($IP . '/settings/LocalDevSettingsBefore.php')) {
	// Before Site Detection
	require $IP . '/settings/LocalDevSettingsBefore.php';
}

/******************************************/
/* Site Detection                         */
/******************************************/
// These settings are not really meant to be used outside the settings bootstrap.
// They can be referenced in LocalSettings.php if really needed.
$dsHost = str_ireplace('www.', '', (isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : ''));
$dsHostDir = $IP . '/sites/' . $dsHost;
$siteSettings = "{$dsHostDir}/LocalSettings.php";

if (!function_exists('recacheSettings')) {

	/**
	 * Recache Site Settings
	 * Returns true if at least the main 'recache' task was successful as that is the minimum for the site to come online.
	 *
	 * @access public
	 * @param  string	Site LocalSettings.php path.
	 * @return boolean	Successful Recache
	 */
	function recacheSettings($host) {
		global $IP;

		if (!filter_var('http://' . $host, FILTER_VALIDATE_URL)) {
			return false;
		}
		$recacheCommand = "export PHP_ENV=" . escapeshellarg($_SERVER['PHP_ENV']) . "; " . PHP_BINDIR . DIRECTORY_SEPARATOR . "php " . $IP . "/extensions/DynamicSettings/maintenance/Tools.php --task=recache,rebuildLanguage,recacheAds,recachePromotions --json --domain=" . escapeshellarg($host);
		$raw = shell_exec($recacheCommand);
		$json = json_decode($raw, true);
		if (is_array($json) && isset($json['tasks']['recache']['success'])) {
			if (isset($json['fatal'])) {
				return false;
			}
			return boolval($json['tasks']['recache']['success']);
		}
		return false;
	}

}

if (!$dsHost || !is_dir($dsHostDir)) {
	$masterHost = str_ireplace(['http://', 'https://', 'www.'], '', ltrim($wgServer, '/'));
	if ($dsHost != $masterHost && !defined('SETTINGS_ONLY') && PHP_SAPI != 'cli') {
		$doRedirect = true;
		if (strlen($dsHost) && !is_dir($dsHostDir)) {
			if (recacheSettings($dsHost)) {
				if (is_dir($dsHostDir) && is_readable($siteSettings)) {
					$doRedirect = false;
				}
			}
		}
		if ($doRedirect) {
			header('HTTP/1.1 410 Gone');
			$output = file_get_contents(__DIR__.'/410Gone.html');
			$output = str_replace('{{BARE_DOMAIN}}', $bareDomain, $output);
			echo $output;
			exit;
		}
	} else {
		$dsHost = $masterHost;
		$dsHostDir = $IP . '/settings';
		$siteSettings = "{$dsHostDir}/LocalSettingsMaster.php";
	}
}

// Strip www. off host names and send them to the correct spot first.
if (isset($_SERVER['HTTP_HOST']) && stripos($_SERVER['HTTP_HOST'], 'www.') !== false) {
	// No www., or capes!
	header('HTTP/1.1 301 Moved Permanently');
	header('Location: https://' . $dsHost . $_SERVER['REQUEST_URI']);
	exit;
}

/******************************************/
/* Bootstrap Individal Wiki Settings      */
/******************************************/

// All instances of require() below must be done within the global scope or variables will be lost.
if (is_readable($siteSettings)) {
	// If file readable, try loading it.
	try {
		require $siteSettings;
	} catch (Exception $e) {
		http_response_code(500);
		throw new Exception("LocalSettings.php cache file missing or unreadable for site {$dsHost}.  Run the recache script or fix the cache share.  Original Error: " . $e->getMessage());
	}
} else {
	// Lets perform an automatic recache for the missing or unreadable file.
	if (recacheSettings($dsHost)) {
		try {
			require $siteSettings;
		} catch (Exception $e) {
			http_response_code(500);
			throw new Exception("LocalSettings.php cache file for site {$dsHost} seems to contain errors.  Running the recache script may resolve this.  Original Error: " . $e->getMessage());
		}
	} else {
		http_response_code(500);
		throw new Exception("LocalSettings.php cache file missing or unreadable for site {$dsHost}.  Run the recache script or fix the sites folder permissions.");
	}
}

if (isset($_SERVER['PHP_ENV']) && $_SERVER['PHP_ENV'] === 'development' && is_file($IP . '/settings/LocalDevSettingsAfter.php')) {
	// After LocalSettings.php include.
	require $IP . '/settings/LocalDevSettingsAfter.php';
}
