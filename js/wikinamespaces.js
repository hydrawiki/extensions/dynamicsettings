function WikiNamespaces($) {
	'use strict';
	this.init = function() {
		$(document).on('click', '.add_namespace', namespace.addNamespace);
		$(document).on('click', '.remove_namespace', namespace.removeNamespace);
	};

	var namespace = {
		addNamespace: function(e) {
			var template = $('#hidden_namespace_template').html();
			$(template).insertBefore('#commit_fieldset');
		},

		removeNamespace: function(e) {
			var parent = $(this).parent();
			$(parent).remove();
		}
	};
}

var WN = new WikiNamespaces(jQuery);
jQuery(document).ready(WN.init);