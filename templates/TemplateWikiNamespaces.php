<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Wiki Namespaces Skin
 *
 * @author    Alex Smith
 * @copyright (c) 2014 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
**/

class TemplateWikiNamespaces {
	/**
	 * Output HTML
	 *
	 * @var string
	 */
	private $HMTL;

	/**
	 * Site Namespace Form
	 *
	 * @param array Array of wiki information.
	 * @param array Site Namespaces
	 * @param array Form Errors
	 *
	 * @return string Built HTML
	 */
	public function namespacesForm($wiki, $namespaces, $errors) {
		global $wgRequest;

		$page = Title::newFromText('Special:WikiNamespaces');

		$html = "
		<form id='wiki_settings_form' method='post' action='{$page->getFullURL(['do' => 'save'])}'>
			<strong>" . wfMessage('namespace_registration')->parse() . "</strong>
			<input type='button' value='" . wfMessage('add_namespace')->escaped() . "' class='add_namespace mw-ui-button'>
		";
		if (is_array($namespaces) && count($namespaces)) {
			$i = 0;
			foreach ($namespaces as $snid => $namespace) {
				$html .= $this->namespacesTemplate($namespace, isset($errors[$i]) ? $errors[$i] : []);
				$i++;
			}
		}
		$html .= "
			<fieldset id='commit_fieldset'>
				" . (isset($errors['commit_message']) ? '<span class="error">' . $errors['commit_message'] . '</span>' : '') . "
				<label for='commit_message' class='label_above'>" . wfMessage('commit_message')->escaped() . "</label>
				<input id='commit_message' name='commit_message' type='text' value='" . htmlentities(trim($wgRequest->getText('commit_message')), ENT_QUOTES) . "'/><br/>
				<input id='siteKey' name='siteKey' type='hidden' value='{$wiki->getSiteKey()}'/>
				<button id='wiki_submit' name='wiki_submit' type='submit' class='mw-ui-button mw-ui-progressive'>" . wfMessage('save_key_value')->escaped() . "</button>
			</fieldset>
		</form>
		<div id='hidden_namespace_template'>
			" . $this->namespacesTemplate() . "
		</div>
		";

		return $html;
	}

	/**
	 * Individual Site Namespace Template
	 *
	 * @param object Site Namespace
	 * @param array [Optional] Form Errors
	 *
	 * @return string Built HTML
	 */
	public function namespacesTemplate($namespace = [], $errors = []) {
		global $wgRequest;

		$html = "
			<fieldset class='namespace_fieldset'>
				" . (isset($errors['namespace_name']) ? '<span class="error">' . $errors['namespace_name'] . '</span>' : '') . "
				<label for='namespace_name' class='label_above'>" . wfMessage('namespace_name_desc') . "</label>
				<input id='namespace_name' name='namespace_name[]' type='text' maxlength='255' value='" . (is_object($namespace) ? $namespace->getName() : null) . "'/><br/>

				" . (isset($errors['namespace_alias']) ? '<span class="error">' . $errors['namespace_alias'] . '</span>' : '') . "
				<label for='namespace_alias' class='label_above'>" . wfMessage('namespace_alias_desc') . "</label>
				<input id='namespace_alias' name='namespace_alias[]' type='text' maxlength='255' value='" . (is_object($namespace) ? $namespace->getNameAlias() : null) . "'/><br/>

				" . (isset($errors['namespace_talk_name']) ? '<span class="error">' . $errors['namespace_talk_name'] . '</span>' : '') . "
				<label for='namespace_talk_name' class='label_above'>" . wfMessage('namespace_talk_name_desc') . "</label>
				<input id='namespace_talk_name' name='namespace_talk_name[]' type='text' maxlength='255' value='" . (is_object($namespace) ? $namespace->getTalk() : null) . "'/><br/>

				" . (isset($errors['namespace_talk_alias']) ? '<span class="error">' . $errors['namespace_talk_alias'] . '</span>' : '') . "
				<label for='namespace_talk_alias' class='label_above'>" . wfMessage('namespace_talk_alias_desc') . "</label>
				<input id='namespace_talk_alias' name='namespace_talk_alias[]' type='text' maxlength='255' value='" . (is_object($namespace) ? $namespace->getTalkAlias() : null) . "'/><br/>

				<label for='is_content'>" . wfMessage('is_content_label') . "</label><br/>
				<select id='is_content' name='is_content[]'>
					<option value='0'" . (is_object($namespace) && !$namespace->isContent() ? " selected='selected'" : null) . ">" . wfMessage('no') . "</option>
					<option value='1'" . (is_object($namespace) && $namespace->isContent() ? " selected='selected'" : null) . ">" . wfMessage('yes') . "</option>
				</select><br/>

				<label for='is_searchable'>" . wfMessage('is_searchable_label') . "</label><br/>
				<select id='is_searchable' name='is_searchable[]'>
					<option value='0'" . (is_object($namespace) && !$namespace->isSearchable() ? " selected='selected'" : null) . ">" . wfMessage('no') . "</option>
					<option value='1'" . (is_object($namespace) && $namespace->isSearchable() ? " selected='selected'" : null) . ">" . wfMessage('yes') . "</option>
				</select><br />

				<label for='is_semantic'>" . wfMessage('is_semantic_label') . "</label><br/>
				<select id='is_semantic' name='is_semantic[]'>
					<option value='0'" . (is_object($namespace) && !$namespace->isSemantic() ? " selected='selected'" : null) . ">" . wfMessage('no') . "</option>
					<option value='1'" . (is_object($namespace) && $namespace->isSemantic() ? " selected='selected'" : null) . ">" . wfMessage('yes') . "</option>
				</select>
		";

		if (!is_object($namespace) || !$namespace->getId()) {
			$html .= "<br/><label for='namespace_id'>" . wfMessage('choose_namespace_id') . "</label><br/>
			" . (isset($errors['namespace_id']) ? '<span class="error">' . $errors['namespace_id'] . '</span>' : '') . "
			<input id='namespace_id' name='namespace_id[]' type='text' value='" . (is_object($namespace) ? $namespace->getId() : null) . "'/><br/>
			";
		} else {
			$html .= "<br/><label for='namespace_id'>" . wfMessage('namespace_id') . ":</label> " . $namespace->getId() . "<br/>
			<input id='namespace_id' name='namespace_id[]' type='hidden' value='" . (is_object($namespace) ? $namespace->getId() : null) . "'/>";
		}

		$html .= (isset($errors['namespace_protection']) ? '<span class="error">' . $errors['namespace_protection'] . '</span>' : '') . "
				<label for='namespace_protection' class='label_above'>" . wfMessage('namespace_protection_desc') . "</label>
				<input id='namespace_protection' name='namespace_protection[]' type='text' value='" . (is_object($namespace) ? $namespace->getProtection() : null) . "'/><br/>

				<input id='snid' name='snid[]' type='hidden' value='" . (is_object($namespace) ? $namespace->getDatabaseId() : null) . "'/><br/>
				<input type='button' value='" . wfMessage('remove_namespace') . "' class='remove_namespace mw-ui-button mw-ui-destructive'>
			</fieldset>";

		return $html;
	}
}
