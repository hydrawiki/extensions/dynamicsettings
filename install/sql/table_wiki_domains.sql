CREATE TABLE /*_*/wiki_domains (
  `did` int(11) NOT NULL,
  `site_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(1) NOT NULL
) /*$wgDBTableOptions*/;

ALTER TABLE /*_*/wiki_domains
  ADD PRIMARY KEY (`did`),
  ADD KEY `site_key` (`site_key`,`type`),
  ADD KEY `domain` (`domain`);

ALTER TABLE /*_*/wiki_domains
  MODIFY `did` int(11) NOT NULL AUTO_INCREMENT;