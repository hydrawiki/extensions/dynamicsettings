<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Dynamic Settings All Sitemap Generator
 *
 * @author    Alex Smith
 * @copyright (c) 2015 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
**/

require_once dirname(__DIR__, 3) . '/maintenance/Maintenance.php';
class GenerateAllWikisSitemap extends Maintenance {
	/**
	 * Main Constructor
	 */
	public function __construct() {
		parent::__construct();
		$this->mDescription = "Generate Sitemaps for All of Hydra.";

		define('MAINTENANCE_DIR', __DIR__);
	}

	/**
	 * Main executor for the maintenance script.
	 */
	public function execute() {
		$this->output('Queuing Generate All Wiki Sitemap job');
		\DynamicSettings\Job\GenerateAllWikiSitemapsJob::queue();
	}
}

$maintClass = "GenerateAllWikisSitemap";
require_once RUN_MAINTENANCE_IF_MAIN;
