<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * DatabaseNodes Class
 *
 * @package   DynamicSettings
 * @author    Cameron Chunn
 * @copyright (c) 2016 Curse Inc.
 * @license   GPL-2.0-or-later
 * @link      https://gitlab.com/hydrawiki
 **/

namespace DynamicSettings\Node;

use ConfigFactory;
use Exception;
use MediaWiki\MediaWikiServices;
use MWLBFactory;
use Wikimedia\Rdbms\DBConnectionError;

class DatabaseNodes {
	/**
	 * Output HTML
	 *
	 * @var array
	 */
	private $config;

	/**
	 * Setup initial config variables.
	 */
	public function __construct() {
		$cf = MediaWikiServices::getInstance()->getMainConfig();
		$this->config = $cf->get('DSInstallDBNodeConfig');

		// Setup database node password.
		$DBUser = $cf->get('DSInstallDBUser');
		$this->config['nodeUser'] = $DBUser['user'];
		$this->config['nodePassword'] = $DBUser['password'];
	}

	/**
	 * Call the defined worker's API too run the getAvailableNodes() function on the worker.
	 *
	 * @return array Nodes
	 */
	public function getAvailableNodesFromWorker() {
		global $wgServerName, $wgDSHydraHttpAuthInfo;

		$key = DatabaseNodesApi::currentKey();

		$curl = curl_init();

		curl_setopt_array(
			$curl,
			[
				CURLOPT_RETURNTRANSFER 	=> 1,
				CURLOPT_URL				=> 'https://' . $this->getWorkerNodeAddress() . '/api.php?action=databasenodes&format=json&key=' . $key,
				CURLOPT_HTTPHEADER		=> ['Host: ' . $wgServerName],
				// @TODO: Not fine if going over the internet.
				CURLOPT_SSL_VERIFYHOST	=> false,
				// @TODO: Not fine if going over the internet.
				CURLOPT_SSL_VERIFYPEER	=> false
			]
		);

		if (!empty($wgDSHydraHttpAuthInfo)) {
			curl_setopt($curl, CURLOPT_USERPWD, $wgDSHydraHttpAuthInfo);
		}

		$result = curl_exec($curl);

		$error = ['error' => "Couldn't get node list from worker"];
		if ($result === false) {
			$result = ['error' => curl_error($curl)];
		} else {
			try {
				$result = json_decode($result, 1);
			} catch (Exception $e) {
				$result = $error;
			}
			if (!is_array($result)) {
				$result = $error;
			}
		}

		curl_close($curl);

		return $result;
	}

	/**
	 * Get available nodes
	 *
	 * @return array Nodes
	 */
	public function getAvailableNodes() {
		$config = MediaWikiServices::getInstance()->getMainConfig();
		$childClusterServers = $config->get('ChildClusterServers');
		$childClusterServers = array_diff_key(array_flip(array_reverse($childClusterServers)), array_flip($this->config['skipNodes']));

		$nodes = [];
		foreach ($childClusterServers as $node => $externalServer) {
			$nodes[$node] = $this->getNodeCount($node);
		}

		foreach ($nodes as $node => $dbCount) {
			$childClusterServers[$node] = [
				'node' => $node,
				'count' => $dbCount !== null ? $dbCount : 0,
				'free' => $dbCount !== null ? ($this->config['wikisPerNode'] - $dbCount) : 0
			];
		}

		return $childClusterServers;
	}

	/**
	 * Get number of databases currently on a node.
	 *
	 * @param string $node The node name.
	 *
	 * @return integer|null Available nodes or null for connection error.
	 */
	public function getNodeCount($node) {
		$db = $this->getNodeDatabase($node);

		if ($db) {
			$dbs = $db->query('show databases');
			$count = 0;
			foreach ($dbs as $db) {
				if (!in_array($db->Database, ['mysql', 'percona', 'performance_schema', 'information_schema'])) {
					$count++;
				}
			}
			return $count;
		}
		return null;
	}

	/**
	 * Get connection to a node's database.
	 *
	 * @param string $node The node name
	 *
	 * @return mixed Database Connection or false on exception.
	 */
	private function getNodeDatabase($node) {
		try {
			$lbFactory = MediaWikiServices::getInstance()->getDBLoadBalancerFactory();
			$conn = $lbFactory->newExternalLB($node)->getConnection(DB_REPLICA, false, 'mysql');
		} catch (DBConnectionError $e) {
			$conn = false;
		}

		return $conn;
	}

	/**
	 * Get the address for connecting to the worker node.
	 *
	 * @return string Worker Node Address
	 */
	private function getWorkerNodeAddress() {
		return $this->config['workerNode'];
	}

	/**
	 * Get configuration.
	 *
	 * @param bool $stripPasswords
	 *
	 * @return array
	 */
	public function getConfig($stripPasswords = false) {
		$config = $this->config;
		if ($stripPasswords) {
			unset($config['nodePassword']);
		}
		return $config;
	}
}
