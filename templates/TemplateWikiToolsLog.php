<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Wiki Tools Log Skin
 *
 * @author    Alex Smith
 * @copyright (c) 2014 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
**/

class TemplateWikiToolsLog {
	/**
	 * Output HTML
	 *
	 * @var string
	 */
	private $HMTL;

	/**
	 * Wiki Tools Log
	 *
	 * @param array Array of log entries.
	 * @param array Calculated totals of entries.
	 * @param array Wiki Information
	 * @param string Selected Log Type
	 * @param array Log Types
	 *
	 * @return string Built HTML
	 */
	public function wikiToolsLog($logs, $pagination, $totals, $wikis, $logType, $logTypes) {
		global $wgRequest;

		$wikiToolsLogPage = Title::newFromText('Special:WikiToolsLog');
		$wikiToolsLogURL = $wikiToolsLogPage->getFullURL();

		$HTML = '';

		if ($wgRequest->getVal('cleared') == 'true') {
			$HTML .= "<div class='successbox'>" . wfMessage('log_cleared', $logType)->escaped() . "</div>";
		}
		$HTML .= "
		<div>{$pagination}</div>
		<div class='button_bar'>
			<div class='buttons_left'>
				<div id='edit_totals'>" . wfMessage('queue_exception_start_finish_jobs', intval($totals[\DynamicSettings\Job\ToolsJob::JOB_QUEUE]), intval($totals[\DynamicSettings\Job\ToolsJob::JOB_START]), intval($totals[\DynamicSettings\Job\ToolsJob::JOB_ERROR]), intval($totals[\DynamicSettings\Job\ToolsJob::JOB_FINISH]))->escaped() . "</div>
			</div>
			<div class='button_break'></div>
			<div class='buttons_right'>
				<form id='set_logs' method='get' action='{$wikiToolsLogURL}'>
					<fieldset>
						<select name='log_type'>";
		foreach ($logTypes as $type) {
			$type = ucfirst($type);
			$HTML .= "
							<option value='{$type}'" . ($logType == $type ? " selected='selected'" : null) . ">{$type}</option>";
		}
		$HTML .= "
						</select>
						<input type='submit' class='mw-ui-button mw-ui-progressive' value='" . wfMessage('switch_log')->escaped() . "'/>
					</fieldset>
				</form>
				<form id='clear_logs' method='post' action='{$wikiToolsLogURL}?action=clearlog'>
					<fieldset>
						<input type='hidden' name='log_type' value='{$logType}'/>
						<input type='submit' class='mw-ui-button mw-ui-destructive' value='" . wfMessage('clear_log')->escaped() . "'/>
					</fieldset>
				</form>
			</div>
		</div>
		<table id='wikilist'>
			<thead>
				<tr>
					<th>" . wfMessage('tools_log_wiki') . "</th>
					<th>" . wfMessage('tools_log_time') . "</th>
					<th>" . wfMessage('tools_log_message') . "</th>
					<th style='width: 100px'>Output</th>
				</tr>
			</thead>
			<tbody>";
		if (count($logs) && is_array($logs)) {
			foreach ($logs as $log) {
				if (!isset($wikis[$log['site_key']])) {
					continue;
				}
				$HTML .= "
					<tr >
						<td><a href='https://" . $wikis[$log['site_key']]->getDomains()->getDomain() . "/' target='_blank'>" . $wikis[$log['site_key']]->getName() . " (" . strtoupper($wikis[$log['site_key']]->getLanguage()) . ")</a></td>
						<td>{$log['time']}</td>
						<td class='{$log['status_class']}'>{$log['status_message']}</td>
						<td style='text-align: center;'><a class='mw-ui-button mw-ui-progressive' href='{$wikiToolsLogURL}?action=viewoutput&jobkey=" . $log['job_key'] . "'>View Log</a></td>
					</tr>";
			}
		} else {
			$HTML .= "
				<tr>
					<td colspan='2'>" . wfMessage('no_logs_found')->escaped() . "</td>
				</tr>
				";
		}
		$HTML .= "
			</tbody>
		</table>
		<div>{$pagination}</div>";

		return $HTML;
	}

	/**
	 * View the status of a single queued job.
	 *
	 * @param string Raw Console Output
	 *
	 * @return string Built HTML
	 */
	public function viewJob($content, $jobStatus, $jobKey) {
		$wikiToolsLogPage = Title::newFromText('Special:WikiToolsLog');
		$wikiToolsLogURL = $wikiToolsLogPage->getFullURL();
		$wikiSitesPage = Title::newFromText('Special:WikiSites');
		$wikiSitesURL = $wikiSitesPage->getFullURL();

		$status = wfMessage('jobstatus_' . strtolower($jobStatus))->plain();

		$statusLength = strlen($status) + 12;
		$statusOutput = "<strong>Status:</strong> <span id='statusline'>$status</span>";

		$html = "
		<div class='button_bar'>
			<div class='buttons_left'>
				{$statusOutput}
			</div>
			<div class='button_break'></div>
			<div class='buttons_right'>
				<a href='{$wikiToolsLogURL}' class='mw-ui-button'>" . wfMessage('return_to_queue_status')->escaped() . "</a>
				<a href='{$wikiSitesURL}' class='mw-ui-button'>" . wfMessage('wikisites')->escaped() . "</a>
			</div>
		</div>
		<input type='hidden' id='jobkey' value='{$jobKey}'>
		<input type='hidden' id='status' value='{$jobStatus}'>
		<pre id='toolLogOutput'>" . htmlentities($content, ENT_QUOTES) . "</pre>";

		return $html;
	}
}
