<?php
/**
 * DynamicSettings
 * Tools Log API
 *
 * @author    Cameron Chunn
 * @copyright (c) 2018 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   DynamicSettings
 * @link      https://gitlab.com/hydrawiki
 **/

namespace DynamicSettings\Api;

use DynamicSettings\Environment;
use DynamicSettings\Job\ToolsJob;

class ToolsLogApi extends \ApiBase {
	/**
	 * Main Executor
	 *
	 * @return void [Outputs to screen]
	 */
	public function execute() {
		if (!Environment::isMasterWiki()) {
			$this->dieWithError(['apierror-permissiondenied-generic']);
			return;
		}

		$this->checkUserRightsAny('wiki_tools_log');
		$this->params = $this->extractRequestParams();

		switch ($this->params['do']) {
			case 'getLogOutput':
				$response = $this->getLogOutput();
				break;
			default:
				$this->dieWithError(['invaliddo', $this->params['do']]);
				break;
		}

		foreach ($response as $key => $value) {
			$this->getResult()->addValue(null, $key, $value);
		}
	}

	/**
	 * Requirements for API call parameters.
	 *
	 * @return array Merged array of parameter requirements.
	 */
	public function getAllowedParams() {
		return [
			'do' => [
				\ApiBase::PARAM_TYPE		=> 'string',
				\ApiBase::PARAM_REQUIRED => true
			],
			'jobkey' => [
				\ApiBase::PARAM_TYPE		=> 'string',
				\ApiBase::PARAM_REQUIRED => true
			],
			'truncate' => [
				\ApiBase::PARAM_TYPE		=> 'integer',
				\ApiBase::PARAM_REQUIRED => false
			]
		];
	}

	/**
	 * Merge API errors into the mediawiki error array.
	 *
	 * @return array
	 */
	public function getPossibleErrors() {
		return array_merge(
			parent::getPossibleErrors(),
			[
				[
					'code' => 'NoJobKey',
					'info' => 'No jobkey was passed.'
				]
			]
		);
	}

	/**
	 * Search for possible wikis to return.
	 *
	 * @return array API result.
	 */
	public function getLogOutput() {
		if (!isset($this->params['jobkey'])) {
			return $this->dieWithError(['NoJobKey']);
		}
		$jobkey = $this->params['jobkey'];
		if ($this->params['truncate']) {
			$start = 0 - $this->params['truncate'];
			$end = -1;
		} else {
			$start = false;
			$end = false;
		}
		$content = ToolsJob::getJobOutput($jobkey, $start, $end);
		$content = ($content) ? trim($content) : "";
		$jobStatus = ToolsJob::getJobStatus($jobkey);
		$status = wfMessage('jobstatus_' . strtolower($jobStatus))->plain();
		return [
			'success' 		=> true,
			'status_code' 	=> $jobStatus,
			'status'		=> $status,
			'content' 		=> $content
		];
	}

	/**
	 * Get version of this API Extension.
	 *
	 * @return string API Extension Version
	 */
	public function getVersion() {
		return '1.0';
	}
}
