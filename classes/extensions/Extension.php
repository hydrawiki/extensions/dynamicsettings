<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Extension class
 *
 * @author    Noah Manneschmidt
 * @copyright (c) 2015 Curse Inc.
 * @license   Proprietary
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
**/

namespace DynamicSettings\Extensions;

use DynamicSettings\DSDBFactory;
use DynamicSettings\EditLog;
use DynamicSettings\Settings\Setting;
use DynamicSettings\Settings\Generated\GeneratedSetting;
use DynamicSettings\Wiki;
use DynamicSettings\WikiContextException;
use RequestContext;

class Extension {
	use \DynamicSettings\Traits\CheckPhpSyntaxTrait;
	use \DynamicSettings\Traits\WikiContext;

	/**
	 * Allowed extension database data.
	 *
	 * @var array
	 */
	private $data = [
		'extension_name'		=> '',
		'extension_desc'		=> '',
		'extension_filename'	=> '',
		'extension_folder'		=> '',
		'md5_key'				=> '',
		'deleted'				=> 0,
		'weight'				=> 0,
		'extension_extra'		=> '',
		'extension_information'	=> '',
		'update_required'		=> 0,
		'default_extension'		=> 0,
		'critical_extension'	=> 0,
		'load_type'				=> 0,
		'dependencies'			=> []
	];

	/**
	 * @var boolean
	 */
	private static $allLoaded = false;

	/**
	 * Cache of extension info (for loading by key)
	 *
	 * @var array
	 */
	private static $allCache = [];

	/**
	 * Cache of extension info of default extensions
	 *
	 * @var array
	 */
	private static $defaultCache = [];

	/**
	 * Cache of extensions which are marked as deleted
	 *
	 * @var array
	 */
	private static $deletedCache = [];

	/**
	 * Extension settings.
	 *
	 * @var SettingCollection
	 */
	protected $settings;

	/**
	 * is this extension enabled on the current context wiki?
	 *
	 * @var boolean
	 */
	protected $enabled;

	/**
	 * Constants for Load Type
	 */
	const LOAD_EXTENSION = 0;
	const LOAD_EXTENSION_LEGACY = 1;
	const LOAD_SKIN = 2;

	/**
	 * Main Constructor
	 *
	 * @param array Database row from the allowed extensions table.
	 */
	public function __construct($data = []) {
		// assure dependencies is an array
		if (!empty($data['dependencies'])) {
			$data['dependencies'] = explode(',', $data['dependencies']);
		} else {
			$data['dependencies'] = [];
		}
		$this->data = array_merge($this->data, $data);
	}

	/**
	 * Reset some things if the context is changed.
	 *
	 * @param object Old Wiki object context.
	 *
	 * @return void
	 */
	protected function resetContext($oldContext) {
		$this->settings = null;
	}

	/**
	 * Return a new Extension object by database row.
	 *
	 * @param array $row
	 *
	 * @return object Extension
	 */
	public static function newFromRow($row) {
		return new self($row);
	}

	/**
	 * Get the currently enabled extensions for a given wiki.
	 * Dependencies are resolved and default extensions are included.
	 *
	 * @param object Wiki $wiki
	 * @param boolean     $includeDeleted
	 *
	 * @return object ExtensionCollection
	 */
	public static function loadFromWiki(Wiki $wiki, $includeDeleted = false) {
		self::loadAll();

		// start with explicitly enabled extensions
		$extensions = array_intersect_key(self::$allCache, $wiki->getEnabledExtensionHashes());
		// add defaults
		$extensions = array_merge($extensions, array_intersect_key(self::$allCache, self::$defaultCache));
		// include dependencies
		$extensions = self::addMissingDeps($extensions, null, $includeDeleted);

		$extensions = new ExtensionCollection($extensions);
		$extensions->setWikiContext($wiki);
		return $extensions;
	}

	/**
	 * Recursively finds missing dependencies in the given array of extensions
	 *
	 * @param array      $extensions
	 * @param array|null $searchScope    [optional] subset of extensions whose dependencies are the only ones unresolved
	 * @param boolean    $includeDeleted whether to include deleted extensions
	 *
	 * @return array the contents of $extensions with any dependencies merged in
	 */
	private static function addMissingDeps($extensions, $searchScope = null, $includeDeleted = false) {
		if (is_null($searchScope)) {
			$searchScope = $extensions;
		}
		if (empty($searchScope)) {
			return $extensions;
		}
		$newAdds = [];
		foreach ($searchScope as $key => $ext) {
			$deps = $ext->getDependencies();
			if (!empty($deps)) {
				foreach ($deps as $md5) {
					if (!isset($extensions[$md5])) {
						if (isset(self::$allCache[$md5])) {
							$newAdds[$md5] = self::$allCache[$md5];
						} elseif ($includeDeleted && isset(self::$deletedCache[$md5])) {
							$newAdds[$md5] = self::$deletedCache[$md5];
						}
					}
				}
			}
		}
		$extensions = array_merge($extensions, $newAdds);
		return self::addMissingDeps($extensions, $newAdds, $includeDeleted);
	}

	/**
	 * Get an allowed extension by hash key.
	 *
	 * @param string MD5 Key
	 *
	 * @return object Extension or null
	 */
	public static function newFromKey($md5key) {
		if (isset(self::$allCache[$md5key])) {
			return self::$allCache[$md5key];
		} else {
			$db = DSDBFactory::getMasterDB(DB_MASTER);
			$result = $db->select(
				'wiki_allowed_extensions',
				'*',
				['md5_key' => $md5key],
				__METHOD__
			);
			$row = $result->fetchRow();
			if (!empty($row)) {
				$extension = new self($row);
				self::$allCache[$md5key] = $extension;
				return $extension;
			}
		}
	}

	/**
	 * @return object ExtensionCollection
	 */
	public static function getAll() {
		self::loadAll();
		return new ExtensionCollection(self::$allCache);
	}

	/**
	 * @return object ExtensionCollection
	 */
	public static function getDeleted() {
		self::loadAll();
		return new ExtensionCollection(self::$deletedCache);
	}

	/**
	 * @return object ExtensionCollection
	 */
	public static function getDefault() {
		self::loadAll();
		return new ExtensionCollection(array_intersect_key(self::$allCache, self::$defaultCache));
	}

	/**
	 * @return object ExtensionCollection
	 */
	public static function getNonDefault() {
		self::loadAll();
		return new ExtensionCollection(array_diff_key(self::$allCache, self::$defaultCache));
	}

	/**
	 * Check if an allowed extension exists.
	 *
	 * @param string MD5 Key of the extension.
	 *
	 * @return boolean
	 */
	public static function exists($hashKey) {
		// may already be cached
		if (isset(self::$allCache[$hashKey])) {
			return true;
		} else {
			// otherwise try DB lookup
			$db = DSDBFactory::getMasterDB(DB_MASTER);
			$result = $db->select(
				'wiki_allowed_extensions',
				'*',
				['md5_key' => $hashKey],
				__METHOD__
			);
			$row = $result->fetchRow();
			if (!empty($row)) {
				$extension = new self($row);
				self::$allCache[$hashKey] = $extension;
				return true;
			}
		}
		return false;
	}

	/**
	 * Check if an allowed extension exists and is a default extension.
	 *
	 * @param string MD5 Key of the extension.
	 *
	 * @return boolean
	 */
	public static function isDefault($hashKey) {
		if (isset(self::$defaultCache[$hashKey])) {
			return true;
		} else {
			$db = DSDBFactory::getMasterDB(DB_MASTER);
			$default = boolval(
				$db->selectField(
					'wiki_allowed_extensions',
					'default_extension',
					['md5_key' => $hashKey],
					__METHOD__
				)
			);
			self::$defaultCache[$hashKey] = $default;
			return $default;
		}
		return false;
	}

	/**
	 * Loads the allowed settings into the data cache
	 *
	 * @return void
	 */
	private static function loadAll() {
		if (self::$allLoaded) {
			return;
		}

		$db = DSDBFactory::getMasterDB(DB_MASTER);
		$results = $db->select(
			['wiki_allowed_extensions'],
			['*'],
			[],
			__METHOD__,
			[
				'ORDER BY'	=> 'extension_name ASC'
			]
		);

		while ($row = $results->fetchRow()) {
			$extension = new self($row);

			// cache extensions rows separately
			if ($extension->isDeleted()) {
				self::$deletedCache[$extension->getExtensionKey()] = $extension;
				continue;
			}
			if ($extension->isDefaultExtension()) {
				// We fill this array with boolean true and the value we want as the key since the built in PHP functions nicely work off the keys.
				self::$defaultCache[$extension->getExtensionKey()] = true;
			}
			self::$allCache[$extension->getExtensionKey()] = $extension;
		}

		self::$allLoaded = true;
	}

	/**
	 * Returns settings for this extension
	 *
	 * @return object SettingCollection
	 */
	public function getSettings() {
		if (is_null($this->settings)) {
			$this->settings = Setting::loadFromExtension($this);
		}
		return $this->settings;
	}

	/**
	 * Check whether this extension is currently enabled
	 *
	 * @return boolean
	 * @throws WikiContextException	When there is no wiki context.
	 */
	public function isEnabled() {
		if (!$this->wikiContext) {
			throw new WikiContextException();
		}
		$enabledHashes = $this->wikiContext->getEnabledExtensionHashes();
		return (isset($enabledHashes[$this->data['md5_key']]) ? $enabledHashes[$this->data['md5_key']] : false);
	}

	/**
	 * Save all information to the database.
	 *
	 * @param string [Optional] Commit Message
	 *
	 * @return boolean Success
	 */
	public function save($commitMessage = '') {
		$wgUser = RequestContext::getMain()->getUser();

		$success = false;
		$db = DSDBFactory::getMasterDB(DB_MASTER);

		if (empty($this->getExtensionKey())) {
			$this->setExtensionKey();
		}

		$save = [
			'extension_name'		=> $this->getExtensionName(),
			'extension_desc'		=> $this->getExtensionDesc(),
			'extension_filename'	=> $this->getExtensionFilename(),
			'extension_folder'		=> $this->getExtensionFolder(),
			'md5_key'				=> $this->getExtensionKey(),
			'deleted'				=> intval($this->isDeleted()),
			'weight'				=> $this->getWeight(),
			'extension_extra'		=> $this->getExtensionExtra(),
			'extension_information'	=> $this->getExtensionInformation(),
			'update_required'		=> intval($this->isUpdateRequired()),
			'default_extension'		=> intval($this->isDefaultExtension()),
			'critical_extension'	=> intval($this->isCriticalExtension()),
			'load_type'				=> $this->getLoadType(),
			'dependencies'			=> implode(',', $this->getDependencies())
		];

		$db->startAtomic(__METHOD__);
		if ($this->getId()) {
			$result = $db->update(
				'wiki_allowed_extensions',
				$save,
				['aeid' => $this->getId()],
				__METHOD__
			);

			$databaseId = $this->getId();
		} else {
			$result = $db->insert(
				'wiki_allowed_extensions',
				$save,
				__METHOD__
			);

			$databaseId = $db->insertId();
		}

		if (!$result) {
			$db->cancelAtomic(__METHOD__);
		} else {
			$success = true;
			$db->endAtomic(__METHOD__);

			$this->data['aeid'] = $databaseId;

			$miscData = [
				'display_name' => $this->getExtensionName(),
			];

			EditLog::addEntry(
				$this->getExtensionKey(), // Log Key: allowed_extension_md5_key
				$save,
				__METHOD__,
				$wgUser,
				$commitMessage,
				$miscData
			);
		}

		return $success;
	}

	/**
	 * Delete this extension.
	 *
	 * @return boolean Success
	 */
	public function delete() {
		$wgUser = RequestContext::getMain()->getUser();

		$success = false;

		$db = DSDBFactory::getMasterDB(DB_MASTER);
		$db->startAtomic(__METHOD__);
		$result = $db->update(
			'wiki_allowed_extensions',
			['deleted' => 1],
			['aeid' => $this->getId()],
			__METHOD__
		);

		if (!$result) {
			$db->cancelAtomic(__METHOD__);
		} else {
			$this->setDeleted(true);
			$success = true;
			$db->endAtomic(__METHOD__);

			$miscData = [
				'display_name' => $this->getExtensionName()
			];

			EditLog::addEntry(
				$this->getExtensionKey(), // Log Key: allowed_extension_md5_key
				$this->data,
				__METHOD__,
				$wgUser,
				$this->getExtensionName() . ' deleted',
				$miscData
			);
		}

		return $success;
	}

	/**
	 * Get the database ID.
	 *
	 * @return int Database ID
	 */
	public function getId() {
		return isset($this->data['aeid']) ? intval($this->data['aeid']) : 0;
	}

	/**
	 * Get the extension name.
	 *
	 * @return string Extension Name
	 */
	public function getExtensionName() {
		return $this->data['extension_name'];
	}

	/**
	 * Set the extension name.
	 *
	 * @param string Extension Name
	 *
	 * @return boolean Success, False if invalid.
	 */
	public function setExtensionName($extensionName) {
		$extensionName = trim($extensionName);
		if (empty($extensionName)) {
			return false;
		}
		$this->data['extension_name'] = $extensionName;
		return true;
	}

	/**
	 * Get the extension description.
	 *
	 * @return string Extension Description
	 */
	public function getExtensionDesc() {
		return $this->data['extension_desc'];
	}

	/**
	 * Set the extension description.
	 *
	 * @param string Extension Description
	 *
	 * @return boolean Success, False if invalid.
	 */
	public function setExtensionDesc($extensionDesc) {
		$extensionDesc = trim($extensionDesc);
		if (empty($extensionDesc)) {
			return false;
		}
		$this->data['extension_desc'] = $extensionDesc;
		return true;
	}

	/**
	 * Get the extension filename.
	 *
	 * @return string Extension Filename
	 */
	public function getExtensionFilename() {
		return $this->data['extension_filename'];
	}

	/**
	 * Set the extension filename.
	 *
	 * @param string Extension Filename
	 *
	 * @return boolean Success, False if invalid.
	 */
	public function setExtensionFilename($extensionFilename) {
		$extensionFilename = trim($extensionFilename);
		if (empty($extensionFilename)) {
			return false;
		}
		$this->data['extension_filename'] = $extensionFilename;
		return true;
	}

	/**
	 * Get the extension folder.
	 *
	 * @return string Extension Folder
	 */
	public function getExtensionFolder() {
		return $this->data['extension_folder'];
	}

	/**
	 * Set the extension folder.
	 *
	 * @param string Extension Folder
	 *
	 * @return boolean Success, False if invalid.
	 */
	public function setExtensionFolder($extensionFolder) {
		$extensionFolder = trim($extensionFolder);
		if (empty($extensionFolder)) {
			return false;
		}
		$this->data['extension_folder'] = $extensionFolder;
		return true;
	}

	/**
	 * Get the extension key.
	 *
	 * @return string Extension Key
	 */
	public function getExtensionKey() {
		return $this->data['md5_key'];
	}

	/**
	 * Set the extension key.
	 *
	 * @param string Extension Key
	 *
	 * @return boolean Success
	 */
	public function setExtensionKey() {
		if (!empty($this->data['md5_key'])) {
			throw new \MWException('Tried to change the hash key of an allowed extension ' . $this->data['md5_key'] . '.  This action is not allowed.');
		}
		if (empty($this->getExtensionFolder()) || empty($this->getExtensionFilename())) {
			throw new \MWException('Can not set the hash key of an allowed extension without a folder and filename.');
		}

		$this->data['md5_key'] = md5($this->getExtensionFolder() . $this->getExtensionFilename());

		return true;
	}

	/**
	 * Is deleted?
	 *
	 * @return boolean Deleted
	 */
	public function isDeleted() {
		return boolval($this->data['deleted']);
	}

	/**
	 * Set deleted.
	 *
	 * @param boolean Deleted
	 *
	 * @return void
	 */
	public function setDeleted($deleted = false) {
		$this->data['deleted'] = intval($deleted);
	}

	/**
	 * Get the weight.
	 *
	 * @return int Weight
	 */
	public function getWeight() {
		return intval($this->data['weight']);
	}

	/**
	 * Set the weight.
	 *
	 * @param integer Weight
	 *
	 * @return void
	 */
	public function setWeight($weight) {
		$this->data['weight'] = intval($weight);
	}

	/**
	 * Get the extension extra.
	 *
	 * @return string Extension Extra
	 */
	public function getExtensionExtra() {
		return $this->data['extension_extra'];
	}

	/**
	 * Set the extension extra.
	 *
	 * @param string Extension Extra
	 *
	 * @return mixed True for no errors, array of errors if there is a syntax error.
	 */
	public function setExtensionExtra($extensionExtra) {
		$errors = true; // This does not mean that there are errors.

		$syntax = self::checkPHPSyntax("<?php " . $extensionExtra . " ?>");
		if (!$syntax['is_valid']) {
			$errors = [];
			$errors['extension_extra'] = $syntax['error'];
			if ($syntax['line']) {
				$errors['extension_extra_line'] = $syntax['line'];
			}
		}
		$this->data['extension_extra'] = $extensionExtra;
		return $errors;
	}

	/**
	 * Get the extension information.
	 *
	 * @return string Extension Information
	 */
	public function getExtensionInformation() {
		return $this->data['extension_information'];
	}

	/**
	 * Set the extension information.
	 *
	 * @param string Extension Information
	 *
	 * @return void
	 */
	public function setExtensionInformation($extensionInformation) {
		$this->data['extension_information'] = trim($extensionInformation);
	}

	/**
	 * Is a database update required?
	 *
	 * @return boolean Update Required
	 */
	public function isUpdateRequired() {
		return boolval($this->data['update_required']);
	}

	/**
	 * Set if a database update is required.
	 *
	 * @param boolean Update Required
	 *
	 * @return void
	 */
	public function setUpdateRequired($updateRequired = false) {
		$this->data['update_required'] = intval($updateRequired);
	}

	/**
	 * Is a default extension?
	 *
	 * @return boolean Default Extension
	 */
	public function isDefaultExtension() {
		return boolval($this->data['default_extension']);
	}

	/**
	 * Set if a default extension.
	 *
	 * @param boolean Default Extension
	 *
	 * @return void
	 */
	public function setDefaultExtension($defaultExtension = false) {
		$this->data['default_extension'] = intval($defaultExtension);
	}

	/**
	 * Is a critical extension?
	 *
	 * @return boolean Critical Extension
	 */
	public function isCriticalExtension() {
		return boolval($this->data['critical_extension']);
	}

	/**
	 * Set if a critical extension.
	 *
	 * @param boolean Critical Extension
	 *
	 * @return void
	 */
	public function setCriticalExtension($criticalExtension = false) {
		$this->data['critical_extension'] = intval($criticalExtension);
	}

	/**
	 * Get the load type.
	 *
	 * @return int Load Type
	 */
	public function getLoadType() {
		return intval($this->data['load_type']);
	}

	/**
	 * Set the load type.
	 *
	 * @param integer Load Type
	 *
	 * @return void
	 */
	public function setLoadType($loadType = 0) {
		$this->data['load_type'] = intval($loadType);
	}

	/**
	 * Get extension dependencies.
	 *
	 * @return string Dependencies
	 */
	public function getDependencies() {
		return $this->data['dependencies'];
	}

	/**
	 * Set extension dependencies.
	 *
	 * @param string Dependencies
	 *
	 * @return void
	 */
	public function setDependencies($_dependencies) {
		$dependencies = [];
		if (is_array($_dependencies) && count($_dependencies)) {
			foreach ($_dependencies as $md5Key) {
				if (self::exists($md5Key) && $md5Key != $this->getExtensionKey()) {
					$dependencies[] = $md5Key;
				}
			}
		}
		$this->data['dependencies'] = $dependencies;
	}

	/**
	 * Requires a wiki context.
	 *
	 * @return string PHP code defining settings and loader code.
	 * @throws WikiContextException	When a wiki context does not exist.
	 */
	public function getExportedCode() {
		if (!$this->wikiContext) {
			throw new WikiContextException();
		}

		$php = "
	/**************
	* Extension: {$this->data['extension_name']}
	**************/";

		$settings = $this->getSettings();

		foreach ($settings->beforeRequire() as $setting) {
			$php .= $setting->getExportedCode() . "\n\n";
		}

		$php .= "\n";
		switch ($this->data['load_type']) {
			case self::LOAD_EXTENSION_LEGACY:
				// Legacy load flag set, loading using require_once method
				$php .= "	require_once(\"{\$IP}/extensions/{$this->data['extension_folder']}/{$this->data['extension_filename']}.php\");";
			break;
			case self::LOAD_EXTENSION:
				// Extension may be loaded using wfLoadExtensions
				$php .= "	wfLoadExtension(\"{$this->data['extension_folder']}\");";
			break;
			case self::LOAD_SKIN:
				// Load extension as skin
				$php .= "	wfLoadSkin(\"{$this->data['extension_folder']}\");";
			break;
		}

		$php .= "\n\n";

		if (!empty($this->data['extension_extra'])) {
			$php .= $this->data['extension_extra'];
			$php .= "\n\n";
		}

		foreach ($settings->afterRequire() as $setting) {
			$php .= $setting->getExportedCode() . "\n\n";
		}

		foreach (GeneratedSetting::getForExtension($this) as $setting) {
			$php .= $setting->getExportedCode() . "\n\n";
		}

		// indent all lines to fit cleanly inside setting_only code block.

		return $php;
	}
}
