CREATE TABLE /*_*/wiki_settings (
  `ssid` int(12) NOT NULL,
  `site_key` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `allowed_setting_id` int(12) NOT NULL DEFAULT '0',
  `setting_value` text COLLATE utf8_unicode_ci
) /*$wgDBTableOptions*/;

ALTER TABLE /*_*/wiki_settings
  ADD PRIMARY KEY (`ssid`),
  ADD KEY `allowed_setting_id` (`allowed_setting_id`),
  ADD KEY `site_key` (`site_key`);

ALTER TABLE /*_*/wiki_settings
  MODIFY `ssid` int(12) NOT NULL AUTO_INCREMENT;