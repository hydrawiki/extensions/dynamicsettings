<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Allowed Setting class for text
 *
 * @author    Noah Manneschmidt
 * @copyright (c) 2015 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
 **/

namespace DynamicSettings\Settings;

class TextSetting extends StringSetting {
}
