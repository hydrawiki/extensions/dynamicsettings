<?php
/**
 * Farm installer, based on CliInstaller.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @file
 * @ingroup Deployment
 */

namespace DynamicSettings\Wiki\Installer;

use ExtensionRegistry;
use Installer;
use Sanitizer;

/**
 * Farm installer, based on CliInstaller.
 */
class FarmInstaller extends Installer {
	private $specifiedScriptPath = false;

	private $optionMap = [
		'dbtype' => 'wgDBtype',
		'dbserver' => 'wgDBserver',
		'dbname' => 'wgDBname',
		'dbuser' => 'wgDBuser',
		'dbpass' => 'wgDBpassword',
		'dbprefix' => 'wgDBprefix',
		'dbtableoptions' => 'wgDBTableOptions',
		'dbmysql5' => 'wgDBmysql5',
		'dbport' => 'wgDBport',
		'dbschema' => 'wgDBmwschema',
		'dbpath' => 'wgSQLiteDataDir',
		'server' => 'wgServer',
		'scriptpath' => 'wgScriptPath',
	];

	/**
	 * The actual list of installation steps. This will be initialized by getInstallSteps()
	 *
	 * @var array
	 */
	private $installSteps = [];

	/**
	 * Extra steps for installation, for things like DatabaseInstallers to modify
	 *
	 * @var array
	 */
	protected $extraInstallSteps = [];

	/**
	 * Temporary storage for original database connection.
	 *
	 * @var object
	 */
	private static $originalDb = null;

	/**
	 * Temporary storage for original bootstrap Config for MediaWikiServices.
	 *
	 * @var object
	 */
	private static $bootstrapConfig = null;

	/**
	 * Temporary storage for hooks.
	 *
	 * @var array
	 */
	private static $wgHooks = [];

	/**
	 * Temporary storage for $GLOBALS.
	 *
	 * @var array
	 */
	private static $GLOBALS = [];

	/**
	 * Extension Registration Queue Storage
	 *
	 * @var array
	 */
	private static $registrationQueue = [];

	/**
	 * Constructor.
	 *
	 * @param string $siteName
	 * @param string $admin
	 * @param array  $option
	 */
	function __construct($siteName, $admin = null, array $option = []) {
		global $wgContLang, $wgMasterDatabaseName;

		self::overrideConfig();

		parent::__construct();

		if (isset($option['scriptpath'])) {
			$this->specifiedScriptPath = true;
		}

		foreach ($this->optionMap as $opt => $global) {
			if (isset($option[$opt])) {
				$GLOBALS[$global] = $option[$opt];
				$this->setVar($global, $option[$opt]);
			}
		}

		if (isset($option['lang'])) {
			global $wgLang, $wgLanguageCode;
			$this->setVar('_UserLang', $option['lang']);
			$wgContLang = \Language::factory($option['lang']);
			$wgLang = \Language::factory($option['lang']);
			$wgLanguageCode = $option['lang'];
		}

		$this->setVar('wgMasterDatabaseName', $wgMasterDatabaseName);
		$this->setVar('wgSitename', $siteName);

		$this->setVar('wgMetaNamespace', $option['metaNamespace']);

		if ($admin) {
			$this->setVar('_AdminName', $admin);
		}

		if (!isset($option['installdbuser'])) {
			$this->setVar(
				'_InstallUser',
				$this->getVar('wgDBuser')
			);
			$this->setVar(
				'_InstallPassword',
				$this->getVar('wgDBpassword')
			);
		} else {
			$this->setVar(
				'_InstallUser',
				$option['installdbuser']
			);
			$this->setVar(
				'_InstallPassword',
				isset($option['installdbpass']) ? $option['installdbpass'] : ""
			);

			// Assume that if we're given the installer user, we'll create the account.
			$this->setVar('_CreateDBAccount', true);
		}

		if (isset($option['pass'])) {
			$this->setVar('_AdminPassword', $option['pass']);
		}
	}

	/**
	 * Main entry point.
	 */
	public function execute() {
		$this->performInstallation(
			[$this, 'startStage'],
			[$this, 'endStage']
		);
		self::restoreConfig();
	}

	public function startStage($step) {
		// Messages: config-install-database, config-install-tables, config-install-interwiki,
		// config-install-stats, config-install-keys, config-install-sysop, config-install-mainpage,
		// config-install-extensions
		$this->showMessage("config-install-$step");
	}

	public function endStage($step, $status) {
		$this->showStatusMessage($status);
		$this->showMessage('config-install-step-done');
	}

	public function showMessage($msg) {
		echo $this->getMessageText(func_get_args()) . "\n";
		flush();
	}

	public function showError($msg) {
		echo "***{$this->getMessageText(func_get_args())}***\n";
		flush();
	}

	/**
	 * @param array $params
	 *
	 * @return string
	 */
	protected function getMessageText($params) {
		$msg = array_shift($params);

		$text = wfMessage($msg, $params)->parse();

		$text = preg_replace('/<a href="(.*?)".*?>(.*?)<\/a>/', '$2 &lt;$1&gt;', $text);

		return Sanitizer::stripAllTags($text);
	}

	/**
	 * Dummy
	 */
	public function showHelpBox($msg /*, ... */) {
	}

	public function showStatusMessage(\Status $status) {
		$warnings = array_merge(
			$status->getWarningsArray(),
			$status->getErrorsArray()
		);

		if (count($warnings) !== 0) {
			foreach ($warnings as $w) {
				call_user_func_array([$this, 'showMessage'], $w);
			}
		}

		if (!$status->isOk()) {
			echo "\n";
			exit(1);
		}
	}

	public function envCheckPath() {
		if (!$this->specifiedScriptPath) {
			$this->showMessage('config-no-cli-uri', $this->getVar("wgScriptPath"));
		}

		return parent::envCheckPath();
	}

	protected function envGetDefaultServer() {
		return null; // Do not guess if installing from CLI
	}

	public function dirIsExecutable($dir, $url) {
		$this->showMessage('config-no-cli-uploads-check', $dir);

		return false;
	}

	/**
	 * Get an array of install steps. Should always be in the format of
	 * array(
	 *   'name'     => 'someuniquename',
	 *   'callback' => array($obj, 'method'),
	 * )
	 * There must be a config-install-$name message defined per step, which will
	 * be shown on install.
	 *
	 * @param object DatabaseInstaller $installer DatabaseInstaller so we can make callbacks
	 *
	 * @return array
	 */
	protected function getInstallSteps(\DatabaseInstaller $installer) {
		$installSteps = parent::getInstallSteps($installer);
		foreach ($installSteps as $key => $step) {
			if ($step['name'] == 'sysop') {
				unset($installSteps[$key]);
			}

			if ($step['name'] === 'user' && $step['callback'][1] === 'setupUser') {
				// Override setupUser with our configurable setup.
				$installSteps[$key]['callback'] = [$this, 'setupUser'];
			}
		}
		$this->installSteps = $installSteps;

		return $this->installSteps;
	}

	/**
	 * Override the necessary bits of the config to run an installation.
	 */
	public static function overrideConfig() {
		global $wgHooks;

		$lb = \MediaWiki\MediaWikiServices::getInstance()->getDBLoadBalancerFactory()->getMainLB();
		self::$originalDb = $lb->getConnection(DB_MASTER);
		self::$bootstrapConfig = \MediaWiki\MediaWikiServices::getInstance()->getBootstrapConfig();

		foreach ($GLOBALS as $key => $value) {
			self::$GLOBALS[$key] = $value;
		}

		$wgHooks = []; // No hooks!
		$registry = ExtensionRegistry::getInstance();
		self::$registrationQueue = $registry->getQueue();
		$registry->clearQueue();

		parent::overrideConfig();
	}

	/**
	 * Restore configuration.
	 *
	 * @return void
	 */
	public static function restoreConfig() {
		foreach (self::$GLOBALS as $key => $value) {
			$GLOBALS[$key] = $value;
		}

		\MediaWiki\MediaWikiServices::resetGlobalInstance(new \GlobalVarConfig());

		$services = \MediaWiki\MediaWikiServices::getInstance();
		$connection = self::$originalDb;
		$services->redefineService('DBLoadBalancerFactory', function () use ($connection) {
			return \Wikimedia\Rdbms\LBFactorySingle::newFromConnection($connection);
		});

		$registry = ExtensionRegistry::getInstance();
		foreach (self::$registrationQueue as $path => $mTime) {
			$registry->queue($path);
		}
	}

	/**
	 * Override for the installer's setupUser so that the farm style grants are used.
	 * This is overriden in the getInstallSteps() function.
	 *
	 * @return object Status
	 */
	public function setupUser() {
		$config = \ConfigFactory::getDefaultInstance()->makeConfig('main');
		$dbUserGrants = $config->get('DSDBUserGrants');
		if ($dbUserGrants === false) {
			return \Status::newGood();
		}

		$dbUser = $this->getVar('wgDBuser');
		if ($dbUser == $this->getVar('_InstallUser')) {
			return \Status::newGood();
		}
		$dbInstaller = $this->getDBInstaller();
		$status = $dbInstaller->getConnection();
		if (!$status->isOK()) {
			return $status;
		}

		$dbInstaller->setupSchemaVars();
		$dbName = $this->getVar('wgDBname');
		$dbInstaller->db->selectDB($dbName);
		$server = $this->getVar('wgDBserver');
		$password = $this->getVar('wgDBpassword');
		$grantableNames = [];

		foreach ($dbUserGrants as $details) {
			$dbUser = (isset($details['username']) && !empty($details['username']) ? $details['username'] : $dbUser);
			$escPass = $dbInstaller->db->addQuotes((isset($details['password']) && !empty($details['password']) ? $details['password'] : $password));
			$host = $details['host'];
			$fullName = $this->buildFullDBUserName($dbUser, $host, $dbInstaller);
			if (!$this->userDefinitelyExists($dbUser, $host, $dbInstaller)) {
				try {
					$dbInstaller->db->startAtomic(__METHOD__);
					$dbInstaller->db->query("CREATE USER $fullName IDENTIFIED BY $escPass", __METHOD__);
					$dbInstaller->db->endAtomic(__METHOD__);
					$grantableNames[] = $fullName;
				} catch (\DBQueryError $dqe) {
					if ($dbInstaller->db->lastErrno() == 1396 /* ER_CANNOT_USER */) {
						// User (probably) already exists
						$dbInstaller->db->rollback(__METHOD__);
						$status->warning('config-install-user-alreadyexists', $dbUser);
						$grantableNames[] = $fullName;
						break;
					} else {
						// If we couldn't create for some bizzare reason and the
						// user probably doesn't exist, skip the grant
						$dbInstaller->db->rollback(__METHOD__);
						$status->warning('config-install-user-create-failed', $dbUser, $dqe->getMessage());
					}
				}
			} else {
				$status->warning('config-install-user-alreadyexists', $dbUser);
				$grantableNames[] = $fullName;
				break;
			}
		}

		// Try to grant to all the users we know exist or we were able to create.
		$dbAllTables = $dbInstaller->db->addIdentifierQuotes($dbName) . '.*';
		$master = $this->getVar('wgMasterDatabaseName');
		foreach ($grantableNames as $name) {
			try {
				$dbInstaller->db->startAtomic(__METHOD__);
				$dbInstaller->db->query("GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, REFERENCES, INDEX, ALTER, CREATE TEMPORARY TABLES, LOCK TABLES, CREATE VIEW, SHOW VIEW, CREATE ROUTINE, ALTER ROUTINE ON $dbAllTables TO $name", __METHOD__);
				$dbInstaller->db->query("GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, REFERENCES, INDEX, ALTER, CREATE TEMPORARY TABLES, LOCK TABLES, CREATE VIEW, SHOW VIEW, CREATE ROUTINE, ALTER ROUTINE ON $master.* TO $name;", __METHOD__);
				$dbInstaller->db->query("GRANT REPLICATION CLIENT ON *.* TO $name", __METHOD__); // Standard MySQL replication.
				if ($config->get('DSDBMySQLDriver') == 'aurora') {
					$dbInstaller->db->query("GRANT SELECT ON mysql.ro_replica_status TO $name", __METHOD__); // AWS Aurora replication.
				}
				$dbInstaller->db->endAtomic(__METHOD__);
			} catch (\DBQueryError $dqe) {
				$dbInstaller->db->rollback(__METHOD__);
				$status->fatal('config-install-user-grant-failed', $name, $dqe->getMessage());
			}
		}

		return $status;
	}

	/**
	 * Return a formal 'User'@'Host' username for use in queries
	 *
	 * @param string                   $name Username, quotes will be added
	 * @param string                   $host Hostname, quotes will be added
	 * @param object DatabaseInstaller
	 *
	 * @return string
	 */
	private function buildFullDBUserName($name, $host, \DatabaseInstaller $dbInstaller) {
		return $dbInstaller->db->addQuotes($name) . '@' . $dbInstaller->db->addQuotes($host);
	}

	/**
	 * Try to see if the user account exists. Our "superuser" may not have access to mysql.user, so false means "no" or "maybe".
	 *
	 * @param string                   $user Username to check
	 * @param string                   $host Hostname to check
	 * @param object DatabaseInstaller
	 *
	 * @return boolean
	 */
	private function userDefinitelyExists($user, $host, \DatabaseInstaller $dbInstaller) {
		try {
			$res = $dbInstaller->db->selectRow(
				'mysql.user',
				['Host', 'User'],
				['Host' => $host, 'User' => $user],
				__METHOD__
			);

			return (bool)$res;
		} catch (\DBQueryError $dqe) {
			return false;
		}
	}
}
