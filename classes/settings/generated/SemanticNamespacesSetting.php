<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Generated setting class for SMW (previously SemanticSettings.php)
 *
 * @author    Noah Manneschmidt
 * @copyright (c) 2015 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
 **/

namespace DynamicSettings\Settings\Generated;

use DynamicSettings\WikiContextException;

class SemanticNamespacesSetting extends GeneratedSetting {
	public function getExportedCode() {
		if (!$this->wikiContext) {
			throw new WikiContextException();
		}
		$PHP = "\$smwgNamespacesWithSemanticLinks = [
	NS_MAIN => true,
	NS_TALK => false,
	NS_USER => true,
	NS_USER_TALK => false,
	NS_PROJECT => true,
	NS_PROJECT_TALK => false,
	NS_IMAGE => true,
	NS_IMAGE_TALK => false,
	NS_MEDIAWIKI => false,
	NS_MEDIAWIKI_TALK => false,
	NS_TEMPLATE => false,
	NS_TEMPLATE_TALK => false,
	NS_HELP => true,
	NS_HELP_TALK => false,
	NS_CATEGORY => true,
	NS_CATEGORY_TALK => false,
	SMW_NS_PROPERTY  => true,
	SMW_NS_PROPERTY_TALK  => false,
	SMW_NS_TYPE => true,
	SMW_NS_TYPE_TALK => false,
	SMW_NS_CONCEPT => true,
	SMW_NS_CONCEPT_TALK => false,";

		$namespaces = $this->wikiContext->getNamespaces();
		if (is_array($namespaces) && count($namespaces)) {
			foreach ($namespaces as $snid => $namespace) {
				$nameDefine = "NS_" . strtoupper($namespace->getName());
				$talkDefine = "NS_" . strtoupper($namespace->getTalk());

				if ($namespace->isSemantic()) {
					$PHP .= "\n\t{$nameDefine} => true,";
				} else {
					$PHP .= "\n\t{$nameDefine} => false,";
				}

				$PHP .= "\n\t{$talkDefine} => false,";
			}
		}
		$PHP .= "\n];";

		return $PHP;
	}
}
