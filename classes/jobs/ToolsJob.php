<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Tools SyncService Class
 *
 * @author    Alex Smith
 * @copyright (c) 2014 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
 **/

namespace DynamicSettings\Job;

use DynamicSettings\Environment;

class ToolsJob extends \SyncService\Job {
	/**
	 * JOB QUEUE
	 *
	 * @var integer
	 */
	const JOB_QUEUE = 0;

	/**
	 * JOB START
	 *
	 * @var integer
	 */
	const JOB_START = 2;

	/**
	 * JOB ERROR
	 *
	 * @var integer
	 */
	const JOB_ERROR = 7;

	/**
	 * JOB FINISH
	 *
	 * @var integer
	 */
	const JOB_FINISH = 10;

	/**
	 * Redis Log Key
	 *
	 * @var string
	 */
	protected $logKey = 'ToolsWorkerLog';

	/**
	 * Handles distributing Tools worker tasks.
	 *
	 * @param array Named arguments passed by the command that queued this job.
	 * - task	string	Task to handle.
	 * - site_key	string	The MD5 site key for the wiki.
	 *
	 * @return boolean Success
	 */
	public function execute($args = []) {
		$tasks		= $args['tasks'];
		$siteKey	= $args['site_key'];
		$domain		= $args['domain'];
		$logKeys 	= $args['log_key'];
		$obLevel = ob_get_level();

		try {
			$htmlErrors = ini_get('html_errors');
			ini_set('html_errors', '0');

			foreach ($tasks as $index => $task) {
				ob_start();
				$success = false;
				$logKey = $logKeys[$index];
				self::updateLogBuffer($logKey, self::JOB_START, $siteKey);
				try {
					$tools = new \DynamicSettings\Tools;
					if (strlen($siteKey) == 32) {
						$params = [
							'task'		=> $task,
							'sitekey'	=> $siteKey,
							'script'	=> $args['script'],
							'loadout'	=> $args['loadout'],
							'interwiki'	=> $args['interwiki']
						];
						if (isset($args['scraperArgs'])) {
							$params['scraperArgs'] = "--redisKey ${logKey}:output " . $args['scraperArgs'];
						}
						$tools->loadParamsAndArgs('Tools', $params);
						$tools->setLogKey($logKey . ":output"); // set the tools redis logger to the output key of this job
					} else {
						throw new \Exception("Invalid SiteKey ({$siteKey}) passed for: " . $task);
					}
					if (!$tools->execute()) {
						throw new \Exception("There was a fatal issue running task: " . $task);
					}
					$success = true;
				} catch (\Exception $e) {
					echo $e->getMessage();
					$success = false;
				}

				$output = ob_get_contents();
				self::storeJobOutput($logKey, $output);
				ob_end_clean();

				if ($success) {
					self::updateLogBuffer($logKey, self::JOB_FINISH, $siteKey);
					continue;
				} else {
					self::updateLogBuffer($logKey, self::JOB_ERROR, $siteKey);
				}
				ini_set('html_errors', $htmlErrors);
				return 1; // Error
			}
		} catch (\Exception $e) {
			// A failure here is a failure of ALL tasks (presumably)
			$output = $e->getMessage();
			if (ob_get_level() > $obLevel) {
				$output .= ob_get_contents();
				ob_end_clean();
			}
			foreach ($tasks as $index => $task) {
				$logKey = $logKeys[$index];
				self::storeJobOutput($logKey, $output);
				self::updateLogBuffer($logKey, self::JOB_ERROR, $siteKey);
			}
			return 1;
		}
		return 0;
	}

	/**
	 * Store the output of a job
	 *
	 * @param string $jobKey
	 * @param string $content
	 *
	 * @return void
	 */
	public static function storeJobOutput($jobKey, $content) {
		$redis = \RedisCache::getClient('cache');
		$redis->set($jobKey . ":output", (string)$content);
	}

	/**
	 * Get output of a job
	 *
	 * @param string  $jobKey
	 * @param integer $start
	 * @param integer $end
	 *
	 * @return string
	 */
	public static function getJobOutput($jobKey, $start = false, $end = false) {
		$redis = \RedisCache::getClient('cache');
		if (is_numeric($start) && is_numeric($end)) {
			return $redis->getRange($jobKey . ":output", $start, $end);
		} else {
			return $redis->get($jobKey . ":output");
		}
	}

	/**
	 * Get a job from redis
	 *
	 * @param string $jobKey
	 *
	 * @return object
	 */
	public static function getJob($jobKey) {
		$redis = \RedisCache::getClient('cache');
		return $redis->hGetAll($jobKey);
	}

	/**
	 * Get Job Status
	 *
	 * @param string $jobKey
	 *
	 * @return mixed constant name | false
	 */
	public static function getJobStatus($jobKey) {
		$job = self::getJob($jobKey);
		$ts = new \ReflectionClass('DynamicSettings\Job\ToolsJob');
		foreach ($ts->getConstants() as $name => $value) {
			if ($value == $job['status']) {
				return $name;
			}
		}
		return false;
	}

	/**
	 * Output a line into Redis buffer.
	 *
	 * @param array Log Key(s)
	 * @param integer Status(Score)
	 * @param string Site Key
	 * @param string Domain for the wiki being process.
	 *
	 * @return void
	 */
	private static function addLogBuffer($logKeys, $status, $siteKey, $domain) {
		$redis = \RedisCache::getClient('cache');

		$checkout = Environment::getCheckoutFor($domain);
		foreach ($logKeys as $logKey) {
			$parts = [
				$logKey . "WorkerLog",
				$siteKey,
				time(),
				rand(11111, 999999999),
				$checkout
			];
			$parts = array_filter($parts);
			$redisKey = implode(':', $parts);
			$redis->lPush($logKey . "WorkerLog", $redisKey);
			$redis->hMSet($redisKey, ['siteKey' => $siteKey, 'status' => $status]);
			$redisKeys[] = $redisKey;
		}

		return $redisKeys;
	}

	/**
	 * Update the status of a job.
	 *
	 * @param string Redis Log Key
	 * @param integer Status(Score)
	 * @param string Site Key
	 *
	 * @return void
	 */
	public static function updateLogBuffer($logKey, $status, $siteKey) {
		$redis = \RedisCache::getClient('cache');
		$redis->hMSet($logKey, ['siteKey' => $siteKey, 'status' => $status]);
	}

	/**
	 * Wrap queue with addLogBuffer support.
	 *
	 * @param array Job Arguments
	 *
	 * @return array Redis Log Keys
	 */
	public static function queueJob($args) {
		$logKeys = self::addLogBuffer(array_map('ucfirst', $args['tasks']), self::JOB_QUEUE, $args['site_key'], $args['domain']);
		$args['log_key'] = $logKeys;
		self::queue($args, Environment::getCheckoutFor($args['domain']));

		return $logKeys;
	}

	/**
	 * Clear a log queue buffer.
	 *
	 * @param string Log Key for the log buffer.
	 *
	 * @return boolean Success
	 */
	public static function clearLogBuffer($logKey) {
		$redis = \RedisCache::getClient('cache');
		return (bool)$redis->del($logKey . 'WorkerLog');
	}

	/**
	 * Return portion of a log queue buffer.
	 *
	 * @param string Log Key for the log buffer.
	 * @param integer Position to start.
	 * @param integer Total items to return.
	 *
	 * @return array Log buffer items.
	 */
	public static function getLogBuffer($logKey, $start, $total) {
		$redis = \RedisCache::getClient('cache');

		$end = $start + $total;

		$entries = $redis->lRange($logKey . 'WorkerLog', $start, $end);

		$buffer = [];
		if (is_array($entries)) {
			foreach ($entries as $key) {
				$buff = $redis->hGetAll($key);
				$buff['key'] = $key;
				if (strpos($key, ':') !== false) {
					list($buff['type'], , $buff['time']) = explode(":", $key);
				}
				$buffer[] = $buff;
			}
		}

		return $buffer;
	}

	/**
	 * Return count of a log queue buffer.
	 *
	 * @param string Log Key for the log buffer.
	 *
	 * @return integer Log buffer length
	 */
	public static function getLogBufferCount($logKey) {
		$redis = \RedisCache::getClient('cache');
		return $redis->lSize($logKey . 'WorkerLog');
	}
}
