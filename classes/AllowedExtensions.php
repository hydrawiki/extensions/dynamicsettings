<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Allowed Extensions Class
 *
 * @author    Alexia E. Smith
 * @copyright (c) 2014 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
 **/

namespace DynamicSettings;

class AllowedExtensions {
	/**
	 * Mediawiki Database Object
	 *
	 * @var object
	 */
	private $DB;

	/**
	 * Allowed Extensions Container
	 *
	 * @var array
	 */
	private $allowedExtensions = [];

	/**
	 * Allowed Settings Container
	 *
	 * @var array
	 */
	private $defaultExtensions = [];

	/**
	 * Cached Object Instance
	 *
	 * @var object
	 */
	private static $instance = null;

	/**
	 * Main Constructor
	 *
	 * @return void
	 */
	public function __construct() {
		$this->DB = DSDBFactory::getMasterDB(DB_MASTER);

		$this->loadAllowedExtensions();
	}

	/**
	 * Get a cached object instance.  Use new to get a fresh instance if required.
	 *
	 * @return object Self
	 */
	public static function getInstance() {
		if (self::$instance === null) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	/**
	 * Loads the allowed settings.
	 *
	 * @return void
	 */
	private function loadAllowedExtensions() {
		$results = $this->DB->select(
			['wiki_allowed_extensions'],
			['*'],
			[],
			__METHOD__,
			[
				'ORDER BY'	=> 'extension_name ASC'
			]
		);

		while ($row = $results->fetchRow()) {
			$this->allowedExtensions[$row['md5_key']] = $row;
			if (!empty($row['dependencies'])) {
				$this->allowedExtensions[$row['md5_key']]['dependencies'] = explode(',', $row['dependencies']);
			} else {
				$this->allowedExtensions[$row['md5_key']]['dependencies'] = [];
			}

			if ($row['default_extension']) {
				// We fill this array with boolean true and the value we want as the key since the built in PHP functions nicely work off the keys.
				$this->defaultExtensions[$row['md5_key']] = true;
				$this->allowedExtensions[$row['md5_key']]['enabled'] = true;
			}
		}
	}

	/**
	 * Returns all the loaded allowed settings.
	 *
	 * @return array Multidimensional array of allowed setting information.  $allowedSettingID => $allowedSettingInfo[]
	 */
	public function getAll() {
		return $this->allowedExtensions;
	}

	/**
	 * Get an allowed extension by hash key.
	 *
	 * @param string $hashKey MD5 Key of the extension.
	 *
	 * @return mixed False for does not exist.  Array of extension information for an existing result.
	 */
	public function getByKey($hashKey) {
		if ($this->exists($hashKey)) {
			return $this->allowedExtensions[$hashKey];
		}
		return false;
	}

	/**
	 * Return only default extensions.
	 *
	 * @return array Extension Information
	 */
	public function getDefault() {
		return array_intersect_key($this->allowedExtensions, $this->defaultExtensions);
	}

	/**
	 * Return only non-default extensions.
	 *
	 * @return array Extension Information
	 */
	public function getNonDefault() {
		return array_diff_key($this->allowedExtensions, $this->defaultExtensions);
	}

	/**
	 * Check if an allowed extension exists.
	 *
	 * @param string $hashKey MD5 Key of the extension.
	 *
	 * @return boolean
	 */
	public function exists($hashKey) {
		return array_key_exists($hashKey, $this->allowedExtensions);
	}

	/**
	 * Check if an allowed extension exists and is a default extension.
	 *
	 * @param string $hashKey MD5 Key of the extension.
	 *
	 * @return boolean
	 */
	public function isDefault($hashKey) {
		return array_key_exists($hashKey, $this->defaultExtensions);
	}
}
