<?php
/**
 * DynamicSettings
 * Test Run Jobs
 *
 * @license GPL - Code used from MediaWiki
 * @package DynamicSettings
 **/

require_once dirname(__DIR__, 3) . '/maintenance/Maintenance.php';

class TestRunJobs extends Maintenance {
	/**
	 * Main Constructor
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
		$this->mDescription = "TestRunJobs";
	}

	/**
	 * Test the job runners and queue.
	 *
	 * @return void
	 */
	public function execute() {
		global $wgJobRunRate, $wgServer, $wgRunJobsAsync;

		if ($wgJobRunRate <= 0 || wfReadOnly()) {
			return;
		}

		$section = new ProfileSection(__METHOD__);

		if ($wgJobRunRate < 1) {
			$max = mt_getrandmax();
			if (mt_rand(0, $max) > $max * $wgJobRunRate) {
				return; // the higher $wgJobRunRate, the less likely we return here
			}
			$n = 1;
		} else {
			$n = intval($wgJobRunRate);
		}

		$query = [ 'title' => 'Special:RunJobs',
			'tasks' => 'jobs', 'maxjobs' => $n, 'sigexpiry' => time() + 5
		];
		$query['signature'] = SpecialRunJobs::getQuerySignature($query);

		$url = wfAppendQuery(wfScript('index'), $query);
		$test = Http::post(wfExpandUrl($wgServer . $url));

		var_dump($test);
	}
}

$maintClass = "TestRunJobs";
require_once RUN_MAINTENANCE_IF_MAIN;
