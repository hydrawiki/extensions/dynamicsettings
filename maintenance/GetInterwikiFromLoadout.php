<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Pull Interwiki's over from a loadout domain
 *
 * @author    Cameron Chunn
 * @copyright (c) 2017 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @link      https://gitlab.com/hydrawiki
 */

require_once dirname(__DIR__, 3) . "/maintenance/Maintenance.php";

/**
 * Class PopulateDefaultSkin
 */
class GetInterwikiFromLoadout extends Maintenance {
	/**
	 * Main Constructor
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
		$this->mDescription = "Pulls interwiki data from a default loadout";
		$this->addOption('loadout', 'The default loadout to pull from.', true, true);
		$this->addOption('purgeFirst', 'Delete current interwiki before importing.', true, false);
	}

	/**
	 * Chunk dump file and import single page dumps at a time.
	 *
	 * @return void
	 */
	public function execute() {
		$loadoutDomain = $this->getOption('loadout', false);
		$purgeFirst = $this->getOption('purgeFirst', false);

		$dbw = wfGetDB(DB_MASTER);

		if ($purgeFirst) {
			$this->output("Deleting current Interwiki Table\n");
			$dbw->delete('interwiki', '*', __METHOD__);
		}

		$this->output("Contacting {$loadoutDomain} to get interwiki links.");
		$iwApi = "https://" . $loadoutDomain . "/api.php?action=query&meta=siteinfo&siprop=interwikimap&format=json&formatversion=2";
		$request = \MWHttpRequest::factory($iwApi, [], __METHOD__);
		$status = $request->execute();
		if ($status->isOK()) {
			$data = @json_decode($request->getContent(), true);
			if (isset($data['query']['interwikimap'])) {
				$iwMap = $data['query']['interwikimap'];
				foreach ($iwMap as $iw) {
					$data = [];
					if (isset($iw['prefix']) && $iw['prefix']) {
						$data['iw_prefix'] = $iw['prefix'];
					}
					if (isset($iw['url']) && $iw['url']) {
						$data['iw_url'] = $iw['url'];
					}
					if (isset($iw['api']) && $iw['api']) {
						$data['iw_api'] = $iw['api'];
					}
					if (isset($iw['local']) && $iw['local']) {
						$data['iw_local'] = 1;
					}
					if (isset($iw['trans']) && $iw['trans']) {
						$data['iw_trans'] = 1;
					}

					$dbw->insert('interwiki', $data, __METHOD__, 'IGNORE');
					if ($dbw->affectedRows() === 0) {
						$this->output("Skipping " . $iw['prefix'] . " because it already exists.\n");
					} else {
						$this->output("Added " . $iw['prefix'] . " to interwiki.\n");
					}
				}
			} else {
				$this->output("ERROR: Interwiki Data appears invalid...");
			}

		} elseif (!$status->isOK()) {
			$this->output("ERROR: We were unable to pull interwiki data...");
		}
	}
}

$maintClass = "GetInterwikiFromLoadout";
require_once RUN_MAINTENANCE_IF_MAIN;
