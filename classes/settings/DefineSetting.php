<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Allowed Setting class for constant definitions
 *
 * @author    Noah Manneschmidt
 * @copyright (c) 2015 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
 **/

namespace DynamicSettings\Settings;

class DefineSetting extends Setting {
	/**
	 * @return string of php code for assigning/defining this setting for the current wiki context
	 */
	public function getExportedCode() {
		$name = var_export(strtoupper($this->data['setting_key']), true);
		$value = var_export($this->getValue(), true);
		return "if (!defined($name)) { define($name, $value); }";
	}

	/**
	 * Validates the given value against the current setting type
	 *
	 * @param string Potential value for this setting.
	 *
	 * @return boolean
	 */
	public function isValid($value) {
		return is_null($value) || is_bool($value) || is_string($value) || is_numeric($value);
	}
}
