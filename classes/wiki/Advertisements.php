<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Advertisements Class
 *
 * @author    Alexia E. Smith
 * @copyright (c) 2014 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
 **/

namespace DynamicSettings\Wiki;

use DynamicSettings\DSDBFactory;
use DynamicSettings\EditLog;
use RequestContext;

class Advertisements {
	/**
	 * Wiki Object
	 *
	 * @var object
	 */
	private $wiki;

	/**
	 * Advertisements Container
	 *
	 * @var array
	 */
	private $data = [
		'adid' => 0,
		'site_key' => null,
		'config' => null,
		'wiki_ads_disabled' => 0
	];

	/**
	 * Advertisements Configuration(Disable, Append, Override)
	 *
	 * @var array
	 */
	private $config = [];

	/**
	 * Last serach result total.
	 *
	 * @var integer
	 */
	private static $lastSearchResultTotal = 0;

	/**
	 * Constants for slot states.
	 */
	const SLOT_DISABLE = -1;
	const SLOT_APPEND = 0;
	const SLOT_OVERRIDE = 1;

	/**
	 * Slot types.  Do not reorder this list.
	 *
	 * @var array
	 */
	public static $slotTypes = [
		'id',
		'js',
		'ad',
		'misc'
	];

	/**
	 * Leftover cruft.
	 *
	 * @var array
	 */
	private static $miscSlots = [
		'footerlinks'
	];

	/**
	 * Identifiers for something.
	 *
	 * @var array
	 */
	private static $idSlots = [
		'googleanalyticsid'
	];

	/**
	 * Javascript Slots
	 *
	 * @var array
	 */
	private static $jsSlots = [
		'analytics',
		'instart',
		'jsbot',
		'jstop',
		'mobilebannerjs',
		'mobilejsbot',
		'mobilejstop'
	];

	/**
	 * Advertisement Slots
	 *
	 * @var array
	 */
	private static $adSlots = [
		'anchor',
		'atflb',
		'atfmrec',
		'btfhero',
		'btflb',
		'btfmrec',
		'btfsrec',
		'footermrec',
		'middlemrec',
		'mobileatflb',
		'mobileatfmrec',
		'mobilebtfmrec'
	];

	/**
	 * Default Slots
	 *
	 * @var array
	 */
	private static $defaultSlots = [
		// Miscellaneous
		'footerlinks' => '',
		// Identifiers
		'googleanalyticsid' => '',
		// Javascript
		'analytics' => '',
		'instart' => '',
		'jsbot' => '',
		'jstop' => '',
		'mobilebannerjs' => '',
		'mobilejsbot' => '',
		'mobilejstop' => '',
		// Advertisement
		'anchor' => '',
		'atflb' => '',
		'atfmrec' => '',
		'btfhero' => '',
		'btflb' => '',
		'btfmrec' => '',
		'btfsrec' => '',
		'footermrec' => '',
		'middlemrec' => '',
		'mobileatflb' => '',
		'mobileatfmrec' => '',
		'mobilebtfmrec' => ''
	];

	/**
	 * Main Constructor
	 *
	 * @param object Wiki
	 *
	 * @return void
	 */
	private function __construct(\DynamicSettings\Wiki $wiki) {
		$this->wiki = $wiki;
	}

	/**
	 * Load multiple Wiki Objects into an array based on search parameters.
	 *
	 * @param integer Zero based start position.
	 * @param integer Total number of results to return.
	 * @param string [Optional] Search term to filter by.
	 * @param string [Optional] Database field name to sort by, defaults to 'wiki_name'.
	 * @param string [Optional] Database sort direction, defaults to 'ASC'.
	 * @param boolean [Optional] Hide deleted wikis, default false.
	 * @param boolean [Optional] Hide in development wikis, default false.
	 *
	 * @return array an array of resulting objects, possibly empty.
	 */
	public static function loadFromSearch($start, $itemsPerPage, $searchTerm = null, $sortKey = 'wiki_name', $sortDir = 'ASC', $hideDeleted = false) {
		$db = DSDBFactory::getMasterDB(DB_MASTER);
		$searchableFields = array_merge(self::getSlots(), ['wiki_name', 'domain', 'md5_key']);
		$tables = ['wiki_sites', 'wiki_advertisements', 'wiki_domains'];

		if (!empty($searchTerm)) {
			// Normal table search.
			foreach ($searchableFields as $field) {
				$where[] = "`" . $field . "` LIKE '%" . $db->strencode($searchTerm) . "%'";
			}
			$where = "(" . implode(' OR ', $where) . ")";
		}

		$joins['wiki_advertisements'] = [
			'LEFT JOIN', 'wiki_sites.md5_key = wiki_advertisements.site_key'
		];
		$joins['wiki_domains'] = [
			'LEFT JOIN', 'wiki_domains.site_key = wiki_sites.md5_key'
		];
		$and[] = "wiki_sites.deleted = 0";
		$and[] = "wiki_domains.type = " . Domains::getDomainEnvironment();
		$and[] = "(wiki_advertisements.site_key != -1 OR wiki_advertisements.site_key IS NULL OR CHAR_LENGTH(wiki_advertisements.site_key) >= 32)";

		if (count($and)) {
			if (!empty($where)) {
				$where .= ' AND (' . implode(' AND ', $and) . ')';
			} else {
				$where = implode(' AND ', $and);
			}
		}

		$options['ORDER BY'] = ($db->fieldExists('wiki_advertisements', $sortKey) || $sortKey == 'domain' ? $sortKey : 'wiki_name') . ' ' . (strtoupper($sortDir) == 'DESC' ? 'DESC' : 'ASC');
		if ($start !== null) {
			$options['OFFSET'] = $start;
		}
		if ($itemsPerPage !== null) {
			$options['LIMIT'] = $itemsPerPage;
		}

		$wikis = [];
		$results = $db->select(
			$tables,
			['*'],
			$where,
			__METHOD__,
			$options,
			$joins
		);

		if (!$results) {
			self::$lastSearchResultTotal = 0;
			return [];
		}
		while ($row = $results->fetchRow()) {
			$wiki = \DynamicSettings\Wiki::loadFromHash($row['md5_key']);
			if ($wiki !== false) {
				$wikis[$row['md5_key']] = $wiki;
			}
		}

		$resultsTotal = $db->select(
			$tables,
			['count(*) as total'],
			$where,
			__METHOD__,
			null,
			$joins
		);
		$resultsTotal = $resultsTotal->fetchRow();
		self::$lastSearchResultTotal = intval($resultsTotal['total']);

		return $wikis;
	}

	/**
	 * Return the last search result total.
	 *
	 * @return integer Total
	 */
	public static function getLastSearchTotal() {
		return intval(self::$lastSearchResultTotal);
	}

	/**
	 * Load a new Settings object from Wiki object.
	 *
	 * @param object Wiki
	 *
	 * @return mixed Setting object or false on failure.
	 */
	public static function loadFromWiki(\DynamicSettings\Wiki $wiki) {
		$advertisements = new Advertisements($wiki);
		$db = DSDBFactory::getMasterDB(DB_MASTER);
		$results = $db->select(
			['wiki_advertisements'],
			['*'],
			['site_key' => ($wiki->getSiteKey() !== false ? $wiki->getSiteKey() : "-1")], // The -1 has to be a string.
			__METHOD__
		);

		if ($results !== false && !$advertisements->load($results)) {
			$advertisements = false;
		}

		return $advertisements;
	}

	/**
	 * Loads advertisements for this wiki.
	 *
	 * @param mixed Fetched database results.
	 *
	 * @return boolean Success
	 */
	public function load($results) {
		$row = $results->fetchRow();
		$row['config'] = @json_decode($row['config'], true);
		if (!is_array($row['config'])) {
			$row['config'] = [];
		}

		$this->data = array_merge($this->data, $row);

		// Get defaults for reference.
		$db = DSDBFactory::getMasterDB(DB_MASTER);
		$result = $db->select(
			['wiki_advertisements'],
			self::getSlots(),
			['site_key' => '-1'],
			__METHOD__
		);
		$defaults = $result->fetchRow();

		foreach (self::$defaultSlots as $slot => $data) {
			if (isset($defaults[$slot])) {
				self::$defaultSlots[$slot] = $defaults[$slot];
			}
		}

		return true;
	}

	/**
	 * Save advertisements.
	 *
	 * @return boolean Success
	 */
	public function save() {
		$wgUser = RequestContext::getMain()->getUser();

		$db = DSDBFactory::getMasterDB(DB_MASTER);

		$success = false;

		$save = [
			'site_key'			=> ($this->wiki->getSiteKey() !== false ? $this->wiki->getSiteKey() : -1),
			'wiki_ads_disabled'	=> intval($this->data['wiki_ads_disabled']),
			'config'			=> json_encode($this->data['config'])
		];
		foreach (self::getSlots() as $slot) {
			$save[$slot] = (!empty($this->data[$slot]) ? $this->data[$slot] : '');
		}

		$db->startAtomic(__METHOD__);
		if ($this->data['adid']) {
			$result = $db->update(
				'wiki_advertisements',
				$save,
				['site_key' => $save['site_key']],
				__METHOD__
			);

			$databaseId = $this->data['adid'];
		} else {
			$result = $db->insert(
				'wiki_advertisements',
				$save,
				__METHOD__
			);

			$databaseId = $db->insertId();
		}

		if (!$result) {
			$db->cancelAtomic(__METHOD__);
		} else {
			$success = true;
			$db->endAtomic(__METHOD__);

			$this->data['adid'] = $databaseId;

			$miscData = [
				'display_name' => $this->wiki->getNameForDisplay()
			];

			EditLog::addEntry(
				$this->wiki->getSiteKey(), // Log Key: site_key
				$save,
				__METHOD__,
				$wgUser,
				'`' . $this->wiki->getNameForDisplay() . '` advertisements changed',
				$miscData
			);
		}

		return $success;
	}

	/**
	 * Returns all configured advertisements for this wiki.
	 *
	 * @return array Multidimensional array of wiki advertisement information.  $advertisementId => $advertisement[]
	 */
	public function getAll() {
		return $this->data;
	}

	/**
	 * Return the database ID for advertisements row.
	 *
	 * @return integer Database ID
	 */
	public function getDatabaseId() {
		return intval($this->data['adid']);
	}

	/**
	 * Return the content for the specified slot.
	 *
	 * @param string Slot Name
	 *
	 * @return mixed Advertisement text or false to not render the slot.
	 */
	public function getBySlot($slot) {
		/*
			Rules:
			1.) If all slots or the individual slot is disabled, return false to not render.
			2.) If SLOT_APPEND, the default state, and a default slot is set then return this wiki's slot appended to it.
			3.) Finally, if the trimmed content is empty then return false to not render.
		*/

		if (!in_array($slot, self::getSlots()) || $this->getConfigBySlot($slot) === self::SLOT_DISABLE || $this->isDisabled()) {
			// Don't render if disabled or the slot is invalid.
			return false;
		}

		$rawSlot = $this->getRawSlot($slot);
		if ($this->getConfigBySlot($slot) === self::SLOT_APPEND && isset(self::$defaultSlots[$slot])) {
			$slot = self::$defaultSlots[$slot] . "\n" . $rawSlot;
		} else {
			$slot = $rawSlot;
		}
		$slot = trim($slot);

		// Check if the output is empty and return false if so.
		if (empty($slot)) {
			return false;
		}

		return $slot;
	}

	/**
	 * Return the raw content for the specified slot.
	 * Meant for editing.  Does not append, override, or respect disablement.
	 *
	 * @param string Slot Name
	 *
	 * @return string Slot Data
	 */
	public function getRawSlot($slot) {
		if (!isset($this->data[$slot])) {
			return '';
		}
		return $this->data[$slot];
	}

	/**
	 * Set the advertisement for the specified slot.
	 *
	 * @param string Advertisement Slot
	 * @param string Advertisement Text
	 * @param string [Reference] Error that occurred from Tidy.
	 *
	 * @return boolean Success
	 */
	public function setBySlot($slot, $text, &$error) {
		if (!in_array($slot, self::getSlots())) {
			return false;
		}

		$this->data[$slot] = $text;

		if (\MWTidy::isEnabled()) {
			$tidy = \MWTidy::singleton();
			if ($tidy->supportsValidate()) {
				if (!$tidy->validate($text, $error)) {
					return false;
				}
			}
		}

		return true;
	}

	/**
	 * Return the default text for the specified slot.
	 *
	 * @param string Slot Name
	 *
	 * @return string Slot Text
	 */
	public function getDefaultBySlot($slot) {
		if (isset(self::$defaultSlots[$slot])) {
			return self::$defaultSlots[$slot];
		}
		return '';
	}

	/**
	 * Get the configuration for this wiki.
	 *
	 * @return array Configuration ['slotName' => SLOT_CONSTANT]
	 */
	public function getConfig() {
		return (array)$this->data['config'];
	}

	/**
	 * Get the configuration for this wiki.
	 *
	 * @param string Slot Name
	 *
	 * @return array Configuration ['slotName' => SLOT_CONSTANT]
	 */
	public function getConfigBySlot($slot) {
		if (isset($this->data['config'][$slot])) {
			return intval($this->data['config'][$slot]);
		}
		return self::SLOT_APPEND;
	}

	/**
	 * Set the configuration for this wiki.
	 *
	 * @param string Configuration ['slotName' => SLOT_CONSTANT]
	 *
	 * @return boolean Success
	 */
	public function setConfig($config = []) {
		if (!is_array($config)) {
			throw new \MWException("Invalid configuration data '" . gettype($config) . "'.  Array expected.");
		}
		if (empty($this->wiki->getSiteKey())) {
			$this->data['config'] = [];
			wfDebug("Must have a valid child wiki to set a configuration on.");
			return false;
		}
		foreach ($config as $slot => $state) {
			if ($state < self::SLOT_DISABLE || $state > self::SLOT_OVERRIDE) {
				throw new \MWException("Invalid slot state {$state} for {$slot}.");
			}
		}
		$this->data['config'] = $config;
		return true;
	}

	/**
	 * Return if this wiki context has customized advertisement slots.
	 *
	 * @return boolean Has customized(not default) slots.
	 */
	public function hasCustomSlots() {
		foreach (self::getSlots() as $slot) {
			if (array_key_exists($slot, $this->data) && !empty($this->data[$slot])) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Are advertisements disabled for this wiki?
	 *
	 * @return boolean Disabled
	 */
	public function isDisabled() {
		return (isset($this->data['wiki_ads_disabled']) ? boolval($this->data['wiki_ads_disabled']) : false);
	}

	/**
	 * Set advertisements as disabled.
	 *
	 * @param boolean Set to disabled.
	 *
	 * @return void
	 */
	public function setDisabled($disabled = true) {
		$this->data['wiki_ads_disabled'] = intval($disabled);
	}

	/**
	 * Get all slots or just a certain type.
	 *
	 * @param string [Optional] Slot Type
	 *
	 * @return array Slots
	 */
	public static function getSlots($type = null) {
		if ($type !== null && in_array($type, self::$slotTypes)) {
			$property = $type . 'Slots';
			return self::$$property;
		}
		return array_merge(self::$adSlots, self::$idSlots, self::$jsSlots, self::$miscSlots);
	}

	/**
	 * Get advertisement slots.
	 *
	 * @return array Slots
	 */
	public static function getAdSlots() {
		return self::$adSlots;
	}

	/**
	 * Get identifier slots.
	 *
	 * @return array Slots
	 */
	public static function getIdSlots() {
		return self::$idSlots;
	}

	/**
	 * Get Javascript slots.
	 *
	 * @return array Slots
	 */
	public static function getJsSlots() {
		return self::$jsSlots;
	}

	/**
	 * Get miscellaneous slots.
	 *
	 * @return array Slots
	 */
	public static function getMiscSlots() {
		return self::$miscSlots;
	}
}
