<?php
/**
 * Curse Inc.
 * Dynamic Settings
 *
 * @author    Alex Smith
 * @copyright (c) 2014 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
 **/

namespace DynamicSettings;

class Constants {
	/**
	 * Initialize constants relevant to DynamicSettings.
	 *
	 * @return void
	 */
	public static function initConstants() {
		global $wgReverbNotifications, $wgSyncServices;

		if (!defined('SITE_DIR')) {
			define('SITE_DIR', dirname(__DIR__, 3));
		}

		if (!defined('SITES_FOLDER')) {
			define('SITES_FOLDER', SITE_DIR . '/sites');
		}

		if (!defined('MAINTENANCE_DIR')) {
			define('MAINTENANCE_DIR', SITE_DIR . '/maintenance');
		}

		$reverbNotifications = [
			"site-management-wiki-edit" => [
				"importance" => 1,
				"requires" => ["hydra_admin"]
			],
			"site-management-tools" => [
				"importance" => 1,
				"requires" => ["hydra_admin"]
			]
		];
		$wgReverbNotifications = array_merge($wgReverbNotifications, $reverbNotifications);

		$wgSyncServices["GenerateAllWikiSitemapsJob"] = "DynamicSettings\\Job\\GenerateAllWikiSitemapsJob";
		$wgSyncServices["GlobalPageJob"] = "DynamicSettings\\Job\\GlobalPageJob";
		$wgSyncServices["GlobalPageRunner"] = "DynamicSettings\\Job\\GlobalPageRunner";
		$wgSyncServices["PingAllSitemapsJob"] = "DynamicSettings\\Job\\PingAllSitemapsJob";
		$wgSyncServices["ToolsJob"] = "DynamicSettings\\Job\\ToolsJob";
	}
}
