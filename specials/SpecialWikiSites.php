<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Wiki Sites Special Page
 *
 * @package   DynamicSettings
 * @author    Alex Smith
 * @copyright (c) 2014 Curse Inc.
 * @license   GPL-2.0-or-later
 * @link      https://gitlab.com/hydrawiki
**/

use DynamicSettings\Lock;
use DynamicSettings\Node\DatabaseNodes;
use DynamicSettings\Sites;
use DynamicSettings\SpecialPage as DSSpecialPage;
use DynamicSettings\Tools;
use DynamicSettings\Wiki;
use DynamicSettings\Wiki\Domains;

class SpecialWikiSites extends DSSpecialPage {
	/**
	 * Output HTML
	 *
	 * @var string
	 */
	private $content;

	/**
	 * Array of possible Wiki objects for listing and searching.
	 *
	 * @var array
	 */
	private $wikis;

	/**
	 * Wiki object storage for form editing.
	 *
	 * @var array
	 */
	private $wiki;

	/**
	 * SiteSettings object
	 *
	 * @var object
	 */
	private $siteSettings;

	/**
	 * Valid actions for this page.
	 *
	 * @var array
	 */
	private $validActions = [
		'add',
		'edit',
		'delete',
		'massedit',
		'massaction',
		'resetSearch',
		'search',
		'undelete'
	];

	/**
	 * Current action.
	 *
	 * @var string|null
	 */
	private $action = null;

	/**
	 * Main Constructor
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct('WikiSites', 'wiki_sites');
	}

	/**
	 * Main Executor
	 *
	 * @param string $subpage Sub page passed in the URL.
	 *
	 * @return void [Outputs to screen]
	 */
	public function execute($subpage) {
		$this->checkPermissions();

		if (Lock::isLocked() == true) {
			$this->output->showErrorPage('dynamic_settings_error', 'error_settings_locked');
			return;
		}

		$this->siteSettings = new Sites();

		$this->templatesWikiSites = new TemplateWikiSites();

		$this->output->addModuleStyles(['ext.wikiSites.styles']);
		$this->output->addModules(['ext.wikiSites.scripts']);

		$this->wikis = [];

		$this->setHeaders();

		if (!$this->loadMassActionWikis()) {
			return;
		}

		if (in_array($this->getRequest()->getVal('action'), $this->validActions)) {
			$this->action = $this->getRequest()->getVal('action');
		}

		$section = $this->wgRequest->getVal('section');
		switch ($section) {
			default:
			case 'list':
				$this->wikiSites();
				break;
			case 'wiki':
				$this->wikiForm();
				break;
			case 'togglelock':
				$this->toggleLock();
				break;
			case 'recache':
			case 'update':
			case 'rebuildLanguage':
			case 'searchReindex':
				$this->runTool($section);
				break;
			case 'install':
				$loadout = $this->wgRequest->getVal('loadout');
				$interwiki = $this->wgRequest->getVal('interwiki');

				$this->setSkin();

				$this->runTool('install', [
					'loadout' => $loadout,
					'interwiki' => $interwiki
				]);
				break;
			case 'maintenanceRunner':
				$this->maintenanceRunner();
				break;
			case 'scrape':
				$this->runTool('scrape', [
					'scraperArgs' => $this->wgRequest->getVal('scraperArgs')
				]);
				break;
		}

		$this->output->addHTML($this->content);
	}

	/**
	 * Wiki Sites
	 *
	 * @return void [Outputs to screen]
	 */
	public function wikiSites() {
		$start = $this->wgRequest->getInt('st');
		$itemsPerPage = 100;
		$searchTerm = null;
		$cookieExpire = time() + 900;

		if ($this->action == 'resetSearch') {
			$this->wgRequest->response()->setcookie('wikiSitesSearchTerm', '', 1);
		} else {
			$listSearch = $this->wgRequest->getVal('list_search');
			$cookieSearch = $this->wgRequest->getCookie('wikiSitesSearchTerm');
			if (($this->action == 'search' && !empty($listSearch)) || !empty($cookieSearch)) {
				if (!empty($cookieSearch) && empty($listSearch)) {
					$searchTerm = $cookieSearch;
				} else {
					$searchTerm = $listSearch;
				}
				$this->wgRequest->response()->setcookie('wikiSitesSearchTerm', $searchTerm, $cookieExpire);
				$extra['list_search'] = $searchTerm;
			}
		}

		/**
		 * Show/Hide Options
		 */
		$hideDeleted = false;
		if ($this->wgRequest->getVal('hideDeleted') == '1' || ($this->wgRequest->getCookie('hideDeletedWikis') == '1' && $this->wgRequest->getVal('hideDeleted') != '0')) {
			$hideDeleted = true;
			$this->wgRequest->response()->setcookie('hideDeletedWikis', '1');
		} elseif ($this->wgRequest->getVal('hideDeleted') == '0') {
			$this->wgRequest->response()->setcookie('hideDeletedWikis', '0');
		}
		$extra['hideDeleted'] = intval($hideDeleted);

		if ($this->wgRequest->getCookie('wikiSitesSortKey') && !$this->wgRequest->getVal('sort')) {
			$sort = $this->wgRequest->getCookie('wikiSitesSortKey');
		} else {
			$sort = $this->wgRequest->getVal('sort');
		}
		if (!empty($sort)) {
			$sortKey = $sort;
		} else {
			$sortKey = 'wiki_name';
		}
		$extra['sort'] = $sortKey;
		$this->wgRequest->response()->setcookie('wikiSitesSortKey', $sortKey, $cookieExpire);

		$sortDir = $this->wgRequest->getVal('sort_dir');
		if ((strtolower($this->wgRequest->getCookie('wikiSitesSortDir')) == 'desc' && !$sortDir) || strtolower($sortDir) === 'desc') {
			$sortDir = 'DESC';
		} else {
			$sortDir = 'ASC';
		}
		$extra['sort_dir'] = $sortDir;
		$this->wgRequest->response()->setcookie('wikiSitesSortDir', $sortDir, $cookieExpire);

		$this->wikis = Wiki::loadFromSearch($start, $itemsPerPage, $searchTerm, $sortKey, $sortDir, $hideDeleted);

		$total = Wiki::getLastSearchTotal();

		$pagination = HydraCore::generatePaginationHtml($this->getFullTitle(), $total, $itemsPerPage, $start, 4, $extra);

		$this->output->setPageTitle(wfMessage('wikisites'));
		$this->content = $this->templatesWikiSites->wikiSites($this->wikis, $pagination, $hideDeleted, $sortKey, strtolower($sortDir), $searchTerm);
	}

	/**
	 * Entry point for various wiki forms.  Handles most of the common setup.
	 * Add, Edit, Mass Edit, Delete, Undelete
	 *
	 * @return void [Outputs to screen]
	 */
	public function wikiForm() {
		$editMode = false;
		$massEditMode = false;

		$siteKey = $this->wgRequest->getVal('siteKey');

		if (Lock::isLocked($siteKey) == true) {
			$this->output->showErrorPage('dynamic_settings_error', 'error_wiki_locked');
			return;
		}

		if ($this->action === null) {
			$this->action = 'add';
		}

		if ($this->action == 'edit' || $this->action == 'delete' || $this->action == 'undelete') {
			$editMode = true;

			$this->wiki = Wiki::loadFromHash($siteKey);

			if ($this->wiki === false) {
				$this->output->showErrorPage('site_settings_error', 'wiki_not_found');
				return;
			}

			if ($this->wiki->isDeleted() && $this->action != 'undelete') {
				$this->output->showErrorPage('site_settings_error', 'deleted_wikis_no_modifications');
				return;
			}
		} elseif ($this->action == 'massedit') {
			$massEditMode = true;

			foreach ($this->wikis as $wiki) {
				if ($wiki->isDeleted()) {
					$this->output->showErrorPage('site_settings_error', 'deleted_wikis_no_modifications');
					return;
				}
			}
		}

		// This check is performed after the edit check above so that bad site loads do not go through the edit form.
		if (!$this->wiki) {
			$this->wiki = Wiki::loadFromNew();
		}

		if (!empty($this->wiki->getSiteKey())) {
			switch ($this->action) {
				case 'delete':
					$this->wikiDelete();
					return;
				case 'undelete':
					$this->wikiUndelete();
					return;
			}
		}

		if (!$this->wgUser->isAllowed('wiki_add_edit')) {
			throw new PermissionsError('wiki_add_edit');
		}

		$errors = [];
		if ($this->getRequest()->getVal('do') === 'save') {
			$errors = $this->wikiSave($massEditMode);
		}

		$databaseNodes = new DatabaseNodes();
		$this->output->addJsConfigVars('dbNodesConfig', $databaseNodes->getConfig(true));

		$config = ConfigFactory::getDefaultInstance()->makeConfig('main');
		$this->output->addJsConfigVars('HydraTLDDevelopment', $config->get('HydraTLDDevelopment'));
		$this->output->addJsConfigVars('HydraTLDLive', $config->get('HydraTLDLive'));
		$this->output->addJsConfigVars('HydraTLDStaging', $config->get('HydraTLDStaging'));

		switch ($this->action) {
			case 'massedit':
				$this->output->setPageTitle(wfMessage('mass_edit_wikis')->escaped());
				break;
			case 'edit':
				$this->output->setPageTitle(wfMessage('edit_wiki')->escaped() . ' - ' . $this->wiki->getNameForDisplay());
				break;
			default:
			case 'add':
				$this->output->setPageTitle(wfMessage('add_wiki')->escaped());
				break;
		}

		$this->content = $this->templatesWikiSites->wikiForm(
			$this->wiki,
			$this->wikis,
			$errors,
			$this->action,
			Language::fetchLanguageNames(),
			Sites::getAllCategories(),
			Sites::getAllManagers(),
			Sites::$dbTypes,
			Sites::$searchTypes,
			Sites::getPortalMasters(),
			Sites::getGroupMasters(),
			Sites::getAllTags(),
			$databaseNodes->getAvailableNodesFromWorker()
		);
	}

	/**
	 * Saves submitted Wiki Forms.
	 *
	 * @param boolean $massEditMode [Optional] Is this mass edit mode?
	 *
	 * @return array Array of errors by form_field => error message.
	 */
	private function wikiSave($massEditMode = false) {
		$errors = [];

		$massEditSections = [];

		if ($massEditMode) {
			$massEditSections = $this->getRequest()->getArray('mass_edit_section', []);
			$wikis = $this->wikis;
		} else {
			$wikis[] = $this->wiki;
		}

		if ($this->getRequest()->wasPosted() && $this->getRequest()->getVal('do') === 'save') {
			foreach ($wikis as $wiki) {
				// Name/Domain
				if (!$massEditMode && $this->wgUser->isAllowed('sites_edit_name_domain')) {
					// Wiki Name
					$status = $wiki->setName($this->wgRequest->getText('wiki_name'));
					$wiki->extractStatusValueErrors($status, $errors);

					// Meta Name
					$status = $wiki->setMetaName($this->wgRequest->getText('wiki_meta_name'));
					$wiki->extractStatusValueErrors($status, $errors);

					// Domain
					$originalDomains = [
						Domains::ENV_PRODUCTION => $wiki->getDomains()->getDomain(Domains::ENV_PRODUCTION),
						Domains::ENV_DEVELOPMENT => $wiki->getDomains()->getDomain(Domains::ENV_DEVELOPMENT),
						Domains::ENV_STAGING => $wiki->getDomains()->getDomain(Domains::ENV_STAGING)
					];

					$status = $wiki->getDomains()->setDomain($this->wgRequest->getText('wiki_domain'), Domains::ENV_PRODUCTION);
					$wiki->extractStatusValueErrors($status, $errors);

					// Domain Development
					$status = $wiki->getDomains()->setDomain($this->wgRequest->getText('wiki_domain_local'), Domains::ENV_DEVELOPMENT);
					$wiki->extractStatusValueErrors($status, $errors);

					// Domain Staging
					$status = $wiki->getDomains()->setDomain($this->wgRequest->getText('wiki_domain_staging'), Domains::ENV_STAGING);
					$wiki->extractStatusValueErrors($status, $errors);

					// Domain Redirects
					// Redirects should always be processed after regular domains as its logic will check duplicates against the updated regular domain information.
					$domainRedirects = [];
					if (strlen(mb_strtolower($this->wgRequest->getText('wiki_domain_redirects'), 'UTF-8'))) {
						$_domains = explode("\n", $this->wgRequest->getText('wiki_domain_redirects'));
						foreach ($_domains as $index => $domain) {
							$domain = trim($domain);
							if (empty($domain)) {
								continue;
							}
							$domainRedirects[$index] = $domain;
						}
					}
					$domainRedirects = array_unique($domainRedirects);
					$status = $wiki->getDomains()->setRedirects($domainRedirects);
					$wiki->extractStatusValueErrors($status, $errors);
				}

				// Information
				if ($this->wgUser->isAllowed('sites_edit_information')) {
					if (!$massEditMode || ($massEditMode && in_array('wiki_information', $massEditSections))) {
						// Category
						$status = $wiki->setCategory($this->wgRequest->getText('wiki_category'));
						$wiki->extractStatusValueErrors($status, $errors);

						// Tags
						if (strlen(mb_strtolower($this->wgRequest->getText('wiki_tags'), 'UTF-8'))) {
							$tags = explode(',', $this->wgRequest->getText('wiki_tags'));
							$status = $wiki->setTags($tags);
						} else {
							$status = $wiki->setTags([]);
						}
						$wiki->extractStatusValueErrors($status, $errors);
					}

					if (!$massEditMode) {
						// Language
						$status = $wiki->setLanguage($this->wgRequest->getVal('wiki_language'));
						$wiki->extractStatusValueErrors($status, $errors);
					}

					$removedManagers = [];
					$addedManagers = [];
					if (!$massEditMode || ($massEditMode && in_array('wiki_managers', $massEditSections))) {
						// Managers
						$previousManagers = $wiki->getManagers();
						$status = $wiki->setManagers($this->wgRequest->getArray('wiki_managers', []));
						$wiki->extractStatusValueErrors($status, $errors);
						$newManagers = $wiki->getManagers();
						$removedManagers = array_diff($previousManagers, $newManagers);
						$addedManagers = array_diff($newManagers, $previousManagers);
					}

					if (!$massEditMode || ($massEditMode && in_array('wiki_notes', $massEditSections))) {
						// Notes
						$status = $wiki->setNotes($this->wgRequest->getVal('wiki_notes'));
						$wiki->extractStatusValueErrors($status, $errors);
					}
				}

				// Wiki "Type"
				if ($this->wgUser->isAllowed('sites_edit_type')) {
					if (!$massEditMode || ($massEditMode && in_array('portal_option', $massEditSections))) {
						// Portal Type
						$status = $wiki->setPortalKey($this->wgRequest->getVal('portal'));
						$wiki->extractStatusValueErrors($status, $errors);
					}

					if (!$massEditMode || ($massEditMode && in_array('group_option', $massEditSections))) {
						// Grouping
						$status = $wiki->setGroupKey($this->wgRequest->getVal('group'));
						$wiki->extractStatusValueErrors($status, $errors);

						$status = $wiki->setGroupFileRepo($this->wgRequest->getBool('group_file_repo'));
						$wiki->extractStatusValueErrors($status, $errors);
					}
				}

				// Database
				if (!$massEditMode && $this->wgUser->isAllowed('sites_edit_database')) {
					$status = $wiki->setAWSRegion($this->wgRequest->getVal('aws_region'));
					$wiki->extractStatusValueErrors($status, $errors);

					$status = $wiki->setS3Bucket($this->wgRequest->getVal('s3_bucket'));
					$wiki->extractStatusValueErrors($status, $errors);

					$status = $wiki->setCloudfrontId($this->wgRequest->getVal('cloudfront_id'));
					$wiki->extractStatusValueErrors($status, $errors);

					$status = $wiki->setCloudfrontDomain($this->wgRequest->getVal('cloudfront_domain'));
					$wiki->extractStatusValueErrors($status, $errors);

					$database['db_cluster'] = $this->wgRequest->getText('db_cluster');
					$database['db_name'] = $this->wgRequest->getText('db_name');
					$status = $wiki->setDatabase($database);
					$wiki->extractStatusValueErrors($status, $errors);
				}

				// Search
				if (!$massEditMode && $this->wgUser->isAllowed('sites_edit_search')) {
					$search['search_type'] = $this->wgRequest->getVal('search_type');
					$search['search_server'] = $this->wgRequest->getText('search_server');
					$search['search_port'] = $this->wgRequest->getInt('search_port');
					$status = $wiki->setSearchSetup($search);
					$wiki->extractStatusValueErrors($status, $errors);
				}

				// Commit Message
				$commitMessage = trim($this->wgRequest->getText('commit_message'));
				if (!$commitMessage) {
					$errors['commit_message'] = wfMessage('error_no_commit_message')->escaped();
				}

				if (!count($errors)) {
					if ($wiki->save($commitMessage)) {
						if (!$massEditMode) {
							if (!$wiki->getDomains()->save()) {
								throw new MWException('There was a fatal error saving the domain data for this wiki.');
							}

							$newDomains = [
								Domains::ENV_PRODUCTION => $wiki->getDomains()->getDomain(Domains::ENV_PRODUCTION),
								Domains::ENV_DEVELOPMENT => $wiki->getDomains()->getDomain(Domains::ENV_DEVELOPMENT),
								Domains::ENV_STAGING => $wiki->getDomains()->getDomain(Domains::ENV_STAGING)
							];

							foreach ($originalDomains as $key => $value) {
								if ($newDomains[$key] !== $value) {
									$wiki->getDomains()->logDomainChange($value, $newDomains[$key], $key);
								}
							}
						}

						$this->emailAffectedManagers($wiki, $removedManagers, $addedManagers);
					} else {
						throw new MWException('There was a fatal error saving the data for this wiki.');
					}
				}
			}
			if (!count($errors)) {
				$this->output->redirect($this->getPageTitle()->getFullURL());
				return;
			}
		}

		return $errors;
	}

	/**
	 * Wiki Deletion
	 *
	 * @access private
	 *
	 * @return void [Outputs to screen]
	 */
	private function wikiDelete() {
		if (!$this->wgUser->isAllowed('wiki_delete')) {
			throw new PermissionsError('wiki_delete');
		}

		$errorHtml = null;

		if ($this->wgRequest->wasPosted() && $this->wgRequest->getVal('do') == 'confirm') {
			if ($this->wiki->delete()) {
				$this->runTool('recache');
				return;
			} else {
				$errorHtml = wfMessage('error-wiki_delete')->escaped();
			}
		}
		$this->output->setPageTitle(wfMessage('delete_wiki') . ' - ' . $this->wiki->getName());
		$this->content = $this->templatesWikiSites->wikiDelete($this->wiki, true, $errorHtml);
	}

	/**
	 * Wiki Undeletion
	 *
	 * @access private
	 *
	 * @return void [Outputs to screen]
	 */
	private function wikiUndelete() {
		if (!$this->wgUser->isAllowed('wiki_undelete')) {
			throw new PermissionsError('wiki_undelete');
		}

		$errorHtml = null;

		$conflicts = $this->wiki->getDomains()->checkForConflicts();
		if (count($conflicts)) {
			$conflictLines = [];
			foreach ($conflicts as $conflict) {
				$conflictLines[] = "<li>" . $conflict['site_key'] . " - " . $conflict['domain'] . "</li>";
			}
			$errorHtml = wfMessage('error-wiki_undelete_conflicts')->escaped() . "<ul>" . implode("\n", $conflictLines) . "</ul>";
		}
		if ($this->wgRequest->wasPosted() && $this->wgRequest->getVal('do') == 'confirm') {
			$unDeleteSuccess = $this->wiki->undelete();
			if ($unDeleteSuccess->isGood()) {
				$this->runTool('recache');
				return;
			}
		}
		$this->output->setPageTitle(wfMessage('undelete_wiki') . ' - ' . $this->wiki->getName());
		$this->content = $this->templatesWikiSites->wikiDelete($this->wiki, false, $errorHtml);
	}

	/**
	 * Toggle Wiki Lock
	 *
	 * @return void [Outputs to screen]
	 */
	public function toggleLock() {
		if (!$this->wgRequest->wasPosted()) {
			throw new ErrorPageError(wfMessage('dynamic_settings_error'), wfMessage('error_must_be_posted'));
		}

		$siteKey = $this->wgRequest->getVal('siteKey');

		$this->wiki = Wiki::loadFromHash($siteKey);

		if ($this->wiki === false) {
			$this->output->showErrorPage('site_settings_error', 'wiki_not_found');
			return;
		}

		if ($this->wiki->isDeleted()) {
			$this->output->showErrorPage('site_settings_error', 'deleted_wikis_no_modifications');
			return;
		}

		Lock::toggleLock($this->wiki->getSiteKey());
		$this->output->redirect($this->getPageTitle()->getFullURL());
	}

	/**
	 * Maintenance Runner
	 *
	 * @return void
	 */
	public function maintenanceRunner() {
		global $wgValidMaintenanceRunnerScripts;

		$script = $this->wgRequest->getVal('script');
		if (!array_key_exists($script, $wgValidMaintenanceRunnerScripts)) {
			$this->output->showErrorPage('dynamic_settings_error', 'error_invalid_script');
			return;
		}

		$scriptPath = $wgValidMaintenanceRunnerScripts[$script]['path'];
		if (!$scriptPath || empty($scriptPath)) {
			$this->output->showErrorPage('dynamic_settings_error', 'error_invalid_script');
			return;
		}

		$fullPath = MAINTENANCE_DIR . "/" . $scriptPath;
		$this->runTool('hostHelper', ['script' => $fullPath]);
	}

	/**
	 * Calls into the Tools class to run tools.
	 *
	 * @param string $task
	 * @param array  $extraArgs
	 *
	 * @return void
	 */
	private function runTool($task, $extraArgs = []) {
		if (!in_array($task, Tools::$tasks)) {
			throw new MWException(__METHOD__ . ': Unknown tool runner: ' . $task);
		}

		if (!$this->wgUser->isAllowed('wiki_' . $task)) {
			throw new PermissionsError('wiki_' . $task);
		}
		if (!$this->getRequest()->wasPosted()) {
			$this->output->showErrorPage('tools_' . $task . '_error', 'error_must_be_posted');
			return;
		}

		$location = strstr($_SERVER['HTTP_REFERER'], 'WikiEditLog') ? 'WikiEditLog' : 'WikiSites';

		ob_start();
		$html_errors = ini_get('html_errors');
		ini_set('html_errors', '0');
		$taskOutput = '';
		$success = false;
		try {
			$taskTool = new Tools;

			$taskArgs = [
				'task'	=> $task,
				'dist'	=> true
			];

			$allWikis = false;
			$siteKey = $this->wgRequest->getVal('siteKey');
			$wiki = false;
			if (!empty($siteKey) && $this->action !== 'massaction') {
				$wiki = Wiki::loadFromHash($siteKey);
				if (!$wiki) {
					throw new ErrorPageError(wfMessage('tools_' . $task . '_error'), wfMessage('wiki_not_found'));
				}
				if (Lock::isLocked($siteKey)) {
					throw new ErrorPageError(wfMessage('dynamic_settings_error'), wfMessage('error_wiki_locked'));
				}
				$taskArgs['sitekey'] = $wiki->getSiteKey();
				$displayName = $wiki->getNameForDisplay();
			} elseif ($this->action === 'massaction') {
				$displayNames = [];
				foreach ($this->wikis as $wiki) {
					$siteKeys[] = $wiki->getSiteKey();
					$displayNames[] = $wiki->getNameForDisplay();
				}
				$taskArgs['sitekey'] = implode(',', $siteKeys);
				$displayName = implode(', ', $displayNames);
			} else {
				$taskArgs['all'] = true;
				$allWikis = true;
				$displayName = wfMessage('all_wikis')->escaped();
				try {
					$redisKey = 'mass_action:all:' . $task . ':timeout';
					$redis = RedisCache::getClient('cache');
					if ($redis->exists($redisKey)) {
						$timeout = $redis->ttl($redisKey);
						throw new ErrorPageError(wfMessage('tools_' . $task . '_error'), wfMessage('mass_action_all_timeout', $task, $timeout));
					} else {
						$redis->setEx($redisKey, 600, 'snooze'); // Ten minute time out.
					}
				} catch (RedisException $e) {
					throw new ErrorPageError(wfMessage('tools_' . $task . '_error'), wfMessage('mass_action_all_error', $e->getMessage()));
				}
			}

			if (count($extraArgs)) {
				foreach ($extraArgs as $k => $v) {
					if (!isset($taskArgs[$k])) {
						$taskArgs[$k] = $v;
					}
				}
			}

			$taskTool->loadParamsAndArgs('Tools', $taskArgs);
			$success = $taskTool->execute();

			if ($task === 'recache' && $wiki !== false && !$wiki->isDeleted()) {
				$taskTool->loadParamsAndArgs('Tools', array_merge($taskArgs, ['task' => 'recacheAds']));
				$taskTool->execute();
			}
		} catch (Exception $e) {
			$taskOutput .= $e->getMessage() . "\n";
		}

		if ($success) {
			$this->output->setPageTitle(wfMessage('tools_' . $task . '_queued')->escaped());
		} else {
			$this->output->setPageTitle(wfMessage('tools_' . $task . '_error')->escaped());
		}

		ini_set('html_errors', $html_errors);
		$taskOutput .= ob_get_contents();
		ob_end_clean();

		// Look for a Job Key in the output, and parse it if it exists.
		$jobKeys = false;
		if (strpos($taskOutput, "Job Key:") !== false) {
			$outSplit = explode("\n", $taskOutput);
			foreach ($outSplit as $i => $v) {
				if (strpos($v, "Job Key:") !== false) {
					if (preg_match("#\(Job Key: ([\w|:]*?)\)#", $v, $matches)) {
						list($taskKey) = explode('WorkerLog', $matches[1]);
						$jobKeys[$taskKey] = $matches[1];
					}
				}
			}
		}

		$this->content = $this->templatesWikiSites->runToolOutput($taskOutput, $location, $task, $jobKeys);
	}

	/**
	 * Email managers that were removed or added to a wiki.
	 *
	 * @param object $wiki            Wiki
	 * @param array  $removedManagers Removed manager user names.
	 * @param array  $addedManagers   Added manager user names.
	 *
	 * @return void
	 */
	private function emailAffectedManagers($wiki, $removedManagers, $addedManagers) {
		if (is_array($removedManagers) && count($removedManagers)) {
			foreach ($removedManagers as $username) {
				$user = User::newFromName($username);
				$this->sendAffectedManagerEmail($wiki, $user, false);
			}
		}

		if (is_array($addedManagers) && count($addedManagers)) {
			foreach ($addedManagers as $username) {
				$user = User::newFromName($username);
				$this->sendAffectedManagerEmail($wiki, $user, true);
			}
		}
	}

	/**
	 * Actually send the email to the affected manager.
	 *
	 * @param object  $wiki  Wiki
	 * @param object  $user  User
	 * @param boolean $added Was Added
	 *
	 * @return mixed False for an invalid user or a Status object for the mail sending attempt.
	 */
	private function sendAffectedManagerEmail($wiki, $user, $added = false) {
		if ($user->getId()) {
			$body['html'] = wfMessage(
				'wiki_manager_changed_body',
				$this->getUser()->getName(),
				mb_strtolower(wfMessage('wiki_manager_' . ($added ? 'added' : 'removed'))->escaped(), "UTF-8"),
				$wiki->getNameForDisplay(),
				wfTimestamp(TS_ISO_8601, time())
			)->parse();
			$body['text'] = strip_tags($body['html']);

			return $user->sendMail(
				wfMessage('wiki_manager_status_changed', wfMessage('wiki_manager_' . ($added ? 'added' : 'removed')))->escaped(),
				$body
			);
		}
		return false;
	}

	/**
	 * Load wikis from the sites POST parameter.
	 *
	 * @return boolean Success - Stop processing and let the error page display on false.
	 */
	private function loadMassActionWikis() {
		$siteKeys = false;
		$sitesRaw = $this->wgRequest->getVal('sites');
		if (strlen($sitesRaw)) {
			$siteKeys = explode(',', $this->wgRequest->getVal('sites'));
		}
		if (is_array($siteKeys) && count($siteKeys)) {
			foreach ($siteKeys as $siteKey) {
				if (Lock::isLocked($siteKey) == true) {
					$this->output->showErrorPage('dynamic_settings_error', 'error_wikis_locked');
					return false;
				}
			}

			$this->wikis = Wiki::loadFromHash($siteKeys);
			if (count($siteKeys) != count($this->wikis)) {
				$this->output->showErrorPage('site_settings_error', 'wikis_not_found');
				return false;
			}
		}

		return true;
	}

	/**
	 * Set the default Skin during spin up
	 *
	 * @return void
	 */
	private function setSkin() {
		$siteKey = $this->wgRequest->getVal('siteKey');
		$skin = $this->wgRequest->getVal('skin');
		$addSkip = $this->wgRequest->getVal('skip');
		$wiki = Wiki::loadFromHash($siteKey);
		$settings = $wiki->getSettings();

		// Set Skin Default
		$wgDefaultSkin = $settings->search(['setting_key'], '$wgDefaultSkin')->getArrayCopy();
		$defaultSkin = array_pop($wgDefaultSkin);
		$skinSettingId = $defaultSkin->getId();
		$settings[$skinSettingId]->setValueFromForm($skin);

		// Set Skip Skin
		$wgSkipSkins = $settings->search(['setting_key'], '$wgSkipSkins')->getArrayCopy();
		$skipSkins = array_pop($wgSkipSkins);
		$skipSettingId = $skipSkins->getId();
		$skipDefault = $skipSkins->getSettingDefault();
		if ($addSkip) {
			$skip = $skin == 'hydra' ? 'hydradark' : 'hydra';
			$skipDefault = array_replace(
				$skipDefault,
				array_fill_keys(
					array_keys($skipDefault, $skin),
					$skip
				)
			);
		} else {
			// strip out hydra and hydradark
			if (($key = array_search('hydra', $skipDefault)) !== false) {
				unset($skipDefault[$key]);
			}
			if (($key = array_search('hydradark', $skipDefault)) !== false) {
				unset($skipDefault[$key]);
			}
		}

		$settings[$skipSettingId]->setValueFromForm(json_encode($skipDefault));
		$wiki->saveSettings($settings, 'Default skin set during spin-up.');
	}
}
