<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * GrantAuroraReplicaStatus Maintenance Class
 *
 * @author    Alexia E. Smith
 * @copyright (c) 2018 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
**/

namespace DynamicSettings;

require_once dirname(__DIR__, 3) . "/maintenance/Maintenance.php";

class GrantAuroraReplicaStatus extends \Maintenance {
	/**
	 * Constructor
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();

		$this->mDescription = 'Grant the ability for wikis to read from the Aurora replica status table.';
	}

	/**
	 * Main Executor
	 *
	 * @return void
	 */
	public function execute() {
		$config = \ConfigFactory::getDefaultInstance()->makeConfig('main');
		$dbUserGrants = $config->get('DSDBUserGrants');

		$db = wfGetDB(DB_MASTER);

		$results = $db->select(
			['wiki_sites'],
			[
				'db_server',
				'db_port',
				'db_name',
				'db_user',
				'db_password',
				'db_type'
			],
			[],
			__METHOD__
		);

		while ($row = $results->fetchRow()) {
			if (empty($row['db_server']) || empty($row['db_port'])) {
				continue;
			}
			try {
				$clusterKey = $row['db_server'] . ':' . $row['db_port'];
				if (!isset($clusterDBs[$clusterKey])) {
					$lb = $this->getDBLB($row);
					$clusterDBs[$clusterKey] = $lb;
				}
				$clusterDB = $clusterDBs[$clusterKey]->getConnection(DB_MASTER);
			} catch (\Wikimedia\Rdbms\DBConnectionError $e) {
				continue;
			}
			foreach ($dbUserGrants as $details) {
				$name = $clusterDB->addQuotes($row['db_user']) . '@' . $db->addQuotes($details['host']);
				$grant = "GRANT SELECT ON mysql.ro_replica_status TO $name";
				$this->output($grant . "\n..." . $clusterKey);
				try {
					$success = $clusterDB->query($grant, __METHOD__); // AWS Aurora replication.
					$this->output($success ? "\n...success!\n" : "\n...fail!\n");
				} catch (\DBQueryError $dqe) {
					$this->output("\n...fail!\n");
					continue;
				}
			}
		}
	}

	/**
	 * Function Documentation
	 *
	 * @return void
	 */
	private function getDBLB($database) {
		global $wgExternalServers;

		$config = \ConfigFactory::getDefaultInstance()->makeConfig('main');
		$dsInstallDBUser = $config->get('DSInstallDBUser');

		$master = [
			'host'		=> $database['db_server'] . ':' . $database['db_port'],
			'dbname'	=> $database['db_name'],
			'user'		=> $dsInstallDBUser['user'],
			'password'	=> $dsInstallDBUser['password'],
			'type'		=> $database['db_type'],
			'load'		=> 1
		];

		$wgExternalServers[$master['host']] = [
			$master
		];

		// This code is copied from ServiceWiring get around shitty MediaWikiServices singleton issues.
		$services = \MediaWiki\MediaWikiServices::getInstance();
		$mainConfig = $services->getMainConfig();

		$lbConf = \MWLBFactory::applyDefaultConfig(
			$mainConfig->get('LBFactoryConf'),
			$mainConfig,
			$services->getConfiguredReadOnlyMode()
		);
		$class = \MWLBFactory::getLBFactoryClass($lbConf);

		$lb = new $class($lbConf);

		return $lb->newExternalLB($master['host']);
	}
}

$maintClass = '\DynamicSettings\GrantAuroraReplicaStatus';
require_once RUN_MAINTENANCE_IF_MAIN;
