<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Wiki Allowed Settings Special Page
 *
 * @author    Alex Smith
 * @copyright (c) 2014 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
**/

use DynamicSettings\Settings\Setting;
use DynamicSettings\Extensions\Extension;
use DynamicSettings\Lock;

class SpecialWikiAllowedSettings extends DynamicSettings\SpecialPage {
	/**
	 * Output HTML
	 *
	 * @var string
	 */
	private $content;

	/**
	 * Valid actions for this page.
	 *
	 * @var array
	 */
	private $validActions = [
		'add',
		'edit',
		'delete',
		'resetSearch',
		'save',
		'search'
	];

	/**
	 * Current action.
	 *
	 * @var string|null
	 */
	private $action = null;

	/**
	 * Main Constructor
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct('WikiAllowedSettings', 'wiki_allowed_settings');
	}

	/**
	 * Main Executor
	 *
	 * @param string Sub page passed in the URL.
	 *
	 * @return void [Outputs to screen]
	 */
	public function execute($subpage) {
		$this->checkPermissions();

		if (Lock::isLocked() == true) {
			$this->output->showErrorPage('dynamic_settings_error', 'error_settings_locked');
			return;
		}

		$this->output->addModuleStyles(['ext.wikiSettings.styles']);
		$this->output->addModules(['ext.wikiSettings.scripts']);

		$this->setHeaders();

		$this->templateWikiAllowedSettings = new TemplateWikiAllowedSettings();
		$this->templateWikiSettings = new TemplateWikiSettings();

		if (in_array($this->getRequest()->getVal('action'), $this->validActions)) {
			$this->action = $this->getRequest()->getVal('action');
		}

		switch ($this->wgRequest->getVal('section')) {
			default:
			case 'list':
				$this->allowedSettingsList();
				break;
			case 'setting':
				$this->allowedSettingsForm();
				break;
		}

		$this->output->addHTML($this->content);
	}

	/**
	 * Wiki Allowed Settings Form
	 *
	 * @return void [Outputs to screen]
	 */
	public function allowedSettingsList() {
		$start = $this->wgRequest->getInt('st');
		$itemsPerPage = 100;
		$cookieExpire = time() + 900;

		$this->allowedSettings = Setting::getAll();

		if ($this->wgRequest->getVal('hide_deleted') == 'true' or ($this->wgRequest->getCookie('hideDeletedSettings') == 'true' and $this->wgRequest->getVal('hide_deleted') != 'false')) {
			$hideDeleted = true;
			$this->wgRequest->response()->setcookie('hideDeletedSettings', 'true');
		} else {
			$hideDeleted = false;
			$this->allowedSettings->includeDeleted();
			if ($this->wgRequest->getVal('hide_deleted') == 'false') {
				$this->wgRequest->response()->setcookie('hideDeletedSettings', 'false');
			}
		}

		$searchTerm = '';

		$total = 0;
		$sortKey = 'setting_group_name';
		$reverseSort = false;
		$sortDir = 'asc';
		if ($this->allowedSettings->count()) {
			if ($this->action == 'resetSearch') {
				$this->wgRequest->response()->setcookie('wikiAllowedSettingsSearchTerm', '', 1);
			} else {
				$listSearch = $this->wgRequest->getVal('list_search');
				$cookieSearch = $this->wgRequest->getCookie('wikiAllowedSettingsSearchTerm');
				if (($this->action == 'search' && !empty($listSearch)) || !empty($cookieSearch)) {
					if (!empty($cookieSearch) && empty($listSearch)) {
						$searchTerm = $cookieSearch;
					} else {
						$searchTerm = $listSearch;
					}
					$this->allowedSettings = $this->allowedSettings->search(['setting_key', 'setting_type', 'setting_group_name'], $searchTerm);
					$this->wgRequest->response()->setcookie('wikiAllowedSettingsSearchTerm', $searchTerm, $cookieExpire);
				}
			}

			$sortDir = $this->wgRequest->getVal('sort_dir');
			if ((strtolower($this->wgRequest->getCookie('wikiAllowedSettingsSortDir')) === 'desc' && !$sortDir) || strtolower($sortDir) === 'desc') {
				$reverseSort = true;
				$sortDir = 'desc';
			}
			$this->wgRequest->response()->setcookie('wikiAllowedSettingsSortDir', $sortDir, $cookieExpire);

			if ($this->wgRequest->getCookie('wikiAllowedSettingsSortKey') && empty($this->wgRequest->getVal('sort'))) {
				$sortKey = $this->wgRequest->getCookie('wikiAllowedSettingsSortKey');
			} elseif (!empty($this->wgRequest->getVal('sort'))) {
				$sortKey = $this->wgRequest->getVal('sort');
				$this->wgRequest->response()->setcookie('wikiAllowedSettingsSortKey', $sortKey, $cookieExpire);
			}

			$total = $this->allowedSettings->count();
			$this->allowedSettings = $this->allowedSettings->sort($sortKey, $reverseSort)->slice($start, $itemsPerPage);

		}

		$pagination = HydraCore::generatePaginationHtml($this->getFullTitle(), $total, $itemsPerPage, $start);

		$this->output->setPageTitle(wfMessage('wikiallowedsettings'));
		$this->content = $this->templateWikiAllowedSettings->allowedSettingsList($this->allowedSettings, $pagination, $hideDeleted, $sortKey, $sortDir, $searchTerm);
	}

	/**
	 * Wiki Allowed Settings Form
	 *
	 * @return void [Outputs to screen]
	 */
	public function allowedSettingsForm() {
		$setting = [];

		$asid = $this->wgRequest->getInt('asid');

		$setting = Setting::newFromId($asid);
		if (is_null($setting)) {
			$setting = new Setting;
		}

		if ($this->action == 'edit' && $asid != $setting->getId()) {
			$this->output->showErrorPage('allowed_settings_error', 'allowed_settings_no_setting');
			return;
		}

		if ($setting->isDeleted()) {
			$this->output->showErrorPage('allowed_settings_error', 'allowed_settings_deleted');
			return;
		}

		if ($this->action == 'delete' && $setting->getId()) {
			if ($this->wgRequest->wasPosted() && $this->wgRequest->getVal('do') === 'confirm') {
				if ($setting->delete()) {
					$this->output->redirect($this->getPageTitle()->getFullURL());
					return;
				}
			}
			$this->output->setPageTitle(wfMessage('delete_setting') . ' - ' . $setting->getSettingKey());
			$this->content = $this->templateWikiAllowedSettings->allowedSettingsDelete($setting);
		} else {
			$errors = [];
			$errors = $this->allowedSettingsSave($setting);

			$this->output->setPageTitle(($setting->getId() ? wfMessage('edit_setting') . ' - ' . $setting->getSettingKey() : wfMessage('add_setting')));
			$this->content = $this->templateWikiAllowedSettings->allowedSettingsForm($setting, Extension::getAll(), $errors, $this->action);
		}
	}

	/**
	 * Saves submitted Wiki Allowed Settings Forms.
	 *
	 * @param object Existing setting information.
	 *
	 * @return array Array of errors.
	 */
	private function allowedSettingsSave($setting) {
		$errors = [];

		if ($this->wgRequest->wasPosted() && $this->wgRequest->getVal('do') === 'save') {
			$this->allowedSettings = Setting::getAll();

			$newSettingKey = trim($this->wgRequest->getText('setting_key'));

			// When changing the setting key make sure it will not conflict.
			if ($newSettingKey != $setting->getSettingKey()) {
				$existingSetting = Setting::newFromKey($newSettingKey);
				if ($existingSetting !== null) {
					$errors['setting_key'] = wfMessage('error_in_use_setting_key');
				} else {
					if (!$setting->setSettingKey($newSettingKey)) {
						$errors['setting_key'] = wfMessage('error-setting_key-invalid')->escaped();
					}
				}
			}

			if (!$setting->setSettingType($this->wgRequest->getText('setting_type'))) {
				$errors['setting_type'] = wfMessage('error-setting_type-invalid')->escaped();
			}

			if (!$setting->setSettingDefault(trim($this->wgRequest->getText('setting_default_' . $setting->getSettingType())))) {
				$errors['setting_default'] = wfMessage('error-setting_default-invalid')->escaped();
			}

			$save['setting_example'] = trim($this->wgRequest->getText('setting_example'));

			if (!$setting->setSettingExample($this->wgRequest->getText('setting_example'))) {
				$errors['setting_example'] = wfMessage('error-setting_example-invalid')->escaped();
			}

			if (!$setting->setAllowedExtensionMd5Key($this->wgRequest->getVal('allowed_extension_md5_key'))) {
				$errors['allowed_extension_md5_key'] = wfMessage('error-allowed_extension_md5_key-invalid')->escaped();
			}

			if (empty($setting->getAllowedExtensionMd5Key())) {
				$setting->setSettingGroupName($this->wgRequest->getText('setting_group_name'));
			}

			$setting->setSettingVisible($this->wgRequest->getBool('setting_visible'));

			if (!$setting->setSettingLoadOrder($this->wgRequest->getInt('setting_load_order'))) {
				$errors['setting_load_order'] = wfMessage('error-setting_load_order-invalid')->escaped();
			}

			$commitMessage = trim($this->wgRequest->getText('commit_message'));
			if (!$commitMessage) {
				$errors['commit_message'] = wfMessage('error_no_commit_message');
			}

			if (!count($errors)) {
				if ($setting->save($commitMessage)) {
					$this->output->redirect($this->getPageTitle()->getFullURL());
					return;
				} else {
					throw new MWException('There was a fatal error saving the data for this allowed setting.');
				}
			}
		}
		return $errors;
	}
}
