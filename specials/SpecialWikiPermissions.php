<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Wiki Permissions Special Page
 *
 * @author    Alex Smith
 * @copyright (c) 2014 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
**/

use DynamicSettings\Lock;

class SpecialWikiPermissions extends DynamicSettings\SpecialPage {
	/**
	 * Output HTML
	 *
	 * @var string
	 */
	private $content;

	/**
	 * Wiki object storage for form editing.
	 *
	 * @var array
	 */
	private $wiki;

	/**
	 * Main Constructor
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct('WikiPermissions', 'wiki_group_permissions', false);
	}

	/**
	 * Main Executor
	 *
	 * @param string Sub page passed in the URL.
	 *
	 * @return void [Outputs to screen]
	 */
	public function execute($subpage) {
		$this->checkPermissions();

		if (Lock::isLocked() == true) {
			$this->output->showErrorPage('dynamic_settings_error', 'error_settings_locked');
			return;
		}

		$this->siteSettings = new \DynamicSettings\Sites();

		$this->output->addModuleStyles(['ext.wikiPermissions.styles']);
		$this->output->addModules(['ext.wikiPermissions.scripts']);

		$siteKey = $this->wgRequest->getVal('siteKey');

		if (Lock::isLocked($siteKey) == true) {
			$this->output->showErrorPage('dynamic_settings_error', 'error_wiki_locked');
			return;
		}

		$this->setHeaders();

		$this->wiki = \DynamicSettings\Wiki::loadFromHash($siteKey);

		if ($this->wiki === false) {
			$this->output->showErrorPage('site_permissions_error', 'site_permissions_no_wiki');
			return;
		}

		if ($this->wiki->isDeleted()) {
			$this->output->showErrorPage('site_permissions_error', 'site_permissions_error');
			return;
		}

		$this->permissionsForm();

		$this->output->addHTML($this->content);
	}

	/**
	 * Wiki Permissions Form
	 *
	 * @return void [Outputs to screen]
	 */
	public function permissionsForm() {
		$this->permissions = $this->wiki->getPermissions();
		if ($this->permissions === false) {
			$this->permissions = [];
		}

		$errors = $this->permissionsSave();

		$this->output->setPageTitle(wfMessage('wikigrouppermissions') . ' - ' . $this->wiki->getNameForDisplay());
		$this->templates = new TemplateWikiPermissions();
		$this->content = $this->templates->permissionsForm($this->wiki, $this->permissions, $errors);
	}

	/**
	 * Saves submitted Wiki Permissions Forms.
	 *
	 * @return array Array of errors.
	 */
	private function permissionsSave() {
		$errors = [];

		if ($this->wgRequest->wasPosted() && $this->wgRequest->getVal('do') == 'save') {
			$arrays['gpid']			= $this->wgRequest->getArray('gpid');
			$arrays['group_name']	= $this->wgRequest->getArray('group_name');
			$arrays['allow']		= $this->wgRequest->getArray('group_allow');
			$arrays['deny']			= $this->wgRequest->getArray('group_deny');

			$total = count($arrays['group_name']);

			$gpids = [];
			if ($total && is_array($arrays['group_name'])) {
				for ($i = 0; $i < $total; $i++) {
					if (intval($arrays['gpid'][$i]) > 0 && array_key_exists(intval($arrays['gpid'][$i]), $this->permissions)) {
						// Make a list of known gpids to delete old ones later.
						$gpids[] = intval($arrays['gpid'][$i]);
						$permission = $this->permissions[$arrays['gpid'][$i]];
					} else {
						$permission = \DynamicSettings\Wiki\Permission::loadFromNew($this->wiki);
						$this->permissions[] = $permission;
					}

					if (!$permission->setGroupName($arrays['group_name'][$i])) {
						$errors[$i]['group_name'] = wfMessage('error-group_name-invalid')->escaped();
					}

					$allow = trim($arrays['allow'][$i]);
					if ($allow) {
						$allow = explode("\n", $arrays['allow'][$i]);
					}
					$permission->clearAllow();
					if (count($allow) && is_array($allow)) {
						$allowed = [];
						foreach ($allow as $value) {
							if (trim($value)) {
								$permission->addAllow(trim($value));
							}
						}
					}

					$deny = trim($arrays['deny'][$i]);
					if ($deny) {
						$deny = explode("\n", $arrays['deny'][$i]);
					}
					$permission->clearDeny();
					if (count($deny) && is_array($deny)) {
						foreach ($deny as $value) {
							if (trim($value)) {
								$permission->addDeny(trim($value));
							}
						}
					}
				}
			}

			$commitMessage = trim($this->wgRequest->getText('commit_message'));
			if (!$commitMessage) {
				$errors['commit_message'] = wfMessage('error_no_commit_message')->escaped();
			}

			if (!count($errors)) {
				if (count($this->permissions)) {
					foreach ($this->permissions as $permissionId => $permission) {
						if ($permission->getDatabaseId() && !in_array($permissionId, $gpids)) {
							$permission->delete($commitMessage);
						} else {
							$permission->save($commitMessage);
						}
					}
				}

				$page = Title::newFromText('Special:WikiSites');
				$this->output->redirect($page->getFullURL());
				return;
			}
		}
		return $errors;
	}
}
