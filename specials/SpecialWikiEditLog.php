<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Wiki Edit Log Special Page
 *
 * @author    Alex Smith
 * @copyright (c) 2014 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
**/

use \DynamicSettings\EditLog;
use \DynamicSettings\EditLog\Entry;

class SpecialWikiEditLog extends DynamicSettings\SpecialPage {
	/**
	 * Site Settings
	 *
	 * @var	object	SiteSettings object
	 */
	private $siteSettings;

	/**
	 * @var	object	EditLog object
	 */
	private $editLog;

	/**
	 * @var	array	Allowed Settings object holder
	 */
	private static $allowedSettings;

	/**
	 * @var	array	Allowed Extensions object holder
	 */
	private static $allowedExtensions;

	/**
	 * Valid actions for this page.
	 *
	 * @var array
	 */
	private $validActions = [
		'delete'
	];

	/**
	 * Current action.
	 *
	 * @var string|null
	 */
	private $action = null;

	/**
	 * Wiki objects storage.
	 *
	 * @var array
	 */
	private $wikis = [];

	/**
	 * Main Constructor
	 */
	public function __construct() {
		parent::__construct('WikiEditLog', 'wiki_edit_log');
	}

	/**
	 * Main Executor
	 *
	 * @throws object	PermissionsError page
	 *
	 * @return void [Outputs to screen]
	 */
	public function execute($subpage) {
		$this->checkPermissions();

		$this->editLog = new \DynamicSettings\EditLog();

		self::$allowedExtensions = \DynamicSettings\AllowedExtensions::getInstance()->getAll();

		self::$allowedSettings = \DynamicSettings\AllowedSettings::getInstance()->getAll();

		$this->templates = new TemplateWikiEditLog();

		$this->output->addModuleStyles(['ext.wikiEditLog.styles', 'ext.hydraCore.font-awesome.styles']);
		$this->output->addModules(['ext.wikiEditLog.scripts']);

		$this->setHeaders();

		if (in_array($this->getRequest()->getVal('action'), $this->validActions)) {
			$this->action = $this->getRequest()->getVal('action');
		}

		switch ($this->wgRequest->getVal('section')) {
			case 'log':
				$content = $this->wikiEditDetail();
				break;
			case 'list':
			default:
				$content = $this->wikiEditLog();
		}

		$this->output->addHTML($content);
	}

	/**
	 * Wiki Edit Log
	 *
	 * @return string HTML
	 */
	private function wikiEditLog() {
		$logs = [];
		$hidden['legacy'] = false;
		$hidden['deleted'] = false;
		$start = $this->wgRequest->getInt('st');
		$itemsPerPage = 100;

		$where = [];
		$parameters = [];

		if ($this->wgRequest->getVal('hide_deleted') == 'true' ||
			($this->wgRequest->getCookie('hideDeletedEntries') == 'true' && $this->wgRequest->getVal('hide_deleted') != 'false')
		) {
			$hidden['deleted'] = true;
			$where['deleted'] = 0;
			$this->wgRequest->response()->setcookie('hideDeletedEntries', 'true');
		} elseif ($this->wgRequest->getVal('hide_deleted') == 'false') {
			$this->wgRequest->response()->setcookie('hideDeletedEntries', 'false');
		}

		if ($this->wgRequest->getVal('hide_legacy') == 'true' ||
			($this->wgRequest->getCookie('hideLegacyEntries') == 'true' && $this->wgRequest->getVal('hide_legacy') != 'false')
		) {
			$hidden['legacy'] = true;
			$where['legacy'] = 0;
			$this->wgRequest->response()->setcookie('hideLegacyEntries', 'true');
		} elseif ($this->wgRequest->getVal('hide_legacy') == 'false') {
			$this->wgRequest->response()->setcookie('hideLegacyEntries', 'false');
		}

		$siteKey = $this->wgRequest->getVal('siteKey');
		if (!empty($siteKey)) {
			$wiki = \DynamicSettings\Wiki::loadFromHash($siteKey);
			if (!empty($wiki)) {
				$where['log_key'] = $wiki->getSiteKey();
				$where[] = 'log_type >= ' . \DynamicSettings\EditLog\Entry::LOGTYPE_WIKI . ' AND log_Type <= ' . \DynamicSettings\EditLog\Entry::LOGTYPE_DOMAINS;
				$parameters['siteKey'] = $wiki->getSiteKey();
			}
		}

		$result = $this->DB->select(
			'wiki_edit_log',
			['*'],
			$where,
			__METHOD__,
			[
				'ORDER BY'	=> 'lid DESC',
				'OFFSET'	=> $start,
				'LIMIT'		=> $itemsPerPage
			]
		);

		while ($row = $result->fetchRow()) {
			$entry = Entry::newFromRow($row);
			$logs[$entry->getId()] = $entry;
		}

		$staleEntries = EditLog::getStaleEntries((!empty($siteKey) ? $siteKey : null));

		$uncachedEntries = [];
		foreach ($staleEntries as $staleEntry) {
			$uncachedEntries[$staleEntry->getLogKey()] = $staleEntry->getDisplayText() . ' - ' . $staleEntry->getCommitMessage();
		}

		$totals['total'] = EditLog::getTotalEntries($where);

		$pagination = HydraCore::generatePaginationHtml($this->getFullTitle(), $totals['total'], $itemsPerPage, $start, 4, $parameters);

		$this->output->setPageTitle(wfMessage('wikieditlog') . (!empty($wiki) ? ' - ' . $wiki->getNameForDisplay() : ''));
		$content = $this->templates->wikiEditLog(
			$logs,
			$hidden,
			$pagination,
			$uncachedEntries
		);

		return $content;
	}

	/**
	 * Display an edit log detail page.
	 *
	 * @return mixed HTML Content or null.
	 */
	private function wikiEditDetail() {
		$logId = $this->wgRequest->getInt('log');

		if ($this->wgRequest->getval('action') == 'delete') {
			return $this->deleteLog($logId);
		}

		$entry = Entry::newFromId($logId);
		if (!$entry) {
			$this->output->showErrorPage('edit_log_error', 'entry_not_found');
			return;
		}

		$this->output->setPageTitle(wfMessage('wikieditlogdetail'));
		$content = $this->templates->wikiEditDetail($entry);

		return $content;
	}

	/**
	 * Delete an entry in the edit log.
	 *
	 * @param integer Log ID
	 *
	 * @return void
	 */
	private function deleteLog($logId) {
		if (!$this->wgUser->isAllowed('wiki_edit_log_soft_delete')) {
			throw new PermissionsError('wiki_edit_log_soft_delete');
		}
		$permanent = $this->wgRequest->getBool('permanent');
		if (!$this->wgUser->isAllowed('wiki_edit_log_hard_delete') && $permanent) {
			throw new PermissionsError('wiki_edit_log_hard_delete');
		}

		$entry = Entry::newFromId($logId);
		if (!$entry) {
			$this->output->showErrorPage('edit_log_error', 'entry_not_found');
			return;
		}

		$success = $entry->delete($permanent);

		$this->output->redirect($this->getPageTitle()->getFullURL());
	}
}
