<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Maint Runner Special Page
 *
 * @author    Cameron Chunn
 * @copyright (c) 2018 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
**/

class TemplateMaintenanceRunner {
	/**
	 * Maintenance Runner
	 *
	 * @param object Wiki
	 *
	 * @return string HTML
	 */
	public function maintenanceForm($wiki) {
		global $wgValidMaintenanceRunnerScripts;

		$wikiSitesPage = Title::newFromText('Special:WikiSites');

		$html = wfMessage('maintenance_runner_instructions')->parse();

		foreach ($wgValidMaintenanceRunnerScripts as $key => $script) {
			$blocks[] = "
			<h3>" . htmlspecialchars($script['title']) . "</h3>
			<p>" . htmlspecialchars($script['desc']) . "</p>
			<form action='{$wikiSitesPage->getFullURL()}' onsubmit=\"return confirm('" . wfMessage('run_tool_confirm', $script['title'])->escaped() . "')\" method='POST'>
				<input type='hidden' name='section' value='maintenanceRunner'>
				<input type='hidden' name='script' value='{$key}'>
				<input type='hidden' name='siteKey' value='{$wiki->getSiteKey()}'>
				<input type='submit' value='" . wfMessage('run_script')->escaped() . "'>
			</form>";
		}
		$html .= implode("<hr/>", $blocks);

		return $html;
	}
}
