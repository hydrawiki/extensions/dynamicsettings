<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Wiki Extensions Skin
 *
 * @author    Alex Smith
 * @copyright (c) 2014 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
**/

use DynamicSettings\Extensions\ExtensionCollection;

class TemplateWikiExtensions {
	/**
	 * Output HTML
	 *
	 * @var string
	 */
	private $HMTL;

	/**
	 * Wiki Information
	 *
	 * @param object ExtensionCollection $extensions (with wiki context set)
	 * @param array Form Errors
	 *
	 * @return string Built HTML
	 */
	public function extensionsForm(ExtensionCollection $extensions, array $errors) {
		global $wgRequest;
		$page = Title::newFromText('Special:WikiExtensions');
		$html = "
		<input id='inline_extension_search' type='text' name='inline_extension_search' placeholder='" . wfMessage('type_to_search')->escaped() . "' class='search_field'/>
		<form id='wiki_settings_form' method='post' action='{$page->getFullURL(['do' => 'save'])}'>
		";
		if (isset($errors['extensions'])) {
			$html .= '<span class="error">' . $errors['extensions'] . '</span>';
		}
		$html .= "
			<div id='extensions_container'>";
		if ($extensions->count()) {
			foreach ($extensions as $ext) {
				$html .= "
				<label class='hideable" . (!empty($ext->getExtensionInformation()) ? " hover_hand" : null) . "' data-name='" . htmlentities(strtolower($ext->getExtensionName()), ENT_QUOTES) . "'>
					<input type='checkbox' name='extensions[]' value='{$ext->getExtensionKey()}'" . ($ext->isEnabled() ? " checked='checked'" : null) . "/> {$ext->getExtensionName()}"
							. ($ext->isUpdateRequired() || !empty($ext->getExtensionInformation()) ? ' <span class="extension_warning">' . ($ext->isUpdateRequired() ? '(' . wfMessage('db_update_required')->escaped() . ')' : null) . (!empty($ext->getExtensionInformation()) ? '*' : null) . '</span>' : null)
							. (!empty($ext->getExtensionInformation()) ? "<div class='hover_container'>" . str_replace(["\r\n", "\n\r", "\n", "\r"], '<br/>', htmlentities($ext->getExtensionInformation(), ENT_QUOTES)) . "</div>" : null)
				. "</label>
						";
			}
		}
		$html .= "
			</div>
			<fieldset id='commit_fieldset'>
				" . (isset($errors['commit_message']) ? '<span class="error">' . $errors['commit_message'] . '</span>' : '') . "
				<label for='commit_message' class='label_above'>" . wfMessage('commit_message')->escaped() . "</label>
				<input id='commit_message' name='commit_message' type='text' value='" . htmlentities(trim($wgRequest->getText('commit_message')), ENT_QUOTES) . "'/><br/>
				<input id='wiki_id' name='siteKey' type='hidden' value='{$extensions->getWikiContext()->getSiteKey()}'/>
				<button id='wiki_submit' name='wiki_submit' type='submit' class='mw-ui-button mw-ui-progressive'>" . wfMessage('save_key_value')->escaped() . "</button>
			</fieldset>
		</form>
		";

		return $html;
	}
}
