<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Collection class for holding an array of Setting instances
 *
 * @author    Noah Manneschmidt
 * @copyright (c) 2015 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
 **/

namespace DynamicSettings\Settings;

use DynamicSettings\WikiContextArrayObject;

class SettingCollection extends WikiContextArrayObject {
	use SettingArrayAccessContext;

	protected static $iteratorClass = 'DynamicSettings\Settings\SettingIterator';

	/**
	 * Filter this collection down to only settings that need to come before the extension is required
	 *
	 * @return object SettingCollection
	 */
	public function beforeRequire() {
		return $this->filter(function ($setting) {
			return $setting->getSettingLoadOrder() < 0;
		});
	}

	/**
	 * Filter collection down to only settings that need to come after the extension is required
	 *
	 * @return object SettingCollection
	 */
	public function afterRequire() {
		return $this->filter(function ($setting) {
			return $setting->getSettingLoadOrder() >= 0;
		});
	}

	/**
	 * Filter collection down to only settings belonging to a specific group
	 *
	 * @param string $group name of group
	 *
	 * @return object SettingCollection
	 */
	public function getGroup($group) {
		return $this->filter(function ($setting) use ($group) {
			return $setting->getSettingGroupName() === $group;
		});
	}

	/**
	 * Filter collection down to remove settings belonging to a specific group
	 *
	 * @param string $group name of group
	 *
	 * @return object SettingCollection
	 */
	public function withoutGroup($group) {
		return $this->filter(function ($setting) use ($group) {
			return $setting->getSettingGroupName() !== $group;
		});
	}

	/**
	 * Filter collection down to only settings not belonging to an extension
	 *
	 * @return object SettingCollection
	 */
	public function withoutExtensions() {
		return $this->filter(function ($setting) {
			return empty($setting->getAllowedExtensionMd5Key());
		});
	}

	/**
	 * Add deleted allowed settings to the collection
	 * This adds ALL deleted settings regardless of wiki context
	 *
	 * @return object $this
	 */
	public function includeDeleted() {
		$this->merge(Setting::getDeleted());
		return $this;
	}
}
