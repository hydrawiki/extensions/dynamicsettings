<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Wiki Notices Special Page
 *
 * @author    Tim Aldridge
 * @copyright (c) 2014 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
 **/

class SpecialWikiNotices extends SpecialWikiPromotions {
	/**
	 * Output HTML
	 *
	 * @var string
	 */
	private $content;

	/**
	 * The Type
	 *
	 * @var string
	 */
	public $type = 'notice';

	/**
	 * Main Constructor
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct('WikiNotices', 'wiki_promotions');
	}
}
