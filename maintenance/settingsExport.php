<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Settings Export
 *
 * @package   DynamicSettings
 * @author    Michael Chaudhary
 * @copyright (c) 2020 Fandom Inc.
 * @license   GNU General Public License v2.0 or later
 * @link      https://gitlab.com/hydrawiki
**/

namespace DynamicSettings;

use Maintenance;

require_once __DIR__ . '/../../../maintenance/Maintenance.php';

class SettingsExport extends Maintenance {
	/**
	 * Whitelist of settings to move to UCP
	 *
	 * @var array
	 */
	private static $settingsWhitelist = [
		'$wgAbuseFilterDisallowGlobalLocalBlocks','$egApprovedRevsBlankIfUnapproved','$egApprovedRevsSelfOwnedNamespaces','$wgBabelCategoryNames','$wgCargoAllowedSQLFunctions','$wgCargoPageDataColumns','$wgCargoDigitGroupingCharacter','$wgCargoDecimalMark','$wgClaimWikiEnabled', '$wgClaimWikiGuardianTotal','$wgCustomLogsLogs','$egDraftsAutoSaveInputBased','$wgDplSettings[\'allowUnlimitedCategories\']','$wgDplSettings[\'allowUnlimitedResults\']','$wgDplSettings[\'maxCategoryCount\']','$wgDplSettings[\'behavingLikeIntersection\']','$wgDplSettings[\'maxResultCount\']','$wgDplSettings[\'fixedCategories\']','$wgExcludeRandomPages','$wgFlaggedRevsNamespaces','$wgFlaggedRevsHandleIncludes','$wgFlaggedRevsAutopromote','$wgFlaggedRevsExceptions','$wgHighlightLinksInCategory','$wgIsOfficialWiki','$wgHydraSkinHideSideRailPages','$wgHydraSkinShowSideRail','$wgHydraSkinShowFooterAd','$wgHydraSkinShowAnchorAd','$wgSideRailCollapseWidth','$wgHydraSkinBodyName','$wgSkipSkins','$wgDefaultSkin','$wgUsersNotifiedOnAllChanges','$wgRightsIcon','$wgRightsText','$wgNamespaceAliases','$wgRestrictDisplayTitle','$wgAllowCopyUploads','$wgAllowExternalImages','$wgRestrictionLevels','$wgCascadingRestrictionLevels','$wgEmailConfirmToEdit','$wgUseInstantCommons','$wgExtraLanguageNames','$wgExtraInterlanguageLinkPrefixes','$wgRCMaxAge','$wgMFSiteStylesRenderBlocking','$wgMFAutodetectMobileView','$wgAppleTouchIcon','$wgMFCollapseSectionsByDefault','$wgPopupsOptInDefaultState','$wgRegexFunctionsPerPage','$wgRoadblockNewWiki','$wgRSSUrlWhitelist','$wgRSSUrlNumberOfAllowedRedirects','$wgSocialSettings[\'profile_urls\']','$wgSocialSettings[\'sidebar_enabled\']','$wgSocialSettings[\'prompts_enabled\']','$wgSocialSettings[\'navigation_enabled\']','$wgSocialSettings[\'twitter_via\']','$wgTemplateSandboxEditNamespaces','$wgtoNamespacesWithTooltips','$wgtoEarlyCategoryFiltering','$wgtoAssumeNonemptyPageTitle','$wgtoEarlyTargetRedirectFollow','$wgtoEnableInNamespaces','$wgtoLateCategoryFiltering','$wgtoEnableOnImageLinks','$wgtoLoadingTooltip','$wgtoEarlyExistsCheck','$wgtoEarlyPageTitleParse','$wgVisualEditorDisableForAnons','$wgVisualEditorEnableTocWidget','$wgVisualEditorShowBetaWelcome', '$wgScribuntoEngineConf[\'luaautodetect\'][\'memoryLimit\']'
	];

	/**
	 * Map of whitelisted Hydra allowed_extension_md5_key to UCP extension enable toggle
	 *
	 * @var array
	 */
	private static $enableVarMap = [
		"d392c52b360b7024e7f343cee32a107e" => "wgEnableAbuseFilterExtension", "d3c350b4942a572c36754b6454d554d1" => "wgEnableApprovedRevs", "3304509c48914a75a67c025bb2414914" => "wgEnableArrayExt", "08348a92db1457d64cc268943be139f8" => "wgEnableAudioButton", "01544acee9da416b705bffb60caecfcd" => "wgEnableBabel", "52ae115db0dfedf8538705f60214c6bc" => "wgEnableBoilerRoom", "d202395025143887571b4a5f857b1ac1" => "wgEnableCargo", "65b059da2161ea7196195430d3b1608f" => "wgEnableCategorySkins", "1d2d48987603e916bd0af0e3247f3459" => "wgEnableCatisect", "751dcb9b06b64919efabeb5a6d35c2b9" => "wgEnableCharacterEscapes", "4507a59b2089cad277775a1d945c53c8" => "wgEnableCleanChanges", "6ecc81de0a64c395c7aa9830318f50e5" => "wgEnableCodeEditor", "a3b8353ba72a747a985a29e672591dc0" => "wgEnableCountdown", "7e206f272027f09abcb1e823b5dfb98d" => "wgEnableCrusher", "f426f86927519de1feda70b985ceb9cc" => "wgEnableCurseProfile", "21ae1e72984187f4f5c3de8f345d7215" => "wgEnableCustomLogs","8cbcb5e2123f74012fdce11f73044ffe" => "wgEnableDefaultLinks", "ea0bbbf2ef86d98db118d9cb0c52253d" => "wgEnableDeleteBatch", "235378c259c392dcbc3d1da9159c5091" => "wgEnableDescription2", "3f13dcb925fe780a7c9391f94261ffea" => "wgWikiaEnableDPLForum", "b113f7897b36545e8cc694dc177a167f" => "wgEnableDrafts", "59bb68017db5bc7fc740a85c9c8dd0c8" => "wgWikiaEnableDPLExt", "a48a3f4196feaef7ad3c2cbe611b1096" => "wgEnableEditcount", "72ffab2cbc9ab6047e2f4b29ac8ac1d1" => "wgEnableEvonyCalculator", "155a209d8c0d03449a5000b479c8d545" => "wgEnableExcludeRandom", "c2008d954c4efe4d87067aa7fcc644ca" => "wgEnableGadgetsExt", "1c42c636af27152f2724c914e4c72ab7" => "wgEnableGuildWikiSkills", "1cb097b05048ee7b4cb97cba5c612c74" => "wgEnableHeaderCount", "96e6886e90fbbeae9da2a9de71c268fe" => "wgEnableHeaderFooter", "f5796fd961bfb70c8f002f60dcc52bc3" => "wgEnableHeaderTabs", "fe74309a88cbe0fbe5770617738881e5" => "wgEnableHighlightLinksInCategory", "f002f32ebeb8cdba3871bb290d9917d0" => "wgEnableFileInfoFunctionsExt", "4d777c0486ab7d01d3a797b5fe0854ea" => "wgEnableFlaggedRevs", "f7117023ac70cc93501de99936039a28" => "wgEnableJavascriptSlideshow", "e0d1c5800a5d537838e99a0cb89d6510" => "wgEnableLocalisationUpdate", "3f2752d66d93b59ebd9e83398933802e" => "wgEnableLoopsExt", "25c3f44196f3155d27d88ab645d876f4" => "wgEnableLuaCache", "b9002351b338518866c88c8f21d6d158" => "wgEnableMagicNoCache", "de47d93adb670c3f34c215acd9933352" => "wgEnableMultimediaViewer", "ce411f351851b5095b38ff3478108788" => "wgEnableMyVariables", "83e47b8b4496a018e28462d8504862aa" => "wgEnableNewestPages", "56715b1292a54c4f3e28b6d3e19a0257" => "wgEnableNumbertext", "799cdfbe31e97f08505204a8d563d250" => "wgEnableOpenGraphMeta", "1b93810bc60cb813856dacd49cf347c1" => "wgEnableOreDict", "99f757fbd7b4392e4313b7bde143d8b7" => "wgEnablePageForms", "f542a3f0affb383c0821a79d858bf61a" => "wgEnablePageSchemas", "cf87ef0ce1efda52ea0a6dcf7d0e6cac" => "wgEnableParserHooks", "e9ad37cf5cd4ebbd723bc55972ea6063" => "wgEnableParserPower", "d3b8c0cd2c597458fd92ac2284f10126" => "wgEnablePDFEmbed", "94c1acefb7a2669da08948663b6752ec" => "wgEnablePopups", "3b1a6737f6ac7032085568255ee53096" => "wgEnablePvXCode", "cdd0707d1be0f81f248888827980649a" => "wgEnablePvXRate", "855392510d70ede20057ccfe5e46644a" => "wgEnableRegexFunctions", "9dad176c2fbab54a69f6cc8184ad38d2" => "wgEnableRevisionSlider", "3ea2ecd95ad1a66789b7bfef112e95c1" => "wgEnableRoadblock", "a1e38d56c6195ad4eb4c971cb9ef2b0a" => "wgEnableScryfallLinks", "0bdc18adbd2c072fe437781598af3228" => "wgEnableSimpleSort", "b7d7b6e5b2d55a1b15ec2e227cbac4e7" => "wgEnableSimpleTooltipExt", "8d308632591b045bd2f0356deac84f61" => "wgEnableSpoilers", "683e3369b97b4a0fa421634e3ac904d7" => "wgEnableSubPageList", "2946558a80b2d958954586ddda1993d6" => "wgEnableSpriteSheet", "0130bc24bdf845a5c3c4206d31eb8539" => "wgEnableStreamer", "46275d88df04fc9629c2cc50e829b957" => "wgEnableTemplateSandbox", "9e332aa706c1be8de23062668836ea83" => "wgEnableTilesheets", "ad0900cb21b8c6e4e719dae89ccd919b" => "wgEnableTippingOver", "42f2553d4ad4c106c6c4954e917e45d1" => "wgEnableTopContributors", "9a1884dd44197ea2b5b9eef6f43cb286" => "wgEnableTorBlock", "bbce23eaef02844dc5e032aa252cc682" => "wgEnableTranslate", "80e1e2b48758f2848ddb7ac1e478795c" => "wgEnableUniversalLanguageSelector", "23fa8ce84f1e7edaf5e73232c41f8ef8" => "wgEnableUploadFields", "4a190140c45549e431e0cb7a1c5e24fd" => "wgEnableUploadWizard", "96940ff4ef99d861880907e7e44f30b3" => "wgEnableUsingData", "2662f6fbe663fc5cce72372ec2306f98" => "wgEnableVariablesLua", "786ecc03da8b403c8b559bbfe5d84718" => "wgEnableWidgets", "fbb642879b55c4452c29d00fe676d777" => "wgEnablewikihiero"
	];

	/**
	 * Constructor
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
		$this->addDescription('Exports whitelisted settings to insert into UCP database');
		$this->addOption('domain', 'The Hydra wiki domain', true, true);
		$this->addOption('newCityID', 'The new city_id from the UCP', true, true);
	}

	/**
	 * Export settings
	 *
	 * @return void
	 */
	public function execute() {
		// Validate settings
		$domain = $this->getOption('domain');
		if (!is_string($domain)) {
			$this->error('Invalid domain, must be string');
			return;
		}

		$newCityID = $this->getOption('newCityID');
		if (!is_numeric($newCityID)) {
			$this->error('Invalid new city_id, must be numeric');
			return;
		}

		$wiki = Wiki::loadFromDomain($domain);
		if (!is_object($wiki)) {
			$this->error('Unable to find domain ' . $domain);
			return;
		}

		$siteKey = $wiki->getSiteKey();

		// Grab the settings from wiki_allowed settings
		// This query excludes settings that are deleted from wiki_allowed_settings and settings that haven't been changed from the default
		$db = wfGetDB(DB_MASTER);

		$result = $db->select(
			['wiki_settings', 'wiki_allowed_settings'],
			['*'],
			[	"site_key = '$siteKey'",
				"wiki_allowed_settings.deleted = 0",
				"setting_value != setting_default"
			],
			__METHOD__,
			[],
			[
				'wiki_allowed_settings' => [
					'JOIN', [
						'wiki_allowed_settings.asid = wiki_settings.allowed_setting_id'
					]
				]
			]
		);

		// Build array of settings data that we want to migrated, based on the whitelist
		$settingsData = [];
		while ($row = $db->fetchRow($result)) {
			if (in_array($row['setting_key'], self::$settingsWhitelist)) {
				$strippedName = str_replace("$", '', $row['setting_key']);
				$settingsData[$strippedName]['name'] = $strippedName;
				$settingsData[$strippedName]['type'] = $row['setting_type'];
				$settingsData[$strippedName]['value'] = $row['setting_value'];
				$settingsData[$strippedName]['default'] = $row['setting_default'];
			}
		}

		$db->freeResult($result);

		// Remap settings to existing UCP settings to cut down on duplicate settings
		$settingsData = $this->remapSettingNamesGPtoFandom($settingsData);

		// Some wikis don't have a setting set and we aren't moving the default in code
		// We need to add a default setting if there isn't an existing one for every wiki
		$settingsData = $this->setMissingDefaults($settingsData);

		// Namespace data is stored seperately from other settings on Gamepedia, but with regular settings on UCP
		$settingsData = $this->setExtraNamespaceSettings($db, $settingsData, $siteKey);

		// Some wikis have ads disabled on specific pages. Translate our setting to UCP setting
		$settingsData = $this->setAdsDisabledPageList($db, $settingsData, $siteKey);

		// Only move wgDefaultSkin if it isn't set to hydra
		// We already set hydra as the default for Gamepedia wikis on the UCP
		if (isset($settingsData['wgDefaultSkin']) && $settingsData['wgDefaultSkin'] == 'hydra') {
			unset($settingsData['wgDefaultSkin']);
		}

		// Handle Social settings special case
		$settingsData = $this->mergeSocialSettings($settingsData);

		// Handle DPL settings special case
		$settingsData = $this->mergeDPLSettings($settingsData);

		// Set some other one off settings
		$settingsData = $this->setOneOffSettings($settingsData, $wiki);

		// If this wiki uses a group file repo, we need to set some additional settings
		if ($wiki->isGroupFileRepo()) {
			$settingsData = $this->setGroupFileRepoSettings($db, $settingsData, $wiki, $newCityID);
		}

		// Enabled extensions are stored in wiki_extensions.
		// Should we move settings for disabled extensions? Probably...
		// Table has site md5 key and allowed_extension md5 key, so map those to the new wgEnable settings
		$result = $db->select(
			['wiki_extensions'],
			['*'],
			["site_key = '$siteKey'"],
			__METHOD__
		);

		while ($row = $db->fetchRow($result)) {
			if (array_key_exists($row['allowed_extension_md5_key'], self::$enableVarMap)) {
				$enableName = self::$enableVarMap[$row['allowed_extension_md5_key']];
				$settingsData[$enableName]['name'] = $enableName;
				$settingsData[$enableName]['type'] = "boolean";
				$settingsData[$enableName]['value'] = serialize(true);
			}
			// Handle special case for Gadgets, need both $wgUseSiteJs && $wgEnableGadgetsExt
			if ($row['allowed_extension_md5_key'] == "c2008d954c4efe4d87067aa7fcc644ca") {
				$enableName = "wgUseSiteJs";
				$settingsData[$enableName]['name'] = $enableName;
				$settingsData[$enableName]['type'] = "boolean";
				$settingsData[$enableName]['value'] = serialize(true);
			}
			// Handle special case for Widgets, need to set $wgWidgetsCompileDir
			if ($row['allowed_extension_md5_key'] == "786ecc03da8b403c8b559bbfe5d84718") {
				$enableName = "wgWidgetsCompileDir";
				$settingsData[$enableName]['name'] = $enableName;
				$settingsData[$enableName]['type'] = "string";
				$settingsData[$enableName]['value'] = serialize("/tmp/widgets/" . $wiki->getDatabase()['db_name'] . "/");
			}
			// For now, we can skip this. It looks like wgWikiaDisableAllDPLExt defaults to false and doesn't even exist in WikiConfig on live
			// Leaving this code just in case wgWikiaDisableAllDPLExt returns
			/*
			// Handle special case for DPLForum and DPL, need both wgEnable variable && !$wgWikiaDisableAllDPLExt
			if ($row['allowed_extension_md5_key'] == "3f13dcb925fe780a7c9391f94261ffea" || $row['allowed_extension_md5_key'] == "59bb68017db5bc7fc740a85c9c8dd0c8") {
				$enableName = "wgWikiaDisableAllDPLExt";
				$settingsData[$enableName]['name'] = $enableName;
				$settingsData[$enableName]['type'] = "boolean";
				$settingsData[$enableName]['value'] = serialize(false);
			}
			*/
		}

		$db->freeResult($result);

		// Handle wgGroupPermissionsLocal, wgAddGroupsLocal, wgRemoveGroupsLocal
		// This needs to come after adding enabled extension data, as we add extra groups for wikis with wgEnableRoadblock on
		$settingsData = $this->setGroupSettings($db, $settingsData, $siteKey);

		// Generate setting queries
		foreach ($settingsData as $key => $value) {
			$encodedSetting = json_encode(unserialize($value['value']), JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
			$encodedSetting = $db->addQuotes($encodedSetting);
			$quotedSettingName = $db->addQuotes($key);

			$statement = "INSERT INTO city_variables (cv_city_id, cv_variable_id, cv_value, cv_variable_value) VALUES ($newCityID, (SELECT cv_id FROM city_variables_pool WHERE cv_name = $quotedSettingName), '', $encodedSetting) ON DUPLICATE KEY UPDATE cv_variable_value = VALUES(cv_variable_value);\r\n";

			echo $statement;
		}

		// Update the wiki vertical
		echo "UPDATE city_list SET city_vertical = 2 WHERE city_id = $newCityID;\r\n";
	}

	/**
	 * Change setting names that we've identified are similar to existing settings on the UCP
	 *
	 * @param array $settingsData
	 *
	 * @return array
	 */
	private function remapSettingNamesGPtoFandom ($settingsData) {
		// Remap wgIsOfficialWiki to wgOfficialWiki
		if (isset($settingsData['wgIsOfficialWiki'])) {
			$settingsData['wgOfficialWiki'] = $settingsData['wgIsOfficialWiki'];
			$settingsData['wgOfficialWiki']['name'] = 'wgOfficialWiki';
			unset($settingsData['wgIsOfficialWiki']);
		}

		return $settingsData;
	}

	/**
	 * Handle setting some one off settings
	 *
	 * @param array $settingsData
	 * @param Wiki $wiki
	 *
	 * @return array
	 */
	private function setOneOffSettings($settingsData, $wiki) {
		// wgClaimWikiEnabled is a special case. It is set to off by default on the UCP.
		// On Hydra it defaults to on, isn't in wiki_allowed_settings, instead is a noraml setting
		// We need to set it to true by default, unless any settings loaded from wiki_settings override it
		if (!isset($settingsData['wgClaimWikiEnabled'])) {
			$settingsData['wgClaimWikiEnabled']['name'] = 'wgClaimWikiEnabled';
			$settingsData['wgClaimWikiEnabled']['type'] = "boolean";
			$settingsData['wgClaimWikiEnabled']['value'] = serialize(true);
		}

		// Add wgEnableHydraFeatures
		$settingsData['wgEnableHydraFeatures']['name'] = 'wgEnableHydraFeatures';
		$settingsData['wgEnableHydraFeatures']['type'] = "boolean";
		$settingsData['wgEnableHydraFeatures']['value'] = serialize(true);

		// Set wgCacheEpoch to the current timestamp to invalidate all the things
		$settingsData['wgCacheEpoch']['name'] = 'wgCacheEpoch';
		$settingsData['wgCacheEpoch']['type'] = "string";
		$settingsData['wgCacheEpoch']['value'] = serialize(gmdate('YmdHis'));

		// Set wgActorTableSchemaMigrationStage to SCHEMA_COMPAT_NEW (0x30)
		$settingsData['wgActorTableSchemaMigrationStage']['name'] = 'wgActorTableSchemaMigrationStage';
		$settingsData['wgActorTableSchemaMigrationStage']['type'] = "integer";
		$settingsData['wgActorTableSchemaMigrationStage']['value'] = serialize(48);

		// Set wgUploadPath to https://images.wikia.com/%s/images
		$settingsData['wgUploadPath']['name'] = 'wgUploadPath';
		$settingsData['wgUploadPath']['type'] = "string";
		$settingsData['wgUploadPath']['value'] = serialize("https://images.wikia.com/" . $wiki->getDatabase()['db_name'] . "/images");

		// Set wgMetaNamespace
		$settingsData['wgMetaNamespace']['name'] = 'wgMetaNamespace';
		$settingsData['wgMetaNamespace']['type'] = "string";
		$settingsData['wgMetaNamespace']['value'] = serialize($wiki->getMetaName());

		// Set dsSiteKey to the md5_key
		$settingsData['dsSiteKey']['name'] = 'dsSiteKey';
		$settingsData['dsSiteKey']['type'] = "string";
		$settingsData['dsSiteKey']['value'] = serialize($wiki->getSiteKey());

		// Add wiki managers
		if (isset($wiki->getManagers()[0]) && \User::newFromName($wiki->getManagers()[0])->getId() != false) {
			$settingsData['wgWikiManager']['name'] = 'wgWikiManager';
			$settingsData['wgWikiManager']['type'] = "string";
			$settingsData['wgWikiManager']['value'] = serialize(\User::newFromName($wiki->getManagers()[0])->getId());
		}

		// Handle esports commons
		if ($wiki->getGroupKey() == "496330b51310bb098236024a7e1f4eb8") {
			$settingsData['wgUseEsportsCommons']['name'] = 'wgUseEsportsCommons';
			$settingsData['wgUseEsportsCommons']['type'] = "boolean";
			$settingsData['wgUseEsportsCommons']['value'] = serialize(true);
		}

		// Set post-migration banner settings
		$settingsData['wgUCPMigrationDone']['name'] = 'wgUCPMigrationDone';
		$settingsData['wgUCPMigrationDone']['type'] = "boolean";
		$settingsData['wgUCPMigrationDone']['value'] = serialize(true);

		$settingsData['wgUCPMigrationDate']['name'] = 'wgUCPMigrationDate';
		$settingsData['wgUCPMigrationDate']['type'] = "string";
		$settingsData['wgUCPMigrationDate']['value'] = serialize(date("Y-m-d"));

		return $settingsData;
	}

	/**
	 * Handle group file repo wikis
	 *
	 * @param \Wikimedia\Rdbms\Database $db
	 * @param array $settingsData
	 * @param Wiki $wiki
	 *
	 * @return array
	 */
	private function setGroupFileRepoSettings($db, $settingsData, $wiki, $newCityID) {
		$groupFileRepo = $wiki->getGroupFileRepo();

		$enableName = "wgUseSharedUploads";
		$settingsData[$enableName]['name'] = $enableName;
		$settingsData[$enableName]['type'] = "boolean";
		$settingsData[$enableName]['value'] = serialize(true);

		$enableName = "wgFetchCommonsDescriptions";
		$settingsData[$enableName]['name'] = $enableName;
		$settingsData[$enableName]['type'] = "boolean";
		$settingsData[$enableName]['value'] = serialize(true);

		$enableName = "wgSharedUploadDBname";
		$settingsData[$enableName]['name'] = $enableName;
		$settingsData[$enableName]['type'] = "string";
		$settingsData[$enableName]['value'] = serialize($groupFileRepo['repo']['wiki']);

		// wgSharedUploadPath https://images.wikia.com/DBNAME/images/
		$enableName = "wgSharedUploadPath";
		$settingsData[$enableName]['name'] = $enableName;
		$settingsData[$enableName]['type'] = "string";
		$settingsData[$enableName]['value'] = serialize("https://images.wikia.com/" . $groupFileRepo['repo']['wiki'] . "/images");

		// wgRepositoryBaseUrl
		// https://mainDomain.gamepedia.com/File:
		$enableName = "wgRepositoryBaseUrl";
		$settingsData[$enableName]['name'] = $enableName;
		$settingsData[$enableName]['type'] = "string";
		$settingsData[$enableName]['value'] = serialize($groupFileRepo['repo']['descBaseUrl']);

		// wgSharedUploadDirectory should be set to wgUploadDirectory of the parent
		// This is a special case, we are going to output this setting query directly
		// We'll use the parent DB name as a lookup ($groupFileRepo['repo']['wiki']) for the cityID, which we'll then use to get the wgUploadPath of the parent
		// You can't update a table you are using directly in a subquery, so we've added another level and selected from that as tempTable
		// I apologize for my SQL sins

		$quotedDBName = $db->addQuotes($groupFileRepo['repo']['wiki']);
		$encodedSetting = "(SELECT cv_variable_value FROM (SELECT cv_variable_value FROM city_variables WHERE cv_city_id = (SELECT city_id FROM city_list WHERE city_dbname = $quotedDBName) AND cv_variable_id = (SELECT cv_id FROM city_variables_pool WHERE cv_name ='wgUploadPath')) as tempTable)";

		$statement = "INSERT INTO city_variables (cv_city_id, cv_variable_id, cv_value, cv_variable_value) VALUES ($newCityID, (SELECT cv_id FROM city_variables_pool WHERE cv_name = " . $db->addQuotes('wgSharedUploadDirectory') . "), '', $encodedSetting) ON DUPLICATE KEY UPDATE cv_variable_value = VALUES(cv_variable_value);\r\n";

		echo $statement;

		return $settingsData;
	}

	/**
	 * Set wgAdDriverPagesWithoutAds for pages without ads
	 *
	 * @param \Wikimedia\Rdbms\Database $db
	 * @param array $settingsData
	 * @param string $siteKey
	 *
	 * @return array
	 */
	private function setAdsDisabledPageList($db, $settingsData, $siteKey) {
		// We've disabled ads on some pages and the data is stored in jstop in wiki_advertisements
		$result = $db->select(
			['wiki_advertisements'],
			['*'],
			[	"site_key = '$siteKey'",
				"jstop like '%wgAdDriverPagesWithoutAds%'"
			],
			__METHOD__
		);

		$disabledAdPages = [];

		$row = $db->fetchRow($result);

		preg_match_all("#window\.wgAdDriverPagesWithoutAds(.*?)(\[(.*?)\])\;#ism", $row['jstop'], $pageListMatches);

		if (isset($pageListMatches) && !empty($pageListMatches)) {
			$pageList = json_decode($pageListMatches[2][0]);
			$disabledAdPages = $pageList;
		}

		if (!empty($disabledAdPages)) {
			$settingName = "wgAdDriverPagesWithoutAds";
			$settingsData[$settingName]['name'] = $settingName;
			$settingsData[$settingName]['type'] = "array";
			$settingsData[$settingName]['value'] = serialize($disabledAdPages);
		}

		$db->freeResult($result);

		return $settingsData;
	}

	/**
	 * Set extra namespace information
	 *
	 * @param \Wikimedia\Rdbms\Database $db
	 * @param array $settingsData
	 * @param string $siteKey
	 *
	 * @return array
	 */
	private function setExtraNamespaceSettings($db, $settingsData, $siteKey) {
		// Extra namespaces are stored in wiki_namespaces
		// These need to be put into wgExtraNamespacesLocal, wgContentNamespaces, and wgNamespacesToBeSearchedDefault
		$result = $db->select(
			['wiki_namespaces'],
			['*'],
			["site_key = '$siteKey'"],
			__METHOD__
		);

		$extraNamespacesLocal = [];
		$contentNamespaces = [0];
		$namespacesToBeSearchedDefault = [0 => "true"];
		$namespacesWithSubpages = [];
		$namespaceProtection = [];

		while ($row = $db->fetchRow($result)) {
			$extraNamespacesLocal[$row['namespace_id']] = $row['namespace_name'];
			$extraNamespacesLocal[$row['namespace_talk_id']] = $row['namespace_talk'];
			$namespacesWithSubpages[intval($row['namespace_id'])] = true;

			if ($row['is_content'] == 1) {
				$contentNamespaces[] = intval($row['namespace_id']);
			}

			if ($row['is_searchable'] == 1) {
				$namespacesToBeSearchedDefault[$row['namespace_id']] = "true";
			}

			if ($row['namespace_protection'] != '') {
				$namespaceProtection[$row['namespace_id']] = $row['namespace_protection'];
			}
		}

		if (!empty($namespacesWithSubpages)) {
			$settingName = "wgNamespacesWithSubpagesLocal";
			$settingsData[$settingName]['name'] = $settingName;
			$settingsData[$settingName]['type'] = "array";
			$settingsData[$settingName]['value'] = serialize($namespacesWithSubpages);
		}

		if (!empty($extraNamespacesLocal)) {
			$settingName = "wgExtraNamespacesLocal";
			$settingsData[$settingName]['name'] = $settingName;
			$settingsData[$settingName]['type'] = "array";
			$settingsData[$settingName]['value'] = serialize($extraNamespacesLocal);
		}

		if (count($contentNamespaces) > 1) {
			$settingName = "wgContentNamespaces";
			$settingsData[$settingName]['name'] = $settingName;
			$settingsData[$settingName]['type'] = "array";
			$settingsData[$settingName]['value'] = serialize($contentNamespaces);
		}

		if (count($namespacesToBeSearchedDefault) > 1) {
			$settingName = "wgNamespacesToBeSearchedDefault";
			$settingsData[$settingName]['name'] = $settingName;
			$settingsData[$settingName]['type'] = "array";
			$settingsData[$settingName]['value'] = serialize($namespacesToBeSearchedDefault);
		}

		if (!empty($namespaceProtection)) {
			$settingName = "wgNamespaceProtection";
			$settingsData[$settingName]['name'] = $settingName;
			$settingsData[$settingName]['type'] = "array";
			$settingsData[$settingName]['value'] = serialize($namespaceProtection);
		}

		$db->freeResult($result);

		return $settingsData;
	}

	/**
	 * Set defaults when they are needed and missing
	 *
	 * @param  array $settingsData
	 *
	 * @return array
	 */
	private function setMissingDefaults($settingsData) {
		if (!isset($settingsData['wgSkipSkins'])) {
			$settingsData['wgSkipSkins']['name'] = 'wgSkipSkins';
			$settingsData['wgSkipSkins']['type'] = "array";
			$settingsData['wgSkipSkins']['value'] = serialize(["chick", "cologneblue", "exvius", "hydradark", "minerva", "minervaneue", "modern", "monobook", "myskin", "netbar", "nostalgia", "simple", "standard", "vector"]);
		}

		// Some wikis don't have $wgRightsIcon set, so we need to add the default setting
		if (!isset($settingsData['wgRightsIcon'])) {
			$settingsData['wgRightsIcon']['name'] = 'wgRightsIcon';
			$settingsData['wgRightsIcon']['type'] = "string";
			$settingsData['wgRightsIcon']['value'] = serialize("//i.creativecommons.org/l/by-nc-sa/3.0/88x31.png");
		}

		// Some wikis don't have $wgRightsText set, so we need to add the default setting
		if (!isset($settingsData['wgRightsText'])) {
			$settingsData['wgRightsText']['name'] = 'wgRightsText';
			$settingsData['wgRightsText']['type'] = "string";
			$settingsData['wgRightsText']['value'] = serialize("CC BY-NC-SA 3.0");
		}

		return $settingsData;
	}

	/**
	 * Handle Group and Permission settings
	 *
	 * @param \Wikimedia\Rdbms\Database $db
	 * @param array $settingsData
	 * @param string $siteKey
	 *
	 * @return array
	 */
	private function setGroupSettings($db, $settingsData, $siteKey) {
		// Grab group configs from wiki_group_permissions
		$result = $db->select(
			['wiki_group_permissions'],
			['*'],
			["site_key = '$siteKey'"],
			__METHOD__
		);

		/* wgGroupPermissionsLocal format example: group|permission|<0|1>
			bureaucrat|edit|0,
			staff|edit|1,
			*|upload|0,
			sysop|protect|1,
		*/
		$wikiGroups = [];
		$groupsData = [];

		while ($row = $db->fetchRow($result)) {
			$groupConfig = unserialize($row['group_config']);

			// Build a list of groups for wgAddGroupsLocal and wgRemoveGroupsLocal
			if ($row['group_name'] != "*") {
				$wikiGroups[] = $row['group_name'];
			}

			// Build a list for wgGroupPermissionsLocal
			foreach ($groupConfig as $key => $permissionList) {
				$keyInt = ($key == 'allow') ? 1 : 0;

				foreach ($permissionList as $plKey => $permission) {
					$groupsData[] = $row['group_name'] . "|$permission|$keyInt";
				}
			}
		}

		// If this is a Roadblock wiki, we need to grant some additional permissions on the UCP
		if (isset($settingsData['wgEnableRoadblock'])) {
			$permissionsToGrant = ['edit', 'upload', 'reupload', 'createpage', 'createtalk', 'upload_by_url', 'move'];
			$groupsToGrant = ['staff', 'wiki-manager', 'content-team-member', 'helper'];

			foreach ($groupsToGrant as $keyGroups => $valueGroups) {
				foreach ($permissionsToGrant as $key => $value) {
					$groupsData[] = "$valueGroups|$value|1";
				}
			}
		}

		if (!empty($wikiGroups)) {
			$settingsData['wgGroupPermissionsLocal']['name'] = 'wgGroupPermissionsLocal';
			$settingsData['wgGroupPermissionsLocal']['type'] = "text";
			$settingsData['wgGroupPermissionsLocal']['value'] = serialize(implode(",\r\n", $groupsData));
		}

		if (!empty($groupsData)) {
			// Diff list of groups for wgAddGroupsLocal and wgRemoveGroupsLocal
			$restrictedGroups = ['global_bureaucrat', 'global_sysop', 'grasp', 'hydra_admin', 'hydra_staff', 'checkuser', 'global_bot', 'wiki_guardian'];
			$addGroups['bureaucrat'] = array_values(array_diff($wikiGroups, $restrictedGroups));
			$removeGroups['bureaucrat'] = array_values(array_diff($wikiGroups, $restrictedGroups));

			$settingsData['wgAddGroupsLocal']['name'] = 'wgAddGroupsLocal';
			$settingsData['wgAddGroupsLocal']['type'] = "array";
			$settingsData['wgAddGroupsLocal']['value'] = serialize($addGroups);

			$settingsData['wgRemoveGroupsLocal']['name'] = 'wgRemoveGroupsLocal';
			$settingsData['wgRemoveGroupsLocal']['type'] = "array";
			$settingsData['wgRemoveGroupsLocal']['value'] = serialize($removeGroups);
		}

		return $settingsData;
	}

	/**
	 * DPL settings are an array, but we save individual elements
	 * Merge elements with defaults for UCP
	 *
	 * @param array $settingsData
	 *
	 * @return array
	 */
	private function mergeDPLSettings($settingsData) {
		$DPLStillDefault = true;
		$wgDplSettings = [
			"allowedNamespaces" => null,
			"allowUnlimitedCategories" => false,
			"allowUnlimitedResults" => false,
			"behavingLikeIntersection" => false,
			"categoryStyleListCutoff" => 6,
			"fixedCategories" => [],
			"functionalRichness" => 3,
			"maxCategoryCount" => 4,
			"minCategoryCount" => 0,
			"maxResultCount" => 500,
			"recursiveTagParse" => false,
			"runFromProtectedPagesOnly" => false,
			"handleSectionTag" => false
		];

		// Settings that we have overriden
		$DPLOverrideSettings = [
			'allowUnlimitedCategories'		=> 'wgDplSettings[\'allowUnlimitedCategories\']',
			'allowUnlimitedResults'			=> 'wgDplSettings[\'allowUnlimitedResults\']',
			'maxCategoryCount'				=> 'wgDplSettings[\'maxCategoryCount\']',
			'behavingLikeIntersection'		=> 'wgDplSettings[\'behavingLikeIntersection\']',
			'maxResultCount'				=> 'wgDplSettings[\'maxResultCount\']',
			'fixedCategories'				=> 'wgDplSettings[\'fixedCategories\']'
		];

		// If any DPL settings need to be migrated, we have to set them all
		foreach ($DPLOverrideSettings as $settingName => $hydraDBSettingName) {
			if (isset($settingsData[$hydraDBSettingName]) && unserialize($settingsData[$hydraDBSettingName]['value']) != $wgDplSettings[$settingName]) {
				// Swap the constructed DPL settings array value with the DB value
				$wgDplSettings[$settingName] = unserialize($settingsData[$hydraDBSettingName]['value']);

				// Unset DPL settings, as we can't bring them over in this format
				unset($settingsData[$hydraDBSettingName]);
				$DPLStillDefault = false;
			}
		}

		// If we need to actually bring over any of these settings
		if ($DPLStillDefault === false) {
			$settingsData['wgDplSettings']['name'] = "wgDplSettings";
			$settingsData['wgDplSettings']['type'] = "array";
			$settingsData['wgDplSettings']['value'] = serialize($wgDplSettings);
		}

		return $settingsData;
	}

	/**
	 * Social settings are an array, but we save individual elements
	 * Merge elements with defaults for UCP
	 *
	 * @param array $settingsData
	 *
	 * @return array
	 */
	private function mergeSocialSettings($settingsData) {
		$socialStillDefault = true;
		$wgSocialSettings = [
			'sub_reddit'			=> '',
			'hash_tags'				=> '',
			'twitter_via'			=> 'CurseGamepedia',
			'prompt_cookie_time'	=> 3600,
			'sidebar_enabled'		=> true,
			'navigation_enabled'	=> true,
			'prompts_enabled'		=> true,
			'sidebar_image_set'		=> 'square_icons',
			'share_image_set'		=> 'square_icons',
			'profile_urls'			=> [
				'twitter'		=> 'https://twitter.com/CurseGamepedia',
				'facebook'		=> 'https://www.facebook.com/CurseGamepedia',
				'twitch'		=> 'https://www.twitch.tv/gamepedia/videos'
			]
		];

		$oldSocialSettingsProfileURLs = [
			'twitter'		=> 'https://twitter.com/CurseGamepedia',
			'facebook'		=> 'https://www.facebook.com/CurseGamepedia',
			'twitch'		=> 'https://www.twitch.tv/curse/videos/all'
		];

		// Handle the various states profile URLs can be in
		if (isset($settingsData['wgSocialSettings[\'profile_urls\']'])) {

			// No need to move the default (or bad default) settings
			if (unserialize($settingsData['wgSocialSettings[\'profile_urls\']']['value']) != $wgSocialSettings['profile_urls'] &&
				unserialize($settingsData['wgSocialSettings[\'profile_urls\']']['value']) != $oldSocialSettingsProfileURLs) {

					$wgSocialSettings['profile_urls'] = unserialize($settingsData['wgSocialSettings[\'profile_urls\']']['value']);
					$socialStillDefault = false;
			}

			unset($settingsData['wgSocialSettings[\'profile_urls\']']);
		}

		// Less complicated Social settings
		$otherSocialSettings = [
			'sidebar_enabled'		=> 'wgSocialSettings[\'sidebar_enabled\']',
			'prompts_enabled'		=> 'wgSocialSettings[\'prompts_enabled\']',
			'navigation_enabled'	=> 'wgSocialSettings[\'navigation_enabled\']',
			'twitter_via'			=> 'wgSocialSettings[\'twitter_via\']'
		];

		// If any Social settings need to be migrated, we have to set them all
		foreach ($otherSocialSettings as $settingName => $hydraDBSettingName) {
			if (isset($settingsData[$hydraDBSettingName]) && unserialize($settingsData[$hydraDBSettingName]['value']) != $wgSocialSettings[$settingName]) {
				// Swap the constructed Social settings array value with the DB value
				$wgSocialSettings[$settingName] = unserialize($settingsData[$hydraDBSettingName]['value']);

				// Unset Social settings, as we can't bring them over in this format
				unset($settingsData[$hydraDBSettingName]);
				$socialStillDefault = false;
			}
		}

		// If we need to actually bring over any of these settings
		if ($socialStillDefault === false) {
			$settingsData['wgSocialSettings']['name'] = "wgSocialSettings";
			$settingsData['wgSocialSettings']['type'] = "array";
			$settingsData['wgSocialSettings']['value'] = serialize($wgSocialSettings);
		}

		return $settingsData;
	}
}

$maintClass = 'DynamicSettings\SettingsExport';
require_once RUN_MAINTENANCE_IF_MAIN;
