<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * SiteAds PHP Template
 *
 * @author    Alex Smith
 * @copyright (c) 2014 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
**/

class TemplateSiteAds {
	/**
	 * Site Advertisements
	 *
	 * @param array Identifier Slots
	 * @param array Javascript Slots
	 * @param array Advertisement Slots
	 * @param array Miscellaneous Slots
	 * @param boolean Are advertisements disabled?
	 *
	 * @return string Built PHP
	 */
	public static function siteAds($idSlots, $jsSlots, $adSlots, $miscSlots, $adsDisabled) {
		$php = "<?php\n";
		if (count($idSlots)) {
			$php .= '$wgSiteIdSlots = ' . htmlspecialchars_decode(var_export($idSlots, true), ENT_NOQUOTES) . ";\n";
		}
		if (count($jsSlots)) {
			$php .= '$wgSiteJsSlots = ' . htmlspecialchars_decode(var_export($jsSlots, true), ENT_NOQUOTES) . ";\n";
		}
		if (count($adSlots)) {
			$php .= '$wgSiteAdSlots = ' . htmlspecialchars_decode(var_export($adSlots, true), ENT_NOQUOTES) . ";\n";
		}
		if (count($miscSlots)) {
			$php .= '$wgSiteMiscSlots = ' . htmlspecialchars_decode(var_export($miscSlots, true), ENT_NOQUOTES) . ";\n";
		}
		$php .= "?>";

		return $php;
	}
}
