<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Allowed Setting class for arrays
 *
 * @author    Noah Manneschmidt
 * @copyright (c) 2015 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
 **/

namespace DynamicSettings\Settings;

class ArraySetting extends Setting {
	/**
	 * Validates the given value against the current setting type
	 *
	 * @param string Potential value for this setting.
	 *
	 * @return boolean
	 */
	public function isValid($value) {
		return is_array($value);
	}

	/**
	 * Force array type on the text input.
	 *
	 * @param string JSON Form Value
	 *
	 * @return array
	 */
	protected function decodeFormString($value) {
		return json_decode($value, true);
	}

	/**
	 * @param array $value
	 *
	 * @return string
	 */
	protected function encodeFormString($value) {
		return json_encode($value, JSON_UNESCAPED_SLASHES);
	}
}
