<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Move to S3 Storage
 *
 * @author    Alexia E. Smith
 * @copyright (c) 2017 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
**/

require_once dirname(__DIR__, 3) . '/maintenance/Maintenance.php';

class MoveToS3Storage extends Maintenance {
	/**
	 * AWS Region
	 *
	 * @var string
	 */
	private $region = '';

	/**
	 * AWS Bucket
	 *
	 * @var string
	 */
	private $bucket = null;

	/**
	 * Wiki ID
	 *
	 * @var string
	 */
	private $wikiId = null;

	/**
	 * Host Name
	 *
	 * @var string
	 */
	private $host = null;

	/**
	 * The full final run of this migration.
	 *
	 * @var boolean
	 */
	private $isFinal = false;

	/**
	 * Maximum Children
	 *
	 * @var integer
	 */
	private $maxChildren = 50;

	/**
	 * Array of running child workers PID => file path.
	 *
	 * @var array
	 */
	private $children = [];

	/**
	 * Main Constructor
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
		$this->mDescription = "Move media storage to S3.";

		$this->addOption('region', 'AWS Region', true, true);
		$this->addOption('bucket', 'S3 Bucket', true, true);
		$this->addOption('files', 'Subdirectory to move instead of starting at the top.', false, false);
		$this->addOption('subdir', 'Subdirectory to move instead of starting at the top.', false, true);
		$this->addOption('final', "Don't just copy the media.  Lock the wiki and switch over to S3 in full.", false, false);
	}

	/**
	 * Lock the wiki, copy the media, update settings, and unlock the wiki.
	 *
	 * @return void
	 */
	public function execute() {
		global $wgUploadDirectory, $dsSiteKey, $wgServer, $wgLocalFileRepo;

		if (isset($wgLocalFileRepo['backend']) && strpos($wgLocalFileRepo['backend'], 's3') === 0) {
			$this->error("Wiki is already using S3.", 1);
		}

		$url = wfParseUrl($wgServer);
		$this->host = $url['host'];
		if (empty($this->host)) {
			$this->error("Could not parse host from \$wgServer.\n", 1);
		}

		$startDir = $wgUploadDirectory;
		$endReport = true;

		$this->redis = \RedisCache::getClient('worker', [], true);

		$this->wikiId = wfWikiId();

		if (empty($this->wikiId)) {
			$this->error("Could not get a valid wiki ID.\n", 1);
		}

		if ($this->hasOption('subdir')) {
			$startDir = trim($this->getOption('subdir'));
			$endReport = false;
		} else {
			$this->redis->del('s3move:' . $this->wikiId . ':threads');
		}

		$this->isFinal = $this->hasOption('final');

		$this->region = trim($this->getOption('region'));
		$bucketName = trim($this->getOption('bucket'));

		if (!$this->hasOption('subdir')) {
			$this->output("Migrating '{$startDir}' to '" . $bucketName . "' in region '" . $this->region . "' under '" . $this->wikiId . "'...\n");
		}

		if (!is_dir($startDir) || !is_readable($startDir)) {
			$this->error("Directory to move does not exist or is not readable.\n", 1);
		}

		$this->s3 = new \Aws\S3\S3Client(
			[
				'version' => 'latest',
				'region'  => $this->region
			]
		);

		try {
			$allBuckets = $this->s3->listBuckets();
		} catch (Exception $e) {
			$this->error("Could not list buckets from S3: " . $e->getMessage(), 1);
		}

		$bucket = $allBuckets->search("Buckets[?Name == '{$bucketName}']");
		if (is_array($bucket) && count($bucket) == 1) {
			$bucket = array_shift($bucket);
			if ($bucket['Name'] === $bucketName) {
				$this->bucket = $bucket;
			}
		}
		if (empty($this->bucket)) {
			$this->error("Bucket not found.\n", 1);
		}

		$this->lockWiki();

		$this->scanDir($startDir);

		while (count($this->children)) {
			$this->retireChild(true);
		}

		if ($endReport) {
			$status = $this->redis->hgetall('s3move:' . $this->wikiId . ':files');
			if (in_array(0, $status)) {
				$this->output("The following files failed to upload:\n");
				foreach ($status as $file => $result) {
					if (!$result) {
						$this->output("{$file}\n");
					}
				}
				$this->isFinal = false;
				$this->error("Aborting: Unlocking wiki and not switching over to S3.", 1);
				exit;
			}

			if ($this->isFinal) {
				$this->output("Calling DS to change 'use_s3'...\n");
				$output = shell_exec(PHP_BINDIR . '/php ' . __DIR__ . '/Edit.php --wiki=' . escapeshellarg($dsSiteKey) . " --use_s3=true --commit_message='Swap to S3FileBackend'");
				if (strpos($output, 'Errors found') !== false) {
					$this->error($output);
				} else {
					$output = shell_exec(PHP_BINDIR . '/php ' . __DIR__ . '/Tools.php --task=recache --sitekey=' . escapeshellarg($dsSiteKey));
					echo $output;
				}
			}

			$this->unlockWiki();
		}
	}

	/**
	 * Scan a directory for files.
	 *
	 * @param string Directory
	 *
	 * @return void
	 */
	private function scanDir($directory) {
		$directory = rtrim($directory, '/');
		$this->output("{$directory}\n");
		$files = scandir($directory);
		foreach ($files as $index => $file) {
			if (strpos($file, '.') === 0 || $file === 'lock_yBgMBwiR' || $file === 'index.html') {
				continue;
			}
			$filePath = $directory . '/' . $file;
			if (is_dir($filePath)) {
				if ($this->hasOption('files')) {
					continue;
				}
				while (count($this->children) >= $this->maxChildren) {
					$this->output("Sleeping a bit while thread count is too high.\n");
					$this->retireChild(true);
					sleep(1);
				}

				$newPid = pcntl_fork();
				$command = PHP_BINARY . ' ' . dirname(__DIR__, 2) . '/HydraCore/maintenance/hostHelper.php ' . escapeshellarg($this->host) . ' ' . __FILE__ . ' --region=' . escapeshellarg($this->region) . ' --bucket=' . escapeshellarg($this->bucket['Name']) . ' --files --subdir=' . escapeshellarg($filePath) . ' 2>&1';
				// $command = PHP_BINARY.'                                                                                 '.__FILE__.' --region='.escapeshellarg($this->region).' --bucket='.escapeshellarg($this->bucket['Name']).' --files --subdir='.escapeshellarg($filePath).' 2>&1';

				if ($newPid === 0) {
					$output = (string)shell_exec($command);
					echo $output;

					// Do not let this child survive.
					exit(0);
				} elseif ($newPid === -1 || $newPid === null) {
					// Something failed.
					$this->output("Forking failed!  Running serialized...\n");
					$output = (string)shell_exec($command);
					echo $output;
				} else {
					// Parent saves info about child processes
					$this->output("Spawned child process {$newPid} for {$filePath}\n");
					$this->children[$newPid] = $filePath;
				}
				while ($this->retireChild());
				$this->scanDir($filePath);
				continue;
			}
			if (!$this->hasOption('files')) {
				continue;
			}

			$this->output("Upload: {$filePath} ... ");
			$result = $this->putFile($filePath);
			if ($result) {
				$this->output("done!\n");
				$this->redis->hset('s3move:' . $this->wikiId . ':files', $filePath, 1);
			} else {
				$this->output("FAILURE!\n");
				$this->redis->hset('s3move:' . $this->wikiId . ':files', $filePath, 0);
			}
		}
	}

	/**
	 * Cleans up a finished child worker process and removes it from the child list.
	 *
	 * @param boolean [Optional], if true this will block until a child finishes
	 *
	 * @return boolean True if child collected, false if not
	 */
	private function retireChild($wait = false) {
		$flags = 0;
		if (!$wait) {
			$flags = WNOHANG;
		}

		$pid = pcntl_wait($status, $flags);

		if ($pid > 0) {
			$this->cleanupAfterChild($pid, $status);
			return true;
		}
		return false;
	}

	/**
	 * Removes child from our list of children
	 *
	 * @param integer process id
	 * @param integer optional status id returned from pcntl_wait
	 * @param integer optional status code returned by child process
	 */
	private function cleanupAfterChild($pid = 0, $statusId = null, $statusCode = null) {
		if ($statusId !== null) {
			$statusCode = pcntl_wexitstatus($statusId);
		}
		unset($this->children[$pid]);
	}

	/**
	 * Put a file up into S3.
	 *
	 * @param string File Path
	 *
	 * @return boolean Success
	 */
	private function putFile($file) {
		global $wgUploadDirectory;

		$s3Key = $this->wikiId . str_replace($wgUploadDirectory, '', $file);

		$fileHandle = fopen($file, 'rb');
		if ($fileHandle === false) {
			return false;
		}

		$contentType = $this->guessMimeInternal($file);

		$sha1Hash = sha1_file($file);

		$tagging = [
			'mtime'	=> intval(filemtime($file)),
			'sha1'	=> $sha1Hash,
			'size'	=> filesize($file),
			'type'	=> $contentType
		];

		try {
			$result = $this->s3->putObject(
				[
					'ACL'			=> 'public-read',
					'Bucket'		=> $this->bucket['Name'],
					'Key'			=> $s3Key,
					'Body'			=> $fileHandle,
					'ContentLength'	=> filesize($file),
					'ContentType'	=> $contentType,
					'Tagging'		=> http_build_query($tagging)
				]
			);
		} catch (Exception $e) {
			$this->error("Could not store file: " . $e->getMessage());
			return false;
		}
		return true;
	}

	/**
	 * Lock Wiki
	 *
	 * @return void
	 */
	private function lockWiki() {
		global $wgReadOnlyFile;

		if ($this->isFinal) {
			$this->output("Locking the wiki.\n\n");
			file_put_contents($wgReadOnlyFile, "Migrating media to S3 storage.");
		}
	}

	/**
	 * Unlock Wiki
	 *
	 * @return void
	 */
	private function unlockWiki() {
		global $wgReadOnlyFile;

		if ($this->isFinal) {
			$this->output("Unlocking the wiki.\n\n");
			unlink($wgReadOnlyFile);
		}
	}

	/**
	 * @param string      $storagePath
	 * @param string|null $content
	 * @param string|null $fsPath
	 *
	 * @return string
	 * @since  1.27
	 */
	private function guessMimeInternal($storagePath) {
		$magic = MediaWiki\MediaWikiServices::getInstance()->getMimeAnalyzer();
		// Trust the extension of the storage path (caller must validate)
		$ext = FileBackend::extensionFromPath($storagePath);
		$type = $magic->guessTypesForExtension($ext);
		// For files without a valid extension (or one at all), inspect the contents
		if (!$type && $fsPath) {
			$type = $magic->guessMimeType($storagePath, false);
		}
		return $type ?: 'unknown/unknown';
	}
}

$maintClass = "MoveToS3Storage";
require_once RUN_MAINTENANCE_IF_MAIN;
