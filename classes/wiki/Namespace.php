<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Namespaces Class
 *
 * @author    Alexia E. Smith
 * @copyright (c) 2014 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
 **/

namespace DynamicSettings\Wiki;

use DynamicSettings\DSDBFactory;
use DynamicSettings\EditLog;
use RequestContext;

class DSNamespace {
	/**
	 * Wiki Object
	 *
	 * @var object
	 */
	private $wiki;

	/**
	 * Data Container
	 *
	 * @var array
	 */
	private $data = [
		'snid'					=> 0,
		'site_key'				=> null,
		'namespace_name'		=> null,
		'namespace_talk'		=> null,
		'namespace_id'			=> -100000, // Ugh.  These should match the database defaults, but there are serious data conflict concerns with using 0 or the database default.  Negative values are reserved in MediaWiki.
		'namespace_talk_id'		=> -100001,
		'namespace_alias'		=> null,
		'namespace_talk_alias'	=> null,
		'is_content'			=> 0,
		'is_searchable'			=> 0,
		'is_semantic'			=> 0,
		'namespace_protection'	=> null
	];

	/**
	 * Core Mediawiki Namespaces
	 *
	 * @var array
	 */
	public static $protectedNamespaces = ['Media', 'Special', 'User', 'File', 'Project', 'Mediawiki', 'Template', 'Help', 'Category'];

	/**
	 * Core Mediawiki Talk Namespaces
	 *
	 * @var array
	 */
	public static $protectedTalkNamespaces = ['Talk', 'User_talk', 'Project_talk', 'File_talk', 'Mediawiki_talk', 'Template_talk', 'Help_talk', 'Category_talk'];

	/**
	 * Main Constructor
	 *
	 * @param object $wiki
	 *
	 * @return void
	 */
	private function __construct(\DynamicSettings\Wiki $wiki) {
		$this->wiki = $wiki;
	}

	/**
	 * Construct new Namespace objects from a Wiki.
	 *
	 * @param object Wiki
	 *
	 * @return mixed Array of Namespace objects or false for no results.
	 */
	public static function loadFromWiki(\DynamicSettings\Wiki $wiki) {
		$namespaces = false;
		$namespace = new DSNamespace($wiki);

		$db = DSDBFactory::getMasterDB(DB_MASTER);

		$results = $db->select(
			['wiki_namespaces'],
			['*'],
			[
				'site_key' => $wiki->getSiteKey()
			],
			__METHOD__
		);

		while ($row = $results->fetchRow()) {
			$namespace = new DSNamespace($wiki);
			if ($namespace->load($row) === true) {
				$namespaces[$row['snid']] = $namespace;
			}
		}
		return $namespaces;
	}

	/**
	 * Construct a new Namespace object from a name.
	 *
	 * @param string Namespace name.
	 * @param object Wiki
	 *
	 * @return mixed Namespace object from an existing name, a new Namespace object from a new name, or false on error.
	 */
	public static function loadFromName($name, \DynamicSettings\Wiki $wiki) {
		$namespace = new DSNamespace($wiki);

		$db = DSDBFactory::getMasterDB(DB_MASTER);

		$results = $db->select(
			['wiki_namespaces'],
			['*'],
			[
				'site_key'			=> $wiki->getSiteKey(),
				'namespace_name'	=> $name
			],
			__METHOD__
		);

		if (!$namespace->load($results->fetchRow()) && !$namespace->setName($name)) {
			return false;
		}

		if (!$namespace->getId()) {
			$namespace->setId();
			$namespace->setContent(false);
			$namespace->setSearchable(false);
			$namespace->setSemantic(false);
		}

		return $namespace;
	}

	/**
	 * Returns a new Namespace object.
	 *
	 * @param object Wiki
	 *
	 * @return object Fresh Namespace
	 */
	public static function loadFromNew(\DynamicSettings\Wiki $wiki) {
		$namespace = new DSNamespace($wiki);

		// $namespace->setId();
		$namespace->setContent(false);
		$namespace->setSearchable(false);
		$namespace->setSemantic(false);

		return $namespace;
	}

	/**
	 * Load data into a fresh Namespace object.
	 *
	 * @param array Array of information from the database.
	 *
	 * @return boolean Success
	 */
	public function load($row) {
		if (!$row['snid']) {
			return false;
		}

		$this->data = $row;

		return true;
	}

	/**
	 * Save this namespace data.
	 *
	 * @param string [Optional] Commit Message
	 *
	 * @return boolean Success
	 */
	public function save($commitMessage = '') {
		$wgUser = RequestContext::getMain()->getUser();

		$success = false;

		$db = DSDBFactory::getMasterDB(DB_MASTER);

		if (strlen($this->wiki->getSiteKey()) != 32 || !$this->getName() || !$this->getId()) {
			return false;
		}

		$db->startAtomic(__METHOD__);

		$save = [
			'site_key'				=> $this->wiki->getSiteKey(),
			'namespace_name'		=> $this->getName(),
			'namespace_talk'		=> $this->getTalk(),
			'namespace_id'			=> $this->getId(),
			'namespace_talk_id'		=> $this->getTalkId(),
			'namespace_alias'		=> $this->getNameAlias(),
			'namespace_talk_alias'	=> $this->getTalkAlias(),
			'is_content'			=> intval($this->isContent()),
			'is_searchable'			=> intval($this->isSearchable()),
			'is_semantic'			=> intval($this->isSemantic()),
			'namespace_protection'	=> $this->getProtection()
		];

		if ($this->data['snid'] > 0) {
			$result = $db->update(
				'wiki_namespaces',
				$save,
				['snid' => $this->data['snid']],
				__METHOD__
			);

			$databaseId = $this->data['snid'];
		} else {
			$result = $db->insert(
				'wiki_namespaces',
				$save,
				__METHOD__
			);

			$databaseId = $db->insertId();
		}

		if (!$result) {
			$db->cancelAtomic(__METHOD__);
		} else {
			$success = true;
			$db->endAtomic(__METHOD__);

			$this->data['snid'] = $databaseId;

			$miscData = [
				'display_name' => $this->wiki->getNameForDisplay()
			];

			EditLog::addEntry(
				$this->wiki->getSiteKey(), // Log Key: site_key
				$save,
				__METHOD__,
				$wgUser,
				'`' . $this->getName() . '` ' . $commitMessage,
				$miscData
			);
		}

		return $success;
	}

	/**
	 * Delete this namespace.
	 *
	 * @return boolean Success
	 */
	public function delete() {
		$wgUser = RequestContext::getMain()->getUser();

		$success = false;

		if (!$this->getDatabaseId()) {
			return $success;
		}

		$db = DSDBFactory::getMasterDB(DB_MASTER);
		$db->startAtomic(__METHOD__);
		$result = $db->delete(
			'wiki_namespaces',
			['snid' => $this->data['snid']],
			__METHOD__
		);

		if (!$result) {
			$db->cancelAtomic(__METHOD__);
		} else {
			$success = true;
			$db->endAtomic(__METHOD__);

			$miscData = [
				'display_name' => $this->wiki->getNameForDisplay()
			];

			EditLog::addEntry(
				$this->wiki->getSiteKey(), // Log Key: site_key
				[],
				__METHOD__,
				$wgUser,
				'`' . $this->getName() . '` deleted',
				$miscData
			);
		}

		return $success;
	}

	/**
	 * Return the database ID for this namespace.
	 *
	 * @return integer Database ID
	 */
	public function getDatabaseId() {
		return intval($this->data['snid']);
	}

	/**
	 * Get Namespace Name
	 *
	 * @return string Namespace Name
	 */
	public function getName() {
		return $this->data['namespace_name'];
	}

	/**
	 * Set the namespace name and talk.
	 *
	 * @param string Namespace Name
	 *
	 * @return boolean Success
	 */
	public function setName($name) {
		$name = self::replaceSpaces(trim($name));
		if (!$this->isValidName($name) || strpos(strtolower($name), 'talk') !== false) {
			return false;
		}

		$this->data['namespace_name'] = $name;
		return true;
	}

	/**
	 * Get Talk Name
	 *
	 * @return string Namespace Talk
	 */
	public function getTalk() {
		return $this->data['namespace_talk'];
	}

	/**
	 * Set Talk Name
	 *
	 * @param string $talk Talk Name
	 *
	 * @return boolean
	 */
	public function setTalk($talk) {
		$talk = self::replaceSpaces(trim($talk));
		if (!$this->isValidName($talk)) {
			return false;
		}

		$this->data['namespace_talk'] = $talk;
		return true;
	}

	/**
	 * Get Namespace Alias
	 *
	 * @return string
	 */
	public function getNameAlias() {
		return $this->data['namespace_alias'];
	}

	/**
	 * Set Namespace Alias
	 *
	 * @param mixed $alias String Namespace Alias or null to unset.
	 *
	 * @return boolean
	 */
	public function setNameAlias($alias) {
		if ($alias !== null) {
			$alias = self::replaceSpaces(trim($alias));
			if (!$this->isValidName($alias) || strpos(strtolower($alias), 'talk') !== false || $alias == $this->getName()) {
				return false;
			}
		}

		$this->data['namespace_alias'] = $alias;
		return true;
	}

	/**
	 * Get Namespace Talk Alias
	 *
	 * @return string
	 */
	public function getTalkAlias() {
		return $this->data['namespace_talk_alias'];
	}

	/**
	 * Set Namespace Talk Alias
	 *
	 * @param mixed $alias String Namespace Talk Alias or null to unset.
	 *
	 * @return boolean
	 */
	public function setTalkAlias($alias) {
		if ($alias !== null) {
			$alias = self::replaceSpaces(trim($alias));
			if (!$this->isValidName($alias) || $alias == $this->getTalk()) {
				return false;
			}
		}

		$this->data['namespace_talk_alias'] = $alias;
		return true;
	}

	/**
	 * Get the namespace ID.
	 *
	 * @return integer Namespace ID
	 */
	public function getId() {
		return intval($this->data['namespace_id']);
	}

	/**
	 * Set the namespace ID.
	 *
	 * @param integer [Optional] Namespace ID
	 *
	 * @return mixed Boolean success or string error message.
	 */
	public function setId($namespaceID = 0) {
		$db = DSDBFactory::getMasterDB(DB_MASTER);

		$namespaceID = intval($namespaceID);
		if ($namespaceID > 0 && $this->data['namespace_id'] >= 100 && $namespaceID == $this->data['namespace_id']) {
			return true;
		}

		if (!isset($this->data['snid']) || !$this->data['snid'] || $this->data['namespace_id'] < 100) {
			if ($namespaceID > 0) {
				if ($namespaceID % 2 || $namespaceID < 100) {
					return wfMessage('error_namespace_id_uneven')->escaped();
				} else {
					$result = $db->select(
						['wiki_namespaces'],
						['snid'],
						['site_key' => $this->wiki->getSiteKey(), 'namespace_id' => $namespaceID],
						__METHOD__,
						[
							'LIMIT'		=> 1
						]
					);

					$row = $result->fetchRow();
					if (isset($row['snid'])) {
						return wfMessage('error_namespace_id_taken')->escaped();
					} else {
						$this->data['namespace_id']			= $namespaceID;
						$this->data['namespace_talk_id']	= $namespaceID + 1;
					}
				}
			} else {
				// Need to get the last highest internal ID and increment.
				$result = $db->select(
					['wiki_namespaces'],
					['MAX(namespace_id) as max_id, MAX(namespace_talk_id) as max_talk_id'],
					['site_key' => $this->wiki->getSiteKey()],
					__METHOD__,
					[
						'LIMIT'		=> 1
					]
				);

				$row = $result->fetchRow();

				$lastID = max($row['max_id'], $row['max_talk_id']);

				if ($this->data['namespace_id'] < 1) {
					$this->data['namespace_id'] = $lastID + 1;
					if ($this->data['namespace_id'] < 10000) {
						// This piece of code should only occur once per wiki ever.
						$this->data['namespace_id'] = 10000;
					}

					$this->data['namespace_talk_id'] = $lastID + 2;
					if ($this->data['namespace_talk_id'] < 10001) {
						// This piece of code should only occur once per wiki ever.
						$this->data['namespace_talk_id'] = 10001;
					}
				}
			}
		} else {
			return false;
		}
		return true;
	}

	/**
	 * Get the namespace talk ID.
	 *
	 * @return integer Namespace Talk ID
	 */
	public function getTalkId() {
		return intval($this->data['namespace_talk_id']);
	}

	/**
	 * Is this namespace content?
	 *
	 * @return boolean Content
	 */
	public function isContent() {
		return boolval($this->data['is_content']);
	}

	/**
	 * Set Content
	 *
	 * @param boolean Content
	 *
	 * @return void
	 */
	public function setContent($content = true) {
		$this->data['is_content'] = intval($content);
	}

	/**
	 * Is this namespace searchable?
	 *
	 * @return boolean Searchable
	 */
	public function isSearchable() {
		return boolval($this->data['is_searchable']);
	}

	/**
	 * Set Searchable
	 *
	 * @param boolean Searchable
	 *
	 * @return void
	 */
	public function setSearchable($searchable = true) {
		$this->data['is_searchable'] = intval($searchable);
	}

	/**
	 * Is this namespace semantic?
	 *
	 * @return boolean Semantic
	 */
	public function isSemantic() {
		return boolval($this->data['is_semantic']);
	}

	/**
	 * Set Semantic
	 *
	 * @param boolean Semantic
	 *
	 * @return void
	 */
	public function setSemantic($semantic = true) {
		$this->data['is_semantic'] = intval($semantic);
	}

	/**
	 * Get the protection permission.
	 *
	 * @return string Permission String
	 */
	public function getProtection() {
		return $this->data['namespace_protection'];
	}

	/**
	 * Set the protection permission.
	 *
	 * @return boolean Success
	 */
	public function setProtection($permission) {
		$permission = trim($permission);
		if (strpos($permission, ' ') !== false) {
			return false;
		}
		$this->data['namespace_protection'] = $permission;

		return true;
	}

	/**
	 * Create a valid constant name from any text.
	 *
	 * @param string Any text
	 *
	 * @return mixed Valid Constant Name, false on error.
	 */
	public static function createConstantName($name) {
		if (empty($name)) {
			return false;
		}
		$characters = str_split($name);
		if (!preg_match("#[a-zA-Z_\\x7f-\\xff]#", $characters[0])) {
			unset($characters[0]);
		}
		foreach ($characters as $index => $character) {
			if (!preg_match("#[a-zA-Z0-9_\\x7f-\\xff]#", $character)) {
				unset($characters[$index]);
			}
		}
		$name = mb_strtoupper(implode($characters), "UTF-8");
		if (empty($name)) {
			return false;
		}
		return "NS_" . $name;
	}

	/**
	 * Replace spaces since Namespaces and Aliases can not use them.
	 *
	 * @param string $namespace
	 *
	 * @return string
	 */
	private function replaceSpaces($namespace) {
		return str_replace(" ", "_", $namespace);
	}

	/**
	 * Return if a namespace does not contain invalid characters or collides with built in protected namespaces.
	 *
	 * @param string Name
	 *
	 * @return boolean Is Valid Name
	 */
	private function isValidName($name) {
		$protectedNamespaces = array_map('strtolower', self::$protectedNamespaces);
		$protectedTalkNamespaces = array_map('strtolower', self::$protectedTalkNamespaces);
		$nameLower = mb_strtolower($name, 'UTF-8');

		return !in_array($nameLower, $protectedNamespaces) && !in_array($nameLower, $protectedTalkNamespaces) && !empty($name) && preg_match('#[ :]+#is', $name) < 1 && strlen($name) <= 255;
	}
}
