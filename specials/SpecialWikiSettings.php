<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Wiki Settings Special Page
 *
 * @author    Alex Smith
 * @copyright (c) 2014 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
**/

use DynamicSettings\Lock;

class SpecialWikiSettings extends DynamicSettings\SpecialPage {
	/**
	 * Output HTML
	 *
	 * @var string
	 */
	private $content;

	/**
	 * Wiki object storage for form editing.
	 *
	 * @var array
	 */
	private $wiki;

	/**
	 * Main Constructor
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct('WikiSettings', 'wiki_settings', false);
	}

	/**
	 * Main Executor
	 *
	 * @param string Sub page passed in the URL.
	 *
	 * @return void [Outputs to screen]
	 */
	public function execute($subpage) {
		$this->checkPermissions();

		if (Lock::isLocked() == true) {
			$this->output->showErrorPage('dynamic_settings_error', 'error_settings_locked');
			return;
		}

		$this->siteSettings = new \DynamicSettings\Sites();

		$this->templates = new TemplateWikiSettings();

		$this->output->addModuleStyles(['ext.wikiSettings.styles']);
		$this->output->addModules(['ext.wikiSettings.scripts']);

		$siteKey = $this->wgRequest->getVal('siteKey');
		if (empty($siteKey) && !empty($subpage)) {
			$siteKey = $subpage;
		}

		if (Lock::isLocked($siteKey) == true) {
			$this->output->showErrorPage('dynamic_settings_error', 'error_wiki_locked');
			return;
		}

		$this->setHeaders();

		$this->wiki = \DynamicSettings\Wiki::loadFromHash($siteKey);

		if ($this->wiki === false) {
			$this->output->showErrorPage('site_settings_error', 'wiki_not_found');
			return;
		}

		if ($this->wiki->isDeleted()) {
			$this->output->showErrorPage('site_settings_error', 'deleted_wikis_no_modifications');
			return;
		}

		$this->settingsForm();

		$this->output->addHTML($this->content);
	}

	/**
	 * Wiki Settings Form
	 *
	 * @return void [Outputs to screen]
	 */
	public function settingsForm() {
		$errors = $this->settingsSave();

		$this->output->setPageTitle(wfMessage('site_settings') . ' - ' . $this->wiki->getNameForDisplay());
		$this->content = $this->templates->settingsForm($this->wiki, $errors);
	}

	/**
	 * Saves submitted Wiki Settings Forms.
	 *
	 * @return array Array of errors.
	 */
	private function settingsSave() {
		$errors = [];
		if ($this->wgRequest->getVal('do') == 'save' && $this->wgRequest->wasPosted()) {
			$settings = $this->wiki->getSettings();
			$keys = $this->wgRequest->getValueNames();
			foreach ($keys as $key) {
				$asid = substr($key, 4);
				if (strpos($key, "wsf_") !== 0 || !is_numeric($asid)) {
					continue;
				}

				$return = $settings[$asid]->setValueFromForm(str_replace(["\r\n", "\r"], ["\n", "\n"], $this->wgRequest->getVal($key))); // Replace \r\n with \n and any remaining \r with just \n.
				if (!is_bool($return)) {
					$errors[$asid] = $return;
				}
			}

			$commitMessage = trim($this->wgRequest->getText('commit_message'));
			if (!$commitMessage) {
				$errors['commit_message'] = wfMessage('error_no_commit_message');
			}

			if (!count($errors)) {
				if ($this->wiki->saveSettings($settings, $commitMessage)) {
					$page = Title::newFromText('Special:WikiSites');
					$this->output->redirect($page->getFullURL());
				} else {
					$errors['commit_message'] = wfMessage('error_failed_save_settings');
				}
			}
		}
		return $errors;
	}
}
