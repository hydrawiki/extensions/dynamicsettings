<?php
/**
 * Curse Inc.
 * Dynamic Settings
 *
 * @author    Alex Smith
 * @copyright (c) 2014 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
 **/

namespace DynamicSettings;

use ConfigFactory;
use MediaWiki\MediaWikiServices;

class DSDBFactory {
	/**
	 * Cached database connections.
	 *
	 * @var array
	 */
	private static $dbs = [];

	/**
	 * Cached master database load balancer.
	 *
	 * @var object
	 */
	private static $lb = null;

	/**
	 * Get a database connection for the master wiki database.
	 *
	 * @param integer $index Connection index such as DB_MASTER or DB_REPLICA.
	 *
	 * @return object Database
	 */
	public static function getMasterDB($index = DB_MASTER) {
		if (isset(self::$dbs[$index]) && self::$dbs[$index] !== null) {
			return self::$dbs[$index];
		}
		$config = ConfigFactory::getDefaultInstance()->makeConfig('main');
		self::$dbs[$index] = self::getMasterDBLB()->getConnection($index, false, $config->get('DSMasterDB'));
		return self::$dbs[$index];
	}

	/**
	 * Get a load balancer for the master wiki database.
	 *
	 * @return object LoadBalancer
	 */
	public static function getMasterDBLB() {
		if (self::$lb !== null) {
			return self::$lb;
		}
		if (!Environment::isMasterWiki() && !Environment::isPretending()) {
			$config = ConfigFactory::getDefaultInstance()->makeConfig('main');
			$lbFactory = MediaWikiServices::getInstance()->getDBLoadBalancerFactory();
			return $lbFactory->getExternalLB($config->get('DSMasterDBLB'));
		}
		self::$lb = MediaWikiServices::getInstance()->getDBLoadBalancer();
		return self::$lb;
	}
}
