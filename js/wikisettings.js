function WikiSettings($) {
	'use strict';
	this.init = function() {
		$("input[type='hidden'].array_builder_input").each(function() {
			settings.initJsonEditor(this);
		});

		if (window.location.href.indexOf('Special:WikiAllowedSettings') > -1) {
			$('#allowed_extension_md5_key').on('change', settings.onChangeBelongsToExtension);
			$('#setting_group_name').on('change', settings.onChangeGroupName);
			$('#setting_type').on('change', settings.onChangeSettingType);
			$('#setting_type').change();
		}
	};

	var settings = {
		onChangeBelongsToExtension: function() {
			var newVal = $(this).val();
			var newText = $("#allowed_extension_md5_key option:selected").text();
			if (newVal != '0') {
				$('#setting_group_name').val(newText);
			}
			$('#setting_group_name').change();
		},

		onChangeGroupName: function() {
			var newVal = $('#setting_group_name').val();
			var oldText = $("#allowed_extension_md5_key option:selected").text();
			if (newVal !== '' && newVal !== oldText) {
				$('#allowed_extension_md5_key').val('0');
			}
			$('#allowed_extension_md5_key').change();
		},

		onChangeSettingType: function() {
			var parent = $('#setting_default');
			$('#setting_default > *').addClass('hidden');

			var newVal = $(this).val();

			if (newVal == 'array') {
				$('div.jsonEditor', parent).removeClass('hidden');
			} else if (newVal == 'boolean') {
				$('span.yesno', parent).removeClass('hidden');
			} else {
				$('*[name="setting_default_'+newVal+'"]', parent).removeClass('hidden');
			}
		},

		initJsonEditor: function(input) {
			var div = document.createElement('div'),
				options = {
					mode: 'tree',
					modes: ['tree', 'text'],
					name: $(input).parent().children('label').text(),
					change: function () {
						$(input).val(editor.getText());
					}
				},
				data, editor;
			try {
				data = JSON.parse(input.value);
			} catch (SyntaxError) {
				data = {};
			}
			editor = new JSONEditor(div, options, data);
			div.className = 'jsonEditor';
			$(input).parent().append(div);
			return div;
		}
	};
}

var WS = new WikiSettings(jQuery);
jQuery(document).ready(WS.init);