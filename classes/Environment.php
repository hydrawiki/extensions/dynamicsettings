<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Environment Detection
 *
 * @author    Alex Smith
 * @copyright (c) 2014 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
 **/

namespace DynamicSettings;

class Environment {
	/**
	 * Are we pretending to be a child wiki right now?
	 * Used by the updater.
	 *
	 * @var boolean
	 */
	private static $pretendChild = false;

	/**
	 * Detect the current checkout and return it.
	 *
	 * @return string Checkout
	 */
	public static function detectCheckout() {
		if (isset($_SERVER['HYDRA_VERSION'])) {
			// Already handled?  Return it.
			return $_SERVER['HYDRA_VERSION'];
		}

		$config = \ConfigFactory::getDefaultInstance()->makeConfig('main');
		$hydraCheckouts = $config->get('DSHydraCheckouts');

		if (is_array($hydraCheckouts)) {
			$checkout = reset($hydraCheckouts);
			if (!empty($checkout)) {
				// Return the first(default) environment in the array as a fall back.
				$_SERVER['HYDRA_VERSION'] = $checkout;
				return $checkout;
			}
		}

		// Otherwise, return the most default default.
		$_SERVER['HYDRA_VERSION'] = "stable";
		return "stable";
	}

	/**
	 * Switch which checkout a wiki is running under.
	 *
	 * @param string $domain   Domain Name
	 * @param string $checkout [Optional] Checkout to switch to.  Omit this to default back to stable.
	 *
	 * @return boolean Successful Switch
	 */
	public static function switchCheckout($domain, $checkout = 'stable') {
		$success = false;

		$config = \ConfigFactory::getDefaultInstance()->makeConfig('main');
		$hydraCheckouts = $config->get('DSHydraCheckouts');
		if (!in_array($checkout, $hydraCheckouts)) {
			throw new \MWException(__METHOD__ . " - Unknown checkout '{$checkout}'.  Valid checkouts: " . implode(', ', $hydraCheckouts));
		}

		$redis = \RedisCache::getClient('checkout');
		if ($redis !== false) {
			// "global:DynamicSettings:environment:{$domain}" = $checkout
			$success = $redis->set("global:DynamicSettings:environment:{$domain}", $checkout);
		}
		return $success;
	}

	/**
	 * Get which checkout a wiki is running under.
	 *
	 * @param string $domain Domain Name
	 *
	 * @return boolean Successful Switch
	 */
	public static function getCheckoutFor($domain) {
		$success = false;

		$config = \ConfigFactory::getDefaultInstance()->makeConfig('main');
		$hydraCheckouts = $config->get('DSHydraCheckouts');

		$redis = \RedisCache::getClient('checkout');
		$checkout = self::detectCheckout();
		if ($redis !== false) {
			$checkout = $redis->get("global:DynamicSettings:environment:{$domain}");
		}

		if (!in_array($checkout, $hydraCheckouts)) {
			$checkout = reset($hydraCheckouts);
		}

		return $checkout;
	}

	/**
	 * Detect the current environment and return it.
	 *
	 * @return string Environment
	 */
	public static function detectEnvironment() {
		if (isset($_SERVER['PHP_ENV']) && !empty($_SERVER['PHP_ENV'])) {
			// Already handled?  Return it.
			return $_SERVER['PHP_ENV'];
		}

		$config = \ConfigFactory::getDefaultInstance()->makeConfig('main');
		$hydraEnvironments = $config->get('DSHydraEnvironments');

		if (is_array($hydraEnvironments)) {
			$env = key($hydraEnvironments);
			if (!empty($env)) {
				// Return the first(default) environment in the array as a fall back.
				$_SERVER['PHP_ENV'] = $env;
				return $env;
			}
		}

		// Otherwise, return the most default default.
		$_SERVER['PHP_ENV'] = "production";
		return "production";
	}

	/**
	 * Are we on the master wiki?
	 *
	 * @return boolean Defaults to false for safety reasons.
	 */
	public static function isMasterWiki() {
		if (self::$pretendChild) {
			return false;
		}

		if (defined('MASTER_WIKI') && is_bool(MASTER_WIKI)) {
			return MASTER_WIKI;
		}
		return false;
	}

	/**
	 * Switch pretend mode on or off.
	 *
	 * @param boolean $pretend [Optional] Should we play pretend?
	 *
	 * @return void
	 */
	public static function pretendIsChild(bool $pretend = false) {
		self::$pretendChild = $pretend;
	}

	/**
	 * Are we pretending to be a child?
	 *
	 * @return boolean
	 */
	public static function isPretending() {
		return self::$pretendChild;
	}

	/**
	 * Return the site key for the current wiki environment.
	 *
	 * @access public
	 * @return string
	 */
	public static function getSiteKey() {
		global $dsSiteKey;
		return (string)$dsSiteKey;
	}
}
