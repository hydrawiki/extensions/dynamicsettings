<?php
/**
 * DynamicSettings
 * Wiki API
 *
 * @author    Alexia E. Smith
 * @copyright (c) 2018 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   DynamicSettings
 * @link      https://gitlab.com/hydrawiki
 **/

namespace DynamicSettings\Api;

use DynamicSettings\DSDBFactory;

class PromotionApi extends \ApiBase {
	/**
	 * Main Executor
	 *
	 * @return void [Outputs to screen]
	 */
	public function execute() {
		if (!Environment::isMasterWiki()) {
			$this->dieWithError(['apierror-permissiondenied-generic']);
			return;
		}

		$this->params = $this->extractRequestParams();

		if (!$this->getUser()->isLoggedIn()) {
			$this->dieWithError(['invaliduser', $this->params['do']]);
		}

		switch ($this->params['do']) {
			case 'updateWeight':
				$response = $this->updateWeight();
				break;
			default:
				$this->dieWithError(['invaliddo', $this->params['do']]);
				break;
		}

		foreach ($response as $key => $value) {
			$this->getResult()->addValue(null, $key, $value);
		}
	}

	/**
	 * Requirements for API call parameters.
	 *
	 * @return array Merged array of parameter requirements.
	 */
	public function getAllowedParams() {
		return [
			'do' => [
				\ApiBase::PARAM_TYPE		=> 'string',
				\ApiBase::PARAM_REQUIRED => true
			],
			'weight' => [
				\ApiBase::PARAM_TYPE		=> 'string',
				\ApiBase::PARAM_REQUIRED => false
			]
		];
	}

	/**
	 * Descriptions for API call parameters.
	 *
	 * @return array Merged array of parameter descriptions.
	 */
	public function getParamDescription() {
		return [
			'do'		=> 'Action to take.',
			'weight'	=> 'Array of promotion IDs to weight to reorder promotion display order.'
		];
	}

	/**
	 * Merge API errors into the mediawiki error array.
	 *
	 * @return array
	 */
	public function getPossibleErrors() {
		return array_merge(
			parent::getPossibleErrors(),
			[
				[
					'code' => 'NoPermission',
					'info' => 'You do not have permission for this action.'
				],
				[
					'code' => 'BadWeightData',
					'info' => 'Bad weight data was passed to update weights.'
				]
			]
		);
	}

	/**
	 * Update the weight(sorting) of promotions.
	 *
	 * @return array API result.
	 */
	public function updateWeight() {
		if (!$this->getUser()->isAllowed('wiki_promotions') || !$this->getRequest()->wasPosted()) {
			return ['success' => false, 'error' => 'NoPermission'];
		}

		$weight = $this->getRequest()->getVal('weight');
		$weights = json_decode($weight, true);
		if (!is_array($weights) || !count($weights)) {
			return ['success' => false, 'error' => 'BadWeightData'];
		}

		$success = true;
		$db = DSDBFactory::getMasterDB(DB_MASTER);
		$db->startAtomic(__METHOD__);
		foreach ($weights as $pid => $weight) {
			if (!intval($pid) || !intval($weight)) {
				$success = false;
				continue;
			}
			$db->update(
				'wiki_promotions',
				['weight' => $weight],
				['pid' => $pid],
				__METHOD__
			);
		}
		$db->endAtomic(__METHOD__);

		return ['success' => $success];
	}
}
