<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * GlobalPageEdit Class
 *
 * @author    Alexia E. Smith
 * @copyright (c) 2015 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
 **/

namespace DynamicSettings;

class GlobalPageEdit {
	/**
	 * Title Object(s)
	 *
	 * @var array
	 */
	private $titles = [];

	/**
	 * Wiki Object(s)
	 *
	 * @var array
	 */
	private $wikis = [];

	/**
	 * Associated Wikis and Titles.
	 *
	 * @var array
	 */
	private $pageList = [];

	/**
	 * Current Wiki being worked on.
	 *
	 * @var object
	 */
	private $currentWiki = false;

	/**
	 * Current Title being worked on.
	 *
	 * @var object
	 */
	private $currentTitle = false;

	/**
	 * Current Page
	 *
	 * @var array
	 */
	private $currentPage = [];

	/**
	 * Find and Replace Identifiers
	 *
	 * @var array
	 */
	private static $frIdentifiers = [];

	/**
	 * Create new pages?
	 *
	 * @var boolean
	 */
	private static $createNewPages = false;

	/**
	 * Content for new pages if we are creating them
	 *
	 * @var string
	 */
	private static $newPageContent = "";

	/**
	 * Number of times to retry on failure.
	 *
	 * @var integer
	 */
	private static $maxEditRetry = 5;

	/**
	 * No more pages to select and process.
	 *
	 * @var integer
	 */
	const NO_MORE_PAGES = 1000;

	/**
	 * No more wikis to select and process.
	 *
	 * @var integer
	 */
	const NO_MORE_WIKIS = 1001;

	/**
	 * Page not found on the wiki.
	 *
	 * @var integer
	 */
	const PAGE_NOT_FOUND = 1003;

	/**
	 * Unknown page error.
	 *
	 * @var integer
	 */
	const UNKNOWN_PAGE_ERROR = 1004;

	/**
	 * Unable to login.
	 *
	 * @var integer
	 */
	const UNABLE_TO_LOGIN = 1005;

	/**
	 * Unknown page edit error.
	 *
	 * @var integer
	 */
	const UNKNOWN_EDIT_ERROR = 1006;

	/**
	 * Could not obtain an edit token.
	 *
	 * @var integer
	 */
	const COULD_NOT_OBTAIN_EDIT_TOKEN = 1007;

	/**
	 * WTF Wiki
	 *
	 * @var integer
	 */
	const BAD_WIKI = 1008;

	/**
	 * Bad Operation
	 *
	 * @var integer
	 */
	const BAD_OP = 1009;

	/**
	 * Main Constructor
	 *
	 * @return void
	 */
	public function __construct() {
		$this->jar = "";
		$this->action = false;
	}

	/**
	 * Set the title(s) being used for this editor.
	 *
	 * @param mixed $titles Array of Title objects or a single Title object.
	 *
	 * @return void
	 */
	public function addTitle($titles) {
		if (!is_array($titles)) {
			$titles = [$titles];
		}

		foreach ($titles as $title) {
			if (!empty($title)) {
				if (!in_array($title, $this->titles)) {
					$this->titles[] = $title;
				}
			} else {
				throw new \MWException("Empty title passed to " . __METHOD__ . ".");
			}
		}
		$this->updatePageList();
	}

	/**
	 * Return all Title objects.
	 *
	 * @return array Title objects.
	 */
	public function getTitles() {
		return $this->titles;
	}

	/**
	 * Return if valid Title objects are loaded.
	 *
	 * @return boolean Has valid Title objects.
	 */
	public function hasTitles() {
		return count($this->titles) > 0;
	}

	/**
	 * Set the wikis being queried for pages and editing.
	 *
	 * @param mixed $wikis Array of Wiki objects or a single Wiki object.
	 *
	 * @return void
	 */
	public function setWikis($wikis) {
		$wikis = (array)$wikis;

		foreach ($wikis as $wiki) {
			if ($wiki instanceof Wiki) {
				$this->wikis[$wiki->getSiteKey()] = $wiki;
			} else {
				throw new \MWException("Non-Wiki object passed to " . __METHOD__ . ".");
			}
		}
		$this->updatePageList();
	}

	/**
	 * Updates the list of Wiki to Title associations.
	 *
	 * @return void
	 */
	private function updatePageList() {
		$this->pageList = [];
		foreach ($this->wikis as $wiki) {
			foreach ($this->titles as $title) {
				$this->pageList[] = ['site_key' => $wiki->getSiteKey(), 'title' => $title];
			}
		}
		reset($this->pageList);
	}

	/**
	 * Return the current wiki being worked on.
	 *
	 * @return mixed Wiki object or false if not working on a wiki.
	 */
	public function getCurrentWiki() {
		return $this->currentWiki;
	}

	/**
	 * Return the current Title being worked on.
	 *
	 * @return mixed Title object or false if not working on a title.
	 */
	public function getCurrentTitle() {
		return $this->currentTitle;
	}

	/**
	 * Iterate over wikis and titles found on them, returning the next one found incrementally.
	 *
	 * @return mixed Raw Page Content or status constant.
	 */
	public function getNextPage() {
		$pageKeys = current($this->pageList);
		next($this->pageList);
		if ($pageKeys === false) {
			return self::NO_MORE_PAGES;
		}
		$this->currentWiki = $this->wikis[$pageKeys['site_key']];
		$this->currentTitle = $pageKeys['title'];

		$apiUrl = $this->createApiUrl($this->currentWiki);

		 // We put the titles in the middle because Mediawiki's IE6 URL fixer breaks Mediawiki!
		$apiUrl .= "?action=query&prop=revisions&rvprop=content&titles=" . urlencode($this->currentTitle) . "&format=json&formatversion=2";
		$result = \HTTP::get($apiUrl);

		$data = @json_decode($result, true);

		if (empty($data)) {
			$data = self::UNKNOWN_PAGE_ERROR;
		}

		if (isset($data['query']['pages'])) {
			$page = current($data['query']['pages']);
		}

		if (!isset($page['pageid']) || $page['pageid'] < 1) {
			$this->currentPage = null;
			return self::PAGE_NOT_FOUND;
		}

		$this->currentPage['page'] = $page;

		if (isset($this->currentPage['page']['revisions'][0]['content'])) {
			return strval($this->currentPage['page']['revisions'][0]['content']);
		}
		return self::UNKNOWN_PAGE_ERROR;
	}

	/**
	 * Get a login tokin
	 *
	 * @return object
	 */
	public function getLoginToken() {
		global $wgDSGlobalPageEditorAccount;

		$botUsername = $wgDSGlobalPageEditorAccount['user'];
		$botPassword = $wgDSGlobalPageEditorAccount['password'];

		$apiUrl = $this->createApiUrl($this->currentWiki);

		$tokenUrl = $apiUrl . "?action=query&meta=tokens&type=login&format=json&formatversion=2";
		$tokenOptions = [
			'method'			=> 'GET',
			'timeout'			=> 'default',
			'connectTimeout'	=> 'default',
			'postData'			=> [
				'action'		=> 'query',
				'meta'			=> 'tokens',
				'type'			=> 'login',
				'format'		=> 'json'
			]
		];

		$loginUrl = $apiUrl . "?action=login&format=json&formatversion=2";
		$loginOptions = [
			'method'			=> 'POST',
			'timeout'			=> 'default',
			'connectTimeout'	=> 'default',
			'postData'			=> [
				'action'		=> 'login',
				'format'		=> 'json',
				'lgname'		=> $botUsername,
				'lgpassword'	=> $botPassword
			]
		];

		self::d('Token Call', $tokenUrl, $tokenOptions);

		$req = \MWHttpRequest::factory($tokenUrl, $tokenOptions);
		$status = $req->execute();

		$login = false;
		if ($status->isOK()) {
			$login = $req->getContent();
			self::d($login);
			$this->jar = $req->getCookieJar();
			if (!empty($login)) {
				$login = @json_decode($login, true);
				if (is_array($login)) {
					if (isset($login['query']['tokens']['logintoken'])) {
						$loginOptions['postData']['lgtoken'] = $login['query']['tokens']['logintoken'];
					}
					self::d('Login Call', $loginUrl, $loginOptions);
					$req = \MWHttpRequest::factory($loginUrl, $loginOptions);
					$req->setCookieJar($this->jar);
					$status = $req->execute();
					if ($status->isOK()) {
						$login = $req->getContent();
						self::d($login);
						$this->jar = $req->getCookieJar();
						if (!empty($login)) {
							$login = @json_decode($login, true);
						}
					}
				}
			}
		}

		return $login;
	}

	/**
	 * Update the raw page text for the current page.
	 *
	 * @param string  $text  Raw Wiki Text
	 * @param integer $retry Number of current try attempt
	 *
	 * @return mixed Result data from the edit or an error code.
	 */
	public function updateCurrentPage($text, $retry = 0) {
		$retryCount = $retry + 1;

		$wiki = $this->currentWiki;
		if (!$wiki instanceof Wiki) {
			return self::BAD_WIKI;
		}

		$login = $this->getLoginToken();

		if (empty($login) || $login['login']['result'] != 'Success') {
			if ($retryCount <= self::$maxEditRetry) {
				return $this->updateCurrentPage($text, $retryCount);
			}
			return self::UNABLE_TO_LOGIN;
		}

		$apiUrl = $this->createApiUrl($this->currentWiki);

		$editTokenUrl = $apiUrl . "?action=query&meta=tokens&format=json&formatversion=2";
		$req = \MWHttpRequest::factory($editTokenUrl, ['method' => 'GET']);
		$req->setCookieJar($this->jar);
		$status = $req->execute();

		self::d('Edit Token Call', $editTokenUrl);
		$editToken = false;
		if ($status->isOK()) {
			$result = $req->getContent();
			self::d($result);
			$this->jar = $req->getCookieJar();
			$result = @json_decode($result, true);
			if (isset($result['query']['tokens']['csrftoken']) > 0) {
				$editToken = $result['query']['tokens']['csrftoken'];
			}
			if ($editToken === false) {
				if ($retryCount <= self::$maxEditRetry) {
					return $this->updateCurrentPage($text, $retryCount);
				}
				return self::COULD_NOT_OBTAIN_EDIT_TOKEN;
			}
		} else {
			if ($retryCount <= self::$maxEditRetry) {
				return $this->updateCurrentPage($text, $retryCount);
			}
			return self::COULD_NOT_OBTAIN_EDIT_TOKEN;
		}

		$title = $this->currentPage['page']['title'] ? $this->currentPage['page']['title'] : $this->currentTitle;

		$editURL = $apiUrl . '?action=edit&format=json&formatversion=2';
		$editOptions = [
			'method'			=> 'POST',
			'timeout'			=> 'default',
			'connectTimeout'	=> 'default',
			'postData'			=> [
				'title'		=> $title,
				'summary'	=> "GPE Bot Edit" . (count(self::$frIdentifiers) ? " (" . implode(',', self::$frIdentifiers) . ")" : null),
				'bot'		=> 1,
				'text'		=> $text,
				'token'		=> $editToken
			]
		];

		self::d('Edit Call', $editURL, $editOptions);
		$req = \MWHttpRequest::factory($editURL, $editOptions);
		$req->setCookieJar($this->jar);
		$status = $req->execute();

		if ($status->isOK()) {
			$result = $req->getContent();
			self::d($result);
			$this->jar = $req->getCookieJar();
			$result = @json_decode(trim($result), true);
			if (isset($result['edit']['result'])) {
				return $result['edit'];
			}
		}

		if ($retryCount <= self::$maxEditRetry) {
			return $this->updateCurrentPage($text, $retryCount);
		}
		return self::UNKNOWN_EDIT_ERROR;
	}

	/**
	 * Perform an alternative, non find-and-replace api action.
	 *
	 * @param string $actionURL
	 * @param array  $actionOptions
	 *
	 * @return void
	 */
	public function altActionCall($actionURL, $actionOptions) {
		$req = \MWHttpRequest::factory($actionURL, $actionOptions);
		$req->setCookieJar($this->jar);
		$status = $req->execute();

		if ($status->isOK()) {
			$result = $req->getContent();
			self::d($result);
			$this->jar = $req->getCookieJar();
			return @json_decode(trim($result), true);
		} else {
			self::d('Failure', $actionURL, $actionOptions);
			return false;
		}
	}

	/**
	 * Perform Other Actions on Pages
	 *
	 * @param array   $operation Operation object to use
	 * @param integer $retry     Number of current try attempt
	 *
	 * @return mixed Result data from the edit or an error code.
	 */
	public function doAlternativeAction($operation, $retry = 0) {
		$retryCount = $retry + 1;

		$wiki = $this->currentWiki;
		if (!$wiki instanceof Wiki) {
			return self::BAD_WIKI;
		}

		$action = $operation['gpe_action'];

		if (!$action) {
			// bad programmers crutch of a fix, maybe.
			$action = $this->action;
		}

		if (!$action) {
			self::d('ACTION NOT FOUND???', $operation);
			$return = [];
			if (is_array($operation['titles'])) {
				foreach ($operation['titles'] as $title) {
					$return[$title] = self::BAD_OP;
				}
			} else {
				$return = self::BAD_OP;
			}
			return $return;
		} else {
			// save for future. They are all the same.
			$this->action = $action;
		}

		$login = $this->getLoginToken();

		if (empty($login) || $login['login']['result'] != 'Success') {
			if ($retryCount <= self::$maxEditRetry) {
				return $this->doAlternativeAction($operations, $retryCount);
			}
			$return = [];
			foreach ($operation['titles'] as $title) {
				$return[$title] = self::UNABLE_TO_LOGIN;
			}
			return $return;
		}

		/*
		 * 	FIRST THINGS FIRST.
		 *  LETS GET A TOKEN SO WE CAN DO THE API ACTION WE ARE ATTEMPTING.
		 */

		switch ($action) {
			case "page_rollback":
				// https://www.mediawiki.org/wiki/API:Rollback#Rolling_back_pages
				$actionTokenType = "rollback";
				$actionTokenName = "rollbacktoken";
			break;
			case "page_move":
			case "page_protect":
			case "page_delete":
				// https://www.mediawiki.org/wiki/API:Move
				// https://www.mediawiki.org/wiki/API:Protect
				// https://www.mediawiki.org/wiki/API:Delete
				$actionTokenType = "csrf";
				$actionTokenName = "csrftoken";
			break;
		}

		$apiUrl = $this->createApiUrl($wiki);

		$actionTokenUrl = $apiUrl . "?action=query&meta=tokens&format=json&type=" . $actionTokenType . "&formatversion=2";

		$req = \MWHttpRequest::factory($actionTokenUrl, ['method' => 'GET']);
		$req->setCookieJar($this->jar);
		$status = $req->execute();

		self::d('Action (' . $action . ') Token Call', $actionTokenUrl);
		$actionToken = false;
		if ($status->isOK()) {
			$result = $req->getContent();
			self::d($result);
			$this->jar = $req->getCookieJar();
			$result = @json_decode($result, true);
			if (isset($result['query']['tokens'][$actionTokenName]) > 0) {
				$actionToken = $result['query']['tokens'][$actionTokenName];
			}
			if ($actionToken === false) {
				if ($retryCount <= self::$maxEditRetry) {
					return $this->doAlternativeAction($operations, $retryCount);
				}
				$return = [];
				foreach ($operation['titles'] as $title) {
					$return[$title] = self::COULD_NOT_OBTAIN_EDIT_TOKEN;
				}
				return $return;
			}
		} else {
			$return = [];
			foreach ($operation['titles'] as $title) {
				$return[$title] = self::COULD_NOT_OBTAIN_EDIT_TOKEN;
			}
			return $return;
		}

		// WE NOW HAVE AN ACTION TOKEN. LETS USE IT.
		$results = [];

		if ($action == "page_rollback") {
			foreach ($operation['page_rollback'] as $title) {
				// because MediaWiki is a super silly place, lets get the last edit user....
				$lastUserURL = $apiUrl . "?action=query&prop=revisions&rvprop=userid|user&format=json&formatversion=2&titles=$title";
				$lastUserReq = \MWHttpRequest::factory($lastUserURL, ['method' => 'GET']);
				$status = $lastUserReq->execute();

				if ($status->isOK()) {
					$result = $lastUserReq->getContent();
					self::d($result);
					$result = @json_decode($result, true);
					$user = $result['query']['pages'][0]['revisions'][0]['user'];
				}

				$actionURL = $apiUrl . '?action=rollback&format=json&formatversion=2';
				$actionOptions = [
					'method'			=> 'POST',
					'timeout'			=> 'default',
					'connectTimeout'	=> 'default',
					'postData'			=> [
						'title'			=> $title,
						'summary'		=> "GPE Bot Rollback",
						'markbot'		=> 1,
						'token'			=> $actionToken,
						'user'			=> $user
					]
				];

				self::d('Rollback Call', $actionURL, $actionOptions);
				$results[$title] = $this->altActionCall($actionURL, $actionOptions);
			}
		}

		if ($action == "page_move") {
			foreach ($operation['page_move'] as $title => $newTitle) {
				$actionURL = $apiUrl . '?action=move&format=json&formatversion=2';
				$actionOptions = [
					'method'			=> 'POST',
					'timeout'			=> 'default',
					'connectTimeout'	=> 'default',
					'postData'			=> [
						'from'		=> $title,
						'to'		=> $newTitle,
						// 'summary'	=> "GPE Bot Move",
						// 'markbot'		=> 1,
						'token'		=> $actionToken
					]
				];
				self::d('Move Call', $actionURL, $actionOptions);
				$results[$title] = $this->altActionCall($actionURL, $actionOptions);
			}
		}

		if ($action == "page_protect") {
			foreach ($operation['page_protect'] as $title) {
				$actionURL = $apiUrl . '?action=protect&format=json&formatversion=2';
				$actionOptions = [
					'method'			=> 'POST',
					'timeout'			=> 'default',
					'connectTimeout'	=> 'default',
					'postData'			=> [
						'title'			=> $title,
						'token'			=> $actionToken,
						'protections'	=> "edit=" . $operation['gpe_action_options']['edit_protection'] . "|move=" . $operation['gpe_action_options']['move_protection'],
						'expiry'		=> (!empty($operation['gpe_action_options']['edit_protection_expire']) ? $operation['gpe_action_options']['edit_protection_expire'] : "infinite") . "|" . (!empty($operation['gpe_action_options']['move_protection_expire']) ? $operation['gpe_action_options']['move_protection_expire'] : "infinite")
					]
				];
				if ($operation['gpe_action_options']['cascade']) {
					$actionOptions['postData']['cascade'] = true;
				}
				self::d('Protect Call', $actionURL, $actionOptions);
				$results[$title] = $this->altActionCall($actionURL, $actionOptions);
			}
		}

		if ($action == "page_delete") {
			foreach ($operation['page_delete'] as $title) {
				$actionURL = $apiUrl . '?action=delete&format=json&formatversion=2';
				$actionOptions = [
					'method'			=> 'POST',
					'timeout'			=> 'default',
					'connectTimeout'	=> 'default',
					'postData'			=> [
						'title'		=> $title,
						'token'		=> $actionToken
					]
				];
				self::d('Delete Call', $actionURL, $actionOptions);
				$results[$title] = $this->altActionCall($actionURL, $actionOptions);
			}
		}

		self::d('THE RESULTS', $results);

		if (count($results)) {
			return $results;
		}

		if ($retryCount <= self::$maxEditRetry) {
			return $this->doAlternativeAction($operations, $retryCount);
		}

		return self::UNKNOWN_EDIT_ERROR;
	}

	/**
	 * Create an API URL to the provided wiki.
	 *
	 * @param object $wiki Wiki
	 *
	 * @return string API URL
	 */
	private function createApiUrl($wiki) {
		global $wgScriptPath;
		$domain = $wiki->getDomains()->getDomain();
		$url = "https://{$domain}{$wgScriptPath}/api.php";

		return $url;
	}

	/**
	 * Do a full page edit operation.
	 *
	 * @param array   $operation  Array of operations to process containing arrays of wiki hashes, titles, and replacements.
	 * @param boolean $isPreview  [Optional] Is a Preview
	 * @param string  $identifier [Optional] Unique identifier for this operation.
	 *
	 * @return mixed Status result or a preview of the modified content.
	 */
	public static function doOperation($operation, $isPreview = true, $identifier = '') {
		$gpe = new GlobalPageEdit;
		$fr = new FindAndReplace;

		self::d('Starting Operation', $operation);

		if ($operation['everywhere']) {
			$wikis = Wiki::loadAll('wiki_domain', 'ASC', true, false);
		} else {
			$wikis = Wiki::loadFromHash($operation['wikis']['added']);
			if ($wikis instanceof Wiki) {
				$wikis[] = $wikis;
			}
		}
		if (is_array($operation['wikis']['removed']) && !empty($operation['wikis']['removed'])) {
			foreach ($operation['wikis']['removed'] as $siteKey) {
				unset($wikis[$siteKey]);
			}
		}

		if (empty($wikis)) {
			throw new \MWException("The wiki hashes given for the GPE operation were all invalid.");
		}

		$gpe->setWikis($wikis);

		foreach ($operation['titles'] as $rawTitle) {
			$title = \Title::newFromText($rawTitle);
			if ($title !== null) {
				$gpe->addTitle($rawTitle);
			}
		}

		if (!$gpe->hasTitles()) {
			// All the titles given were bad.
			return false;
		}

		self::$frIdentifiers = [];
		if (!empty($identifier)) {
			self::$frIdentifiers[] = $identifier;
		}

		if ($operation['gpe_action'] !== "find_and_replace") {
			$isAlternativeAction = true;
		} else {
			// GPE originally only did Find & Replace.
			// Maybe refactoring in the future makes more sense. This works for now.
			$isAlternativeAction = false;
			// don't do this for operations where we don't set this.
			if (isset($operation['find_replace'])) {
				foreach ($operation['find_replace'] as $options) {
					self::$frIdentifiers[] = $fr->addReplacement(
						$options['find'],
						$options['replace'],
						$options['limit'],
						$options['ignore_case'],
						$options['ignore_whitespace'],
						$options['wrap_around'],
						$options['type']
					);

					if ($options['create_new_pages'] == true) {
						self::$createNewPages = true;
						self::$newPageContent = $options['new_page_content'];
					}
				}
			}
		}

		while (($content = $gpe->getNextPage()) !== false) {
			if ($content === self::NO_MORE_PAGES) {
				break;
			}

			$wiki = $gpe->getCurrentWiki();
			$title = $gpe->getCurrentTitle();

			if (($content === self::PAGE_NOT_FOUND && !self::$createNewPages) || $content === self::UNKNOWN_PAGE_ERROR) {
				$status[$wiki->getSiteKey()][$title] = ['result' => 'Failure', 'error_code' => $content];
				continue;
			}

			if ($content === self::PAGE_NOT_FOUND && self::$createNewPages) {
				// Page not found, so we need to create it.
				$content = self::$newPageContent;
			} elseif ($isAlternativeAction) {
				// Do an alternative action besides find & replace.
				// who caresssss
				$content = "";
			} else {
				$content = $fr->doReplacements($content);
			}

			if (!$isPreview) {
				if ($isAlternativeAction) {
					$pageUpdate = $gpe->doAlternativeAction($operation);
				} else {
					$pageUpdate = $gpe->updateCurrentPage($content);
				}
				self::d($pageUpdate);
				if ($isAlternativeAction && is_array($pageUpdate)) {
					$status[$wiki->getSiteKey()] = $pageUpdate;
				} elseif (is_array($pageUpdate)) {
					$status[$wiki->getSiteKey()][$title] = $pageUpdate;
				} else {
					$status[$wiki->getSiteKey()][$title] = ['result' => 'Failure', 'error_code' => $pageUpdate];
				}
			} else {
				$status[$wiki->getSiteKey()][$title] = $content;
			}
		}

		self::d($status);
		return $status;
	}

	/**
	 * Debug output used for testing.
	 *
	 * @return void
	 */
	private static function d() {
		if ($_SERVER['PHP_ENV'] !== "development") {
			return;
		}
		$deco = "\n" . str_repeat('=', 25) . "\n";
		$dump = func_get_args();
		echo $deco;
		var_dump($dump);
		echo $deco;
	}

	/**
	 * Return the last find and replace identifiers.
	 * The first one in the array is the identifier for the overall operation.
	 *
	 * @return array Last used identifiers for find and replace operations.
	 */
	public static function getLastIdentifiers() {
		return self::$frIdentifiers;
	}

	/**
	 * Get a formed Redis queue status key.
	 *
	 * @param string $identifier SHA1 Identifier
	 *
	 * @return string Queue Status Key
	 */
	public static function getRedisQueueStatusKey($identifier) {
		return 'gpe:result:' . $identifier;
	}
}
