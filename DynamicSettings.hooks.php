<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Dynamic Settings Hooks
 *
 * @author    Alex Smith
 * @copyright (c) 2014 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
**/

use DynamicSettings\Environment;

class DynamicSettingsHooks {
	/**
	 * Add Hydra to the "Installed Software" list
	 *
	 * @param array Existing [MediaWiki Link => Version] information.
	 *
	 * @return boolean True
	 */
	public static function onSoftwareInfo(&$software) {
		$software['[https://help.gamepedia.com/What_is_Hydra Hydra]'] = date('Y') . ' Edition';
		return true;
	}

	/**
	 * Setups and Modifies Database Information
	 *
	 * @param object [Optional] DatabaseUpdater Object
	 *
	 * @return boolean true
	 */
	public static function onLoadExtensionSchemaUpdates(DatabaseUpdater $updater = null) {
		if (!Environment::isMasterWiki()) {
			return true;
		}

		$extDir = __DIR__;

		// Install
		// Tables
		$updater->addExtensionUpdate(['addTable', 'wiki_advertisements', "{$extDir}/install/sql/table_wiki_advertisements.sql", true]);
		$updater->addExtensionUpdate(['addTable', 'wiki_allowed_extensions', "{$extDir}/install/sql/table_wiki_allowed_extensions.sql", true]);
		$updater->addExtensionUpdate(['addTable', 'wiki_allowed_settings', "{$extDir}/install/sql/table_wiki_allowed_settings.sql", true]);
		$updater->addExtensionUpdate(['addTable', 'wiki_domains', "{$extDir}/install/sql/table_wiki_domains.sql", true]);
		$updater->addExtensionUpdate(['addTable', 'wiki_edit_log', "{$extDir}/install/sql/table_wiki_edit_log.sql", true]);
		$updater->addExtensionUpdate(['addTable', 'wiki_extensions', "{$extDir}/install/sql/table_wiki_extensions.sql", true]);
		$updater->addExtensionUpdate(['addTable', 'wiki_group_permissions', "{$extDir}/install/sql/table_wiki_group_permissions.sql", true]);
		$updater->addExtensionUpdate(['addTable', 'wiki_namespaces', "{$extDir}/install/sql/table_wiki_namespaces.sql", true]);
		$updater->addExtensionUpdate(['addTable', 'wiki_promotions', "{$extDir}/install/sql/table_wiki_promotions.sql", true]);
		$updater->addExtensionUpdate(['addTable', 'wiki_promotions_sites', "{$extDir}/install/sql/table_wiki_promotions_sites.sql", true]);
		$updater->addExtensionUpdate(['addTable', 'wiki_settings', "{$extDir}/install/sql/table_wiki_settings.sql", true]);
		$updater->addExtensionUpdate(['addTable', 'wiki_sites', "{$extDir}/install/sql/table_wiki_sites.sql", true]);

		// Upgrades - These are periodically culled to remove entries that are older than one year.
		// 2018-04-04
		$updater->addExtensionUpdate(['dropField', 'wiki_promotions', 'contest_slug', "{$extDir}/upgrade/sql/wiki_promotions/drop_contest_slug.sql", true]);
		$updater->addExtensionUpdate(['modifyField', 'wiki_promotions', 'type', "{$extDir}/upgrade/sql/wiki_promotions/alter_type_remove_contest.sql", true]);

		// 2018-06-14
		$updater->addExtensionUpdate(['addField', 'wiki_advertisements', 'config', "{$extDir}/upgrade/sql/wiki_advertisements/add_config.sql", true]);
		$updater->addExtensionUpdate(['dropField', 'wiki_advertisements', 'atflbrecovery', "{$extDir}/upgrade/sql/wiki_advertisements/drop_atflbrecovery.sql", true]);
		$updater->addExtensionUpdate(['dropField', 'wiki_advertisements', 'btflbrecovery', "{$extDir}/upgrade/sql/wiki_advertisements/drop_btflbrecovery.sql", true]);
		$updater->addExtensionUpdate(['dropField', 'wiki_advertisements', 'atfmrecrecovery', "{$extDir}/upgrade/sql/wiki_advertisements/drop_atfmrecrecovery.sql", true]);
		$updater->addExtensionUpdate(['dropField', 'wiki_advertisements', 'btfmrecrecovery', "{$extDir}/upgrade/sql/wiki_advertisements/drop_btfmrecrecovery.sql", true]);
		$updater->addExtensionUpdate(['dropField', 'wiki_advertisements', 'footermrecrecovery', "{$extDir}/upgrade/sql/wiki_advertisements/drop_footermrecrecovery.sql", true]);
		$updater->addExtensionUpdate(['dropField', 'wiki_advertisements', 'middlemrecrecovery', "{$extDir}/upgrade/sql/wiki_advertisements/drop_middlemrecrecovery.sql", true]);
		$updater->addExtensionUpdate(['dropField', 'wiki_advertisements', 'androidpackage', "{$extDir}/upgrade/sql/wiki_advertisements/drop_androidpackage.sql", true]);
		$updater->addExtensionUpdate(['dropField', 'wiki_advertisements', 'iosappid', "{$extDir}/upgrade/sql/wiki_advertisements/drop_iosappid.sql", true]);

		// 2018-07-25
		$updater->addExtensionUpdate(['addField', 'wiki_sites', 'db_server_replica', "{$extDir}/upgrade/sql/wiki_sites/add_db_server_replica.sql", true]);

		// 2018-09-11
		$updater->addExtensionUpdate(['addIndex', 'wiki_namespaces', 'site_key_namespace_id', "{$extDir}/upgrade/sql/wiki_namespaces/add_site_key_namespace_id_unique_key.sql", true]);

		// 2018-11-08
		$updater->addExtensionUpdate(['addField', 'wiki_allowed_extensions', 'critical_extension', "{$extDir}/upgrade/sql/wiki_allowed_extensions/add_critical_extension.sql", true]);
		$updater->addExtensionUpdate(['modifyField', 'wiki_allowed_extensions', 'critical_extension', "{$extDir}/upgrade/sql/wiki_allowed_extensions/insert_critical_extensions.sql", true]);

		// 2019-01-08
		$updater->addExtensionUpdate(['dropIndex', 'wiki_sites', 'db_name', "{$extDir}/upgrade/sql/wiki_sites/drop_db_name_index.sql", true]);
		$updater->addExtensionUpdate(['addIndex', 'wiki_sites', 'db_name-unique', "{$extDir}/upgrade/sql/wiki_sites/add_db_name_unique.sql", true]);

		//2019-12-06
		$updater->addExtensionUpdate(['addField', 'wiki_sites', 'db_cluster', "{$extDir}/upgrade/sql/wiki_sites/add_db_cluster.sql", true]);

		//2020-05-27
		$updater->addExtensionUpdate(['addField', 'wiki_sites', 'use_gcs', "{$extDir}/upgrade/sql/wiki_sites/add_use_gcs.sql", true]);

		//2020-08-25
		$updater->addExtensionUpdate(['modifyField', 'wiki_sites', 'use_s3', "{$extDir}/upgrade/sql/wiki_sites/modify_use_s3_default_zero.sql", true]);
		$updater->addExtensionUpdate(['modifyField', 'wiki_sites', 'use_gcs', "{$extDir}/upgrade/sql/wiki_sites/modify_use_gcs_default_one.sql", true]);

		return true;
	}

	/**
	 * Add resource loader modules.
	 *
	 * @param object Mediawiki Output Object
	 * @param object Mediawiki Skin Object
	 *
	 * @return boolean True
	 */
	public static function onBeforePageDisplay(&$output, &$skin) {
		$output->addModuleStyles('ext.siteGlobals.styles');
		$output->addModules('ext.siteGlobals.scripts');

		return true;
	}

	/**
	* Promotions Side Bar
	*
	* @access public
	* @param  object Skin Object
	* @param  array Array of bar contents to modify.
	* @return boolean True - Must return true or the site will break.
	*/
	public static function onSkinBuildSidebar(Skin $skin, &$bar) {
		global $wgDSPromotions;

		if (!empty($wgDSPromotions) && is_array($wgDSPromotions)) {
			$html = $adSidebarContent = $promoSidebarContent = '';

			foreach ($wgDSPromotions as $promotion) {
				if ($promotion['paused']) {
					continue;
				}
				if (($promotion['begins'] > 0 && time() >= $promotion['begins'] && $promotion['expires'] > 0 && time() <= $promotion['expires']) || ($promotion['begins'] == 0 && $promotion['expires'] == 0)) {
					$promoItem = "<a href=\"" . htmlentities($promotion['link'], ENT_QUOTES) . "\"><img src=\"" . htmlentities($promotion['image'], ENT_QUOTES) . "\" alt=\"" . htmlentities($promotion['description'], ENT_QUOTES) . "\" title=\"" . htmlentities($promotion['description'], ENT_QUOTES) . "\"/></a>\n";
					// split promo items between wiki promotion and paid ad slots
					if ($promotion['type'] == 'advertisement') {
						$adSidebarContent .= $promoItem;
					} else {
						$promoSidebarContent .= $promoItem;
					}
				}
			}

			if (!empty($promoSidebarContent)) {
				$html .= "
				<div class='wikiLinkSidebar'>{$promoSidebarContent}</div>";
			}

			if (!empty($adSidebarContent)) {
				$html .= "
				<div class='adSidebar'>{$adSidebarContent}</div>";
			}

			if (!empty($html)) {
				$html .= "
				<script type='text/javascript'>
					document.getElementById('p-sitePromos').className = 'portal persistent';
				</script>";
				$bar["sitePromos"] = $html;
			}
		}
		return true;
	}

	/**
	* Site Notices
	*
	* @access public
	* @param  string Raw Site Notice HTML
	* @param  object Skin Object
	* @return boolean True - Must return true or the site will break.
	*/
	public static function onSiteNoticeAfter(&$siteNotice, Skin $skin) {
		global $wgDSNotices, $wgParser, $wgRequest;

		if (!empty($wgDSNotices) && is_array($wgDSNotices)) {
			$dismissedNotices = trim($wgRequest->getCookie('dismissedNotices'));
			$dismissedNotices = explode(',', $dismissedNotices);
			if ($dismissedNotices === false) {
				$dismissedNotices = [];
			}
			foreach ($wgDSNotices as $notice) {
				if ($notice['paused']) {
					continue;
				}
				if (($notice['begins'] > 0 && time() >= $notice['begins'] && $notice['expires'] > 0 && time() <= $notice['expires']) || ($notice['begins'] == 0 && $notice['expires'] == 0)) {
					$md5 = md5($notice['name']);
					$thisDimissed = false;
					if (in_array($md5, $dismissedNotices)) {
						$thisDimissed = true;
					}
					if (!empty($notice['description'])) {
						$parserOptions = new ParserOptions();
						$parserOptions->setEditSection(false);
						$rendered = $wgParser->parse($notice['description'], Title::newFromText($notice['name']), $parserOptions);
						$innerHTML = $rendered->getText();
					} else {
						// Fall back to simple link and image.
						$innerHTML = "<a href=\"" . htmlentities($notice['link'], ENT_QUOTES) . "\"><img src=\"" . htmlentities($notice['image'], ENT_QUOTES) . "\"/></a>";
					}
					$siteNotice .= "<div id='notice-{$md5}' class='globalNotice" . ($thisDimissed ? ' dismissed' : null) . "' data-md5='{$md5}'>" . $innerHTML . "</div>\n";
				}
			}
		}

		return true;
	}

	/**
	 * Add wiki information to variables for use in various scripts.
	 *
	 * @param array      $variables
	 * @param OutputPage $outputPage
	 *
	 * @return void
	 */
	public static function onMakeGlobalVariablesScript(array &$variables, OutputPage $outputPage) {
		$variables['dsSiteKey'] = Environment::getSiteKey();
	}
}
