<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Wiki Namespaces Special Page
 *
 * @author    Alex Smith
 * @copyright (c) 2014 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
**/

use DynamicSettings\Lock;

class SpecialWikiNamespaces extends DynamicSettings\SpecialPage {
	/**
	 * Output HTML
	 *
	 * @var string
	 */
	private $content;

	/**
	 * Main Constructor
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct('WikiNamespaces', 'wiki_namespaces', false);
	}

	/**
	 * Main Executor
	 *
	 * @param string Sub page passed in the URL.
	 *
	 * @return void [Outputs to screen]
	 */
	public function execute($subpage) {
		$this->checkPermissions();

		if (Lock::isLocked() == true) {
			$this->output->showErrorPage('dynamic_settings_error', 'error_settings_locked');
			return;
		}

		$this->siteSettings = new \DynamicSettings\Sites();

		$this->output->addModuleStyles(['ext.wikiNamespaces.styles']);
		$this->output->addModules(['ext.wikiNamespaces.scripts']);

		$siteKey = $this->wgRequest->getVal('siteKey');

		if (Lock::isLocked($siteKey) == true) {
			$this->output->showErrorPage('dynamic_settings_error', 'error_wiki_locked');
			return;
		}

		$this->setHeaders();

		$this->wiki = \DynamicSettings\Wiki::loadFromHash($siteKey);

		if ($this->wiki === false) {
			$this->output->showErrorPage('site_namespaces_error', 'wiki_not_found');
			return;
		}

		if ($this->wiki->isDeleted()) {
			$this->output->showErrorPage('site_namespaces_error', 'site_namespaces_deleted');
			return;
		}

		$this->namespacesForm();

		$this->output->addHTML($this->content);
	}

	/**
	 * Wiki Namespaces Form
	 *
	 * @return void [Outputs to screen]
	 */
	public function namespacesForm() {
		$this->namespaces = $this->wiki->getNamespaces();
		if ($this->namespaces === false) {
			$this->namespaces = [];
		}

		$errors = $this->namespacesSave();

		$this->output->setPageTitle(wfMessage('wikinamespaces') . ' - ' . $this->wiki->getName());
		$this->templates = new TemplateWikiNamespaces();
		$this->content = $this->templates->namespacesForm($this->wiki, $this->namespaces, $errors);
	}

	/**
	 * Saves submitted Wiki Namespaces Forms.
	 *
	 * @return mixed Array of errors or false for a fatal error.
	 */
	private function namespacesSave() {
		$errors = [];
		if ($this->wgRequest->wasPosted() && $this->wgRequest->getVal('do') == 'save') {
			$arrays['namespace_name']		= $this->wgRequest->getArray('namespace_name', []);
			$arrays['namespace_talk_name']	= $this->wgRequest->getArray('namespace_talk_name', []);
			$arrays['namespace_alias']		= $this->wgRequest->getArray('namespace_alias', []);
			$arrays['namespace_talk_alias'] = $this->wgRequest->getArray('namespace_talk_alias', []);
			$arrays['namespace_id']			= $this->wgRequest->getArray('namespace_id', []);
			$arrays['is_content']			= $this->wgRequest->getArray('is_content', []);
			$arrays['is_searchable']		= $this->wgRequest->getArray('is_searchable', []);
			$arrays['is_semantic']		    = $this->wgRequest->getArray('is_semantic', []);
			$arrays['namespace_protection']	= $this->wgRequest->getArray('namespace_protection', []);
			$arrays['snid']					= $this->wgRequest->getArray('snid', []);

			$total = count($arrays['namespace_name']);

			foreach ($arrays as $key => $array) {
				$_total = count($array);
				if ($_total != $total) {
					// If all of the arrays do not match size there will be problems parsing it.
					$this->output->showErrorPage('site_namespaces_error', 'site_namespaces_array_mismatch');
					return false;
				}
			}

			$commitMessage = trim($this->wgRequest->getText('commit_message'));
			if (!$commitMessage) {
				$errors['commit_message'] = wfMessage('error_no_commit_message')->escaped();
			}

			$snids = [];
			if ($total && is_array($arrays['namespace_name'])) {
				for ($i = 0; $i < $total; $i++) {
					$namespace = null;
					if (intval($arrays['snid'][$i]) > 0 && array_key_exists(intval($arrays['snid'][$i]), $this->namespaces)) {
						// Make a list of known snids (database ID) to delete old ones later.
						$snids[] = intval($arrays['snid'][$i]);
						$namespace = $this->namespaces[$arrays['snid'][$i]];
					} else {
						$namespace = \DynamicSettings\Wiki\DSNamespace::loadFromNew($this->wiki);
					}

					if (!$namespace->setName($arrays['namespace_name'][$i])) {
						$errors[$i]['namespace_name'] = wfMessage('error_bad_namespace')->escaped();
					}

					if (!$namespace->setTalk($arrays['namespace_talk_name'][$i])) {
						$errors[$i]['namespace_talk_name'] = wfMessage('error_bad_namespace_talk')->escaped();
					}

					$nameAlias = $arrays['namespace_alias'][$i];
					if (empty($nameAlias)) {
						$nameAlias = null;
					}
					if (!$namespace->setNameAlias($nameAlias)) {
						$errors[$i]['namespace_alias'] = wfMessage('error_bad_namespace_alias')->escaped();
					}

					$talkAlias = $arrays['namespace_talk_alias'][$i];
					if (empty($talkAlias)) {
						$talkAlias = null;
					}
					if (!$namespace->setTalkAlias($talkAlias)) {
						$errors[$i]['namespace_talk_alias'] = wfMessage('error_bad_namespace_talk_alias')->escaped();
					}

					// Only set the namespace_id for new inserts.  We do not want to accidentally change the namespace_id.
					$nidReturn = $namespace->setId($arrays['namespace_id'][$i]);
					if ($nidReturn !== true) {
						$errors[$i]['namespace_id'] = $nidReturn;
					}

					$namespace->setContent((bool)$arrays['is_content'][$i]);
					$namespace->setSearchable((bool)$arrays['is_searchable'][$i]);
					$namespace->setSemantic((bool)$arrays['is_semantic'][$i]);

					$namespace->setProtection($arrays['namespace_protection'][$i]);

					if (!count($errors)) {
						if ($namespace->save($commitMessage)) {
							if (!isset($this->namespaces[$namespace->getDatabaseId()])) {
								$this->namespaces[$namespace->getDatabaseId()] = $namespace;
								$snids[] = $namespace->getDatabaseId();
							}
						}
					}
				}
			}

			if (!count($errors)) {
				if (count($this->namespaces)) {
					foreach ($this->namespaces as $namespaceId => $namespace) {
						if ($namespace->getDatabaseId() && !in_array($namespaceId, $snids)) {
							$namespace->delete($commitMessage);
						}
					}
				}

				$page = Title::newFromText('Special:WikiSites');
				$this->output->redirect($page->getFullURL());
				return;
			}
		}
		return $errors;
	}
}
