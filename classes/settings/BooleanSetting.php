<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Allowed Setting class for boolean settings
 *
 * @author    Noah Manneschmidt
 * @copyright (c) 2015 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
 **/

namespace DynamicSettings\Settings;

class BooleanSetting extends Setting {
	/**
	 * Validates the given value against the current setting type
	 *
	 * @param string Potential value for this setting.
	 *
	 * @return boolean
	 */
	public function isValid($value) {
		return is_bool($value);
	}

	/**
	 * Force boolean type on the text input.
	 *
	 * @param string 'true' or 'false' Form Value
	 *
	 * @return boolean
	 */
	protected function decodeFormString($value) {
		return filter_var($value, FILTER_VALIDATE_BOOLEAN);
	}
}
