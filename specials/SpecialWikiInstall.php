<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Wiki Install Special Page
 *
 * @author    Cameron Chunn
 * @copyright (c) 2018 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
**/

class SpecialWikiInstall extends \DynamicSettings\SpecialPage {
	/**
	 * Main Constructor
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct(
			'WikiInstall', // name
			'wiki_install', // required user right
			false // display on Special:Specialpages
		);
	}

	/**
	 * Main Executor
	 *
	 * @param string Sub page passed in the URL.
	 *
	 * @return void [Outputs to screen]
	 */
	public function execute($path) {
		$this->template = new TemplateWikiInstall();
		$this->wgRequest = $this->getRequest();
		$this->wgUser    = $this->getUser();
		$this->output    = $this->getOutput();

		$this->checkPermissions();

		$wiki = \DynamicSettings\Wiki::loadFromHash($this->wgRequest->getText('siteKey', ''));

		if (!$wiki) {
			$this->output->showErrorPage('wiki_install_error', 'wiki_not_found');
			return;
		}

		$title = wfMessage('wiki_install')->escaped();
		if ($wiki) {
			$title .= " - " . $wiki->getName();
		}

		$defaultLoadouts = \DynamicSettings\Wiki::loadFromSearch(0, 100, 'default-loadout', 'wiki_name', 'ASC', true);

		$this->output->setPageTitle($title);
		$this->content = $this->template->wikiInstallForm($wiki, $defaultLoadouts);
		$this->output->addHTML($this->content);
	}

	/**
	 * Return the group name for this special page.
	 *
	 * @return string
	 */
	protected function getGroupName() {
		return 'other'; // Change to display in a different category on Special:SpecialPages.
	}
}
