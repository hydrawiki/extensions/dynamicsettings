<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Wiki Permissions Skin
 *
 * @author    Alex Smith
 * @copyright (c) 2014 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
**/

class TemplateWikiPermissions {
	/**
	 * Output HTML
	 *
	 * @var string
	 */
	private $HMTL;

	/**
	 * Group Permission Form
	 *
	 * @param array Array of wiki information
	 * @param array Array of Permission objects.
	 * @param array Form Errors
	 *
	 * @return string Built HTML
	 */
	public function permissionsForm($wiki, $permissions, $errors) {
		global $wgRequest;

		$page = Title::newFromText('Special:WikiPermissions');

		$html = "
		<form id='wiki_settings_form' method='post' action='{$page->getFullURL(['do' => 'save'])}'>
			<input type='button' value='" . wfMessage('add_group') . "' class='add_group mw-ui-button'>
		";
		$i = 0;
		if (is_array($permissions) && count($permissions)) {
			foreach ($permissions as $gpid => $permission) {
				$html .= $this->permissionsGroup($permission, $gpid, (isset($errors[$i]) ? $errors[$i] : []));
				$i++;
			}
		}
		$html .= "
			<fieldset id='commit_fieldset'>
				" . (isset($errors['commit_message']) ? '<span class="error">' . $errors['commit_message'] . '</span>' : '') . "
				<label for='commit_message' class='label_above'>" . wfMessage('commit_message')->escaped() . "</label>
				<input id='commit_message' name='commit_message' type='text' value='" . htmlentities(trim($wgRequest->getText('commit_message')), ENT_QUOTES) . "'/><br/>
				<input id='wiki_id' name='siteKey' type='hidden' value='{$wiki->getSiteKey()}'/>
				<button id='wiki_submit' name='wiki_submit' type='submit' class='mw-ui-button mw-ui-progressive'>" . wfMessage('save_key_value')->escaped() . "</button>
			</fieldset>
		</form>
		<div id='hidden_group_template'>
			" . $this->permissionsGroup(\DynamicSettings\Wiki\Permission::loadFromNew($wiki)) . "
		</div>
		";

		return $html;
	}

	/**
	 * Individual Group Permission Template
	 *
	 * @param array [Optional] Group Permission
	 * @param integer [Optional] Group ID - Not guaranteed to match the ID in the database.
	 * @param array [Optional] Form Errors
	 *
	 * @return string Built HTML
	 */
	public function permissionsGroup(\DynamicSettings\Wiki\Permission $permission, $gpid = null, $errors = []) {
		$html = "
			<fieldset class='group_fieldset'>
				" . (isset($errors['group_name']) ? '<span class="error">' . $errors['group_name'] . '</span>' : '') . "
				<label for='group_name' class='label_above'>" . wfMessage('group_name') . "</label>
				<input id='group_name' name='group_name[]' type='text' value='{$permission->getGroupName()}'/>

				<label for='group_allow' class='label_above'>" . wfMessage('group_allow') . "</label>
				<textarea id='group_allow' name='group_allow[]' type='text'/>";
		$allow = $permission->getAllow();
		if (is_array($allow) && count($allow)) {
			foreach ($allow as $key => $value) {
				$allow[$key] = htmlentities($value, ENT_QUOTES);
			}
			$html .= implode("\n", $allow);
		}
		$html .= "</textarea>

				<label for='group_deny' class='label_above'>" . wfMessage('group_deny') . "</label>
				<textarea id='group_deny' name='group_deny[]' type='text'/>";
		$deny = $permission->getDeny();
		if (is_array($deny) && count($deny)) {
			foreach ($deny as $key => $value) {
				$deny[$key] = htmlentities($value, ENT_QUOTES);
			}
			$html .= implode("\n", $deny);
		}
		$html .= "</textarea>
				<input id='snid' name='gpid[]' type='hidden' value='" . (is_object($permission) ? $permission->getDatabaseId() : null) . "'/><br/>
				<input type='button' value='" . wfMessage('remove_group') . "' class='remove_group mw-ui-button mw-ui-destructive'>
			</fieldset>";

		return $html;
	}
}
