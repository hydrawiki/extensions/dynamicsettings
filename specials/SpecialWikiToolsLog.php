<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Wiki Edit Log Special Page
 *
 * @author    Alex Smith
 * @copyright (c) 2014 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
**/

class SpecialWikiToolsLog extends DynamicSettings\SpecialPage {
	/**
	 * Output HTML
	 *
	 * @var string
	 */
	private $content;

	/**
	 * Wiki objects storage.
	 *
	 * @var array
	 */
	private $wikis = [];

	/**
	 * Main Constructor
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct('WikiToolsLog', 'wiki_tools_log');
	}

	/**
	 * Main Executor
	 *
	 * @param string Sub page passed in the URL.
	 *
	 * @return void [Outputs to screen]
	 */
	public function execute($subpage) {
		$this->checkPermissions();

		$this->templates = new TemplateWikiToolsLog();

		$this->output->addModuleStyles(['ext.wikiToolsLog.styles']);
		$this->output->addModules(['ext.wikiToolsLog.scripts']);

		$tasks = \DynamicSettings\Tools::getTasks();
		foreach ($tasks as $key => $task) {
			$tasks[$key] = strtolower($task);
		}

		if (in_array(strtolower($this->wgRequest->getVal('log_type')), $tasks)) {
			$this->logType = ucfirst($this->wgRequest->getVal('log_type'));
		} else {
			$this->logType = 'Recache';
		}

		switch ($this->wgRequest->getVal('action')) {
			case 'clearlog':
				$this->clearLog();
				break;
			case 'viewoutput':
				$this->viewOutput();
				break;
			default:
				$this->wikiToolsLog();
				break;
		}

		$this->setHeaders();

		$this->output->addHTML($this->content);
	}

	/**
	 * Wiki Edit Log
	 *
	 * @return void [Outputs to screen]
	 */
	public function wikiToolsLog() {
		$start = $this->wgRequest->getInt('st');
		$itemsPerPage = 25;

		// These are not out of order.  Putting them in this order will result in them being displayed/override in this order.
		$entries	= \DynamicSettings\Job\ToolsJob::getLogBuffer($this->logType, $start, $itemsPerPage);
		$total 		= \DynamicSettings\Job\ToolsJob::getLogBufferCount($this->logType);

		$pagination = HydraCore::generatePaginationHtml($this->getFullTitle(), $total, $itemsPerPage, $start, 4, ['log_type' => $this->logType]);

		$logs = [];
		$totals = [
			\DynamicSettings\Job\ToolsJob::JOB_QUEUE => 0,
			\DynamicSettings\Job\ToolsJob::JOB_START => 0,
			\DynamicSettings\Job\ToolsJob::JOB_ERROR => 0,
			\DynamicSettings\Job\ToolsJob::JOB_FINISH => 0
		];
		if (is_array($entries) && count($entries)) {
			foreach ($entries as $ent) {
				$siteKey = $ent['siteKey'];
				$status = $ent['status'];
				$wiki = false;
				if (strlen($siteKey) == 32) {
					if (!array_key_exists($siteKey, $this->wikis)) {
						$wiki = \DynamicSettings\Wiki::loadFromHash($siteKey);
					} else {
						$wiki = $this->wikis[$siteKey];
					}

					if ($wiki !== false) {
						$this->wikis[$siteKey] = $wiki;
					}
				} else {
					continue;
				}

				$log['job_key'] = $ent['key'];
				$log['site_key'] = $siteKey;
				$log['time'] = date("m/d/Y H:i:s", (isset($ent['time']) ? intval($ent['time']) : 0));
				$log['status_message']	= wfMessage('tools_sync_status_' . $status)->escaped();
				$totals[$status]++;

				switch ($status) {
					case \DynamicSettings\Job\ToolsJob::JOB_QUEUE:
						$log['status_class'] = 'job_queue';
						break;
					case \DynamicSettings\Job\ToolsJob::JOB_START:
						$log['status_class'] = 'job_start';
						break;
					case \DynamicSettings\Job\ToolsJob::JOB_ERROR:
						$log['status_class'] = 'job_error';
						break;
					case \DynamicSettings\Job\ToolsJob::JOB_FINISH:
						$log['status_class'] = 'job_finish';
						break;
				}

				$logs[] = $log;
			}
		}

		$this->output->setPageTitle(wfMessage('wikitoolslog')->escaped());
		$this->content = $this->templates->wikiToolsLog(
			$logs,
			$pagination,
			$totals,
			$this->wikis,
			$this->logType,
			\DynamicSettings\Tools::getTasks()
		);
	}

	/**
	 * Function Documentation
	 *
	 * @return void
	 */
	public function viewOutput() {
		$jobkey = $this->wgRequest->getVal('jobkey');
		$jobStatus = \DynamicSettings\Job\ToolsJob::getJobStatus($jobkey);

		if ($jobStatus !== "JOB_START") {
			$content = \DynamicSettings\Job\ToolsJob::getJobOutput($jobkey);
			if (!$content || empty($content)) {
				$content = wfMessage('no_logs_available')->plain();
			}
			$content = trim($content);
		} else {
			$content = wfMessage('loading_output')->plain();
		}

		$this->content = $this->templates->viewJob($content, $jobStatus, $jobkey);
	}

	/**
	 * Clear the log buffer.
	 *
	 * @return void
	 */
	public function clearLog() {
		\DynamicSettings\Job\ToolsJob::clearLogBuffer($this->logType);
		$this->output->redirect($this->getPageTitle()->getFullURL(['log_type' => $this->logType, 'cleared' => 'true']));
	}
}
