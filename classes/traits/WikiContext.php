<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Trait for objects that optionally maintain a wiki site for context
 * (used by extensions and settings)
 *
 * @author    Noah Manneschmidt
 * @copyright (c) 2015 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
 **/

namespace DynamicSettings\Traits;

use DynamicSettings\Wiki;

trait WikiContext {
	/**
	 * @var Wiki or null when no context is established
	 */
	protected $wikiContext;

	/**
	 * @return object Wiki
	 */
	public function getWikiContext() {
		return $this->wikiContext;
	}

	/**
	 * @param object Wiki site context to use when getting current set value
	 */
	public function setWikiContext(Wiki $wiki = null) {
		$oldContext = $this->wikiContext;
		$this->wikiContext = $wiki;
		if (method_exists($this, 'resetContext')) {
			$this->resetContext($oldContext);
		}
	}
}
