CREATE TABLE /*_*/wiki_namespaces (
  `snid` int(12) NOT NULL,
  `site_key` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `namespace_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `namespace_talk` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `namespace_id` int(12) NOT NULL DEFAULT '10000',
  `namespace_talk_id` int(12) NOT NULL DEFAULT '10001',
  `namespace_alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `namespace_talk_alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_content` tinyint(1) NOT NULL DEFAULT '0',
  `is_searchable` tinyint(1) NOT NULL DEFAULT '0',
  `is_semantic` tinyint(1) NOT NULL DEFAULT '0',
  `namespace_protection` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) /*$wgDBTableOptions*/;

ALTER TABLE /*_*/wiki_namespaces
  ADD PRIMARY KEY (`snid`),
  ADD UNIQUE KEY `site_key_namespace_id` (`site_key`,`namespace_id`),
  ADD KEY `namespace_id` (`namespace_id`),
  ADD KEY `namespace_talk_id` (`namespace_talk_id`),
  ADD KEY `site_key` (`site_key`);

ALTER TABLE /*_*/wiki_namespaces
  MODIFY `snid` int(12) NOT NULL AUTO_INCREMENT;