<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Collection class to hold an array of Extension instances
 *
 * @author    Noah Manneschmidt
 * @copyright (c) 2015 Curse Inc.
 * @license   Proprietary
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
**/

namespace DynamicSettings\Extensions;

use DynamicSettings\WikiContextArrayObject;
use DynamicSettings\WikiContextException;

class ExtensionCollection extends WikiContextArrayObject {
	use ExtensionArrayAccessContext;

	protected static $iteratorClass = 'DynamicSettings\Extensions\ExtensionIterator';

	/**
	 * Sort the collection by the expected load order
	 *
	 * @return self
	 */
	public function inLoadOrder() {
		$this->uasort(function ($ext_first, $ext_second) {
			if ($ext_first->getWeight() == $ext_second->getWeight()) {
				return 0;
			}
			return ($ext_first->getWeight() < $ext_second->getWeight()) ? -1 : 1;
		});
		return $this;
	}

	/**
	 * Add all deleted extensions to the current set
	 * This will add ALL deleted allowed extensions, regardless of wiki context.
	 *
	 * @return object ExtensionCollection
	 */
	public function includeDeleted() {
		$this->merge(Extension::getDeleted());
		return $this;
	}

	/**
	 * Filter out extensions that are enabled by default
	 *
	 * @return object ExtensionCollection
	 */
	public function withoutDefault() {
		return $this->filter(function ($extension) {
			return !$extension->isDefaultExtension();
		});
	}

	/**
	 * Save this set of extensions as the list of those enabed on the current context wiki
	 *
	 * @param string $commitMessage will be written to the wiki change log
	 *
	 * @return boolean success or failure
	 * @throws object WikiContextException when no wiki context exists
	 */
	public function setEnabledOnWiki($commitMessage) {
		if (!$this->wikiContext) {
			throw new WikiContextException();
		}
		return $this->wikiContext->setEnabledExtensions($this, $commitMessage);
	}
}
