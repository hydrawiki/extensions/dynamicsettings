INSERT INTO /*_*/wiki_allowed_extensions (`aeid`, `extension_name`, `extension_desc`, `extension_filename`, `extension_folder`, `md5_key`, `deleted`, `weight`, `extension_extra`, `extension_information`, `update_required`, `default_extension`, `critical_extension`, `load_type`, `dependencies`) VALUES
(null, 'AdminMinder', 'Keeps track of admins added and removed over time on Hydra wikis.', 'AdminMinder', 'AdminMinder', '0a2c9bc69d6e274741f430513a3dfa2d', 0, 0, '', '', 1, 1, 0, 0, ''),
(null, 'CacheBreaker', 'Automatically adds cache breaking URL pieces to media.', 'CacheBreaker', 'CacheBreaker', '3a9fec558b7cb623855e50adb128075c', 0, 0, '', '', 0, 1, 1, 0, ''),
(null, 'Cheevos', 'Awards achievements to editors for varying levels of accomplishments.', 'Cheevos', 'Cheevos', '36aba8e361db1a6bb0ebfa716495195f', 0, 0, '', '', 1, 1, 1, 0, ''),
(null, 'CurseProfile', 'A modular, multi-featured user profile system.', 'CurseProfile', 'CurseProfile', 'f426f86927519de1feda70b985ceb9cc', 0, 0, '', '', 1, 1, 1, 0, ''),
(null, 'DynamicSettings', 'Wiki Farm management tool for the Hydra Platform.', 'DynamicSettings', 'DynamicSettings', '195f3556318c0cfac721155b64e9419d', 0, 0, '', '', 1, 1, 0, 0, ''),
(null, 'GlobalBlock', 'Manage global blocks for anonymous and registered accounts.', 'GlobalBlock', 'GlobalBlock', '89f7e512f614d9335bf668401017bcea', 0, 0, '', '', 1, 1, 1, 0, ''),
(null, 'HydraAuth', 'Central user authentication for the Hydra Wiki Platform.', 'HydraAuth', 'HydraAuth', 'bdf509bd50c0a9019621cefac6884fc2', 0, 0, '', '', 1, 1, 1, 0, ''),
(null, 'Subscription', 'Paid subscription system for Hydra Wiki Platform.', 'Subscription', 'Subscription', 'ab1ec4ba26a015b2d65e3b900be8443b', 0, 0, '', '', 1, 1, 1, 0, ''),
(null, 'SyncService', 'Distributed job queue for MediaWiki with a Redis backend.', 'SyncService', 'SyncService', 'fd101c63d83851f6f0591a17f76cfa59', 0, 0, '', '', 0, 1, 1, 0, ''),
(null, 'Twiggy', 'Twig Template service.', 'Twiggy', 'Twiggy', 'f3024063a4cbb52d361693685b2cc9b3', 0, 0, '', '', 0, 1, 0, 0, '')
ON DUPLICATE KEY UPDATE deleted = 0, default_extension = 1, critical_extension = 1;
