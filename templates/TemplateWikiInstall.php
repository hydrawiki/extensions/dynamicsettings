<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Wiki Install Special Page
 *
 * @package   DynamicSettings
 * @author    Cameron Chunn
 * @copyright (c) 2018 Curse Inc.
 * @license   GPL-2.0-or-later
 * @link      https://gitlab.com/hydrawiki
 **/

use MediaWiki\MediaWikiServices;

class TemplateWikiInstall {
	/**
	 * Wiki Install
	 *
	 * @param object $wiki            Wiki
	 * @param array  $defaultLoadouts
	 *
	 * @return string HTML
	 */
	public function wikiInstallForm($wiki, $defaultLoadouts) {
		$wikiSitesPage = Title::newFromText('Special:WikiSites');
		$config = MediaWikiServices::getInstance()->getMainConfig();
		$skins = Skin::getSkinNames();
		$allowedSkins = $config->get('DSHydraAllowedInstallSkins');
		$defaultSkin = $config->get('DSHydraDefaultSkin');
		$skins = array_filter($skins, function ($key) use ($allowedSkins) {
			return in_array($key, $allowedSkins);
		}, ARRAY_FILTER_USE_KEY);

		$html = "
		<form method='post' action='{$wikiSitesPage->getFullURL(['section' => 'install'])}'>
				<h3>" . wfMessage('wi_loadout')->escaped() . "</h3>
				<p>" . wfMessage('wi_select_loadout')->escaped() . "</p>
				<select name='loadout'>
					<optgroup label='" . wfMessage('wi_defaults')->escaped() . "'>
						<option value='none'>" . wfMessage('wi_no_default')->escaped() . "</option>
						<option selected='selected' value='default'>" . wfMessage('wi_default_loadout')->escaped() . "</option>
					</optgroup>
					<optgroup label='" . wfMessage('wi_manually_select')->escaped() . "'>
					";
		foreach ($defaultLoadouts as $d) {
			$html .= "<option value='{$d->getDomain()}'>{$d->getName()} ({$d->getDomain()})</option>";
		}
				$html .= "
					</optgroup>
				</select>

				<h3>" . wfMessage('wi_interwiki')->escaped() . "</h3>
				<p>" . wfMessage('wi_select_interwiki')->escaped() . "</p>
				<select name='interwiki'>
					<optgroup label='" . wfMessage('wi_defaults')->escaped() . "'>
						<option value='none'>" . wfMessage('wi_interwiki_stock')->escaped() . "</option>
						<option selected='selected' value='default'>" . wfMessage('wi_default_interwiki')->escaped() . "</option>
					</optgroup>
					<optgroup label='" . wfMessage('wi_manually_select')->escaped() . "'>
					";
		foreach ($defaultLoadouts as $d) {
			$html .= "<option value='{$d->getDomain()}'>{$d->getName()} ({$d->getDomain()})</option>";
		}
				$html .= "
					</optgroup>
				</select>

				<h3>" . wfMessage('wi_skin')->escaped() . "</h3>
				<p>" . wfMessage('wi_select_skin')->escaped() . "</p>
				<select name='skin'>";
		array_walk($skins, function ($skin, $key) use (&$html, $defaultSkin) {
			$select = $key == $defaultSkin ? " selected" : "";
			$html .= "<option value='" . $key . "'" . $select . ">" . $skin . "</option>";
		});
				$html .= "
				</select>
				<input type='checkbox' name='skip' checked>" . wfMessage('wi_skip_skin')->escaped() . "<br>
				<input name='siteKey' type='hidden' value='{$wiki->getSiteKey()}'/>
				<br /><br />
				<button type='submit' title='" . wfMessage('wikisites-install_wiki')->escaped() . "'>
					" . HydraCore::awesomeIcon('sync') . wfMessage('install_wiki')->escaped() . "
				</button>
		</form>";

		return $html;
	}
}
