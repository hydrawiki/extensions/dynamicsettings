$(document).ready(function() {
	var dismissedCookieName = mw.config.get('wgCookiePrefix')+'dismissedNotices';
	var dismissedNotices = $.cookie(dismissedCookieName);
	if (dismissedNotices) {
		dismissedNotices = dismissedNotices.split(',');
	} else {
		dismissedNotices = new Array();
	}

	var noticeIsDismissed = function(noticeID) {
		if ($.inArray(noticeID, dismissedNotices) == -1) {
			return false;
		} else {
			return true;
		}
	}

	var setNoticeDismissed = function(noticeID) {
		dismissedNotices.push(noticeID);
	}

	if ($('#localNotice').length > 0) {
		var localNoticeMD5 = md5($('#localNotice').html());
		$('#localNotice').addClass('globalNotice').attr('data-md5', localNoticeMD5);
		if (noticeIsDismissed(localNoticeMD5)) {
			$('#localNotice').addClass('dismissed');
		}
	}

	$('.globalNotice').prepend("<span class='globalNoticeDismiss'>[<a href='#'>"+mw.message('dismiss_notice').escaped()+"</a>]</span>");

	$('.globalNoticeDismiss').click(function() {
		var parent = $(this).parent();
		var noticeID = $(parent).attr('data-md5');

		if (noticeIsDismissed(noticeID) == false) {
			setNoticeDismissed(noticeID);
		}

		if (mw.config.get('wgCookieDomain') !== null) {
			var domain = mw.config.get('wgCookieDomain');
		}
		if (mw.config.get('wgCookiePath') !== null) {
			var path = mw.config.get('wgCookiePath');
		} else {
			var path = "/";
		}

		if (domain) {
			$.cookie(dismissedCookieName, dismissedNotices.join(), {expires: 30, path: path, domain: domain} );
		} else {
			$.cookie(dismissedCookieName, dismissedNotices.join(), {expires: 30, path: path} );
		}

		$(parent).hide('slow');
	});
});
