<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Refresh Links Chunked
 *
 * @author    Cameron Chunn
 * @copyright (c) 2018 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
**/

require_once dirname(__DIR__, 3) . '/maintenance/Maintenance.php';

class RefreshLinksChunked extends Maintenance {
	/**
	 * Main Constructor
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
		$this->addDescription("Runs the refreshLinks script in small chunks to attempt to keep it from failing on large wikis.");
		$this->addOption("start", "Start from page_id", false, true);
	}

	/**
	 * Run refreshLinks in Chunks
	 *
	 * @return void
	 */
	public function execute() {
		$chunk = 10;
		$starttime = microtime(true);
		$dbr = $this->getDB(DB_REPLICA, ['vslow']);
		$maxPage = $dbr->selectField('page', 'max(page_id)', false);
		$maxRD = $dbr->selectField('redirect', 'max(rd_from)', false);
		$end = max($maxPage, $maxRD);

		if (!isset($_SERVER['HTTP_HOST']) || empty($_SERVER['HTTP_HOST'])) {
			$this->output("This script must be ran with host helper.\n");
			exit;
		}
		$host = $_SERVER['HTTP_HOST'];

		$this->output("\n\nGoing forward assuming max page id is {$end}. Going to run chunked in groups of ${chunk}.\n\n");

		$rlPath = escapeshellarg(realpath(dirname(__DIR__, 3) . '/maintenance/refreshLinks.php'));
		$hhPath = escapeshellarg(realpath(dirname(__DIR__, 2) . '/HydraCore/maintenance/hostHelper.php'));
		$escHost = escapeshellarg($host);
		$command = "${hhPath} ${escHost} ${rlPath}";

		$start = (int)$this->getOption("start") ?: 1;
		while ($start < $end) {
			$lEnd = $start + $chunk - 1;
			if ($lEnd > $end) {
				$lEnd = $end;
			}

			$output = shell_exec("php ${command} ${start} -e ${lEnd}");
			$endtime = microtime(true);
			$timediff = $this->secondsToTime($endtime - $starttime);
			$this->output("${lEnd}/${end} ($timediff)\n");
			$start = $lEnd + 1;
		}
	}

	/**
	 * Take seconds in time and convert them to a human readable timestamp
	 *
	 * @param integer $s
	 *
	 * @return void
	 */
	public function secondsToTime($s) {
		$h = floor($s / 3600);
		$s -= $h * 3600;
		$m = floor($s / 60);
		$s -= $m * 60;
		return $h . ':' . sprintf('%02d', $m) . ':' . sprintf('%02d', $s);
	}
}

$maintClass = "RefreshLinksChunked";
require_once RUN_MAINTENANCE_IF_MAIN;
